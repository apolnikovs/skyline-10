# ---------------------------------------------------------------------- #
# Script generated with: DeZign for Databases V7.2.0                     #
# Target DBMS:           MySQL 5                                         #
# Project file:          SkyLine.dez                                     #
# Project name:          SkyLine                                         #
# Author:                Brian Etherington                               #
# Script type:           Alter database script                           #
# Created on:            2012-10-24 11:20                                #
# ---------------------------------------------------------------------- #


# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                                     #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.103');

# ---------------------------------------------------------------------- #
# Drop foreign key constraints                                           #
# ---------------------------------------------------------------------- #

ALTER TABLE `ra_status` DROP FOREIGN KEY `brand_TO_ra_status`;

ALTER TABLE `ra_status` DROP FOREIGN KEY `user_TO_ra_status`;

ALTER TABLE `ra_history` DROP FOREIGN KEY `job_TO_ra_history`;

ALTER TABLE `ra_history` DROP FOREIGN KEY `ra_status_TO_ra_history`;

ALTER TABLE `ra_history` DROP FOREIGN KEY `ra_status_TO_ra_history_2`;

ALTER TABLE `ra_history` DROP FOREIGN KEY `user_TO_ra_history`;

ALTER TABLE `job` DROP FOREIGN KEY `ra_status_TO_job`;

# ---------------------------------------------------------------------- #
# Modify table "ra_status"                                               #
# ---------------------------------------------------------------------- #

ALTER TABLE `ra_status` MODIFY `ModifiedDate` TIMESTAMP;

# ---------------------------------------------------------------------- #
# Modify table "ra_history"                                              #
# ---------------------------------------------------------------------- #

ALTER TABLE `ra_history` MODIFY `ModifiedDate` TIMESTAMP NOT NULL;

# ---------------------------------------------------------------------- #
# Add foreign key constraints                                            #
# ---------------------------------------------------------------------- #

ALTER TABLE `ra_status` ADD CONSTRAINT `brand_TO_ra_status` 
    FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`);

ALTER TABLE `ra_status` ADD CONSTRAINT `user_TO_ra_status` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `ra_history` ADD CONSTRAINT `job_TO_ra_history` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

ALTER TABLE `ra_history` ADD CONSTRAINT `ra_status_TO_ra_history` 
    FOREIGN KEY (`OldStatusID`) REFERENCES `ra_status` (`RAStatusID`);

ALTER TABLE `ra_history` ADD CONSTRAINT `ra_status_TO_ra_history_2` 
    FOREIGN KEY (`NewStatusID`) REFERENCES `ra_status` (`RAStatusID`);

ALTER TABLE `ra_history` ADD CONSTRAINT `user_TO_ra_history` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `job` ADD CONSTRAINT `ra_status_TO_job` 
    FOREIGN KEY (`RAStatusID`) REFERENCES `ra_status` (`RAStatusID`);

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.104');
