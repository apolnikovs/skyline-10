# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.187');

# ---------------------------------------------------------------------- #
# Add table "service_provider"                                                 #
# ---------------------------------------------------------------------- #
ALTER TABLE `service_provider` ADD `EmailServiceInstruction` ENUM( 'Active', 'In-Active' ) NULL DEFAULT 'In-Active';


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.188');
