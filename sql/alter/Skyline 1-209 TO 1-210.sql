# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.209');

# ---------------------------------------------------------------------- #
# Modify Table appointment	                                             #
# ---------------------------------------------------------------------- #
ALTER TABLE appointment ADD COLUMN GeoTagModiefiedByUser ENUM('Yes','No') NOT NULL DEFAULT 'No' AFTER TempSpecified;
				
# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.210');
