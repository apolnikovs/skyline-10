# ---------------------------------------------------------------------- #
# Script generated with: DeZign for Databases V7.3.0                     #
# Target DBMS:           MySQL 5                                         #
# Project file:          SkyLine.dez                                     #
# Project name:          SkyLine                                         #
# Author:                Brian Etherington                               #
# Script type:           Alter database script                           #
# Created on:            2012-11-21 10:16                                #
# ---------------------------------------------------------------------- #


# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                                     #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.124');

# ---------------------------------------------------------------------- #
# Add table "service_provider_trade_account"                             #
# ---------------------------------------------------------------------- #

CREATE TABLE `service_provider_trade_account` (
    `ServiceProviderTradeAccountID` INTEGER NOT NULL AUTO_INCREMENT,
    `ServiceProviderID` INTEGER,
    `TradeAccount` VARCHAR(40),
    `Postcode` VARCHAR(10),
    `Monday` ENUM('Yes','No') DEFAULT 'No',
    `Tuesday` ENUM('Yes','No') DEFAULT 'No',
    `Wednesday` ENUM('Yes','No') DEFAULT 'No',
    `Thursday` ENUM('Yes','No') DEFAULT 'No',
    `Friday` ENUM('Yes','No') DEFAULT 'No',
    `Saturday` ENUM('Yes','No') DEFAULT 'No',
    `Sunday` ENUM('Yes','No') DEFAULT 'No',
    `CreatedDate` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    `EndDate` TIMESTAMP,
    `Status` ENUM('Active', 'In-active') NOT NULL DEFAULT 'Active',
    `ModifiedUserID` INTEGER,
    `ModifiedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    CONSTRAINT `service_provider_trade_accountID` PRIMARY KEY (`ServiceProviderTradeAccountID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.125');
