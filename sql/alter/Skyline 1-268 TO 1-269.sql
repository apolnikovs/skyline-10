# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.268');

# ---------------------------------------------------------------------- #
# Add Table ra_status                                                    #
# ---------------------------------------------------------------------- # 
ALTER TABLE ra_status ADD COLUMN SBAuthorisationStatusID INT(11) NULL DEFAULT NULL AFTER AuthorisationTypeID, 
					  ADD CONSTRAINT sb_authorisation_status_TO_ra_status FOREIGN KEY (SBAuthorisationStatusID) REFERENCES sb_authorisation_statuses (SBAuthorisationStatusID);


# ---------------------------------------------------------------------- #
# Add Permissions                                                        #
# ---------------------------------------------------------------------- # 
INSERT INTO permission (PermissionID, Name, Description, ModifiedUserID) VALUES (11009, 'Booking Page - Setup & Compulsory Fields', 'Booking Page - Setup & Compulsory Fields', 1);

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.269');
