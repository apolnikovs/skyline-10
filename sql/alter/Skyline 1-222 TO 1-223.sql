# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.222');


# ---------------------------------------------------------------------- #
# Add Table courier                                                     #
# ---------------------------------------------------------------------- #
ALTER TABLE `courier`
	ADD COLUMN `FilePrefix` VARCHAR(30) NULL AFTER `Password`,
	ADD COLUMN `FileCount` INT(11) NULL AFTER `FilePrefix`;



# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.223');
