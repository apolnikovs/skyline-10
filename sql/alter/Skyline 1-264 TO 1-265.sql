# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.264');

# ---------------------------------------------------------------------- #
# Add Table service_provider                                         #
# ---------------------------------------------------------------------- # 
ALTER TABLE service_provider ADD COLUMN KeepEngInPCArea ENUM('Yes','No') NOT NULL DEFAULT 'No' AFTER AutoReorderStock;



# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.265');
