# ---------------------------------------------------------------------- #
# Script generated with: DeZign for Databases V7.2.0                     #
# Target DBMS:           MySQL 5                                         #
# Project file:          SkyLine.dez                                     #
# Project name:          SkyLine                                         #
# Author:                Brian Etherington                               #
# Script type:           Alter database script                           #
# Created on:            2012-10-23 09:55                                #
# ---------------------------------------------------------------------- #


# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                                     #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.100');

# ---------------------------------------------------------------------- #
# Drop foreign key constraints                                           #
# ---------------------------------------------------------------------- #

ALTER TABLE `service_provider` DROP FOREIGN KEY `county_TO_service_provider`;

ALTER TABLE `service_provider` DROP FOREIGN KEY `country_TO_service_provider`;

ALTER TABLE `service_provider` DROP FOREIGN KEY `user_TO_service_provider`;

ALTER TABLE `job` DROP FOREIGN KEY `network_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `branch_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `client_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `service_provider_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `customer_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `product_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `service_type_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `manufacturer_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `job_type_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `status_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `model_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `user_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `user_TO_job_ModifiedUser`;

ALTER TABLE `job` DROP FOREIGN KEY `county_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `country_TO_job`;

ALTER TABLE `customer` DROP FOREIGN KEY `county_TO_customer`;

ALTER TABLE `customer` DROP FOREIGN KEY `country_TO_customer`;

ALTER TABLE `customer` DROP FOREIGN KEY `customer_title_TO_customer`;

ALTER TABLE `status` DROP FOREIGN KEY `user_TO_status`;

ALTER TABLE `appointment` DROP FOREIGN KEY `job_TO_appointment`;

ALTER TABLE `appointment` DROP FOREIGN KEY `user_TO_appointment`;

ALTER TABLE `network_service_provider` DROP FOREIGN KEY `service_provider_TO_network_service_provider`;

ALTER TABLE `user` DROP FOREIGN KEY `service_provider_TO_user`;

ALTER TABLE `audit` DROP FOREIGN KEY `job_TO_audit`;

ALTER TABLE `part` DROP FOREIGN KEY `job_TO_part`;

ALTER TABLE `contact_history` DROP FOREIGN KEY `job_TO_contact_history`;

ALTER TABLE `status_history` DROP FOREIGN KEY `status_TO_status_history`;

ALTER TABLE `status_history` DROP FOREIGN KEY `job_TO_status_history`;

ALTER TABLE `central_service_allocation` DROP FOREIGN KEY `service_provider_TO_central_service_allocation`;

ALTER TABLE `town_allocation` DROP FOREIGN KEY `service_provider_TO_town_allocation`;

ALTER TABLE `town_allocation` DROP FOREIGN KEY `customer_TO_town_allocation`;

ALTER TABLE `client` DROP FOREIGN KEY `service_provider_TO_client`;

ALTER TABLE `postcode_allocation` DROP FOREIGN KEY `service_provider_TO_postcode_allocation`;

ALTER TABLE `claim_response` DROP FOREIGN KEY `job_TO_claim_response`;

ALTER TABLE `email_job` DROP FOREIGN KEY `job_TO_email_job`;

# ---------------------------------------------------------------------- #
# Modify table "service_provider"                                        #
# ---------------------------------------------------------------------- #

DROP INDEX `IDX_service_provider_5` ON `service_provider`;

CREATE INDEX `IDX_service_provider_2` ON `service_provider` (`DiaryStatus`);

# ---------------------------------------------------------------------- #
# Modify table "job"                                                     #
# ---------------------------------------------------------------------- #

DROP INDEX `IDX_job_19` ON `job`;

DROP INDEX `IDX_job_20` ON `job`;

DROP INDEX `IDX_job_21` ON `job`;

ALTER TABLE `job` ADD COLUMN `RAType` ENUM('repair','return','estimate','invoice','on hold');

ALTER TABLE `job` ADD COLUMN `RAStatusID` INTEGER;

CREATE INDEX `IDX_job_4` ON `job` (`JobSourceID`);

CREATE INDEX `IDX_job_5` ON `job` (`NetworkRefNo`);

CREATE INDEX `IDX_job_6` ON `job` (`ServiceCentreJobNo`);

CREATE INDEX `IDX_job_RAStatusID_FK` ON `job` (`RAStatusID`);

# ---------------------------------------------------------------------- #
# Modify table "customer"                                                #
# ---------------------------------------------------------------------- #

DROP INDEX `IDX_customer_4` ON `customer`;

DROP INDEX `IDX_customer_5` ON `customer`;

DROP INDEX `IDX_customer_6` ON `customer`;

CREATE INDEX `IDX_customer_1` ON `customer` (`PostalCode`);

CREATE INDEX `IDX_customer_2` ON `customer` (`ContactLastName`);

CREATE INDEX `IDX_customer_3` ON `customer` (`ContactFirstName`);

# ---------------------------------------------------------------------- #
# Modify table "status"                                                  #
# ---------------------------------------------------------------------- #

DROP INDEX `IDX_status_2` ON `status`;

CREATE INDEX `IDX_status_1` ON `status` (`StatusName`);

# ---------------------------------------------------------------------- #
# Modify table "appointment"                                             #
# ---------------------------------------------------------------------- #

DROP INDEX `IDX_appointment_4` ON `appointment`;

DROP INDEX `IDX_appointment_5` ON `appointment`;

DROP INDEX `IDX_appointment_6` ON `appointment`;

DROP INDEX `IDX_appointment_7` ON `appointment`;

DROP INDEX `IDX_appointment_8` ON `appointment`;

DROP INDEX `IDX_appointment_9` ON `appointment`;

DROP INDEX `IDX_appointment_10` ON `appointment`;

DROP INDEX `IDX_appointment_11` ON `appointment`;

DROP INDEX `IDX_appointment_12` ON `appointment`;

ALTER TABLE `appointment` ADD COLUMN `CreatedTimeStamp` TIMESTAMP;

CREATE INDEX `IDX_appointment_2` ON `appointment` (`ServiceProviderID`);

CREATE INDEX `IDX_appointment_3` ON `appointment` (`NonSkylineJobID`);

CREATE INDEX `IDX_appointment_4` ON `appointment` (`DiaryAllocationID`);

CREATE INDEX `IDX_appointment_5` ON `appointment` (`ServiceProviderEngineerID`);

CREATE INDEX `IDX_appointment_6` ON `appointment` (`ServiceProviderID`);

CREATE INDEX `IDX_appointment_7` ON `appointment` (`NonSkylineJobID`);

CREATE INDEX `IDX_appointment_8` ON `appointment` (`DiaryAllocationID`);

CREATE INDEX `IDX_appointment_9` ON `appointment` (`AppointmentDate`);

CREATE INDEX `IDX_appointment_10` ON `appointment` (`ServiceProviderEngineerID`);

# ---------------------------------------------------------------------- #
# Add table "ra_status"                                                  #
# ---------------------------------------------------------------------- #

CREATE TABLE `ra_status` (
    `RAStatusID` INTEGER NOT NULL AUTO_INCREMENT,
    `ListOrder` INTEGER NOT NULL,
    `BrandID` INTEGER NOT NULL,
    `Status` ENUM('Active','Inactive'),
    `CreatedDateTime` TIMESTAMP,
    `ModifiedUserID` INTEGER NOT NULL,
    `ModifiedDate` DATE,
    CONSTRAINT `ra_statusID` PRIMARY KEY (`RAStatusID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_ra_status_BrandID_FK` ON `ra_status` (`BrandID`);

CREATE INDEX `IDX_ra_status_ModifiedUserID_FK` ON `ra_status` (`ModifiedUserID`);

# ---------------------------------------------------------------------- #
# Add table "ra_history"                                                 #
# ---------------------------------------------------------------------- #

CREATE TABLE `ra_history` (
    `RAHistoryID` INTEGER NOT NULL AUTO_INCREMENT,
    `JobID` INTEGER NOT NULL,
    `OldStatusID` INTEGER NOT NULL,
    `NewStatusID` INTEGER NOT NULL,
    `Notes` VARCHAR(1000),
    `CreatedDateTime` TIMESTAMP NOT NULL,
    `ModifiedUserID` INTEGER NOT NULL,
    `ModifiedDate` DATE NOT NULL,
    PRIMARY KEY (`RAHistoryID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_ra_history_JobID_FK` ON `ra_history` (`JobID`);

CREATE INDEX `IDX_ra_history_OldStatusID_FK` ON `ra_history` (`OldStatusID`);

CREATE INDEX `IDX_ra_history_NewStatusID_FK` ON `ra_history` (`NewStatusID`);

CREATE INDEX `IDX_ra_history_ModifiedUserID_FK` ON `ra_history` (`ModifiedUserID`);

# ---------------------------------------------------------------------- #
# Add foreign key constraints                                            #
# ---------------------------------------------------------------------- #

ALTER TABLE `service_provider` ADD CONSTRAINT `county_TO_service_provider` 
    FOREIGN KEY (`CountyID`) REFERENCES `county` (`CountyID`);

ALTER TABLE `service_provider` ADD CONSTRAINT `country_TO_service_provider` 
    FOREIGN KEY (`CountryID`) REFERENCES `country` (`CountryID`);

ALTER TABLE `service_provider` ADD CONSTRAINT `user_TO_service_provider` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `job` ADD CONSTRAINT `network_TO_job` 
    FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`);

ALTER TABLE `job` ADD CONSTRAINT `branch_TO_job` 
    FOREIGN KEY (`BranchID`) REFERENCES `branch` (`BranchID`);

ALTER TABLE `job` ADD CONSTRAINT `client_TO_job` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `job` ADD CONSTRAINT `service_provider_TO_job` 
    FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`);

ALTER TABLE `job` ADD CONSTRAINT `customer_TO_job` 
    FOREIGN KEY (`CustomerID`) REFERENCES `customer` (`CustomerID`);

ALTER TABLE `job` ADD CONSTRAINT `product_TO_job` 
    FOREIGN KEY (`ProductID`) REFERENCES `product` (`ProductID`);

ALTER TABLE `job` ADD CONSTRAINT `service_type_TO_job` 
    FOREIGN KEY (`ServiceTypeID`) REFERENCES `service_type` (`ServiceTypeID`);

ALTER TABLE `job` ADD CONSTRAINT `manufacturer_TO_job` 
    FOREIGN KEY (`ManufacturerID`) REFERENCES `manufacturer` (`ManufacturerID`);

ALTER TABLE `job` ADD CONSTRAINT `job_type_TO_job` 
    FOREIGN KEY (`JobTypeID`) REFERENCES `job_type` (`JobTypeID`);

ALTER TABLE `job` ADD CONSTRAINT `status_TO_job` 
    FOREIGN KEY (`StatusID`) REFERENCES `status` (`StatusID`);

ALTER TABLE `job` ADD CONSTRAINT `model_TO_job` 
    FOREIGN KEY (`ModelID`) REFERENCES `model` (`ModelID`);

ALTER TABLE `job` ADD CONSTRAINT `user_TO_job` 
    FOREIGN KEY (`BookedBy`) REFERENCES `user` (`UserID`);

ALTER TABLE `job` ADD CONSTRAINT `user_TO_job_ModifiedUser` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `job` ADD CONSTRAINT `county_TO_job` 
    FOREIGN KEY (`ColAddCountyID`) REFERENCES `county` (`CountyID`);

ALTER TABLE `job` ADD CONSTRAINT `country_TO_job` 
    FOREIGN KEY (`ColAddCountryID`) REFERENCES `country` (`CountryID`);

ALTER TABLE `job` ADD CONSTRAINT `ra_status_TO_job` 
    FOREIGN KEY (`RAStatusID`) REFERENCES `ra_status` (`RAStatusID`);

ALTER TABLE `customer` ADD CONSTRAINT `county_TO_customer` 
    FOREIGN KEY (`CountyID`) REFERENCES `county` (`CountyID`);

ALTER TABLE `customer` ADD CONSTRAINT `country_TO_customer` 
    FOREIGN KEY (`CountryID`) REFERENCES `country` (`CountryID`);

ALTER TABLE `customer` ADD CONSTRAINT `customer_title_TO_customer` 
    FOREIGN KEY (`CustomerTitleID`) REFERENCES `customer_title` (`CustomerTitleID`);

ALTER TABLE `status` ADD CONSTRAINT `user_TO_status` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `appointment` ADD CONSTRAINT `job_TO_appointment` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

ALTER TABLE `appointment` ADD CONSTRAINT `user_TO_appointment` 
    FOREIGN KEY (`UserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `ra_status` ADD CONSTRAINT `brand_TO_ra_status` 
    FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`);

ALTER TABLE `ra_status` ADD CONSTRAINT `user_TO_ra_status` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `ra_history` ADD CONSTRAINT `job_TO_ra_history` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

ALTER TABLE `ra_history` ADD CONSTRAINT `ra_status_TO_ra_history` 
    FOREIGN KEY (`OldStatusID`) REFERENCES `ra_status` (`RAStatusID`);

ALTER TABLE `ra_history` ADD CONSTRAINT `ra_status_TO_ra_history_2` 
    FOREIGN KEY (`NewStatusID`) REFERENCES `ra_status` (`RAStatusID`);

ALTER TABLE `ra_history` ADD CONSTRAINT `user_TO_ra_history` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `network_service_provider` ADD CONSTRAINT `service_provider_TO_network_service_provider` 
    FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`);

ALTER TABLE `user` ADD CONSTRAINT `service_provider_TO_user` 
    FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`);

ALTER TABLE `audit` ADD CONSTRAINT `job_TO_audit` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

ALTER TABLE `part` ADD CONSTRAINT `job_TO_part` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

ALTER TABLE `contact_history` ADD CONSTRAINT `job_TO_contact_history` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

ALTER TABLE `status_history` ADD CONSTRAINT `status_TO_status_history` 
    FOREIGN KEY (`StatusID`) REFERENCES `status` (`StatusID`);

ALTER TABLE `status_history` ADD CONSTRAINT `job_TO_status_history` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

ALTER TABLE `central_service_allocation` ADD CONSTRAINT `service_provider_TO_central_service_allocation` 
    FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`);

ALTER TABLE `town_allocation` ADD CONSTRAINT `service_provider_TO_town_allocation` 
    FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`);

ALTER TABLE `town_allocation` ADD CONSTRAINT `customer_TO_town_allocation` 
    FOREIGN KEY (`CustomerID`) REFERENCES `customer` (`CustomerID`);

ALTER TABLE `client` ADD CONSTRAINT `service_provider_TO_client` 
    FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`);

ALTER TABLE `postcode_allocation` ADD CONSTRAINT `service_provider_TO_postcode_allocation` 
    FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`);

ALTER TABLE `claim_response` ADD CONSTRAINT `job_TO_claim_response` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

ALTER TABLE `email_job` ADD CONSTRAINT `job_TO_email_job` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.101');
