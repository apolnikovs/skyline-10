// 
// PCCS Gauge Chart
// 
// Version 1.2
// Date: 20/04/2012
// Author: Brian Etherington
// 
// Declare a unique namespace.
var pccs = { };

// Class constructor.
//
// Parameters:
// container is a DOM element on the client that will contain the chart.
// options is a name/value map of options. 
pccs.Gauge = function(container) {

    this.containerElement = container;        
               
}

// Main drawing logic.
// Parameters:
// data is data value to display
pccs.Gauge.prototype.draw = function(data, options) {
    
    function rgbToHex(colour) {      

        var digits = /(.*?)rgb\((\d+), (\d+), (\d+)\)/.exec(colour);
    
        var red = parseInt(digits[2]);
        var green = parseInt(digits[3]);
        var blue = parseInt(digits[4]);
    
        var rgb = blue | (green << 8) | (red << 16);
        return digits[1] + '#' + rgb.toString(16);
    };
    
    function round(value) {
        return Math.round(value*100)/100;
        //return value;
    };
    
    function arc(x, y, innerRadius, outerRadius, startAngle, endAngle) {
        
        if (startAngle < 0) startAngle = 0;
        if (startAngle > 180) startAngle = 180;
        if (endAngle < 0) endAngle = 0;
        if (endAngle > 180) endAngle = 180;
        
        startAngle = startAngle * (Math.PI / 180);
        endAngle   = endAngle * (Math.PI / 180);

        var outerStartX   = round(x - (outerRadius * Math.cos(startAngle)));
        var outerStartY   = round(y - (outerRadius * Math.sin(startAngle)));
        var outerEndX     = round(x - (outerRadius * Math.cos(endAngle)));
        var outerEndY     = round(y - (outerRadius * Math.sin(endAngle)));
            
        var innerStartX   = round(x - (innerRadius * Math.cos(startAngle)));
        var innerStartY   = round(y - (innerRadius * Math.sin(startAngle)));
        var innerEndX     = round(x - (innerRadius * Math.cos(endAngle)));
        var innerEndY     = round(y - (innerRadius * Math.sin(endAngle)));

        var path =
               "M"+outerStartX+" "+outerStartY+
               "A"+outerRadius+" "+outerRadius+" 0 0 "+ (startAngle < endAngle ? "1 " : "0 ")+outerEndX+" "+outerEndY+
               "L"+innerEndX+" "+innerEndY+
               "A"+innerRadius+" "+innerRadius+" 0 0 "+ (startAngle < endAngle ? "0 " : "1 ")+innerStartX+" "+innerStartY+
               "z";
            
        return path;
 
    };
    
    function getBackgroundColour(jqueryElement) {
        
        // Is current element's background color set?
        var color = jqueryElement.css("background-color");

    
        if (color !== 'rgba(0, 0, 0, 0)' && color !== 'transparent') {
            // if so then return that color
            return color;
        }

        // if not: are you at the body element?
        if (jqueryElement.is("body")) {
            // return known 'false' value
            //return false;
            return 'rgb(255, 255, 255)';
        } else {
            // call getBackground with parent item
            return getBackgroundColour(jqueryElement.parent());
        }
    }
    
    function printObject(o) {
        var out = '';
        for (var p in o) {
            out += p + ': ' + o[p] + '\n';
        }
        alert(out);
    }
    
       
    // Initialise options vars...
    if (typeof options == 'undefined') options = new Array();
    this.gauge_colour = ('gauge_colour' in options) ? options['gauge_colour'] : '#FF0000';
    this.mark_colour = ('mark_colour' in options) ? options['mark_colour'] : '#ffffff';
    this.ind_colour = ('ind_colour' in options) ? options['ind_colour'] : this.gauge_colour;
    this.red_colour = ('red_colour' in options) ? options['red_colour'] : '#23A274';
    this.red_start = ('red_start' in options) ? options['red_start'] : 0;
    this.red_end = ('red_end' in options) ? options['red_end'] : 0;
    this.amber_colour = ('amber_colour' in options) ? options['amber_colour'] : '#ffbf00';
    this.amber_start = ('amber_start' in options) ? options['amber_start'] : 0;
    this.amber_end = ('amber_end' in options) ? options['amber_end'] : 0;
    this.text_colour = ('text_colour' in options) ? options['text_colour'] : this.gauge_colour;
    this.text_font = ('text_font' in options) ? options['text_font'] : 'arial';
    this.text_size = ('text_size' in options) ? options['text_size'] : 10;
    this.value_text_size = ('text_size' in options) ? options['text_size'] : 10;
    this.scale_start = ('scale_start' in options) ? options['scale_start'] : 0;
    this.scale_inc = ('scale_inc' in options) ? options['scale_inc'] : 10;
    this.scale_divisions = ('scale_divisions' in options) ? options['scale_divisions'] : 10;      
    this.show_minor_ticks = ('show_minor_ticks' in options) ? options['show_minor_ticks'] : true;
    this.show_gauge_value = ('show_gauge_value' in options) ? options['show_gauge_value'] : true;
            
    var $this = $('#'+this.containerElement);
    var w = $this.width();
    var h = $this.height();
        
    var colour = getBackgroundColour($this);
    this.background_colour = rgbToHex(colour);
    this.base_height = 10;
 
    this.width = w;
    this.height = (this.width / 2) + this.base_height;
        
    $this.height(this.height);
        
    $this.empty();

    var paper = Raphael(this.containerElement, this.width, this.height);
        
    var radians = Math.PI / 180;
        
    var radius = this.height-this.base_height;
    var gauge_thickness = radius / 10;
               
    if (radius < 60) {
        this.text_size = 6;
        //gauge_thickness = 5;
        this.show_gauge_value = false;
    } else if (radius < 100) {
        this.text_size = 9;
        //gauge_thickness = 8;
        this.value_text_size = 15;
    }  
        
    var inc_degrees = 180 / this.scale_divisions;
    var max = this.scale_inc*this.scale_divisions;
        
    // draw gauge..
    // NOTE: do not draw circle because the arc path curvature does not exactly
    //       match that of a circle.  This results in a slight bleed of the 
    //       underlying gauge circle visble behind the red and green arcs.
    //paper.circle(radius, radius+1, radius-1)
    //     .attr( { "fill": this.gauge_colour, "stroke": this.gauge_colour } );         
    paper.path(arc(radius, radius+1, radius-gauge_thickness-5, radius-1, 0, 180))
         .attr( { "fill": this.gauge_colour, "stroke": this.gauger_colour } );
         
    // draw amber arc...    
    if (this.amber_start > 0 && this.amber_end > 0) {
        paper.path(arc(radius, radius+1, radius-gauge_thickness-5, radius-1, (this.amber_start/max)*180, (this.amber_end/max)*180))
            .attr( { "fill": this.amber_colour, "stroke": this.amber_colour } );
    }
         
    // draw red arc...    
    if (this.red_start > 0 && this.red_end > 0) {
        paper.path(arc(radius, radius+1, radius-gauge_thickness-5, radius-1, (this.red_start/max)*180, (this.red_end/max)*180))
            .attr( { "fill": this.red_colour, "stroke": this.red_colour } );
    }
            
    // draw minor scale marks...
    if (this.show_minor_ticks) {
        var num_ticks = 5*this.scale_divisions;
        var tick_degrees = inc_degrees / 5;
        for (i=0; i<num_ticks; i++) { 
            // skip major scale marks...
            if (i%5 != 0) {
                paper.rect(gauge_thickness, radius, radius-gauge_thickness, 1, 2)
                     .attr( { "fill": this.mark_colour, "stroke": this.mark_colour } )
                     .transform("r"+tick_degrees*i+","+radius+","+radius);
            }
        }
        // draw inner gauge circle..
        paper.circle(radius, radius+1, radius-gauge_thickness-5)
             .attr( { "fill": this.gauge_colour, "stroke": this.gauge_colour, "stroke-width": 2 } );
        // draw red arc...    
        paper.path(arc(radius, radius+1, radius-gauge_thickness-5, radius-gauge_thickness-3, (this.red_start/max)*180, (this.red_end/max)*180))
             .attr( { "fill": this.red_colour, "stroke": this.red_colour } );
    }
     
   paper.circle(radius, radius+1, radius-gauge_thickness-6)
         .attr( { "fill": this.background_colour, "stroke": this.background_colour, "stroke-width": 1 } );
          
    // draw major scale marks...       
    for (i=1; i<this.scale_divisions; i++) { 
        paper.rect(0, radius+1, radius, 1)
             .attr( { "fill": this.mark_colour, "stroke": this.mark_colour, "stroke-width": 1 } )
             .transform("r"+inc_degrees*i+","+radius+","+(radius+1));
    } 
    
    // Print Gauge Value...
    if (this.show_gauge_value) {
        var text = paper.text(0, -100, data)
                        .attr( { "font-family":this.text_font,
                                 "font-size":this.value_text_size } ); 
        var bbox = text.getBBox();
        
        paper.text(radius, 4*radius/5, data)
             .attr( { "font-family":this.text_font,
                      "font-size":this.value_text_size,
                      "fill":this.gauge_colour } ); 
        
    }
             
    // draw gauge base block...
    paper.rect(0, radius, this.width, this.base_height+1)
         .attr( { "fill": this.background_colour, "stroke": this.background_colour } ); 
              
    // print scale calibration numerals...
    for (i=0; i<=this.scale_divisions; i++) { 
        
        var text = paper.text(0, -100, this.scale_start+(i*this.scale_inc))
                        .attr( { "font-family":this.text_font,
                                 "font-size":this.text_size } );
        var bbox = text.getBBox();
            
        var angle = inc_degrees*i;
        var c = angle * radians;
        var r = radius - gauge_thickness - 10;
            
        if ( angle < 45 || angle > 135) {
            r -= bbox.width/2;
        } else {
            r -= bbox.height/2;
        }

        var x = radius - (Math.cos(c) * r);
        var y = radius - (Math.sin(c) * r);

        paper.text(x, y, this.scale_start+(i*this.scale_inc))
             .attr( { "font-family":this.text_font,
                      "font-size":this.text_size,
                      "fill":this.gauge_colour} );
    }
        
    // draw indicator...
    var indicator = paper.rect(gauge_thickness+8, radius, radius-(gauge_thickness+8), 1, 10)
                         .attr( { "fill": this.ind_colour, "stroke": this.ind_colour } );
                         
    paper.circle(radius, radius, 4)
         .attr( { "fill": this.ind_colour, "stroke": this.ind_colour } );
          
    // animate indicator...
    var valueAngle = (180 / max) * data;
    if (data <= max*0.9) {
        indicator.animate( { transform: "r" + valueAngle + " "+radius+" "+radius } , 2000, "back-out");
    } else {
        indicator.animate( { transform: "r" + valueAngle + " "+radius+" "+radius } , 2000, "bounce");
    } 

}
    