DROP TEMPORARY TABLE IF EXISTS temp;
            
/*Setting input variables separate from the big query*/
/*SET @networkID := NULL, @clntID := NULL, @branchID := NULL;*/
SET @networkID := [networkID], @clntID := [clientID], @branchID := [branchID];

/*First query - generating temp table with overdue jobs*/
		
CREATE TEMPORARY TABLE temp AS (

	SELECT	j.DateBooked,
			j.RepairCompleteDate,
			j.OpenJobStatus,
			j.JobID,
			j.ServiceCentreJobNo,
			j.CustomerID,
			j.ProductID,
			j.ServiceProviderID,
			j.StatusID,
			j.ModelID,
			@clientID := j.ClientID,
			@turn := turnaround(j.JobID) AS turnaround,
			@days := DATEDIFF(CURDATE(), j.DateBooked) AS days,
			(@days - @turn) AS overdue
		
    FROM	job AS j
    
    WHERE	CASE
                WHEN (@networkID IS NOT NULL) THEN j.NetworkID = @networkID
                ELSE TRUE
    		END
    		
    		AND
    		
    		CASE
                WHEN (@clntID IS NOT NULL) THEN j.ClientID = @clntID
                ELSE TRUE
    		END
    		
            AND

    		CASE
                WHEN (@branchID IS NOT NULL) THEN j.BranchID = @branchID
                ELSE TRUE
    		END
    		
    		AND
                        
            j.ClosedDate IS NULL AND
			j.StatusID != 33 AND
			j.StatusID != 41

    /*HAVING	overdue > 0*/
    
);


/*Second query - filtering out overdue jobs with specific time range*/
/*changed display date format by thirumal..%Y-%m-%d replace with %d/%/m%Y  */
    
SELECT	    DATE_FORMAT(job.DateBooked, '%d/%m/%Y') AS '0',			    -- Column Date
	    
	    	job.days								AS '1',				-- Column Days
	    
		    (
				CASE
				    WHEN job.overdue > 0
				    THEN CONCAT('<span style=\'color:red; font-weight:bold;\'>', job.overdue,	'</span>')
				    ELSE CONCAT('<span style=\'font-weight:bold;\'>', job.overdue, '</span>')
				END
		    )					    				AS '2',			    -- Column Overdue
	    
		    CONCAT( '<span class=\'hoverCell\' title=\'',
			    CONCAT( IF(customer.BuildingNameNumber != '', CONCAT(customer.BuildingNameNumber, ', '), ''),
				    IF(customer.Street != '', customer.Street, ''),
				    IF(customer.LocalArea != '', CONCAT(', ', customer.LocalArea), ''),
				    IF(customer.TownCity != '', CONCAT(', ', customer.TownCity), ''),
				    IF(customer.PostalCode != '', CONCAT(', ', customer.PostalCode), '')
			    ),
			    '\'>',
			    CONCAT( customer.ContactLastName, ', ', title.Title, ' ', customer.ContactFirstName),
			    '</span>'
		    )					     				AS '3',			    -- Column Customer
	    
		    CONCAT( '<span class=\'hoverCell\' ',
			    CASE
					WHEN customer.ContactMobile != '' AND customer.ContactMobile IS NOT NULL
					THEN	CASE
						    	WHEN customer.ContactHomePhone != '' AND customer.ContactHomePhone IS NOT NULL
						    	THEN CONCAT('title=\'Alternative No: ', customer.ContactHomePhone, '\'')
						    	ELSE 'title=\'No alternative phone number\''
							END
					ELSE 'title=\'No alternative phone number\''
			    END,
			    '>',
	            IF(customer.ContactMobile != '', customer.ContactMobile, customer.ContactHomePhone),
			    '</span>'
		    )					    				AS '4',			    -- Column Phone

		    CONCAT(	'<span class=\'hoverCell\' title=\'',
			    	CONCAT(manufacturer.manufacturerName, ' Model No: ', model.modelNumber),
			    	'\'>',
			    	unit.UnitTypeName,
			    	'</span>'
		    )					    				AS '5',			    -- Column Product Type
	    
		    job.JobID				    			AS '6',			    -- Column Skyline No
		    service.Acronym			    			AS '7',			    -- Column Service Provider
		    job.ServiceCentreJobNo		    		AS '8',			    -- Column Job No
            '<input type=\'checkbox\' />'           AS '9'			    -- Column Select

FROM	    temp AS job

LEFT JOIN   customer			   		ON job.CustomerID = customer.CustomerID
LEFT JOIN   customer_title AS title		ON customer.CustomerTitleID = title.CustomerTitleID
LEFT JOIN   product			    		ON job.ProductID = product.ProductID
LEFT JOIN   model			    		ON job.ModelID = model.ModelID
LEFT JOIN   manufacturer		   	 	ON model.ManufacturerID = manufacturer.manufacturerID
LEFT JOIN   unit_type AS unit			ON model.UnitTypeID = unit.UnitTypeID
LEFT JOIN   service_provider AS service ON job.ServiceProviderID = service.ServiceProviderID
LEFT JOIN   status			    		ON job.StatusID = status.StatusID
