<?php
/**
 * AllocationReassignment.class.php
 * 
 * Routines for interaction with the allocation_reasignment table
 *
 * @author     Andrew Williams <a.williams@pccsuk.com>
 * @copyright  2013 PC Control Systems
 * @link       
 * @version    1.0
 * 
 * Changes
 * Date        Version Author                Reason
 * 25/02/2013  1.00    Andrew J. Williams    Initial Version
 ******************************************************************************/

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

class AllocationReassignment extends CustomModel { 
    private $table;                                                             /* For Table Factory Class */
    private $conn;                                                              /* Database Connection */
    
    

    
    public function __construct($Controller) {
                  
        parent::__construct($Controller);
        
        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] ); 
        
        $this->table = TableFactory::AllocationReassignment();
    }
    
    /**
     * create
     *  
     * Create an allocation reassignment record
     * 
     * @param array $args   Associative array of field values for the creation of
     *                      the new allocation reassignment
     * 
     * @return array    (status - Status Code, message - Status message, id - Id of inserted item
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function create($args) {
        $cmd = $this->table->insertCommand( $args );
        if ($this->Execute($this->conn, $cmd, $args)) {
            $result =  array(                                                   /* Allocation reassignment record successfully created */
                             'status' => 'SUCCESS',
                             'id' => $this->conn->lastInsertId()                /* Return the newly created allocation reassignment record's ID */
                            );
        } else {
            $result = array(
                            'status' => 'FAIL',
                            'id' => 0,                                          /* Not created no ID to return */
                            'message' => $this->lastPDOError()                  /* Return the error */
                           );
        }
        return $result;
    }
    
    /**
     * update
     *  
     * Update an allocation reassignment record
     * 
     * @param array $args   Associative array of field values for to update the
     *                      an allocation reassignment record. The array must 
     *                      include the primary key.
     * 
     * @return 
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function update($args) {
        $cmd = $this->table->updateCommand( $args );
        
        if ($this->Execute($this->conn, $cmd, $args)) {
            $result =  array(                                                   /* Entry successfully created */
                             'status' => 'SUCCESS',
                             'message' => ''
                            );
        } else {
            $result = array(
                            'status' => 'FAIL',
                            'message' => $this->lastPDOError()                  /* Return the error */
                           );
        }
        return $result;
    }
    
    /**
     * delete
     *  
     * Delete an allocation reassignment record
     * 
     * @param array $args (Field AllocationReassignmentID => Value )
     * 
     * @return (status - Status Code, message - Status message)
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function delete($arg) {
        $index  = array_keys($arg);
        
        $cmd = $this->table->deleteCommand( $index[0].' = '.$arg[$index[0]] );
        
        if ($this->Execute($this->conn, $cmd, $arg)) {
            $result =  array(                                                   /* Job successfully created */
                             'status' => 'SUCCESS',
                             'message' => 'Deleted'
                            );
        } else {
            $result = array(
                            'status' => 'FAIL',
                            'message' => $this->lastPDOError()                  /* Return the error */
                           );
        }
        return $result;
    }
    
    /**
     * getSecondaryServiceProvider
     *  
     * Get the secomndary service provider based on the Service Provider and
     * Postcode Area
     * 
     * @param integer $spId     The service Provider ID
     * @param string $pc        The post code area
     * 
     * @return integer containing Secondary Service Provider ID or null if not set
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    
    public function getSecondaryServiceProvider($spId, $pc) {
        $sql = "
                SELECT
			`SecondaryServiceProviderID`
		FROM
			`allocation_reassignment`
		WHERE
			`MainServiceProviderID` = $spId
			AND `Postcode` = '$pc'
               ";
                $result = $this->Query($this->conn, $sql);
        
        if (count($result) > 0 ) {
            return($result[0]['SecondaryServiceProviderID']);
        } else {
            return (null);
        }
    }
    
    /**
     * isSecondaryServiceProvider
     *  
     * Check if a secondary service provider Id and post code area exist in the 
     * allocation reassignment table
     * 
     * @param integer $sspId     The secondary service Provider ID
     * @param string $pc        The post code area
     * 
     * @return integer containing Secondary Service Provider ID or null if not set
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    
    public function isSecondaryServiceProvider($sspId, $pc) {
        $sql = "
                SELECT
			`SecondaryServiceProviderID`
		FROM
			`allocation_reassignment`
		WHERE
			`SecondaryServiceProviderID` = $sspId
			AND `Postcode` = '$pc'
               ";
                $result = $this->Query($this->conn, $sql);
        
        if (count($result) > 0 ) {
            return(true);
        } else {
            return (false);
        }
    }
    
}
?>