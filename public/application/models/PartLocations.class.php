<?php

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

/**
 * Description
 *
 * This class is used for handling database actions of PartLocations Page in Lookup Tables section under System Admin
 *
 * @author      Andris Polnikovs <a.polnikovs@gmail.com>
 * @version     1.0 
 * @created     04/06/2013
 */
class PartLocations extends CustomModel {

    public function __construct($controller) {

        parent::__construct($controller);

        $this->conn = $this->Connect($this->controller->config['DataBase']['Conn'], $this->controller->config['DataBase']['Username'], $this->controller->config['DataBase']['Password']);
        $this->SQLGen = $this->controller->loadModel('SQLGenerator');
        $this->fields = [
            
            "LocationName",
            "MainStore",
            "RepairSite",
            "UseStockAllocation",
            "OutWarrantyMarkupPerc",
            "Status",
            "ServiceProviderID"
            
           
            
        ];
    }

    public function insertPartLocations($P, $spid) {
        $P["ServiceProviderID"] = $spid;
        $P["Status"] = isset($P["Status"]) ? $P["Status"] : "Active";
        $id = $this->SQLGen->dbInsert('service_provider_part_location', $this->fields, $P, true, true);
        return $id;
    }

    public function updatePartLocations($P, $spid) {
        $P["ServiceProviderID"] = $spid;
        $P["Status"] = isset($P["Status"]) ? $P["Status"] : "Active";
        $id = $this->SQLGen->dbUpdate('service_provider_part_location', $this->fields, $P, "ServiceProviderPartLocationID=" . $P['ServiceProviderPartLocationID'], true);
    }

    public function getPartLocationsData($id) {
        $sql = "select * from service_provider_part_location where ServiceProviderPartLocationID=$id";
        $res = $this->query($this->conn, $sql);
        return $res[0];
    }

    public function deletePartLocations($id) {
        $sql = "update service_provider_part_location set Status='In-Active' where ServiceProviderPartLocationID=$id";
        $this->execute($this->conn, $sql);
    }

   
    

}

?>