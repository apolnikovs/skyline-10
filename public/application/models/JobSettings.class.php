<?php

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

/**
 * Description
 *
 * This class is used for handling database actions of Job Settings Page 
 *
 * @author      Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
 * @version     1.0
 */

class JobSettings extends CustomModel {
    
    private $conn;
    private $table; 
    private $ActualFields = array(
        
            'BranchIDAddress',
            'ColBranchAddressID',
            'BuildingNameNumber',
            'PostalCode',
            'Street',
            'LocalArea',
            'TownCity',
            'CountyID',
            'CountryID',
            'CustomerTitleID',
            'ContactFirstName',
            'ContactLastName',
            'ContactHomePhone',
            'ContactWorkPhoneExt',
            'ContactWorkPhone',
            'ContactFax',
            'ContactMobile',
            'ContactEmail',
            'CustomerType',
            'CompanyName',
            'ContactAltMobile',
            'StockCode',
            'ProductID',
            'ManufacturerID',
            'UnitTypeID',
            'ServiceTypeID',
            'JobTypeID',
            'AgentRefNo',
            'SerialNo',
            'DateOfPurchase',
            'ReportedFault',
            'RepairType',
            'Notes',
            'ItemLocation',
            'RepairDescription',
            'OriginalRetailerPart1',
            'OriginalRetailerPart2',
            'OriginalRetailerPart3',
            'Accessories',
            'UnitCondition',
            'ProductLocation',
            'LocateBy',
            'ModelName',
            'ProductDescription',
            'ReceiptNo',
            'ColAddCompanyName',
            'ColAddBuildingNameNumber',
            'ColAddStreet',
            'ColAddLocalArea',
            'ColAddTownCity',
            'ColAddCountyID',
            'ColAddCountryID',
            'ColAddPostcode',
            'ColAddEmail',
            'ColAddPhone',
            'ColAddPhoneExt',
            'ImeiNo',
            'MobilePhoneNetworkID',
            'Insurer',
            'PolicyNo',
            'AuthorisationNo',
            'ReferralNo' );
    
      
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] ); 
        
         $this->table = TableFactory::JobSettings();

    }
    
   
    
    
    
     /**
     * Description
     * 
     * This method used for to update/insert data into database.
     * 
     * @param array $args Its an associative array contains all elements of submitted form.
    
     * @return array It contains status and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */   
    
     public function processData($args) {
         
        
         
        
            if(!$args['NetworkID'])
            {
                $args['NetworkID'] = NULL;
            }
            if(!$args['ClientID'])
            {
                $args['ClientID'] = NULL;
            }
        
            $args['CreatedDate'] = date("Y-m-d H:i:s");
            $args['ModifiedUserID'] = $this->controller->user->UserID;
            $args['ModifiedDate'] = date("Y-m-d H:i:s");
            
          //  unset($args['JobSettingsID']);
            
          
            $QueryResponse = false;
            foreach($this->ActualFields as $Field)
            {
                
                 if( 
                        $Field=='RepairType' || 
                        $Field=='RepairDescription' || 
                        $Field=='ContactFirstName' || 
                        $Field=='ContactLastName' || 
                        $Field=='ContactWorkPhoneExt' || 
                        $Field=='ContactWorkPhone' || 
                        $Field=='ContactFax' || 
                        $Field=='ProductID' ||
                        $Field=='AgentRefNo' ||
                        $Field=='LocateBy' ||
                        $Field=='ColAddPhoneExt' 

                        )
                {
                     $args['Display']   = 0;
                     $args['Mandatory'] = 0;
                     $args['Replicate'] = 0;
                }
                else if ($Field=='CustomerTitleID')
                {
                     $args['Display']   = $args['ContactTitleChk1'];
                     $args['Mandatory'] = $args['ContactTitleChk2'];
                     $args['Replicate'] = $args['ContactTitleChk3'];
                }
                else if ($Field=='OriginalRetailerPart1')
                {
                     $args['Display']   = $args['OriginalRetailerChk1'];
                     $args['Mandatory'] = $args['OriginalRetailerChk2'];
                     $args['Replicate'] = $args['OriginalRetailerChk3'];
                }
                else if ($Field=='ItemLocation')
                {
                     $args['Display']   = $args['isProductAtBranchChk1'];
                     $args['Mandatory'] = $args['isProductAtBranchChk2'];
                     $args['Replicate'] = $args['isProductAtBranchChk3'];
                }
                else
                {
                     $args['Display']   = $args[$Field.'Chk1'];
                     $args['Mandatory'] = $args[$Field.'Chk2'];
                     $args['Replicate'] = $args[$Field.'Chk3'];
                }
                
                $dataArgs = array(
                    
                    'NetworkID'=>$args['NetworkID'], 
                    'ClientID'=>$args['ClientID'], 
                    'JobTypeID'=>$args['JobTypeID'], 
                    'FieldName'=>$Field,
                    'FieldValue'=>$args[$Field],
                    'Display'=>$args['Display'],
                    'Mandatory'=>$args['Mandatory'],
                    'Replicate'=>$args['Replicate'],
                    'Status'=>$args['Status'],
                    'CreatedDate'=>$args['CreatedDate'], 
                    'ModifiedUserID'=>$args['ModifiedUserID'], 
                    'ModifiedDate'=>$args['ModifiedDate'],
                    
                    
                    );
                
                $sID  = $this->isExists($dataArgs);
                        
                if($sID)
                {
                    
                    unset($dataArgs['CreatedDate']);
                    
                    
                    $cmd = $this->table->updateCommand( $dataArgs );
                    $QueryResponse = $this->Execute($this->conn, $cmd, $dataArgs);
                }
                else
                {
                    $cmd = $this->table->insertCommand( $dataArgs );
                    $QueryResponse = $this->Execute($this->conn, $cmd, $dataArgs);
                }
                
                
                        
                        
            }    
            
           
            
            
            
            
            if ($QueryResponse) {
                $result =  array(                                                   /* Data successfully created */
                                 'status' => 'OK',
                                 'message' => 'Your data has been processed successfully.'
                                 
                                );
            } else {
                $result = array(
                                'status' => 'ERROR',
                                'message' => $this->lastPDOError()                  /* Return the error */
                               );
            }
            return $result;
        
         
         
         
         
     }
    
     
   
    
     /**
     * Description
     * 
     * This method is used for to fetch a row from database.
     *
     * @param array $args
     * @global $this->table  
     * @return array It contains row of the given primary key.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function fetchRow($args) {
        
        
        /* Execute a prepared statement by passing an array of values */
        $result = array();
        
//        if(isset($args['JobSettingsID']) && $args['JobSettingsID'])
//        {
//            $sql = 'SELECT * FROM job_settings WHERE JobSettingsID=:JobSettingsID';
//            $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
//            $fetchQuery->execute(array(':JobSettingsID' => $args['JobSettingsID']));
//            $result = $fetchQuery->fetch();
//        }
//        else 

        if(isset($args['JobTypeID']) && isset($args['NetworkID']) && $args['NetworkID'] && isset($args['ClientID']) && $args['ClientID'])
        {
            $sql = 'SELECT * FROM job_settings WHERE JobTypeID=:JobTypeID AND NetworkID=:NetworkID AND ClientID=:ClientID';
            $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $fetchQuery->execute(array(':JobTypeID' => $args['JobTypeID'], ':NetworkID' => $args['NetworkID'], ':ClientID' => $args['ClientID']));
            $result = $fetchQuery->fetchAll();
            
        }
        else if(isset($args['JobTypeID']) && isset($args['NetworkID']) && $args['NetworkID'])
        {
            $sql = 'SELECT * FROM job_settings WHERE JobTypeID=:JobTypeID AND NetworkID=:NetworkID AND ClientID IS NULL';
            $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $fetchQuery->execute(array(':JobTypeID' => $args['JobTypeID'], ':NetworkID' => $args['NetworkID']));
            $result = $fetchQuery->fetchAll();
            
        }
        else if(isset($args['JobTypeID']))
        {
            $sql = 'SELECT * FROM job_settings WHERE JobTypeID=:JobTypeID AND NetworkID IS NULL AND ClientID IS NULL';
            $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $fetchQuery->execute(array(':JobTypeID' => $args['JobTypeID']));
            $result = $fetchQuery->fetchAll();
        }
        
        
        if(!$result)
        {
            $result = array();
        } 
        
        $finalResult = array();
        
            
        $firstRow = 1;
        foreach($result as $rIndex=>$rValue)
        {
            if($firstRow)
            {
                $finalResult['JobSettingsID']  = $rValue['JobSettingsID'];
                $finalResult['NetworkID']      = $rValue['NetworkID'];
                $finalResult['ClientID']       = $rValue['ClientID'];
                $finalResult['JobTypeID']      = $rValue['JobTypeID'];
                $finalResult['Status']         = $rValue['Status'];
                $finalResult['CreatedDate']    = $rValue['CreatedDate'];
                $finalResult['EndDate']        = $rValue['EndDate'];
                $finalResult['ModifiedUserID'] = $rValue['ModifiedUserID'];
                $finalResult['ModifiedDate']   = $rValue['ModifiedDate'];
                
                $firstRow = 0;
            }
            
            $finalResult[$rValue['FieldName']]         = $rValue['FieldValue'];
            
            
            if( 
                        $rValue['FieldName']=='RepairType' || 
                        $rValue['FieldName']=='RepairDescription' || 
                        $rValue['FieldName']=='ContactFirstName' || 
                        $rValue['FieldName']=='ContactLastName' || 
                        $rValue['FieldName']=='ContactWorkPhoneExt' || 
                        $rValue['FieldName']=='ContactWorkPhone' || 
                        $rValue['FieldName']=='ContactFax' || 
                        $rValue['FieldName']=='ProductID' ||
                        $rValue['FieldName']=='AgentRefNo' ||
                        $rValue['FieldName']=='LocateBy' ||
                        $rValue['FieldName']=='ColAddPhoneExt' 

                        )
                {
                     $finalResult[$rValue['FieldName'].'Chk1']   = 0;
                     $finalResult[$rValue['FieldName'].'Chk1'] = 0;
                     $finalResult[$rValue['FieldName'].'Chk1'] = 0;
                }
                else if ($rValue['FieldName']=='CustomerTitleID')
                {
                     $finalResult['ContactTitleChk1']   = $rValue['Display'];
                     $finalResult['ContactTitleChk2']   = $rValue['Mandatory'];
                     $finalResult['ContactTitleChk3']   = $rValue['Replicate'];
                }
                else if ($rValue['FieldName']=='OriginalRetailerPart1')
                {
                     $finalResult['OriginalRetailerChk1']  = $rValue['Display'];
                     $finalResult['OriginalRetailerChk2']  = $rValue['Mandatory'];
                     $finalResult['OriginalRetailerChk3']  = $rValue['Replicate'];
                }
                else if ($rValue['FieldName']=='ItemLocation')
                {
                     $finalResult['isProductAtBranchChk1']   = $rValue['Display'];
                     $finalResult['isProductAtBranchChk2']   = $rValue['Mandatory'];
                     $finalResult['isProductAtBranchChk3']   = $rValue['Replicate'];
                }
                else
                {
                     $finalResult[$rValue['FieldName'].'Chk1']  = $rValue['Display'];
                     $finalResult[$rValue['FieldName'].'Chk2']  = $rValue['Mandatory'];
                     $finalResult[$rValue['FieldName'].'Chk3']  = $rValue['Replicate'];
                }
            
            
             
           
        }    
        
        return $finalResult;
    }
    
    
    
    
    /**
     * Description
     * 
     * This method is used for to fetch column names.
     *
     * @return array 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function fetchColumnNames() {
        
//        $selectQuery = $this->conn->prepare("DESCRIBE job_settings");
//        $selectQuery->execute();
//        $table_fields = $selectQuery->fetchAll(PDO::FETCH_COLUMN);
//       
        
      
        
       $table_fields = array(
           
           'NetworkID', 'ClientID', 'JobTypeID', 'Status', 'CreatedDate', 'EndDate', 'ModifiedUserID' , 'ModifiedDate'
	 	
       );
       
       foreach($this->ActualFields as $af)
       {
           $table_fields[] = $af;
           
           if( 
                   $af=='RepairType' || 
                   $af=='RepairDescription' || 
                   $af=='ContactFirstName' || 
                   $af=='ContactLastName' || 
                   $af=='ContactWorkPhoneExt' || 
                   $af=='ContactWorkPhone' || 
                   $af=='ContactFax' || 
                   $af=='ProductID' ||
                   $af=='AgentRefNo' ||
                   $af=='LocateBy' ||
                   $af=='ColAddPhoneExt' 
                   
                   )
           {
               
           }
           else if ($af=='CustomerTitleID')
           {
                $table_fields[] = 'ContactTitleChk1';
                $table_fields[] = 'ContactTitleChk2';
                $table_fields[] = 'ContactTitleChk3';
               
           }
           else if ($af=='OriginalRetailerPart1')
           {
                $table_fields[] = 'OriginalRetailerChk1';
                $table_fields[] = 'OriginalRetailerChk2';
                $table_fields[] = 'OriginalRetailerChk3';
               
           }
           else if ($af=='ItemLocation')
           {
                $table_fields[] = 'isProductAtBranchChk1';
                $table_fields[] = 'isProductAtBranchChk2';
                $table_fields[] = 'isProductAtBranchChk3';
               
           }
           else
           {
                $table_fields[] = $af.'Chk1';
                $table_fields[] = $af.'Chk2';
                $table_fields[] = $af.'Chk3';
           }
           
       }
        
        return $table_fields;
    }
    
    
    
    
     /**
     * Description
     * 
     * This method is used to check whether row is exists or not
     *
     * @param array $args
     * 
     * @global $this->table  
     * @return int JobSettingsID
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function isExists($args) {
        
        //$this->log($args);
        
        $sql = 'SELECT JobSettingsID FROM job_settings WHERE JobTypeID=:JobTypeID AND NetworkID=:NetworkID AND ClientID=:ClientID AND FieldName=:FieldName';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $fetchQuery->execute(array(':JobTypeID' => $args['JobTypeID'], ':NetworkID' => $args['NetworkID'], ':ClientID' => $args['ClientID'], ':FieldName' => $args['FieldName']));
        $result = $fetchQuery->fetch();
       
        if(count($result))
        {
            return $result['JobSettingsID'];
        }   
        else
        {
            return false;
        }
        
    }
    
    
    
    
//      /**
//     * Description
//     * 
//     * This method is used for to udpate a row into database.
//     *
//     * @param array $args
//     
//     * @return array It contains status of operation and message.
//     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
//     */ 
//    public function update($args) {
//        
//            if(!$args['NetworkID'])
//            {
//                $args['NetworkID'] = NULL;
//            }
//            if(!$args['ClientID'])
//            {
//                $args['ClientID'] = NULL;
//            }
//        
//            $EndDate = "0000-00-00 00:00:00";
//            $row_data = $this->fetchRow($args);
//            if($args['Status']=="In-Active")
//            {
//                if($row_data['Status']!=$args['Status'])
//                {
//                        $EndDate = date("Y-m-d H:i:s");
//                }
//                else
//                {
//                    $EndDate = $row_data['EndDate'];
//                }    
//            }   
//        
//            $args['EndDate'] = $EndDate;
//            $args['ModifiedUserID'] = $this->controller->user->UserID;
//            $args['ModifiedDate'] = date("Y-m-d H:i:s");
//            
//            
//            $cmd = $this->table->updateCommand( $args );
//            
//            if ($this->Execute($this->conn, $cmd, $args)) {
//                $result =  array(                                                   /* Data successfully created */
//                                 'status' => 'OK',
//                                 'id' => $args['JobSettingsID'],                /* Return the newly created item's ID */
//                                 'message' => 'Your data has been updated successfully.'
//                                 
//                                );
//            } else {
//                $result = array(
//                                'status' => 'ERROR',
//                                'id' => 0,                                          /* Not created no ID to return */
//                                'message' => $this->lastPDOError()                  /* Return the error */
//                               );
//            }
//            return $result;
//        
//    }
    
    
    
    public function delete(/*$args*/) {
        return array('status' => 'OK',
                     'message' => 'Your data has been deleted successfully.');
    }
    
    
}
?>