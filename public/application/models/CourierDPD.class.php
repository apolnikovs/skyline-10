<?php
/**
 * CourierDPD.class.php
 * 
 * Business logic relating to the DPD courier
 *
 * @author     Andrew Williams <a.williams@pccsuk.com>
 * @copyright  2013 PC Control Systems
 * @link       http://www.pccontrolsystems.com
 * @version    1.08
 *  
 * Changes
 * Date        Version  Author                Reason
 * 18/03/2013  1.0      Andrew J. Williams    Initial Version
 * 03/04/2013  1.01     Andrew J. Williams    Issue 290 - Courier Defaults moved to Courier Table
 * 23/04/2013  1.02     Brian Etherington     remove underscore character from filename prefix
 *                                            add *START* and *END* heade and footer records to csv file
 *                                            ensure csv columns are escaped with str_putscv
 * 24/04/2013  1.03     Brian Etherington     Add support for Recon Table
 * 29/04/2013  1.04     Brian Etherington     Stop proxessing inbound reponse files when *END* row found
 *                                            Introduced mark_file_processed variable because the ftp folder
 *                                            appears to contain files for customers other than skyline.
 * 01/05/2013  1.05     Brian Etherington     Added Packaging Type to Shipping table, Recon table and CSV file
 * 07/05/2013  1.06     Brian Etherington     Update ftp booking csv file to allow blank despatch dates
 * 14/05/2013  1.07     Brian Etherington     Exclude Northern Ireleand Postcodes (start with BT) from UK service type of 42
 *                                            Remove check on response file number when matching response job numbers
 *                                            Do not include mobile number if email address present.
 *                                            When including mobile number, give priority to Alternative Mobile number,
 *                                            but if that is not present use the normal mobile number.
 * 20/05/2013  1.08     Brian Etherington     Enclose the Consignment Details in [ ] when adding to notes column for ServiceBase. 
 ******************************************************************************/

require_once('CustomBusinessModel.class.php');
require_once('Functions.class.php');

class CourierDPD extends CustomBusinessModel {
    
    #$debug = true;

    public function __construct($controller) {
        parent::__construct($controller);
        //$this->debug = true;
    }
    
    /**
     * sendSingleSwapitJob
     * 
     * Book a single job with DPD for the swap it.
     * 
     * @param intger $jId   JobID
     * 
     * @return 
     * 
     * @author Andrew Williams <a.williams@pccsuk.com> 
     **************************************************************************/
    
    public function sendSingleSwapitJob($jId) {
        
        $couriers_model = $this->controller->LoadModel('Couriers');
        $customer_model = $this->controller->LoadModel('Customer');
        $customer_titles_model = $this->controller->LoadModel('CustomerTitles');
        $job_model = $this->controller->LoadModel('Job');
        //$service_providers_model = $this->controller->LoadModel('ServiceProviders');
        $recon_model = $this->controller->LoadModel('Recon');
        $shipping_model = $this->controller->LoadModel('Shipping');
               
        $job_record = $job_model->fetch($jId);
        
        if ( is_null($job_record) ) {                                           /* Check if supplied job exists */
            if($this->debug) $this->log("sendSingleSwapitJob: No Such Job $jId ",'courier_dpd_');       /* No it dosent */
            
            return(array(
                        'status' => 'FAIL',
                        'message' => 'No such job'
               ));
        }
        
        if (empty($job_record['ServiceProviderID'])) {  /* Check if job is allocated to a service centre */
             if($this->debug) $this->log("sendSingleSwapitJob: Unallocated Job $jId ",'courier_dpd_');
            
            return(array(
                        'status' => 'FAIL',
                        'message' => 'Unallocated Job'
               ));           
        }
               
        $courier = $couriers_model->fetchWhere("`CourierName` = 'DPD' AND `ServiceProviderID` = {$job_record['ServiceProviderID']}");
        if ( is_null($courier) ) {
            if($this->debug) $this->log("sendSingleSwapitJob: No match on Courier for ServiceProvider ssigned in job $jId ",'courier_dpd_');
            
            return(array(
                        'status' => 'FAIL',
                        'message' => 'No match for DPD on service centre assigned to job'
               ));   
        }
        
        $courier = $courier[0];
                    
        $title = $customer_titles_model->fetchRow(array('CustomerTitleID'=>$job_record['CustomerTitleID']));
        
        $customer = $customer_model->fetchRow($job_record['CustomerID']);
                                    
        /* Check if there is a collection address or we need to use the customer's address */
        
        if ( 
                is_null($job_record['ColAddStreet']) 
                && is_null($job_record['ColAddLocalArea']) 
                && is_null($job_record['ColAddTownCity']) 
        ) {                                                                 /* None of collection street / local area / city set - assume we wnat customer's address */
            $address1 = $customer['BuildingNameNumber'];
            $address2 = $customer['Street'];
            $address3 = $customer['LocalArea'];
            $address4 = $customer['TownCity'];
            $postcode = $customer['PostalCode'];
            //$mobile = $customer['ContactMobile'];
            //$email = $customer['ContactEmail'];
        } else {
            $address1 = $job_record['ColAddCompanyName'].' '.$job_record['ColAddBuildingNameNumber'];
            $address2 = $job_record['ColAddStreet'];
            $address3 = $job_record['ColAddLocalArea'];
            $address4 = $job_record['ColAddTownCity'];
            $postcode = $job_record['ColAddPostcode'];
            //$mobile = "";
            //$email = $job_record['ContactEmail'];
        }
        
        $email = $customer['ContactEmail'];
        if (empty($email)) {
            $mobile = $customer['ContactAltMobile'];
            if (empty($mobile)) {
                $mobile = $customer['ContactMobile'];
            }
        } else {
            $mobile = '';
        }
               
        /* Set parameters according to country */  
        if (($job_record['CountryName'] == 'UK'  || $job_record['CountryName'] == null) && strtoupper(substr($postcode,0,2)) != 'BT') {
            $servicecode = 42;
            $despatchdate = $job_record['CollectionDate'] === null ? '' : date( 'd/m/Y', strtotime( '-1 day', strtotime($job_record['CollectionDate']) ) );
        } else {
            $servicecode = 41;                                                  /* Standard code for none UK pickup */
            $despatchdate = $job_record['CollectionDate'] === null ? '' : date( 'd/m/Y', strtotime( '-2 day', strtotime($job_record['CollectionDate']) ) );
            if ($job_record['CountryName'] == 'Republic of Ireland') {
                $postcode = "ZZ71";                                             /* Standard Postcode to be used for the Irish Republic */
            }
        }
        
        $shipping_sql = 'select * from shipping where JobID=:JobID order by ShippingID desc limit 1';
        $shipping_record = $shipping_model->Select($shipping_sql, array('JobID' => $jId));
        if (count($shipping_record) > 0) {
            $shipping_record = $shipping_record[0];
        } else {
            $shipping_record = array('ConsignmentNo' => '', 'PackagingType' => '');
        }
        
        /* Write Header Record */
        $csv = "*START*,,,,,,,,,\n";
                      
        /* OK - Job exists */
        $csv .= Functions::str_putcsv($despatchdate);                           /* Column A - SL Consigmnet date (dispatchDate) */
        $csv .= ",,";
        $csv .= Functions::str_putcsv($jId);                                    /* Column C - SL Job Number */
        $csv .= ",";
        //$csv .= Functions::str_putcsv($job_record['ProductNo']);              /* Column D - Product Code */
        $csv .= Functions::str_putcsv($shipping_record['PackagingType']);       /* Column D - Packaging Type */
        $csv .= ",1";                                                           /* Column E - Always 1 */
        $csv .= ",,";
        $csv .= Functions::str_putcsv($servicecode);                            /* Column G - ServiceCode */
        $csv .= ",,";
        $csv .= Functions::str_putcsv("{$title['Title']} {$job_record['ContactFirstName']} {$job_record['ContactLastName']}");     /* Column I, Customer Title */
        $csv .= ",";
        $csv .= Functions::str_putcsv($address1);                               /* Column J - Address line 1 */
        $csv .= ",";
        $csv .= Functions::str_putcsv($address2);                               /* Column K - Address line 2 */
        $csv .= ",";
        $csv .= Functions::str_putcsv($address3);                               /* Column L - Address line 3 */
        $csv .= ",";
        $csv .= Functions::str_putcsv($address4);                               /* Column M - Address line 4 */
        $csv .= ",";
        $csv .= Functions::str_putcsv($postcode);                               /* Column N - Postcode */
        $csv .= ',,,,,,,,,,,,,,,,,,,,,,,,,';                                    /* O to AM blank */
        $csv .= ",";
        $csv .= Functions::str_putcsv($mobile);                                 /* Column AN - Postcode */
        $csv .= ",";
        $csv .= Functions::str_putcsv($email);
        $csv .= "\n";
        
        /* Write Header Record */
        $csv .= "*END*,,,,,,,,,";
        
        if($this->debug) $this->log("sendSingleSwapitJob: CSV $csv ",'courier_dpd_');
                 
        try {
            /* Write to csv */
            $prefix = $courier['FilePrefix'];
            $no = intval($courier['FileCount']) + 1;
            if ($no > 9999) $no = 1;
            $no = sprintf('%04d',$no);
            $filename = "{$prefix}{$no}";
            file_put_contents("reports/{$filename}.txt", $csv);
                       
            $c = ftp_connect($courier['IPAddress']);
            ftp_login($c, $courier['UserName'], $courier['Password']);
            ftp_put($c, "IN/{$filename}.txt", "reports/{$filename}.txt", FTP_ASCII);
            ftp_close($c);
                                    
            $couriers_model->updateByID(array('CourierID' => $courier['CourierID'], 'FileCount' => $no));
            
        } catch (Exception $e) {
            
            $this->log("sendSingleSwapitJob: $jId : ".$e ->getMessage(),'courier_dpd_'); 
            $this->log("sendSingleSwapitJob: CSV $csv ",'courier_dpd_');
            
            return(array(
                        'status' => 'FAIL',
                        'message' => $e
               ));
        }
               
        $recon['CourierId'] = $courier['CourierID'];
        $recon['FileNumber'] = $no;
        $recon['DateCreated'] = date('Y-m-d');
        $recon['SLNumber'] = $jId;
        $recon['ConsignmentNo'] = $shipping_record['ConsignmentNo'];
        $recon['PackagingType'] = $shipping_record['PackagingType'];
        
        $recon_model->Add($recon);
                       
        return(array(
                        'status' => 'SUCCESS',
                        'message' => 'uploaded'
               ));
        
    }
    
    /**
     * pollSwapitJobResponse
     * 
     * poll for a response from DPD.
     * 
     * 
     * @return 
     * 
     * @author brian Etherington <b.etherington@pccsuk.com>
     **************************************************************************/
    public function pollSwapitJobResponses() {
        
        $couriers_model = $this->controller->LoadModel('Couriers');
        $courier_records = $couriers_model->fetchWhere("CourierName = 'DPD'");
        foreach($courier_records as $courier_record) {
            $this->pollSwapitJobCourier( $courier_record['CourierID'], 
                                         $courier_record['IPAddress'], 
                                         $courier_record['UserName'],
                                         $courier_record['Password'] );
        }
        
        
    }
    
    /**
     * pollSwapitJobCourier
     * 
     * poll for a respnse from DPD.
     * 
     * 
     * @return 
     * 
     * @author brian Etherington <b.etherington@pccsuk.com>
     **************************************************************************/
    
    private function pollSwapitJobCourier( $courierID, $ipaddress, $username, $password ) { 
              
         try {
             
            $recon_model = $this->loadModel('Recon');
            $shipping_model = $this->loadModel('Shipping');
            $job_model = $this->loadModel('Job');
                       
            $c = ftp_connect($ipaddress);
            ftp_login($c, $username, $password);
            $files = ftp_nlist($c, './OUT');
            
            if ($files === false) {
               $this->log("pollSwapitJobResponse: $ipaddress Unable to open OUT folder",'courier_dpd_'); 
            } else {
                
                foreach($files as $file) {
                    
                    // skip already processed .old files
                    if (substr($file,-4)=='.old') continue;
                    
                    $local_file = 'reports/'.substr($file, 6);
                    $file_no = substr($file, -8, 4);
                                       
                    // try to download $server_file and save to $local_file
                    if (!ftp_get($c, $local_file, $file, FTP_BINARY)) {
                        $this->log("pollSwapitJobResponse: $ipaddress error fetching file $file",'courier_dpd_'); 
                    } else {
                        if (($handle = fopen($local_file, "r")) === FALSE) {
                            $this->log("pollSwapitJobResponse: $ipaddress error reading file $local_file",'courier_dpd_'); 
                        } else {
                            // Only delete/rename file on server if it contains jobs in skyline
                            // Note:  this means that other customers files will continue to be read by skyline
                            //        and ignored everytime this cron job runs !!!!!!!!
                            $mark_file_processed = false;
                            
                            // ignore header...
                            fgetcsv($handle);
                            while (($csv_row = fgetcsv($handle)) !== FALSE) {
                                                                                             
                                // Stop processing file of *END* row found...
                                if ($csv_row[0] == '*END*') break;
                                                               
                                $order_ref = intval($csv_row[1]);
                                $consignment_no = $csv_row[2];
                                $recon = $recon_model->Find(array(
                                                            'SLNumber' => $order_ref,
                                                            'CourierID' => intval($courierID)
                                                        ));
                                if (count($recon) == 0) {
                                    // do not mark file as process if any of the jobs it contains is in error...
                                    $mark_file_processed = false;
                                    $this->log("pollSwapitJobResponse: $ipaddress can't find RECON table row OrderRef=$order_ref CourierID=$courierID",'courier_dpd_'); 
                                } else {
                                    $recon = $recon[0];
                                    $recon['DateUpdated'] = date('Y-m-d');
                                    $recon['ConsignmentNo'] = $consignment_no;
                                    $recon_model->Update($recon);
                                    $ship_params = array( 'ConsignmentNo' => $consignment_no);
                                    $shipping_model->UpdateRows($ship_params, "JobID = $order_ref");
                                    $collectionDate = date('Y-m-d');
                                    $text = "[CD:$collectionDate CN:$consignment_no C:DPD]";
                                    $job_model->updateNotes ( $order_ref, $text );
                                    $mark_file_processed = true;
                                }

                            }
                            fclose($handle);
                            //if ($mark_file_processed) {
                                //ftp_delete($c, $file);
                                ftp_rename($c, $file, $file.'.old');
                            //}
                        }
                    }
                    
                }
                
            }
            
            ftp_close($c);
            
        } catch (Exception $e) {
            
            $this->log("pollSwapitJobResponse: ".$e ->getMessage(),'courier_dpd_'); 
            
            return(array(
                        'status' => 'FAIL',
                        'message' => $e
               ));
        }
        
        
    }
}

?>