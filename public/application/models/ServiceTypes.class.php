<?php

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');


/**
 * Description
 *
 * This class is used for handling database actions of Service Types Page in Lookup Tables section under System Admin
 *
 * @author      Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
 * @version     1.0
 */

class ServiceTypes extends CustomModel {
    
    private $conn;
    private $dbColumns = array('t1.ServiceTypeID', 't1.ServiceTypeCode', 't1.ServiceTypeName', 't1.Code',  't3.Type', 't1.Status', 't2.BrandName', 't1.BrandID');
    private $tables    = "service_type AS t1 LEFT JOIN brand AS t2 ON t1.BrandID=t2.BrandID LEFT JOIN job_type AS t3 ON t3.JobTypeID=t1.JobTypeID AND t1.BrandID=t3.BrandID";
    private $table     = "service_type";
    
      
      
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );       

    }
    
   
     /**
     * Description
     * 
     * This method is for fetching data from database
     * 
     * @param array $args Its an associative array contains where clause, limit and order etc.
     * @global $this->conn
     * @global $this->tables
     * @global $this->dbColumns
     * @return array 
     * 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */  
    public function fetch($args) {
        
       
        /*
        
        if (( isset($args['StockCodeSearchStr']) && $args['StockCodeSearchStr'] != '')  || 
                (isset($args['ModelNoSearchStr']) && $args['ModelNoSearchStr'] != '' )) {
           
            $args['where'] = '(';
            
            if ( $args['StockCodeSearchStr'] != '') {
                
                for ( $i=0 ; $i<count($columns) ; $i++ ) {
		$args['where'] .= $columns[$i].' LIKE '.$this->conn->quote( '%'.$args['StockCodeSearchStr'].'%' ).' OR ';
                }
                
            }
            else
            {
                for ( $i=0 ; $i<count($columns) ; $i++ ) {
		$args['where'] .= $columns[$i].' LIKE '.$this->conn->quote( '%'.$args['ModelNoSearchStr'].'%' ).' OR ';
                }
            }
            
            
            $args['where'] = substr_replace( $args['where'], "", -3 );
            $args['where'] .= ')';
	}
        
        */
        
       // $output = $this->ServeDataTables($this->conn, $this->tables, $this->dbColumns, $args);
        
        
        if($this->controller->user->SuperAdmin)
        {
            $output = $this->ServeDataTables($this->conn, $this->tables, $this->dbColumns, $args);
        }
        else if(is_array($this->controller->user->Brands))
        {    
            
           $brandsList  = implode(",", array_keys($this->controller->user->Brands));
           
           
           
           if($brandsList)
           {
                $brandsList .= ",".$this->controller->SkylineBrandID;
           }    
          
          
            $args['where'] = "t1.BrandID IN (".$brandsList.")";

            $output = $this->ServeDataTables($this->conn, $this->tables, $this->dbColumns, $args);
       
        }
        
        
       
        return  $output;
        
    }
    
    
     /**
     * Description
     * 
     * This method calls update method if the $args contains primary key.
     * 
     * @param array $args Its an associative array contains all elements of submitted form.
    
     * @return array It contains status and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */   
     public function processData($args) {
         
         if(!isset($args['ServiceTypeID']) || !$args['ServiceTypeID'])
         {
               return $this->create($args);
         }
         else
         {
             return $this->update($args);
         }
     }
    
     
       /**
     * Description
     * 
     * This method finds the maximum code for given brand in database table.
     * 
     * @param interger $BrandID This is primary key of brand table.
     * @global $this->table
     * @return integer It returns maximum code if it finds in the database table otherwise it returns 0.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */   
     public function getCode($BrandID) {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT ServiceTypeCode FROM '.$this->table.' WHERE BrandID=:BrandID ORDER BY ServiceTypeID DESC LIMIT 0,1';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        
        //$this->controller->log(var_export($BrandID, true));
        
        $fetchQuery->execute(array(':BrandID' => $BrandID));
        $result = $fetchQuery->fetch();
        if(isset($result[0]))
        {
           return $result[0];
        }
        else
        {
             return 0;
        }
       
    }
    
    /**
     * Description
     * 
     * This method is used for to validate action code for given brand.
     *
     * @param interger $ServiceTypeCode  
     * @param interger $BrandID.
     * @param interger $ServiceTypeID.
     * @global $this->table
     * 
     * @return boolean.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function isValidAction($ServiceTypeCode, $BrandID, $ServiceTypeID) {
        
         /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT ServiceTypeID FROM '.$this->table.' WHERE ServiceTypeCode=:ServiceTypeCode AND BrandID=:BrandID AND ServiceTypeID!=:ServiceTypeID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $fetchQuery->execute(array(':ServiceTypeCode' => $ServiceTypeCode, ':BrandID' => $BrandID, ':ServiceTypeID' => $ServiceTypeID));
        $result = $fetchQuery->fetch();
        
        if(is_array($result) && $result['ServiceTypeID'])
        {
                return false;
        }
        
        return true;
    
    }
    
    
    /**
     * Description
     * 
     * This method is used for to insert data into database.
     *
     * @param array $args  
     * @global $this->table 
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function create($args) {
        
        $fields = explode(', ', 'ServiceTypeName, ServiceTypeCode, Code, JobTypeID, Priority, Status, BrandID');
        $fields = array_combine($fields , $fields);
        
        $sql = TableFactory::ServiceType()->insertCommand($fields);        
        
        /* Execute a prepared statement by passing an array of values */
//        $sql = 'INSERT INTO '.$this->table.' (ServiceTypeName, ServiceTypeCode, Code, JobTypeID, Status, BrandID)
//            VALUES(:ServiceTypeName, :ServiceTypeCode, :Code, :JobTypeID, :Status, :BrandID)';
        
        if(!isset($args['ServiceTypeCode']) || !$args['ServiceTypeCode'])
        {
            $args['ServiceTypeCode'] = $this->getCode($args['BrandID'])+1;//Preparing next name code.
        }
       // $this->controller->log(var_export('tessss', true));
        
        if($this->isValidAction($args['ServiceTypeCode'], $args['BrandID'], 0))
        {
            $insertQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
          
            
            $insertQuery->execute(array(':ServiceTypeName' => $args['ServiceTypeName'], ':ServiceTypeCode' => $args['ServiceTypeCode'], ':Code' => $args['Code'], ':JobTypeID' => $args['JobTypeID'], ':Priority' => $args['Priority'], ':Status' => $args['Status'], ':BrandID' => $args['BrandID']));
        
        
              return array('status' => 'OK',
                        'message' => $this->controller->page['data_inserted_msg']);
        }
         else
        {
            
            return array('status' => 'ERROR',
                        'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
        }
    }
    
    
    /**
     * Description
     * 
     * This method is used for to fetch a row from database.
     *
     * @param array $args
     * @global $this->table  
     * @return array It contains row of the given primary key.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    
    public function fetchRow($args) {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT ServiceTypeID, ServiceTypeCode, ServiceTypeName, Code, JobTypeID, Priority, Status, BrandID FROM '.$this->table.' WHERE ServiceTypeID=:ServiceTypeID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        
        $fetchQuery->execute(array(':ServiceTypeID' => $args['ServiceTypeID']));
        $result = $fetchQuery->fetch();
        
        return $result;
    }
    
    
    
    
    
    /**
     * Description
     * 
     * This method is used for to service type name (mask name if exists).
     *
     * @param int $NetworkID
     * @param int $ClientID
     * @param int $ServiceTypeID  
     * 
     * @global $this->table  
     * @return array It contains row of the given primary key.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    
    public function fetchMask($NetworkID, $ClientID, $ServiceTypeID) {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT T1.ServiceTypeID, T1.ServiceTypeName, T2.ServiceTypeMask  FROM '.$this->table.' AS T1 LEFT JOIN service_type_alias AS T2 ON T1.ServiceTypeID=T2.ServiceTypeID WHERE T1.Status=:Status AND T1.ServiceTypeID=:ServiceTypeID AND T2.NetworkID=:NetworkID AND T2.ClientID=:ClientID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        
        $fetchQuery->execute(array(':Status' => 'Active', ':ServiceTypeID' => $ServiceTypeID, ':ClientID' => $ClientID, ':NetworkID' => $NetworkID));
        $result = $fetchQuery->fetch();
        
        $stName = (isset($result['ServiceTypeName']))?$result['ServiceTypeName']:'';
        
        if(isset($result['ServiceTypeMask']) && $result['ServiceTypeMask'])
        {
           $maskDetails  = $this->fetchRow(array('ServiceTypeID'=>$result['ServiceTypeMask']));
           
           $stName = (isset($maskDetails['ServiceTypeName']))?$maskDetails['ServiceTypeName']:$stName; 
        }    
        
        return $stName;
    }
    
    
     
     /**
     * Description
     * 
     * This method is used for to udpate a row into database.
     *
     * @param array $args
     * @global $this->table   
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function update($args) {
        
        if($this->isValidAction($args['ServiceTypeCode'], $args['BrandID'], $args['ServiceTypeID']))
        {        
            /* Execute a prepared statement by passing an array of values */
            $sql = 'UPDATE '.$this->table.' SET ServiceTypeName=:ServiceTypeName, ServiceTypeCode=:ServiceTypeCode, Code=:Code, JobTypeID=:JobTypeID, Priority=:Priority, Status=:Status, BrandID=:BrandID
            WHERE ServiceTypeID=:ServiceTypeID';
        
              $updateQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
              $updateQuery->execute(array(':ServiceTypeName' => $args['ServiceTypeName'], ':ServiceTypeCode' => $args['ServiceTypeCode'], ':Code' => $args['Code'], ':JobTypeID' => $args['JobTypeID'], ':Priority' => $args['Priority'], ':Status' => $args['Status'], ':BrandID' => $args['BrandID'], ':ServiceTypeID' => $args['ServiceTypeID']));
        
                
               return array('status' => 'OK',
                        'message' => $this->controller->page['data_updated_msg']);
        }
        else
        {
             return array('status' => 'ERROR',
                        'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
        }
    }
    
    public function delete(/*$args*/) {
        return array('status' => 'OK',
                     'message' => $this->controller->page['data_deleted_msg']);
    }
    
    /**
     * getServiceTypeIdJobTyeId
     * 
     * This method is used to get a ServiceTypeID and a JobTypeID from a Service
     * Type Code (such as W, C, T , I etc)
     * The API used this when importing a job from ServiceBase 
     *
     * @param char $code   Service Type Code
     *   
     * @return array    Containing ServiceTypeID and JobTypeID
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>
     * 
     **************************************************************************/ 
    public function getServiceTypeIdJobTyeId($code) {
        
         $sql = "
                SELECT
			`ServiceTypeID`,
			`JobTypeID`
		FROM
			`$this->table`
		WHERE
			`Code` = '$code'
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return($result[0]);                                                 /* Customer exists */
        } else {
            return(                                                             /* Not found return null */
                   array(
                         'ServiceTypeID' => null,
                         'JobTypeID' => null
                        ) 
                  );                                                       
        }
    }
    
    /**
     * getServiceTypeIdJobTypeIdFromBoth
     * 
     * This method is used to get a ServiceTypeID and a JobTypeID from a Service
     * Type Code (such as W, C, T , I etc) and a job type passed a string to match
     * the specific  Service Typoe code.
     * 
     * The API uses this when importing a job from ServiceBase, and is a change 
     * in hanndling the service type as requested in TraclerBase VMS log 29
     *
     * @param char $code   Service Type Code
     *   
     * @return array    Containing ServiceTypeID and JobTypeID
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>
     * 
     **************************************************************************/ 
    public function getServiceTypeIdJobTypeIdFromBoth($code, $type) {
        
        $sql = "
                SELECT
			st.`ServiceTypeID`,
			st.`JobTypeID`
		FROM
			`job_type` jt,
			`service_type` st
		WHERE
			st.`Code` = '$code'
			AND jt.`Type` = '$type'
			AND st.`JobTypeID` = jt.`JobTypeID`
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return($result[0]);                                                 /* Customer exists */
        } else {
            return(                                                             /* Not found return null */
                   array(
                         'ServiceTypeID' => null,
                         'JobTypeID' => null
                        ) 
                  );                                                       
        }
    }
    
    /**
     * getServiceTypeIdJobTypeIdFromName
     * 
     * This method is used to get a ServiceTypeID and a JobTypeID from a Service
     * Type name (eg Warranty
     * The API used this when importing a job from Samsung
     *
     * @param string $code   Service Type Name
     *   
     * @return array    Containing ServiceTypeID and JobTypeID
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>
     * 
     **************************************************************************/ 
    public function getServiceTypeIdJobTypeIdFromName($stName) {
        
         $sql = "
                SELECT
			`ServiceTypeID`,
			`JobTypeID`
		FROM
			`$this->table`
		WHERE
			`ServiceTypeName` = '$stName'
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return($result[0]);                                                 /* Customer exists */
        } else {
            return(                                                             /* Not found return null */
                   array(
                         'ServiceTypeID' => null,
                         'JobTypeID' => null
                        ) 
                  );                                                       
        }
    }
    
}
?>