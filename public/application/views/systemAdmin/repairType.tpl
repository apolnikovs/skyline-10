{extends "DemoLayout.tpl"}
{block name=config}
    {$Title = $page['Text']['page_title']|escape:'html'}
    {$PageId = $RepairTypePage}
    {$fullscreen=true}
{/block}
{block name=afterJqueryUI}
    <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script>
    <script type="text/javascript" src="{$_subdomain}/js/TableTools.min.js"></script>
    <script type="text/javascript" src="{$_subdomain}/js/datatables.api.js"></script>
    <script type="text/javascript" src="{$_subdomain}/js/ColReorder.js"></script>
    <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" media="screen" charset="utf-8" />
    <style type="text/css" >
        .ui-combobox-input {
            width:280px;
        }
    </style>
{/block}
{block name=scripts}
<script type="text/javascript">
    var $statuses = [
        {foreach from=$statuses item=st}
            ["{$st.Name}", "{$st.Code}"],
        {/foreach}
    ];
    function gotoEditPage($sRow)
    {         
        $('#updateButtonId').removeAttr('disabled').removeClass('gplus-blue-disabled').addClass('gplus-blue');
        $('#updateButtonId').trigger('click');
    }
    function showTablePreferences(){
        $.colorbox({
            href:"{$_subdomain}/LookupTables/tableDisplayPreferenceSetup/page=repairTypes/table=repair_type",
            title: "Table Display Preferences",
            opacity: 0.75,
            overlayClose: false,
            escKey: false
        });
    }  
    $(document).ready(function() {
        $("#manufacturerFilter").combobox({
            change: function() {
                $(location).attr('href', '{$_subdomain}/LookupTables/repairTypes/'+urlencode($("#serviceProviderFilter").val())+'/'+urlencode($("#manufacturerFilter").val()));
            }
        });
        $("#serviceProviderFilter").combobox({
            change: function() {
                $(location).attr('href', '{$_subdomain}/LookupTables/repairTypes/'+urlencode($("#serviceProviderFilter").val()));
            }
        });
        var displayButtons = "UAD";
	$('#RepairTypeResults').PCCSDataTable( {
	    aoColumns: [ 
		/* RepairTypeID */{ bVisible:   false },
		/* RepairType */null,
		/* ManufacturerID */null,
		/* JobWeighting */null,
		/* RepairIndex */null,
		/* Chargeable */null,
                /* Warranty */null,
		/* WarrantyCode */null,
		/* CompulsoryFaultCoding */null,
		/* ExcludeFromEDI */null,
		/* ForceAdjustmentIfNoPartsUsed */null,
		/* ExcludeFromInvoicing */null,
                /* PromptForExchange */null,
		/* NoParts */null,
		/* ScrapExchange */null,
		/* ExcludeRRCHandlingFee */null,
		/* Unavailable */null,
		/* Type */null,
                /* Status */null,
              { "mData" : null, "bSortable": false, "sWidth" : "20px",
                            "mRender": function ( data, type, full ) {
                            return '<input class="taggedRec" type="checkbox" onclick="countTagged()" value="'+full.CountyID+'" name="check'+full.CountyID+'">';
                             }
                             }
	    ],
	    displayButtons:	displayButtons,
            bServerSide: false,
	    addButtonId:	'addButtonId',
	    addButtonText:	'{$page['Buttons']['insert']|escape:'html'}',
	    createFormTitle:	'{$page['Text']['insert_page_legend']|escape:'html'}',
	    createAppUrl:	'{$_subdomain}/LookupTables/repairTypes/insert/',
	    createDataUrl:	'{$_subdomain}/LookupTables/ProcessData/RepairTypes/',
	    formInsertButton:	'insert_save_btn',
	    deleteButtonId:	"deleteButtonID",
            aaSorting: [[ 1, "asc" ]],
	    deleteAppUrl:	'{$_subdomain}/LookupTables/repairTypes/deleteForm/',
	    deleteDataUrl:	'{$_subdomain}/LookupTables/ProcessData/RepairTypes/delete/',
            formDeleteButton:	'delete_save_btn',
	    frmErrorRules: {
		RepairType: { required: true },
                {if $SupderAdmin eq true}
                    ServiceProviderID: { required: true },
                {/if}
		ManufacturerID: { required: true }
	    },
	    frmErrorMessages: {
		RepairType: { required: "{$page['Errors']['repair_type_error']|escape:'html'}" },
                {if $SupderAdmin eq true}
                    ServiceProviderID: { required: "{$page['Errors']['serviceProvider_error']|escape:'html'}" },
                {/if}
		ManufacturerID: { required: "{$page['Errors']['manufacturer_error']|escape:'html'}" }
                
	    },
	    popUpFormWidth:	715,
	    popUpFormHeight:	430,
	    updateButtonId:	'updateButtonId',
	    updateButtonText:	'{$page['Buttons']['edit']|escape:'html'}',
	    updateFormTitle:	'{$page['Text']['update_page_legend']|escape:'html'}',
	    updateAppUrl:	'{$_subdomain}/LookupTables/repairTypes/update/',
	    updateDataUrl:	'{$_subdomain}/LookupTables/ProcessData/RepairTypes/',
	    formUpdateButton:	'update_save_btn',
	    colorboxFormId:	"CForm",
	    frmErrorMsgClass:	"fieldError",
	    frmErrorElement:	"label",
	    htmlTablePageId:	'RepairTypeResultsPanel',
	    htmlTableId:	'RepairTypeResults',
	    fetchDataUrl:	'{$_subdomain}/LookupTables/ProcessData/RepairTypes/fetch/'+urlencode("{$sId}")+'/'+urlencode("{$mId}"),
	    formCancelButton:	'cancel_btn',
	    dblclickCallbackMethod: 'gotoEditPage',
	    //fnRowCallback:      'inactiveRow',
	    searchCloseImage:	'{$_subdomain}/css/Skins/{$_theme}/images/close.png',
	    tooltipTitle:	"{$page['Text']['tooltip_title']|escape:'html'}",
	    iDisplayLength:	25,
	    formDataErrorMsgId: "suggestText",
	    frmErrorSugMsgClass:"formCommonError"
	});
	{* Updated By Praveen Kumar N : Jquery Inactive Function to send the inactive status [START] *}
	$('input[id^=inactivetick]').click( function() {
                     
		    if ($('#inactivetick').is(':checked')) {
			
			    window.location="{$_subdomain}/LookupTables/repairTypes/"+urlencode("{$sId}")+'/'+"In-active";
		     } else {
			    window.location="{$_subdomain}/LookupTables/repairTypes/";
		     }		
                 
	} );	
	
    {* [END] *}
    });
    </script>
{/block}
{block name=body}
<div style="float:right">
         <a href="#" onclick="showTablePreferences();">Display Preferences</a>
            </div>
    <div class="breadcrumb" style="width:100%">
        <div>
            <a href="{$_subdomain}/SystemAdmin" >{$page['Text']['system_admin_home_page']|escape:'html'}</a> / <a href="{$_subdomain}/SystemAdmin/index/lookupTables" >{$page['Text']['lookup_tables']|escape:'html'}</a> / {$page['Text']['repair_types']|escape:'html'}
        </div>
    </div>
    <div class="main" id="home" style="width:100%">
               <div class="ServiceAdminTopPanel" >
                        <fieldset>
                        <legend title="">{$page['Text']['repair_types']|escape:'html'}</legend>
                        <p>
                            <label>{$page['Text']['repair_types_short_description']|escape:'html'}</label>
                        </p>
                        </fieldset>
                </div>
                  <div class="ServiceAdminResultsPanel" id="RepairTypeResultsPanel" >
                      <form id="nIdForm" class="nidCorrections">
                     {if $SupderAdmin eq true}
                        {$page['Labels']['serviceProvider_label']|escape:'html'}
                        <select id="serviceProviderFilter" name="serviceProviderFilter">
                            
                            <option selected="selected">All Service Providers</option>
                            {foreach $splist as $s}
                                <option value="{$s.ServiceProviderID}" {if $sId eq $s.ServiceProviderID}selected="selected"{/if}>{$s.Acronym|default:''}</option>
                            {/foreach}
                        </select>
                        {/if}
                        {$page['Labels']['manufacturer_label']|escape:'html'}
                        <select id="manufacturerFilter" name="manufacturerFilter">
                        <option selected="selected">All Manufacturer</option>
                            {foreach $manufacturerList as $s}
                                <option value="{$s.ManufacturerID}" {if $mId eq $s.ManufacturerID}selected="selected"{/if}>{$s.ManufacturerName|default:''}</option>
                            {/foreach}
                        </select>
                    </form>
                    <table id="RepairTypeResults" border="0" cellpadding="0" cellspacing="0" class="browse" >
                        <thead>
                            <tr>
                                <th title="{$page['Text']['repairtype_id']|escape:'html'}" >{$page['Text']['repairtype_id']|escape:'html'}</th>
                                <th title="{$page['Text']['repair_type']|escape:'html'}" >{$page['Text']['repair_type']|escape:'html'}</th>
                                <th title="{$page['Text']['manufacturer_id']|escape:'html'}"  >{$page['Text']['manufacturer_id']|escape:'html'}</th>
                                <th title="{$page['Text']['job_weighting']|escape:'html'}" >{$page['Text']['job_weighting']|escape:'html'}</th>
                                <th title="{$page['Text']['repair_index']|escape:'html'}" >{$page['Text']['repair_index']|escape:'html'}</th>
                                <th title="{$page['Text']['chargeable']|escape:'html'}" >{$page['Text']['chargeable']|escape:'html'}</th>
                                <th title="{$page['Text']['warranty']|escape:'html'}" >{$page['Text']['warranty']|escape:'html'}</th>
                                <th title="{$page['Text']['warranty_code']|escape:'html'}" >{$page['Text']['warranty_code']|escape:'html'}</th>
                                <th title="{$page['Text']['compulsory_fault_coding']|escape:'html'}"  >{$page['Text']['compulsory_fault_coding']|escape:'html'}</th>
                                <th title="{$page['Text']['exclude_from_EDI']|escape:'html'}" >{$page['Text']['exclude_from_EDI']|escape:'html'}</th>
                                <th title="{$page['Text']['force_adjustment_ifno_partsused']|escape:'html'}" >{$page['Text']['force_adjustment_ifno_partsused']|escape:'html'}</th>
                                <th title="{$page['Text']['exclude_from_invoicing']|escape:'html'}" >{$page['Text']['exclude_from_invoicing']|escape:'html'}</th>
                                <th title="{$page['Text']['prompt_for_exchange']|escape:'html'}" >{$page['Text']['prompt_for_exchange']|escape:'html'}</th>
                                <th title="{$page['Text']['no_parts']|escape:'html'}" >{$page['Text']['no_parts']|escape:'html'}</th>
                                <th title="{$page['Text']['scrap_exchange']|escape:'html'}"  >{$page['Text']['scrap_exchange']|escape:'html'}</th>
                                <th title="{$page['Text']['exclude_rrchandling_fee']|escape:'html'}" >{$page['Text']['exclude_rrchandling_fee']|escape:'html'}</th>
                                <th title="{$page['Text']['unavailable']|escape:'html'}" >{$page['Text']['unavailable']|escape:'html'}</th>
                                <th title="{$page['Text']['type']|escape:'html'}" >{$page['Text']['type']|escape:'html'}</th>
                                <th title="{$page['Text']['status']|escape:'html'}" >{$page['Text']['status']|escape:'html'}</th>
                                <th align="center"> &nbsp; <input title="" type='checkbox' value=0 id='check_all' /> </th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
			      {* Updated By Praveen Kumar N : Show Inactive Tick Box to display Inactive Records [START] *}
     <div style="width:100%;text-align: right"><input id="inactivetick"  type="checkbox"  value="In-active" {if $dataStatus eq 'In-active'} checked=checked {/if} > Show Inactive&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
	  {* [END] *}
		  </div>
	  
			    <div class="bottomButtonsPanel">
                    <button class="gplus-blue" style="float:left;" type="button" onclick="window.location='{$_subdomain}/SystemAdmin/index/lookupTables'">{$page['Buttons']['rep_btn']|escape:'html'}</button>
                </div>
		<p>&nbsp;</p>	    
                <div class="bottomButtonsPanel">
                    <hr><button class="gplus-blue" style="float:right" type="button" onclick="window.location='{$_subdomain}/SystemAdmin/index/lookupTables'">{$page['Buttons']['finish']|escape:'html'}</button>
                </div>
    </div>
{/block}
