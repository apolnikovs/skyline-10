{extends "DemoLayout.tpl"}

{block name=config}
{$Title = "Homepage"}
{$PageId = $LoginPage}
{/block}

{block name=scripts}
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.jlabel-1.3.min.js"></script>*} 

<script type="text/javascript">

$(document).ready(function(){

    $('input[type=password]').jLabel().attr('autocomplete','off').blur(function() {  $('#error').hide('slow'); } );
    $('#pwd').focus();

    /* =======================================================
     *
     * set tab on return for input elements with form submit on auto-submit class...
     *
     * ======================================================= */
     
    $('input[type=text],input[type=password]').keypress( function( e ) {
            if (e.which == 13) {  
                $(this).blur();
                if ($(this).hasClass('auto-submit')) {
                    if (validateForm()) $(this).get(0).form.submit();
                } else {
                    $next = $(this).attr('tabIndex') + 1;
                    $('[tabIndex="'+$next+'"]').focus();
                }
                return false;
            }   
        } );
    
});

function validateForm() {
    if ($('#pwd').val()=='') {        
        $('#error').html("{$page['Errors']['password']}").show('slow');
        $('#pwd').focus();
        return false;
    }
    if ($('#pwd2').val()=='') {        
        $('#error').html("{$page['Errors']['confirm']}").show('slow');
        $('#pwd2').focus();
        return false;
    }
    if ($('#pwd').val() != $('#pwd2').val()) {
        $('#error').html("{$page['Errors']['not_same']}").show('slow');
        $('#pwd2').focus();
        return false;
    }
    return true;
}
</script>

{/block}

{block name=body}
<div class="main" id="resetPassword">
    
        <form id="resetPasswordForm" name="resetPasswordForm" method="post" action="{$_subdomain}/Login/resetPassword" class="prepend-5 span-14 last">
            
            <input type="hidden" name="branch" value="{$branch|escape:'html'}" />
            <input type="hidden" name="username" value="{$username|escape:'html'}" />
            
            <fieldset>

                <legend title="IM5003">{$page['Text']['legend']|escape:'html'}</legend>
                {* <div class="serviceInstructionsBar">IM5003</div> *}
                
                <p class="information" style="text-align: center; margin-bottom: 10px;">
                    {$page['Text']['please_reset']|escape:'html'}
                </p>                                                  
                               
                <p style="text-align: center;">
                    <label for="pwd">{$page['Labels']['pwd']|escape:'html'}</label>
                    <input type="password" name="pwd" id="pwd" value="" class="text" tabIndex="1" />
                </p>
                
                 <p style="text-align: center;">
                    <label for="pwd2">{$page['Labels']['pwd2']|escape:'html'}</label>
                    <input type="password" name="pwd2" id="pwd2" value="" class="text auto-submit" tabIndex="2" />
                </p>               
                
                <p style="text-align: center; margin: 0; ">
                    <span style="display: inline-block; width: 312px; text-align: center; ">
                        <a href="javascript:if (validateForm()) document.resetPasswordForm.submit();" tabIndex="3">{$page['Buttons']['submit']|escape:'html'}</a>
                    </span>
                </p>
		               
                {if $error eq ''}
                <p id="error" class="formError" style="display: none; text-align: center;" />
                {else}
                <p id="error" class="formError" style="text-align: center;" >{$error|escape:'html'}</p>
                {/if}
                
                <p style="text-align: right;">
                    <a href="{$_subdomain}/index/index" tabIndex="4">{$page['Buttons']['cancel']|escape:'html'}</a>
                </p>
                              
            </fieldset>
  
        </form>
    
</div>
{/block}