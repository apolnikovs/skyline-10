<div class="f_label">
    <label for="postSelect" >Select Address: *</label>
</div>
{if $addresses eq false}
<div class="f_item" >
    <span>No Address Found</span>
</div>
{else}       
 <div class="f_item selectBox" >
    <select name="postSelect" id="postSelect">
        <option value="">--Select--</option>  
        {foreach $addresses as $row}
        {if $row[4] ne ''} {* Business Address *}
            {if $row[6] eq ''} {* House Name *}
                {if {$row[5]|capitalize} eq ''} {* House Number *}
                <option value="{$row[4]|capitalize},{$row[0]|capitalize},{$row[3]|capitalize}">{$row[4]|capitalize} {$row[0]|capitalize}</option>
                {else}
                <option value="{$row[4]|capitalize},{$row[5]|capitalize} {$row[0]|capitalize},{$row[3]|capitalize}">{$row[4]|capitalize} {$row[5]|capitalize} {$row[0]|capitalize}</option>
                {/if}
            {else}
                {if {$row[5]|capitalize} eq ''} {* House Number *}
                 <option value="{$row[4]|capitalize} {$row[6]|capitalize},{$row[0]|capitalize},{$row[3]|capitalize}">{$row[4]|capitalize} {$row[6]|capitalize} {$row[0]|capitalize}</option>
                {else}
                <option value="{$row[4]|capitalize} {$row[6]|capitalize},{$row[5]|capitalize} {$row[0]|capitalize},{$row[3]|capitalize}">{$row[4]|capitalize} {$row[6]|capitalize} {$row[5]|capitalize} {$row[0]|capitalize}</option>
                {/if}
            {/if}
        {elseif $row[6] ne ''}
            {if {$row[5]|capitalize} eq ''} {* House Number *}
             <option value="{$row[6]|capitalize},{$row[0]|capitalize},{$row[3]|capitalize}">{$row[6]|capitalize} {$row[0]|capitalize}</option>
            {else}
            <option value="{$row[6]|capitalize},{$row[5]|capitalize} {$row[0]|capitalize},{$row[3]|capitalize}">{$row[6]|capitalize} {$row[5]|capitalize} {$row[0]|capitalize}</option>
            {/if}
        {else}
        <option value="{$row[5]|capitalize},{$row[0]|capitalize},{$row[3]|capitalize}">{$row[5]|capitalize} {$row[0]|capitalize}</option>
        {/if}
        {/foreach}
    </select>
</div>       
{/if}
<div class="clear"></div>