{if $accessErrorFlag eq true} 
    
    <div id="accessDeniedMsg" class="SystemAdminFormPanel"  >   
    
        <form id="accessDeniedMsgForm" name="accessDeniedMsgForm" method="post"  action="#" class="inline">
                <fieldset>
                    <legend title="" > {$page['Text']['error_page_legend']|escape:'html'} </legend>
                    <p>

                        <label class="formCommonError" >{$accessDeniedMsg|escape:'html'}</label><br><br>

                    </p>

                    <p>

                        <span class= "bottomButtons" >


                            <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                        </span>

                    </p>


                </fieldset>   
        </form>            


    </div>    
    
{else}  
    
    <script type="text/javascript" >
        
        function addFields(selected) {	
                    var selected_split = selected.split(","); 	


                    $("#BuildingNameNumber").val(selected_split[0]).blur();	
                    $("#Street").val(selected_split[1]).blur();
                    $("#LocalArea").val(selected_split[2]).blur();
                    $("#TownCity").val(selected_split[3]).blur();
                    $("#selectOutput").slideUp("slow"); 

                }
                
       function addAddressFields(selected, addFlag) {	
                    var selected_split = selected.split(","); 	

                    if(addFlag=="2")
                        {
                            $("#InvoicedBuilding").val(selected_split[0]).blur();	
                            $("#InvoicedStreet").val(selected_split[1]).blur();
                            $("#InvoicedArea").val(selected_split[2]).blur();
                            $("#InvoicedTownCity").val(selected_split[3]).blur();
                            $("#selectOutput2").slideUp("slow"); 
                        }        
                }         
    function setCountryBasedOnCounty($countyId, $countryId, $parentCountryId)
    {
        $($countyId+" option:selected").each(function () {
            $selected_cc_id =  $($countyId+" option:selected").attr('id');
            $country_id_array = $selected_cc_id.split('_');
            if($country_id_array[1]!='0')
            {
                $($countryId).val($country_id_array[1]);
                setValue($countryId);
            }
            else
            {
                 $($parentCountryId+" input:first").val('');
            }
        });
    }    
    $(document).ready(function() {
        $("#NetworkID").combobox();
        $("#CountyID").combobox({
            change: function() {
                setCountryBasedOnCounty("#CountyID", "#CountryID", "#P_CountryID");
            }
        });
        $("#CountryID").combobox({
            change: function() {
                var options = '';
                var selectedCountryRates = vat_rate_array[$('#CountryID').val()];
                $('#VATRateID').empty()
                $('#VATRateID').append('<option value="">{$page['Text']['select_default_option']|escape:'html'}</option>');
                for (var n = 0; n < selectedCountryRates.length; n++) {
                    if (selectedCountryRates[n] !== undefined) {
                    options += '<option value="' + n + '">' + selectedCountryRates[n] + '</option>';
                    }
                }
                $('#VATRateID').append(options);
            }
        });
        $("#ServiceProviderID").combobox();
        $("#VATRateID").combobox();
             function autoHint()
             {
                 $('.auto-hint').each(function() {
                                    $this = $(this);
                                    if ($this.val() == $this.attr('title')) {
                                        $this.val('').removeClass('auto-hint').addClass('auto-hint-hide');
                                        if ($this.hasClass('auto-pwd')) {
                                            $this.prop('type','password');
                                        }
                                    }
                        } );  
             }
             
             
             
            $(document).on('click', '#AutomaticallyCreateBranch', 
                                function() {
                                
                                                       
                    if($('#AutomaticallyCreateBranch').is(':checked') && !$('#AutomaticallyCreateBrand').is(':checked'))
                    {

                        $('#ValidCreateBranch').val('1');
                        alert("{$page['Errors']['auto_create_branch']|escape:'html'}");
                    }
                    else
                    {

                        $('#ValidCreateBranch').val('');
                    }

             });
             
             
             
             
             $(document).on('click', '#insert_save_btn', 
                                function() {
                                
                                autoHint();
                
               });
               
             $(document).on('click', '#update_save_btn', 
                                function() {
                                
                                autoHint();
                
               });   
                
                
                
            /* =======================================================
            *
            * Initialise input auto-hint functions...
            *
            * ======================================================= */

            $('.auto-hint').focus(function() {
                    $this = $(this);
                    if ($this.val() == $this.attr('title')) {
                        $this.val('').removeClass('auto-hint');
                        if ($this.hasClass('auto-pwd')) {
                            $this.prop('type','password');
                        }
                    }
                } ).blur(function() {
                    $this = $(this);
                    if ($this.val() == '' && $this.attr('title') != '')  {
                        $this.val($this.attr('title')).addClass('auto-hint');
                        if ($this.hasClass('auto-pwd')) {
                            $this.prop('type','text');
                        }
                    }         
                } ).each(function(){
                    $this = $(this);
                    if ($this.attr('title') == '') { return; }
                    if ($this.val() == '') { 
                        if ($this.attr('type') == 'password') {
                            $this.addClass('auto-pwd').prop('type','text');
                        }
                        $this.val($this.attr('title')); 
                    } else { 
                        $this.removeClass('auto-hint'); 
                    }
                    $this.attr('autocomplete','off');
                } ); 
                
                
                
                
         });       
        
    </script>
    
    
    <div id="ClientsFormPanel" class="SystemAdminFormPanel" >
    
                <form id="ClientsForm" name="ClientsForm" method="post"  action="#" class="inline">
       
                <fieldset>
                    <legend title="" >{$form_legend|escape:'html'}</legend>
                        
                    <p><label id="suggestText" ></label></p>
                            
                            <p>
                                <label ></label>
                                <span class="topText" >{$page['Text']['top_info_text1']|escape:'html'} <span>*</span> {$page['Text']['top_info_text2']|escape:'html'}</span>
                            </p>
                       
                         
                         
                          <p>
                            <label class="fieldLabel" for="NetworkID" >{$page['Labels']['service_network']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp;  
                             <select name="NetworkID" id="NetworkID" class="text" >
                                <option value="" {if $datarow.NetworkID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                {foreach $networks as $network}

                                    <option value="{$network.NetworkID}" {if $datarow.NetworkID eq $network.NetworkID}selected="selected"{/if}>{$network.CompanyName|escape:'html'}</option>

                                {/foreach}
                            </select>
                          </p>
                            
                            
                           
                            
                         <p>
                            <label class="fieldLabel" for="ClientName" >{$page['Labels']['company_name']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp; <input  type="text" class="text"  name="ClientName" value="{$datarow.ClientName|escape:'html'}" id="ClientName" >
                        
                         </p>
                         
                         
                        <p>
                            <label class="fieldLabel" for="PostalCode">{$page['Labels']['postcode']|escape:'html'}:<sup>*</sup></label>
                            &nbsp;&nbsp; <input  class="text uppercaseText" style="width: 140px;" type="text" name="PostalCode" id="PostalCode" value="{$datarow.PostalCode|escape:'html'}" >&nbsp;

                            <input type="submit" name="quick_find_btn" id="quick_find_btn" class="textSubmitButton postCodeLookUpBtn"  onclick="return false;"   value="Click to find Address" >

                        </p>

                        <p id="selectOutput" ></p>


                        <p>
                            <label class="fieldLabel" for="BuildingNameNumber" >{$page['Labels']['building_name']|escape:'html'}:</label>
                            &nbsp;&nbsp; <input type="text" class="text"  name="BuildingNameNumber" value="{$datarow.BuildingNameNumber|escape:'html'}" id="BuildingNameNumber" >          
                        </p>

                        <p>
                            <label class="fieldLabel" for="Street" >{$page['Labels']['street']|escape:'html'}:<sup>*</sup></label>
                            &nbsp;&nbsp; <input type="text" class="text"  name="Street" value="{$datarow.Street|escape:'html'}" id="Street" >          
                        </p>

                        <p>
                            <label class="fieldLabel" for="LocalArea" >{$page['Labels']['area']|escape:'html'}:</label>
                            &nbsp;&nbsp; <input type="text" class="text" name="LocalArea" value="{$datarow.LocalArea|escape:'html'}" id="LocalArea" >          
                        </p>


                        <p>
                            <label class="fieldLabel" for="TownCity" >{$page['Labels']['city']|escape:'html'}:<sup>*</sup></label>
                            &nbsp;&nbsp; <input type="text" class="text" name="TownCity" value="{$datarow.TownCity|escape:'html'}" id="TownCity" >          
                        </p>

                        <p>
                            <label class="fieldLabel" for="CountyID" >{$page['Labels']['county']|escape:'html'}:</label>
                            &nbsp;&nbsp; 


                            <select name="CountyID" id="CountyID" class="text" >
                                <option value="" id="country_0_county_0" {if $datarow.CountyID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                {foreach $countries as $country}
                                    {foreach $country.Counties as $county}
                                    <option value="{$county.CountyID}"  id="country_{$country.CountryID}_county_{$county.CountyID}" {if $datarow.CountyID eq $county.CountyID}selected="selected"{/if}>{$county.Name|escape:'html'}</option>
                                    {/foreach}
                                {/foreach}

                            </select>

                        </p>
                         
                        
                        
                         <p id="P_CountryID">
                            <label class="fieldLabel" for="CountryID" >{$page['Labels']['country']|escape:'html'}:<sup>*</sup></label>
                            &nbsp;&nbsp; 
                                <select name="CountryID" id="CountryID" class="text" >
                                <option value="" {if $datarow.CountryID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                {foreach $countries as $country}

                                    <option value="{$country.CountryID}" {if $datarow.CountryID eq $country.CountryID}selected="selected"{/if}>{$country.Name|escape:'html'}</option>

                                {/foreach}

                            </select>
                        </p>
                        
                         
                        <p>
                                <label class="fieldLabel" for="ContactEmail">{$page['Labels']['email']|escape:'html'}:<sup>*</sup></label>
                                &nbsp;&nbsp; <input  type="text" name="ContactEmail" class="text auto-hint" title="{$page['Text']['email']|escape:'html'}" value="{$datarow.ContactEmail|escape:'html'}" id="ContactEmail" >
                                <br>
                        </p>

                        <p>
                            <label class="fieldLabel" for="ContactPhone" >{$page['Labels']['phone']|escape:'html'}:<sup>*</sup></label>
                            &nbsp;&nbsp; <input  type="text" class="text phoneField"  name="ContactPhone" value="{$datarow.ContactPhone|escape:'html'}" id="ContactPhone" >
                            <input  type="text" class="text extField auto-hint" title="{$page['Text']['extension_no']|escape:'html'}" name="ContactPhoneExt" value="{$datarow.ContactPhoneExt|escape:'html'}" id="ContactPhoneExt" >

                        </p>
                        
                        
                        
                         <p>
                            <hr>
                         </p>
                        
                        
                         
                         <p>
                            <label class="fieldLabel" for="AccountNumber" >{$page['Labels']['account_number']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp; <input  type="text" class="text"  name="AccountNumber" value="{$datarow.AccountNumber|escape:'html'}" id="AccountNumber" >
                        
                         </p>
                         
                         
                         <p>
                            <label class="fieldLabel" for="GroupName" >{$page['Labels']['group_name']|escape:'html'}:</label>
                           
                             &nbsp;&nbsp; <input  type="text" class="text"  name="GroupName" value="{$datarow.GroupName|escape:'html'}" id="GroupName" >
                        
                         </p>
                         
                         <p>
                            <label class="fieldLabel" for="ServiceProviderID" >{$page['Labels']['service_provider']|escape:'html'}:</label>
                           
                             &nbsp;&nbsp; 
                              <select name="ServiceProviderID" id="ServiceProviderID" class="text" >
                                <option value="" {if $datarow.ServiceProviderID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                {foreach $serviceProviders as $serviceProvider}

                                    <option value="{$serviceProvider.ServiceProviderID}" {if $datarow.ServiceProviderID eq $serviceProvider.ServiceProviderID}selected="selected"{/if}>{$serviceProvider.CompanyName|escape:'html'}</option>

                                {/foreach}

                            </select>
                         </p>
                         
                          <p>
                            <label class="fieldLabel" for="InvoiceNetwork" >{$page['Labels']['invoice_to_network']|escape:'html'}:</label>
                           
                             &nbsp;&nbsp; 
                        
                             <input  type="checkbox" name="InvoiceNetwork"  value="Yes" {if $datarow.InvoiceNetwork eq 'Yes'} checked="checked" {/if}  />  
       
                          </p>
                          
                          
                          {if $datarow.ClientID eq '' || $datarow.ClientID eq '0'}
                              
                            <p>
                                <label class="fieldLabel" for="AutomaticallyCreateBrand" >{$page['Labels']['automatically_create_brand']|escape:'html'}:</label>

                                &nbsp;&nbsp; 

                                <input  type="checkbox" id="AutomaticallyCreateBrand" name="AutomaticallyCreateBrand"  value="Yes" {if $datarow.AutomaticallyCreateBrand eq 'Yes'} checked="checked" {/if}  />  

                            </p>
                            
                            <p>
                                <label class="fieldLabel" for="AutomaticallyCreateBranch" >{$page['Labels']['automatically_create_branch']|escape:'html'}:</label>

                                &nbsp;&nbsp; 

                                <input  type="checkbox" id="AutomaticallyCreateBranch" name="AutomaticallyCreateBranch"  value="Yes" {if $datarow.AutomaticallyCreateBranch eq 'Yes'} checked="checked" {/if}  />  

                                <input  type="hidden" id="ValidCreateBranch" name="ValidCreateBranch"  value="1"  />  
                                
                            </p>
                            
                            
                          {/if}
                         
                         
                         <p>
                            <hr>
                         </p>
                         
                         <p>
                            <label class="fieldLabel" for="VATRateID" >{$page['Labels']['vat_code']|escape:'html'}:</label>
                           
                             &nbsp;&nbsp; 
                             
                            <select name="VATRateID" id="VATRateID" >
                            <option value="" {if $datarow.VATRateID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>
                                    
                            {if $datarow.CountryID && isset($vatRates[$datarow.CountryID])}
                                
                                {foreach $vatRates[$datarow.CountryID] as $vatRate}

                                    <option value="{$vatRate.VatRateID}" {if $datarow.VATRateID eq $vatRate.VatRateID}selected="selected"{/if}>{$vatRate.Code|escape:'html'}</option>

                                {/foreach}
                            {/if}
                            </select>
                            
                          </p>
                          
                          <p>
                            <label class="fieldLabel" for="VATNumber" >{$page['Labels']['vat_number']|escape:'html'}:</label>
                           
                             &nbsp;&nbsp; <input  type="text" class="text"  name="VATNumber" value="{$datarow.VATNumber|escape:'html'}" id="VATNumber" >
                        
                          </p>
                          
                          
                         
                          <p>
                            <label class="fieldLabel" for="InvoicedCompanyName" >{$page['Labels']['invoiced_company_name']|escape:'html'}:</label>
                           
                             &nbsp;&nbsp; <input  type="text" class="text"  name="InvoicedCompanyName" value="{$datarow.InvoicedCompanyName|escape:'html'}" id="InvoicedCompanyName" >
                        
                          </p>
                         
                          
                          
                        <p>
                            <label class="fieldLabel" for="InvoicedPostalCode">{$page['Labels']['invoiced_postal_code']|escape:'html'}:</label>
                            &nbsp;&nbsp; <input  class="text uppercaseText" style="width: 140px;" type="text" name="InvoicedPostalCode" id="InvoicedPostalCode" value="{$datarow.InvoicedPostalCode|escape:'html'}" >&nbsp;

                            <input type="submit" name="quick_find_btn2" id="quick_find_btn2" class="textSubmitButton postCodeLookUpBtn"  onclick="return false;"   value="Click to find Address" >

                        </p>

                        <p id="selectOutput2" ></p>


                        <p>
                            <label class="fieldLabel" for="InvoicedBuilding" >{$page['Labels']['invoiced_building']|escape:'html'}:</label>
                            &nbsp;&nbsp; <input type="text" class="text"  name="InvoicedBuilding" value="{$datarow.InvoicedBuilding|escape:'html'}" id="InvoicedBuilding" >          
                        </p>

                        <p>
                            <label class="fieldLabel" for="InvoicedStreet" >{$page['Labels']['invoiced_street']|escape:'html'}:</label>
                            &nbsp;&nbsp; <input type="text" class="text"  name="InvoicedStreet" value="{$datarow.InvoicedStreet|escape:'html'}" id="InvoicedStreet" >          
                        </p>

                        <p>
                            <label class="fieldLabel" for="InvoicedArea" >{$page['Labels']['invoiced_area']|escape:'html'}:</label>
                            &nbsp;&nbsp; <input type="text" class="text" name="InvoicedArea" value="{$datarow.InvoicedArea|escape:'html'}" id="InvoicedArea" >          
                        </p>
                        
                        
                        <p>
                            <label class="fieldLabel" for="InvoicedTownCity" >{$page['Labels']['invoiced_town_city']|escape:'html'}:</label>
                            &nbsp;&nbsp; <input type="text" class="text" name="InvoicedTownCity" value="{$datarow.InvoicedTownCity|escape:'html'}" id="InvoicedTownCity" >          
                        </p>

                        
                         <p>
                            <label class="fieldLabel" for="PaymentDiscount" >{$page['Labels']['payment_discount']|escape:'html'}:</label>
                           
                             &nbsp;&nbsp; <input  type="text" class="text"  name="PaymentDiscount" value="{$datarow.PaymentDiscount|escape:'html'}" id="PaymentDiscount" >
                        
                          </p>
    
                          <p>
                            <label class="fieldLabel" for="PaymentTerms" >{$page['Labels']['payment_terms']|escape:'html'}:</label>
                           
                             &nbsp;&nbsp; <input  type="text" class="text"  name="PaymentTerms" value="{$datarow.PaymentTerms|escape:'html'}" id="PaymentTerms" >
                        
                          </p>
                          
                         
                          <p>
                            <label class="fieldLabel" for="VendorNumber" >{$page['Labels']['vendor_number']|escape:'html'}:</label>
                           
                             &nbsp;&nbsp; <input  type="text" class="text"  name="VendorNumber" value="{$datarow.VendorNumber|escape:'html'}" id="VendorNumber" >
                        
                          </p>
                          
                          
                         
                          <p>
                            <label class="fieldLabel" for="PurchaseOrderNumber" >{$page['Labels']['purchase_order_number']|escape:'html'}:</label>
                           
                             &nbsp;&nbsp; <input  type="text" class="text"  name="PurchaseOrderNumber" value="{$datarow.PurchaseOrderNumber|escape:'html'}" id="PurchaseOrderNumber" >
                        
                          </p>
                          
                          
                         
                          <p>
                            <label class="fieldLabel" for="SageAccountNumber" >{$page['Labels']['sage_account_number']|escape:'html'}:</label>
                           
                             &nbsp;&nbsp; <input  type="text" class="text"  name="SageAccountNumber" value="{$datarow.SageAccountNumber|escape:'html'}" id="SageAccountNumber" >
                        
                          </p>
                          
                          
                          <p>
                            <label class="fieldLabel" for="SageNominalCode" >{$page['Labels']['sage_nominal_code']|escape:'html'}:</label>
                           
                             &nbsp;&nbsp; <input  type="text" class="text"  name="SageNominalCode" value="{$datarow.SageNominalCode|escape:'html'}" id="SageNominalCode" >
                        
                          </p>


			<p>
			    <label class="fieldLabel" for="DefaultTurnaroundTime" >Default Turnaround Time:</label>
			    &nbsp;&nbsp;
			    <input type="text" style="width: 50px;" class="text" name="DefaultTurnaroundTime" value="{$datarow.DefaultTurnaroundTime|escape:'html'}" id="DefaultTurnaroundTime" />
			</p>
			  
                          
                          <p>
                            <label class="fieldLabel" for="EnableProductDatabase" >{$page['Labels']['product_database']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp; 
                        

                            <input  type="radio" name="EnableProductDatabase"  value="Yes" {if $datarow.EnableProductDatabase eq "Yes"} checked="checked" {/if}  /> <span class="text" >{$page['Text']['yes_text']|escape:'html'}</span> 
                            <input  type="radio" name="EnableProductDatabase"  value="No" {if $datarow.EnableProductDatabase eq "No"} checked="checked" {/if}  /> <span class="text" >{$page['Text']['no_text']|escape:'html'}</span>     
                                   
                          </p>
                        
			  
			<p>
			    <label class="fieldLabel" for="EnableCatalogueSearch" >{$page['Labels']['enable_catalogue_search']|escape:'html'}:<sup>*</sup></label>
			    &nbsp;&nbsp;
                            <input  type="radio" name="EnableCatalogueSearch"  value="Yes" {if $datarow.EnableCatalogueSearch == "Yes"} checked="checked" {/if}  /> <span class="text" >{$page['Text']['yes_text']|escape:'html'}</span> 
                            <input  type="radio" name="EnableCatalogueSearch"  value="No" {if $datarow.EnableCatalogueSearch == "No"} checked="checked" {/if}  /> <span class="text" >{$page['Text']['no_text']|escape:'html'}</span>     
			</p>
			  
                        
                        
                          <p>
                            <label class="fieldLabel" for="ForceReferralNumber" >{$page['Labels']['force_referral_number']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp; 
                        
                                

                              <input  type="radio" name="ForceReferralNumber"  value="Yes" {if $datarow.ForceReferralNumber eq 'Yes'} checked="checked" {/if}  /> <span class="text" >{$page['Text']['yes_text']|escape:'html'}</span> 
                              <input  type="radio" name="ForceReferralNumber"  value="No" {if $datarow.ForceReferralNumber neq 'Yes'} checked="checked" {/if}  /> <span class="text" >{$page['Text']['no_text']|escape:'html'}</span> 

                                
                          </p>
                          
                          <p>
                            <label class="fieldLabel" for="Status" >{$page['Labels']['status']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp; 
                        
                                {foreach $statuses as $status}

                                    <input  type="radio" name="Status"  value="{$status.Code}" {if $datarow.Status eq $status.Code} checked="checked" {/if}  /> <span class="text" >{$status.Name|escape:'html'}</span> 

                                {/foreach}    
                          </p>
                         
                         

                            <p>
                    
                                <span class= "bottomButtons" >

                                    <input type="hidden" name="ClientID"  value="{$datarow.ClientID|escape:'html'}" >
                                 
                                    {if $datarow.ClientID neq '' && $datarow.ClientID neq '0'}
                                        
                                         <input type="submit" name="update_save_btn" class="textSubmitButton centerBtn" id="update_save_btn"  value="{$page['Buttons']['save']|escape:'html'}" >
                                    
                                    {else}
                                        
                                        <input type="submit" name="insert_save_btn" class="textSubmitButton centerBtn" id="insert_save_btn"  value="{$page['Buttons']['save']|escape:'html'}" >
                                        
                                    {/if}
                                   
                                        <br>
                                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['cancel']|escape:'html'}" >



                                </span>

                            </p>

                           
                </fieldset>    
                        
                </form>        
                                             
       
</div>
                 
{/if}  
                                    
                                    
{* This block of code is for to display message after data updation *} 

                                    
<div id="dataUpdatedMsg" class="SystemAdminFormPanel" style="display: none;" >   
    
    <form id="dataUpdatedMsgForm" name="dataUpdatedMsgForm" method="post"  action="#" class="inline">
            <fieldset>
                <legend title="" > {$form_legend|escape:'html'} </legend>
                <p>
                 
                    <b>{$page['Text']['data_updated_msg']|escape:'html'}</b><br><br>
                 
                </p>
                
                <p>

                    <span class= "bottomButtons" >


                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                    </span>

                </p>
                          
                
            </fieldset>   
    </form>            
    
    
</div>    

    
 
  {* This block of code is for to display message after data insertion *}                      
                        
<div id="dataInsertedMsg" class="SystemAdminFormPanel" style="display: none;" >   
    
    <form id="dataInsertedMsgForm" name="dataInsertedMsgForm" method="post"  action="#" class="inline">
            <fieldset>
                <legend title="" > {$form_legend|escape:'html'} </legend>
                <p>
                 
                    <b>{$page['Text']['data_inserted_msg']|escape:'html'}</b><br><br>
                 
                </p>
                
                <p>

                    <span class= "bottomButtons" >


                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                    </span>

                </p>
                          
                
            </fieldset>   
    </form>            
    
    
</div>                            
                        
