{extends "DemoLayout.tpl"}

{block name=config}
{$Title = $page['Text']['page_title']}
{$PageId = $ProductMatchesPage}
{/block}

{block name=scripts}
 {*<link rel="stylesheet" href="{$_subdomain}/css/colorbox/colorbox.css" type="text/css" charset="utf-8" />*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.jlabel-1.3.min.js"></script>*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.form.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.validate.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/additional-methods.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTables.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.colorbox-min.js"></script>*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTablesPCCS.js"></script>*}

<script type="text/javascript" charset="utf-8">
    
    //It opens product page.
    function openProduct($rowData){

        window.open("{$_subdomain}/index/product/details/"+urlencode($rowData[0])) ;

    }

$(document).ready(function() {
     
    {if $showResultsTables}
        
    $superFilter = new Object();
    $superFilter.ProductNo = $('#ProductNo').val();
    $superFilter.ModelDescription = $('#ModelDescription').val();
    $superFilter.ManufacturerID = $('#ManufacturerID').val();
    $superFilter.UnitTypeID = $('#UnitTypeID').val();    
    $superFilter.ModelNumber= $('#ModelNumber').val();
    $superFilter.NetworkID  = $('#NetworkID').val();
    $superFilter.ClientID   = $('#ClientID').val();

    $('#matches_table').PCCSDataTable( {
        displayButtons:     'P',
        aoColumns: [ 
            null,             
            null, 
            null, 
            null, 
            null,
            null],
        htmlTableId:        'matches_table',
        viewButtonId:       'product_ViewButton', 
        colorboxForceClose: false,
        pickCallbackMethod: 'openProduct',
        dblclickCallbackMethod: 'openProduct',
        pickButtonText:'{$page['Buttons']['view']|escape:'html'}',
        //fetchDataUrl:       '{$_subdomain}/Data/product/advancedSearch/'+urlencode($('#ProductNo').val())+'/'+urlencode($('#ModelDescription').val())+'/'+urlencode($('#ManufacturerID').val())+'/'+urlencode($("#UnitTypeID").val())+'/'+urlencode($("#ModelNumber").val())+'/'+urlencode($("#NetworkID").val())+'/'+urlencode($("#ClientID").val()),
        fetchDataUrl:       '{$_subdomain}/Data/product/advancedSearch/',
        parentURL:          '',
        searchCloseImage:'{$_subdomain}/css/Skins/{$_theme}/images/close.png',
        frmErrorMsgClass: 'webformerror',
        frmErrorElement: 'label',
        subdomain: '{$_subdomain}',
        superFilter : $superFilter
       

    });
    
    {/if}
    
    
    
     $.validator.addMethod("atLeastOne", function(value, element) {  
                
                var $form = $(element).parents('form');
                return $form.find('.text:filled').length;
                
                }, "{$page['Errors']['atleast_one']|escape:'html'}");

    
     $(document).on('click', '#product_search_btn', 
                            function() {
                              $('#productAdvancedSearchForm').validate({


                                            rules:  {
                                                        ProductNo:
                                                        {
                                                                atLeastOne: true
                                                           
                                                        }
                                            },
                                            messages: {
                                                        ProductNo:
                                                        {
                                                                atLeastOne: "{$page['Errors']['atleast_one']|escape:'html'}"
                                                        }
                                            },

                                            errorPlacement: function(error, element) {

                                                    error.insertBefore( $("#formError") );


                                            },
                                            errorClass: 'fieldError',
                                            onkeyup: false,
                                            onblur: false,
                                            errorElement: 'label'
                                            });
                            
                            
                            
                            
                            });
    
    
});//end document ready function

</script>
    
{/block}

{block name=body}

<div class="breadcrumb">
    <div>
        <a href="{$_subdomain}/index/index">{$page['Text']['home_page']|escape:'html'}</a> / {$page['Text']['page_title']|escape:'html'}
    </div>
</div>
    
{include file='include/menu.tpl'} 
        
<div class="main" id="multiplematches">        
   <div class="SearchPanel" >
          
        <form id="productAdvancedSearchForm" name="productAdvancedSearchForm" method="post"
                action="{$_subdomain}/index/productAdvancedSearch" >
            <fieldset>
            <legend>{$page['Text']['page_title']|escape:'html'}</legend> 
            
            <p class="TopText" >
                {if $productNoResultsFlag}
                    <span class="noResults" >{$page['Errors']['no_results']|escape:'html'}</span><br>
                {/if}
                
                {$page['Text']['top_text_note']|escape:'html'}</p>
            <p class="centerBtn" >
                <label id="formError" ></label>
            </p>
            <p>
                <label class="fieldLabel" for="ProductNo" >{$page['Labels']['product_no']|escape:'html'}:</label>
                &nbsp;&nbsp; 
                <input type="text" name="ProductNo" id="ProductNo" style="width: 300px;" value="{$searchStr.ProductNo|escape:'html'}" class="text" tabIndex="1"  />
                
            </p>
            <p>
                <label class="fieldLabel" for="ModelDescription" >{$page['Labels']['product_description']|escape:'html'}:</label>
                &nbsp;&nbsp; 
                <input type="text" name="ModelDescription" style="width: 300px; id="ModelDescription" 
                        value="{$searchStr.ModelDescription|escape:'html'}" class="text" tabIndex="2" 
                        title="{$page['Text']['title_product_description']|escape:'html'}" />
            </p>
            <p>
                
                <label class="fieldLabel" for="ManufacturerID" >{$page['Labels']['manufacturer']|escape:'html'}:</label>
                &nbsp;&nbsp; 
                <select  name="ManufacturerID" id="ManufacturerID"   class="text" >
                   
                   <option value="" {if $searchStr.ManufacturerID eq ''}selected="selected"{/if}   >{$page['Text']['select_default_option']|escape:'html'}</option>
                   {foreach from=$manufacturerList item=manufacturer}
                    <option value="{$manufacturer.ManufacturerID}" {if $searchStr.ManufacturerID eq $manufacturer.ManufacturerID}selected="selected"{/if} >{$manufacturer.ManufacturerName|escape:'html'}</option>
                    {/foreach}
                   
                </select> 
                
            </p>  
            <p>
                <label class="fieldLabel" for="UnitTypeID" >{$page['Labels']['product_type']|escape:'html'}:</label>
                &nbsp;&nbsp; 
                 <select  name="UnitTypeID" id="UnitTypeID" class="text" >
                   
                   <option value="" {if $searchStr.UnitTypeID eq ''}selected="selected"{/if}   >{$page['Text']['select_default_option']|escape:'html'}</option>
                   {foreach from=$unitTypeList item=unitType}
                    <option value="{$unitType.UnitTypeID}" {if $searchStr.UnitTypeID eq $unitType.UnitTypeID}selected="selected"{/if} >{$unitType.UnitTypeName|escape:'html'}</option>
                    {/foreach}
                   
                 </select> 
               
            </p>                  
            <p>
               <label class="fieldLabel" for="ModelNumber">{$page['Labels']['model_number']|escape:'html'}:</label>
                &nbsp;&nbsp; 
                <input type="text" name="ModelNumber" style="width: 300px; id="ModelNumber" 
                        value="{$searchStr.ModelNumber|escape:'html'}" class="text" tabIndex="5" 
                        title="{$page['Text']['title_model_no']|escape:'html'}"/>
            </p>
            <p class="centerBtn" >
              
                     <input type="hidden" name="ClientID" id="ClientID" value="{$ClientID|escape:'html'}" >
                     <input type="hidden" name="NetworkID" id="NetworkID" value="{$NetworkID|escape:'html'}" >
                    
                     <input type="submit" name="product_search_btn" id="product_search_btn" class="textSubmitButton"  value="{$page['Buttons']['search']|escape:'html'}" >
            </p>


        </form>
    </div> 

{if $showResultsTables}
   <hr />

   <table id="matches_table" border="0" cellpadding="0" cellspacing="0" class="browse">
	<thead>
            <tr>
                
                 <th>{$page['Text']['catalogue_item_id']|escape:'html'}</th>
                 <th>{$page['Text']['product_code']|escape:'html'}</th>
                 <th>{$page['Text']['product_description']|escape:'html'}</th>
                 <th>{$page['Text']['manufacturer']|escape:'html'}</th>
                 <th>{$page['Text']['model_no']|escape:'html'}</th>
                 <th>{$page['Text']['product_type']|escape:'html'}</th>
                 
            </tr>
	</thead>
	<tbody>
            <tr><td colspan="6" style="color:#666;">Loading....</td></tr>    
	</tbody>
   </table>
{/if}   

{if $session_user_id && $productNoResultsFlag}
    <hr />
        <div class="SearchPanel">
        {include file='include/job_booking_panel.tpl'}
        </div>
{/if} 


{include file='include/button_panel.tpl'} 



                                    
</div>
{/block}
