{extends "DemoLayout.tpl"}

{block name=config}
{$Title = "Multiple Matches"}
{$PageId = $OpenJobMatchesPage}
{/block}

{block name=scripts}

{*<link rel="stylesheet" href="{$_subdomain}/css/colorbox/colorbox.css" type="text/css" charset="utf-8" />*} 
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.jlabel-1.3.min.js"></script>*} 
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.colorbox-min.js"></script>*} 
<script type="text/javascript" src="{$_subdomain}/js/job.form.validation.js"></script> 
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTables.min.js"></script>*}
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTablesPCCS.js"></script>*}

<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
    $('#matches_table').PCCSDataTable( {
        htmlTableId:        'matches_table',
        addButtonId:        'example_AddButton',
        updateButtonId:     'example_UpdateButton',
        deleteButtonId:     'example_DeleteButton',
        viewButtonId:       'example_ViewButton',
        formInsertButton:   'formInsertButton',
        formUpdateButton:   'formUpdateButton',
        formDeleteButton:   'formDeleteButton',
        formCancelButton:   'formCancelButton',
        colorboxFormId:     'jobForm',
        fetchDataUrl:       '{$_subdomain}/data/jobs/fetchallopen/',
        updateDataUrl:      '{$_subdomain}/data/jobs/update/',
        createDataUrl:      '{$_subdomain}/data/jobs/create/',
        deleteDataUrl:      '{$_subdomain}/data/jobs/delete/',
        createAppUrl:       '{$_subdomain}/index/jobs/display_create/',
        updateAppUrl:       '{$_subdomain}/index/jobs/display_update/',
        deleteAppUrl:       '{$_subdomain}/index/jobs/display_delete/',
        viewAppUrl:         '{$_subdomain}/index/jobs/display_view/',
        parentURL:          '',
        popUpFormWidth:     700, //Put 0 for default width
        popUpFormHeight:    600, //Put 0 for default height
        frmErrorRules: {
            JobID: {
                    required: true,
                    digits: true
            }
	    },
        frmErrorMessages : {
            JobID: {
                required: "Please enter a JobID.",
                digits: "Your JobID need to be a number"
            }
        },
        searchCloseImage:'/images/close.png',
        frmErrorMsgClass: 'webformerror',
        frmErrorElement: 'label',
        isExistsDataUrl: '{$_subdomain}/data/jobs/is_exists/',
        suggestTextId: 'suggestText',
        sugestFieldId: 'JobID',
        frmErrorSugMsgClass: 'suggestwebformerror',
        hiddenPK: 'pk',
        passParamsType:'1', // 1 'means' parameters in query string.  Default 0;  
        pickDataIdList: new Array('#JobID')
    });

});//end document ready function
    
</script>
    
{/block}

{block name=body}

<div class="breadcrumb">
    <div>
        <a href="{$_subdomain}/index/index">Home Page</a> / Open Job Matches
    </div>
</div>
        
<div class="main" id="openjobmatches">   
   
    <div class="SearchPanel">
        {include file='include/job_search.tpl'}
    </div> 
                
    <hr />

   <table id="matches_table" border="0" cellpadding="0" cellspacing="0" class="browse">
	<thead>
            <tr>
                {foreach $Job->display_columns as $col}
                    <th>{$col}</th>
                {/foreach}
            </tr>
	</thead>
        <tbody>
            <tr><td colspan="{$Job->display_columns|@count}" style="color:#666;">Loading....</td></tr> 
        </tbody>
   </table>
        
   <div class="buttonPanel">
       <hr />
       <a id="select" href="{$_subdomain}/index/jobdetail">Select Item</a>
       <a id="cancel" href="{$_subdomain}/index/index/cancelAdvancedSearch">Cancel</a>
   </div>
                                    
</div>
{/block}
