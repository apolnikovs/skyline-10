{extends "DemoLayout.tpl"}

{block name=config}
{$Title = "Multiple Matches"}
{$PageId = $ProductMatchesPage}
{/block}

{block name=scripts}
<script type="text/javascript" src="{$_subdomain}/js/product.form.validation.js"></script>
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTables.min.js"></script>*}

<script type="text/javascript" charset="utf-8">
    
    var oTable;
    
    $(document).ready(function() {
        oTable = $('#matches_table').dataTable( {
            'sDom': 'lft<"#dataTables_command">rpi',
            bottomButtonsDivId:'dataTables_command',
	    'sPaginationType': 'full_numbers',
            'oLanguage': {
		'sLengthMenu': '_MENU_ Records per page',
                'sSearch': 'Search within results:'
	    },
            'bInfo': false
        } );
        
    } );
    
</script>
    
{/block}

{block name=body}

<div class="breadcrumb">
    <div>
        <a href="{$_subdomain}/index/index">Home Page</a> / Free Text Job Booking
    </div>
</div>
    
{include file='include/menu.tpl'}
        
<div class="main" id="multiplematches">             
   <div class="SearchPanel">
       
        <div>
            <form id="freeTextJobBookingForm" name="freeTextJobBookingForm" method="post"
                  action="{$_subdomain}/index/freetextjobbooking" >
            <fieldset>
            <legend>Free Text Job Booking</legend>                 
            <div class="serviceInstructionsBar">GP7501</div>   
                <p>
                <span class="info" > Fields marked with a <span >*</span> are compulsory</span><br><br>
                </p>
                <p>
                    <label class="span-3">Catalogue No:</label>
                    <input type="text" name="productCatalogueNo" id="productCatalogueNo" 
                           value="" class="text" tabIndex="1" 
                           title="Please enter a catalogue number"/>
                </p>
                <p>
                    <label class="span-3">Description:<sup>*</sup></label>
                    <input type="text" name="productDescription" id="productDescription" 
                           value="" class="text" tabIndex="2" 
                           title="Please enter a product description"/>
                </p>
                <p>
                    <label class="span-3">Manufacturer:<sup>*</sup></label>
                    <select name="Manufacturer" id="Manufacturer" tabindex="3" title="Please select a manufacturer">
                        <option value="">Please select</option>
                        <option value="BUSH ALBA">BUSH ALBA</option>
                        <option value="PANASONIC">PANASONIC</option>
                        <option value="SONY">SONY</option>
                    </select>
                </p>  
                <p>
                    <label class="span-3">Product type:<sup>*</sup></label>
                    <select name="productType" id="productType" tabindex="3" title="Please select a product type">
                        <option value="">Please select</option>
                        <option value="Audio">Audio</option>
                        <option value="Camera">Camera</option>
                        <option value="TV">TV</option>
                    </select>
                </p>                  
                <p>
                    <label class="span-3">Model:</label>
                    <input type="text" name="productModel" id="productModel" 
                           value="" class="text" tabIndex="5" 
                           title="Please enter a model number"/>
                </p>
                <p class="formError" style="display: none;">
                    <span class="span-3">&nbsp;</span>
                    <span id="productError" >&nbsp;</span>
                </p>
                <p>
                    <span class="span-3">&nbsp;</span>
                    <span>
                        {* <a tabIndex="2" href="javascript:if (validateFreeTextJobBookingForm()) document.validateFreeTextJobBookingForm.submit();" >Search</a> *}
                    </span>
                </p>
                
                </fieldset>
            </form>
        </div>
    </div> 
        
   <div class="buttonPanel">
       <hr />
       <a id="select" href="javascript:if (validateFreeTextJobBookingForm()) document.freeTextJobBookingForm.submit();">Select Item</a>
       <a id="cancel" href="{$_subdomain}/index/index/cancelAdvancedSearch">Cancel</a>
   </div>
                                    
</div>
{/block}