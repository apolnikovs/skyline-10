{extends "DemoLayout.tpl"}

{block name=config}
{$Title = "Product Details"}
{$PageId = $ProductDetailsPage}
{/block}

{block name=scripts}
{/block}

{block name=body}

<div class="breadcrumb">
    <div>
        <a href="{$_subdomain}/index/index">Home Page</a> / Product Error
    </div>
</div>
        
<div class="main" id="productDetails">
    <div class="serviceInstructionsBar">EM1000</div>
    <div class="productDetailsPanel">
        <p>No product found, please try again.</p>
    </div>
        
    <div class="serviceInstructionsBar"></div>
    <div class="serviceInstructionsPanel">
        <hr />
        <h2>Service Instructions</h2>
        <h3>Help Line: 01234 654784</h3>
        <p>
            This unit can be repaired in the customer's home.
        </p>
        <h4>IMPORTANT NOTE</h4>
        <p>
            FOR STORES USE ONLY: there is no need to telephone before sending 
            the faulty product to Camtronics.  This item is repaired by 
            Camtronics. telephone 01462 485947
        </p>
        <h3>Spares Information</h3>
        <p>
            Chargeable spares are avaiable to prchase.
        </p>
        <p>
            Spares Order No: 123456789
        </p>
        <hr />
    </div>
        
                                    
</div>
{/block}