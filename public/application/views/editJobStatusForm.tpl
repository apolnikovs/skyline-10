<script>

$(document).ready(function() {


    $(document).on("click", "#cancelStatus", function() {
	$("#confirmClose").remove();
	$("#editJobStatusForm").show();
	$.colorbox.resize();
	return false;
    });


    $(document).on("click", "#confirmStatus", function() {
	$.post("{$_subdomain}/Job/updateJobStatus?closed=1", $("#editJobStatusForm").serialize(), function() {
	    location.reload();
	});
	return false;
    });


    $(document).on("click", "#save", function() {

	if($("input[name=status]:checked").val() == 25) {
	    $("#editJobStatusForm").hide();
	    var html = "<div id='confirmClose'>\
			    <p>Selecting this status will close the job. Are you sure you want to continue?</p>\
			    <p style='text-align:center;'>\
				<a href='#' class='DTTT_button' id='confirmStatus' style='margin-right:5px; width:50px;'>Proceed</a>\
				<a href='#' class='DTTT_button' id='cancelStatus' style='width:50px;'>Cancel</a>\
			    </p>\
			</div>\
		       ";
	    $("#editJobStatusModalContainer fieldset").append(html);
	    $.colorbox.resize();
	    return false;
	}
	    
	$("#editJobStatusForm").validate({
	    rules: {
		status:  { required: true }
	    },
	    messages: {
		status:  { required: "You must select status." }
	    },
	    errorPlacement: function(error, element) {
		error.insertAfter($("#jobID"));
	    },
	    ignore:	    "",
	    errorClass:	    'fieldError',
	    onkeyup:	    false,
	    onblur:	    false,
	    errorElement:   'p',
	    submitHandler: function() {
		$.post("{$_subdomain}/Job/updateJobStatus", $("#editJobStatusForm").serialize(), function() {
		    location.reload();
		    return false;
		});
		return false;
	    }
	});

	$("#editJobStatusForm").submit();
	
	return false;
	
    });


});


</script>

<div id="editJobStatusModalContainer">
    
    <fieldset>
	<legend align="center">{$page['Labels']['edit_job_status']|escape:'html'}</legend>
	<br/>
	<form id="editJobStatusForm" action="" method="post">
	
	    {foreach $statusList as $status}
		<input type="radio" name="status" value="{$status.StatusID}" {if $job.StatusID == $status.StatusID}checked="checked"{/if} /> {$status.StatusName} <br/>
	    {/foreach}
	    <br/>
	    <input type="hidden" value="{$JobID|default:""}" name="jobID" id="jobID" />
	    
	    <p style="text-align:center;">
		<a href="#" id="save" class="btnStandard" style="margin-right:10px;">Save</a>
		<a href="#" id="cancel" class="btnCancel">Cancel</a>
	    </p>
	
	</form>
	
    </fieldset>
    
</div>