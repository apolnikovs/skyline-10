<?php

/**
 * An example class, this is grouped with
 * other classes in the "sample" package and
 * is part of "classes" subpackage
 * @package skyline
 * @subpackage DataController
 */
require_once('CustomSmartyController.class.php');
include_once ('SkylineRESTClient.class.php');

class DiaryController extends CustomSmartyController {

    public $config;
    public $session;
    public $skyline;
    public $user;
    public $messages;
    private $lang = 'en';
    public $debug = false;
    public $speedtest = false;

    public function __construct() {
        parent::__construct();

        $this->debug = true;

        /* ==========================================
         * Read Application Config file.
         * ==========================================
         */
        $this->config = $this->readConfig('application.ini');

        /* ==========================================
         *  Initialise Session Model
         * ==========================================
         */
        $this->session = $this->loadModel('Session');

        /* ==========================================
         *  Initialise Messages Model
         * ==========================================
         */
        $this->messages = $this->loadModel('Messages');

        /* =========== smarty assign =============== */
        $this->smarty->assign('ref', '');
        $this->smarty->assign('Search', '');
        $this->smarty->assign('ErrorMsg', '');
        $this->smarty->assign('_theme', 'skyline');



        /* ==========================================
         *  Initialise User Class
         * ==========================================
         */

        $user_model = $this->loadModel('Users');
        $this->user = $user_model->GetUser($this->session->UserID, true);
        if (isset($_GET['name']) && $_GET['password'] && isset($_GET['type'])) {
            if ($this->debug)
                $this->log($_GET, 'SB_access_log_');
            foreach ($_GET as $k => $r) {
                if ($k != "name" && $k != "password") {


                    $_GET[$k] = preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s', '', $r);
                }
            }
            if ($this->debug)
                $this->log('--Update remove', 'SB_access_log_');
            if ($this->debug)
                $this->log($_GET, 'SB_access_log_');
        }
        if (isset($this->session->UserID)) {



            $this->smarty->assign('name', $this->user->Name);
            $this->smarty->assign('session_user_id', $this->session->UserID);


            if ($this->user->BranchName) {
                $this->smarty->assign('logged_in_branch_name', " - " . $this->user->BranchName . " " . $this->config['General']['Branch'] . " ");
            } else {
                $this->smarty->assign('logged_in_branch_name', "");
            }

            if ($this->session->LastLoggedIn) {
                $this->smarty->assign('last_logged_in', " - " . $this->config['General']['LastLoggedin'] . " " . date("l jS F Y G:i", $this->session->LastLoggedIn));
            } else {
                $this->smarty->assign('last_logged_in', '');
            }

            $topLogoBrandID = $this->user->DefaultBrandID;
            $this->smarty->assign('loggedin_user', $this->user);
        } else {

            $topLogoBrandID = isset($_COOKIE['brand']) ? $_COOKIE['brand'] : 0;

            $this->smarty->assign('session_user_id', '');
            $this->smarty->assign('name', '');
            $this->smarty->assign('last_logged_in', '');
            $this->smarty->assign('logged_in_branch_name', '');
            $diary = $this->loadModel('Diary');

            $inputData = $_GET;

            // die( $inputData['FaultDescription']);
            $this->smarty->assign('get', $inputData);

            if (isset($_GET['name']) && $_GET['password'] && isset($_GET['type'])) {




                $this->smarty->display('diary/login.tpl');
                die();
            } elseif (isset($_GET['name']) && $_GET['password']) {

                $this->updateJobDetailsAction($_GET);
            } else {

                if (isset($_GET['closed']) && isset($_GET['slid'])) {
                    $this->updateJobDetailsAction($_GET);
                } elseif (isset($_GET['location'])) {
                    $this->updateJobDetailsAction($_GET);
                } elseif (isset($_GET['confirmApp'])) {
                    $this->confirmAppointmentEmailAction($_GET);
                } else {
                    $this->log($_GET, 'tt');

                    $this->redirect('Login', null, null);
                }
            }
        }

        if ($topLogoBrandID) {
            $skyline = $this->loadModel('Skyline');
            $topBrandLogo = $skyline->getBrand($topLogoBrandID);

            $this->smarty->assign('_brandLogo', (isset($topBrandLogo[0]['BrandLogo'])) ? $topBrandLogo[0]['BrandLogo'] : '');
        } else {
            $this->smarty->assign('_brandLogo', '');
        }
        $this->smarty->assign('showTopLogoBlankBox', false);


        $user_agent = $_SERVER['HTTP_USER_AGENT'];

        if (preg_match('/MSIE 7.0/i', $user_agent)) {
            $this->session->browser = "IE7";
        } else {
            $this->session->browser = "Normal";
        }

        $diary = $this->loadModel('Diary');
        $GlobalViamenteStatus = $diary->getGlobalViamenteStatus();
        $this->session->GlobalViamenteStatus = $GlobalViamenteStatus;
        $this->smarty->assign("GlobalViamenteStatus", $GlobalViamenteStatus);
    }

    public function indexAction($args) {
        $this->redirect('DiaryController', 'defaultAction');
    }

    public function defaultAction($args) {
        $startime = microtime();

        $this->checkSA($args);

        //If the type is 99, then it is Diary Wizard...
        if (isset($args['type']) && $args['type'] == 99) {
            $_SESSION['DiaryWizardSetup'] = true;
            $this->redirect('AppointmentDiary/diaryDefaults/spID=' . $this->user->ServiceProviderID . '/sPage=5', null, null);
        }


        $diary = $this->loadModel('Diary');

        if ($this->user->ServiceProviderID != 0) {
            //  if(!isset($this->session->SPInfo)){
            $this->session->SPInfo = $diary->getAllServiceProviderInfo($this->user->ServiceProviderID);
            // }





            if (!isset($this->session->showFullDiary)) {
                $this->session->showFullDiary = 0;
            }


            $this->displaySummary($this->user->ServiceProviderID);





            //this method removes all appointments if no service base apointment id is set and if user is using service base
            $diary->removeUnsyncApp($this->user->ServiceProviderID);





            if (isset($args['name']) || isset($_GET['name'])) {

                $this->session->idata = null;
                $this->session->SQLSelectedDate = null;
                $this->session->ServiceProviderSkillsetID = null;
                $this->session->DiarySelectedDate = null;
                $this->session->repairSkill = null;
                $this->session->editRow = null;
                $this->session->ServiceBase = 1;
                $this->session->chAppType = null;
                $this->session->chSkillType = null;
                $this->session->addressGeoTag = null;
                $this->session->engid = null;
            }
            if (isset($this->session->ServiceBase)) {
                $this->smarty->assign('ServiceBase', 1);
            }
            if (isset($args['type'])) {

                $input = $args;
            } elseif (isset($_GET['type'])) {
                $input = $_GET;
            } else 
                $input = NULL;
            if (isset($input)) {

                if ($input['type'] == 1) {

                    $this->session->ServiceProviderSkillsetID = null;
                    $this->session->JobPostCode = null;
                    $this->session->idata = "";
                    $this->session->GeoTag = null;
                    $this->session->addressGeoTag = null;
                    $this->session->JobFullPostCode = null;
                    //$_GET['SkillType']=2;

                    $this->session->idata = $input;
                }
                if ($input['type'] == 2) {

                    $this->session->GeoTag = null;
                    $this->session->idata = "";
                    $this->session->ServiceProviderSkillsetID = null;
                    $this->session->DiarySelectedDate = null;
                    $this->session->repairSkill = null;
                    $this->session->editRow = null;
                    $this->session->refresh = null;
                    $this->session->DiarySelectedDateSlot = null;

                    $this->session->idata = $input;
                    $appDetails = $diary->getAppointmentDetails($this->session->idata['SLAppointmentID']);

                    if (!$appDetails) {
                        $this->smarty->assign('type', 5);

                        $this->smarty->display("diary/diaryEndMessage.tpl");
                    }
                    $this->session->DiarySelectedDate = $appDetails['AppointmentDate'];
                    $this->session->SQLSelectedDate = $appDetails['AppointmentDate'];
                    if (!isset($this->session->DiarySelectedDateSlot)) {
                        $this->session->DiarySelectedDateSlot = $appDetails['AppointmentTime'];
                    }
                    $this->session->JobPostCode = $appDetails['postCode'];
                    $this->session->ServiceProviderSkillsetID = $appDetails['ServiceProviderSkillsetID'];
                    $this->session->ServiceProviderServiceDuration = $appDetails['Duration'];

                    $args['pc'] = $this->session->JobPostCode;

                    $args['menReq'] = $appDetails['EngineersRequired'];
                    $args['chAppType'] = $appDetails['AppointmentTypeID'];
                }
                if ($input['type'] == 3) {
                    $this->session->GeoTag = null;
                    $this->deleteAppointmentAction($args, $input['SLAppointmentID']);
                    die();
                }
            }



            if (isset($this->session->idata)) {
                $this->session->ActionType = $this->session->idata['type'];
            } else {
                $ret = $diary->checkIfUseSB($this->user->ServiceProviderID);
                if ($ret) {
                    $this->session->ActionType = 5; //free for all
                } else {
                    $this->session->ActionType = 6; //no service base is used
                }
            }

            if (!isset($this->session->JobTable) && isset($this->session->idata)) {
                //will return skyline job no, non skylijne job no or null, if null needs to be insertet in to the non skyline job table
                $JobTable = $diary->getJobSource($this->session->idata['SBJobNo'], $this->user->ServiceProviderID);
                $this->session->JobTable = $JobTable;
            }
            $this->smarty->assign('jobTable', $this->session->JobTable);



            //if chSkill filter is set changing session skillset value to given
            if (isset($args['chSkill']) && !isset($args['chSkillType'])) {

                $this->session->ServiceProviderSkillsetID = $args['chSkill'] * 1;
                $this->session->ServiceProviderServiceDuration = $diary->getSPSkillsetInfo($this->session->ServiceProviderSkillsetID);
            }


            //parts engineer
            if (isset($this->session->idata['PartsEngineer']) && $this->session->idata['PartsEngineer'] != "") {
                $eng = $this->session->idata['PartsEngineer'];
                $this->session->engid = $diary->getEngIdFromSBCode($this->session->idata['PartsEngineer'], $this->user->ServiceProviderID);
                $this->session->engName = $diary->getEngNameFromSBCode($this->session->idata['PartsEngineer'], $this->user->ServiceProviderID);
            } else {
                $eng = false;
            }


            if (isset($args['chSkillType']) && isset($args['chAppType'])) {
                $skillset = $diary->getSkillsetID($args['chSkillType'], $args['chAppType'], $this->user->ServiceProviderID);
                $this->session->ServiceProviderSkillsetID = $skillset; //1* is just to make sure $args['chSkill'] is integer
                $this->session->ServiceProviderServiceDuration = $diary->getSPSkillsetInfo($this->session->ServiceProviderSkillsetID);
                $this->session->chAppType = $args['chAppType'];
                $this->session->chSkillType = $args['chSkillType'];
            }

            //this are service provider default appointment times eg. AM=8:45-12:00 etc
            if (!isset($this->session->DiaryDateSlotTimes)) {
                $this->session->DiaryDateSlotTimes = $diary->getSlotsTimes($this->user->ServiceProviderID);
            }
            $postcode = $this->session->JobPostCode;

            if (isset($this->session->ServiceProviderSkillsetID)) {
                $skillID = $this->session->ServiceProviderSkillsetID;
            }

            $slotsType = array();
            $slotsCount = array();
            if (isset($this->session->idata['SkillType'])) {
                $repSkill = $this->session->idata['SkillType'];
                $appType = null;
            } else {
                $repSkill = null;
                $appType = null;
            }
            if (isset($this->session->idata)) {

                if (!isset($this->session->idata['SkillType'])) {
                    $repairType = $diary->getRepairType($this->session->idata['SLAppointmentID']);
                    $rks = $repairType['RepairSkillID'];
                } else {
                    $rks = $this->session->idata['SkillType'];
                }
                if (isset($rks)) {
                    $this->session->repairSkill = $rks;
                }
            } else {
                $rks = null;
            }
            $skills = $diary->getSkillsets($this->user->ServiceProviderID, $rks, 1); //args=serviceproviderID return array
            $skills2man = $diary->getSkillsets($this->user->ServiceProviderID, $rks, 2); //args=serviceproviderID return array
            $skillsFull = array_merge($skills, $skills2man);

            //separate repait skills from types
            $repskills = array();
            $appTypes = array();
            for ($i = 0; $i < sizeof($skills); $i++) {
                if (!$this->in_object($skills[$i]['RepairSkillName'], $repskills)) {
                    $repskills[$skills[$i]['RepairSkillName']] = $skills[$i]['RepairSkillID'];
                }
                if (!$this->in_object($skills[$i]['Type'], $appTypes)) {
                    $appTypes[$skills[$i]['Type']] = $skills[$i]['AppointmentTypeID'];
                }
            }
            for ($i = 0; $i < sizeof($skills2man); $i++) {
                if (!$this->in_object($skills2man[$i]['RepairSkillName'], $repskills)) {
                    $repskills2man[$skills2man[$i]['RepairSkillName']] = $skills2man[$i]['RepairSkillID'];
                }
                if (!$this->in_object($skills2man[$i]['Type'], $appTypes)) {
                    $appTypes2man[$skills2man[$i]['Type']] = $skills2man[$i]['AppointmentTypeID'];
                }
            }


            $this->smarty->assign('jobtypes', $appTypes);
            $this->smarty->assign('repskills', $repskills);
            $fp = null;
            if ($this->session->SPInfo['DiaryAllocationType'] != "GridMapping") {
                if (isset($postcode)) {
                    $this->session->fullPostcode = $postcode;
                    $postcodeArr = explode(' ', $postcode);
                    $postcode = $postcodeArr[0];
                    $this->session->JobPostCode = $postcode;
                }
                if (isset($args['pc'])) {
                    $this->session->fullPostcode = $args['pc'];
                    $postcodeArr = explode(' ', $args['pc']);
                    $postcode = $postcodeArr[0];
                    $this->session->JobPostCode = $postcode;
                }


                if (isset($this->session->idata['CustPostcode'])) {
                    $fp = $this->session->idata['CustPostcode'];
                }
                if (isset($this->session->idata['CollPostcode']) && $this->session->idata['CollPostcode'] != "") {
                    $fp = $this->session->idata['CollPostcode'];
                }
            }

            //only used for grid mapping
            if ($this->session->SPInfo['DiaryAllocationType'] == "GridMapping") {
                if (isset($input['type']) && $input['type'] == 1) {

                    if ($this->session->idata['CollAddress1'] != "") {
                        $postcode = $this->session->idata['CollAddress1'] . " " . $this->session->idata['CollAddress2'] . " " . $this->session->idata['CollAddress3'] . " " . $this->session->idata['CollAddress4'];
                    } else {
                        $postcode = $this->session->idata['CustAddress1'] . " " . $this->session->idata['CustAddress2'] . " " . $this->session->idata['CustAddress3'] . " " . $this->session->idata['CustAddress4'];
                    }
                }
                if (isset($input['type']) && $input['type'] == 2) {

                    if ($appDetails['ColAddStreet'] != " " && $appDetails['ColAddStreet'] != "" && $appDetails['ColAddStreet'] != '0' && $appDetails['ColAddStreet'] != null) {

                        $postcode = $appDetails['ColAddBuildingNameNumber'] . " " . $appDetails['ColAddStreet'] . " " . $appDetails['ColAddTownCity'];
                        echo $appDetails['ColAddStreet'];
                    } elseif ($appDetails['Street'] != "" && $appDetails['Street'] != '0' && $appDetails['Street'] != ' ') {
                        $postcode = $appDetails['BuildingNameNumber'] . " " . $appDetails['Street'] . " " . $appDetails['LocalArea'] . " " . $appDetails['TownCity'];
                    } else {
                        $postcode = $appDetails['CustomerAddress1'] . " " . $appDetails['CustomerAddress2'] . " " . $appDetails['CustomerAddress3'] . " " . $appDetails['CustomerAddress4'];
                    }
                }
                if (isset($args['pc']) && $args['pc'] != "") {
                    $this->session->JobPostCode = $args['pc'];
                    $postcode = $args['pc'];
                }
                if (isset($postcode) && $postcode != "") {
                    $this->session->JobPostCode = $postcode;
                    $args['pc'] = $postcode;
                    $this->session->fullPostcode = $postcode;
                    $fp = $postcode;
                }
                if (isset($args['pc']) && $args['pc'] != "") {
                    $this->session->JobPostCode = $args['pc'];
                }
                if (!isset($this->session->addressGeoTag) || $this->session->addressGeoTag == "" || $this->session->addressGeoTag == null) {
                    if ($this->debug)
                        $this->log($this->session->JobPostCode . "<-jobpostcode", "addr_");
                    $addressGeoTag = $diary->getGeoTagFromAddress($this->session->JobPostCode, $this->user->ServiceProviderID);
                }else {
                    $addressGeoTag = $this->session->addressGeoTag;
                }
                $this->session->addressGeoTag = $addressGeoTag;
            } else {
                if (!isset($this->session->addressGeoTag) || $this->session->addressGeoTag == "" || $this->session->addressGeoTag == null) {

                    if ($this->session->JobFullPostCode == "" || $this->session->JobFullPostCode == null) {
                        if (isset($this->session->idata['CollPostcode']) && $this->session->idata['CollPostcode'] != "") {
                            $this->session->JobFullPostCode = $this->session->idata['CollPostcode'];
                        } else {
                            if (isset($this->session->idata['CustPostcode'])) {
                                $this->session->JobFullPostCode = $this->session->idata['CustPostcode'];
                            }
                        }
                    }
                    if ($this->debug)
                        $this->log($this->session->JobPostCode . "<fp", "addr_");
                    $addressGeoTag = $diary->getGeoTagFromAddress($this->session->JobFullPostCode, $this->user->ServiceProviderID);
                    $this->session->addressGeoTag = $addressGeoTag;
                }else {

                    $addressGeoTag = $this->session->addressGeoTag;
                }
                if ($this->debug)
                    $this->log($this->session->addressGeoTag, "addr_");
                if ($this->debug)
                    $this->log("geotags", "addr_");
                if ($this->debug)
                    $this->log($this->session->JobPostCode, "addr_");
                if ($this->debug)
                    $this->log(64, "addr_");
                $this->session->addressGeoTag = $addressGeoTag;
//            $addressGeoTag=false;
//            $this->session->addressGeoTag=false;
            }







            $anyAppSlotID = $diary->getAnyTimeSlot($this->user->ServiceProviderID); //this TODO get correct any time slot id by code
            if ((isset($this->user->ServiceProviderID) && isset($skillID) && isset($postcode) && isset($anyAppSlotID)) || in_array($this->session->ActionType, array(5, 6))) {

                //   $diary->realocateAppointments($this->user->ServiceProviderID,$postcode);

                if (in_array($this->session->ActionType, array(1, 2, 3, 4)) || ((isset($this->user->ServiceProviderID) && isset($skillID) && isset($postcode) && isset($anyAppSlotID)) && $this->session->showFullDiary === 1)) {
                    $errrr = microtime();
                    $errrr = explode(" ", $errrr);
                    $errrr = $errrr[1] + $errrr[0];
                    $starttime2 = $errrr;
                    if ($this->session->SPInfo['DiaryAllocationType'] == "GridMapping") {
                        $GO = $addressGeoTag;
                    } else {
                        $GO = null;
                    }

                    $res = $diary->getSlots($this->user->ServiceProviderID, $skillID, $postcode, 27, $GO, $eng, $this->session->SPInfo['DiaryType']); //get all registered slots 1.param provider centre id, 2.serviceProviderEngeneerSkillset 3.postcode
                    // $diary->getSlotsForSamsung($this->user->ServiceProviderID,$skillID,$postcode,7,$addressGeoTag);
                    $res2 = $diary->getSlotsRecomendet($this->user->ServiceProviderID, $skillID, $postcode, $GO); //get all registered slots 1.param provider centre id, 2.serviceProviderEngeneerSkillset 3.postcode
                    $tradeAccountsDays = $diary->getSlotsTradeAccount($this->user->ServiceProviderID, $fp); //get all registered slots 1.param provider centre id, 2.serviceProviderEngeneerSkillset 3.postcode
                } else {
                    if ($this->session->SPInfo['DiaryAllocationType'] == "GridMapping") {
                        $gg = true;
                    } else {
                        $gg = false;
                    }
                    $this->smarty->assign('daysOnlyView', 1);
                    $res = $diary->getDaysOnly($this->user->ServiceProviderID, $gg);
                    $res2 = array();
                }
// $res=$diary->getSlots(1,1,'nn1','');//get all registered slots 1.param service centre id, 2.serviceProviderEngeneerSkillset 3.postcode
                // die();
//              echo"<pre>";  
//              print_r($res);
//              echo"</pre>";  
                if ($this->debug)
                    $this->log($res, "slots_array___");
                $recomendet = array();
                for ($o = 0; $o < sizeof($res2); $o++) {
                    $recomendet[] = $res2[$o]['dayOnly'];
                }





                $filterAMPM = "";
                if (isset($args['am'])) {
                    $filterAMPM = "AM";
                }
                if (isset($args['pm'])) {
                    $filterAMPM = "PM";
                }



                //main filtering loop  
                $daySlots = array();
                if (key_exists(0, $res)) {
                    foreach ($res as $k) {

                        if (!isset($daysSlots[$k['dayOnly']][$k['Type']])) {
                            $daysSlots[$k['dayOnly']][$k['Type']] = $k['slotsLeft'];
                        } else {
                            $daysSlots[$k['dayOnly']][$k['Type']] = $daysSlots[$k['dayOnly']][$k['Type']] + $k['slotsLeft'];
                        }

                        if (isset($k['anyAppCount'])) {
                            $daysSlots[$k['dayOnly']]['anyAppCount'] = $k['anyAppCount'];
                        }

                        // $k['DiaryAllocationID']."=".$k['AppointmentDate']."->".$k['Type']."<br>";
                        if (isset($dlist[$k['dayOnly']][$k['Type']])) {
                            $dlist[$k['dayOnly']][$k['Type']].=$k['DiaryAllocationID'] . ",";
                        } else {
                            $dlist[$k['dayOnly']][$k['Type']] = $k['DiaryAllocationID'] . ",";
                        }
                    }



                    //optimization routine
                    if (isset($this->session->ServiceProviderSkillsetID)) {
                        foreach ($dlist as $k) {

                            foreach ($k as $key => $list) {

                                $diary->diaryLocationOptimize($this->session->ServiceProviderSkillsetID, $list);
                            }
                        }
                    }






                    /////////-------------------------old calculation not dinamic
//foreach($daysSlots as $key2=>$val2)
//    {
//    
//    //echo"$key2->";
//    
//    foreach($val2 as $key=>$item)
//    {
//      
//        if(isset($total[$key2])){$total[$key2]=$total[$key2]+$item;}else{$total[$key2]=$item;}
//        if($key=='AM'){$am[$key2]=$item;}
//        if($key=='PM'){$pm[$key2]=$item;}
//        if($key=='ANY'){$at[$key2]=$item;}
//     //   echo "<div>$key:$item</div>";
//    }
//    
//    }
                    /////////-------------------------old calculation not dinamic
                    ///dinamic 
                    foreach ($daysSlots as $key2 => $val2) {

                        //echo"$key2->";
                        //print_r($val2);
                        $mark = false;
                        foreach ($val2 as $key => $item) {

                            if (isset($total[$key2])) {
                                $total[$key2] = $total[$key2] + $item;
                            } else {
                                $total[$key2] = $item;
                            }
                            if ($key == 'AM') {
                                $am[$key2] = $item;
                            }
                            if ($key == 'PM') {
                                $pm[$key2] = $item;
                            }
                            if ($key == 'ANY') {
                                $at[$key2] = $item;
                            }
                            if ($key == 'anyAppCount') {

                                $anyApps[$key2] = $item;
                                if (!$mark) {
                                    $total[$key2] = $total[$key2] - $anyApps[$key2];
                                    $mark = true;
                                }
                            }

                            // echo "<div>$key:$item</div>";

                            if (!isset($anyApps[$key2])) {
                                $anyApps[$key2] = 0;
                            }
                        }
                    }
                    // print_r($total);

                    if ($this->session->SPInfo['DiaryType'] == "FullViamente") {
                        ///dinamic 
                        //print_r($total);
                        foreach ($total as $key => $value) {

                            if ($filterAMPM == "") {
                                if (isset($at[$key]) && isset($am[$key]) && $am[$key] > 0 && isset($pm[$key]) && $pm[$key] > 0) {
                                    $slotsType[$key] = "ANY";
                                    // $slotsCount[$key]=$total[$key];old
                                    $slotsCount[$key] = $total[$key] - $am[$key] - $pm[$key];
                                } elseif (isset($am[$key])) {
                                    if ($am[$key] >= 0 && isset($pm[$key]) && $pm[$key] <= 0) {
                                        $slotsType[$key] = "AM";
                                        $slotsCount[$key] = $am[$key];
                                    } elseif (isset($pm[$key]) && $pm[$key] > 0) {
                                        $slotsType[$key] = "PM";
                                        $slotsCount[$key] = $pm[$key];
                                    } else {
                                        $slotsType[$key] = "AM";
                                        $slotsCount[$key] = $am[$key];
                                    }
                                } elseif (isset($pm[$key])) {
                                    $slotsType[$key] = "PM";
                                    $slotsCount[$key] = $pm[$key];
                                }
                            } elseif ($filterAMPM == "AM") {
                                if (isset($am[$key])) {
                                    $slotsType[$key] = "AM";
                                    if (!isset($slotsCount[$key])) {
                                        $slotsCount[$key] = $am[$key];
                                    } else {
                                        $slotsCount[$key] = $slotsCount[$key] + $am[$key];
                                    }
                                }
                            } elseif ($filterAMPM == "PM") {

                                if (isset($pm[$key])) {
                                    $slotsType[$key] = "PM";
                                    if (!isset($slotsCount[$key])) {
                                        $slotsCount[$key] = $pm[$key];
                                    } else {
                                        $slotsCount[$key] = $slotsCount[$key] + $pm[$key];
                                    }
                                    if ($slotsCount[$key] > $total[$key]) {
                                        $slotsCount[$key] = $total[$key];
                                    }
                                }
                            }
                            if (isset($slotsCount[$key])) {
                                if (isset($pm[$key])) {
                                    $total[$key] = $total[$key] - $pm[$key];
                                }
                                if (isset($am[$key])) {
                                    $total[$key] = $total[$key] - $am[$key];
                                }
                                if ($total[$key] <= 0 && $anyApps[$key] == 0) {
                                    $total[$key] = 1;
                                }//fixes bug when day is full with no appoointments

                                if ($slotsCount[$key] >= $total[$key]) {
                                    $slotsCount[$key] = $total[$key];
                                }

                                if ($slotsCount[$key] <= 0) {

                                    if ($filterAMPM == "") {
                                        if ($slotsCount[$key] <= -50) {

                                            $slotsCount[$key] = "ERROR";
                                            $slotsType[$key] = "FULL";
                                        } else {
                                            $slotsCount[$key] = "-";
                                            $slotsType[$key] = "FULL";
                                            $slotsType[$key] = "FULL";
                                        }
                                    } else {
                                        $slotsCount[$key] = null;
                                        $slotsType[$key] = null;
                                    }
                                } else {
                                    if ($this->session->SPInfo['DiaryShowSlotNumbers'] == "Yes") {
                                        $slotsCount[$key] = $slotsCount[$key];
                                    } else {
                                        $slotsCount[$key] = "";
                                    }
                                }
                            }
                        }






                        //filter out only any time slots
                        if (isset($args['any'])) {
                            $slotsTypeTmp = array();
                            $slotsCountTmp = array();
                            foreach ($slotsType as $key => $value) {

                                if ($slotsType[$key] == 'ANY') {
                                    $slotsTypeTmp[$key] = "ANY";
                                    $slotsCountTmp[$key] = $slotsCount[$key];
                                }
                            }
                            $slotsType = $slotsTypeTmp;
                            $slotsCount = $slotsCountTmp;
                        }
                    }


                    if ($this->session->SPInfo['DiaryType'] == "NoViamente") {


                        foreach ($total as $key => $value) {
                            // if ($this->debug) $this->log($anyApps,"any apps_");
                            // if ($this->debug) $this->log($am,"any apps_");
                            // if ($this->debug) $this->log($pm,"any apps_");
                            //if ($this->debug) $this->log($total,"any apps_");
                            //if ($this->debug) $this->log("-------------------","any apps_");
                            if ($filterAMPM == "") {
                                if (isset($am[$key]) && $am[$key] > 0 && isset($pm[$key]) && $pm[$key] > 0) {
                                    $slotsType[$key] = "ANY";
                                    // $slotsCount[$key]=$total[$key];old
                                    // echo "$total[$key] ".$anyApps[$key]."-".$key." ||";

                                    $slotsCount[$key] = $total[$key] - $anyApps[$key];
                                } elseif (isset($am[$key])) {
                                    if ($am[$key] >= 0 && isset($pm[$key]) && $pm[$key] <= 0) {
                                        $slotsType[$key] = "AM";


                                        $slotsCount[$key] = $am[$key];
                                        if ($slotsCount[$key] > $total[$key] - $anyApps[$key]) {
                                            $slotsCount[$key] = $total[$key] - $anyApps[$key];
                                        }
                                    } elseif (isset($pm[$key]) && $pm[$key] > 0) {
                                        $slotsType[$key] = "PM";
                                        $slotsCount[$key] = $pm[$key];
                                        if ($slotsCount[$key] > $total[$key] - $anyApps[$key]) {
                                            $slotsCount[$key] = $total[$key] - $anyApps[$key];
                                        }
                                    } else {
                                        $slotsType[$key] = "AM";
                                        $slotsCount[$key] = $am[$key];
                                        if ($slotsCount[$key] > $total[$key] - $anyApps[$key]) {
                                            $slotsCount[$key] = $total[$key] - $anyApps[$key];
                                        }
                                    }
                                } elseif (isset($pm[$key])) {
                                    $slotsType[$key] = "PM";
                                    $slotsCount[$key] = $pm[$key];
                                    if ($slotsCount[$key] > $total[$key] - $anyApps[$key]) {
                                        $slotsCount[$key] = $total[$key] - $anyApps[$key];
                                    }
                                }
                            } elseif ($filterAMPM == "AM") {
                                if (isset($am[$key])) {
                                    $slotsType[$key] = "AM";

                                    $slotsCount[$key] = $am[$key];

                                    if ($slotsCount[$key] > $total[$key] - $anyApps[$key]) {
                                        $slotsCount[$key] = $total[$key] - $anyApps[$key];
                                    }
                                }
                            } elseif ($filterAMPM == "PM") {

                                if (isset($pm[$key])) {
                                    $slotsType[$key] = "PM";

                                    $slotsCount[$key] = $pm[$key];

                                    if ($slotsCount[$key] > $total[$key] - $anyApps[$key]) {
                                        $slotsCount[$key] = $total[$key] - $anyApps[$key];
                                    }
                                }
                            }
                            if (isset($slotsCount[$key])) {


                                if ($slotsCount[$key] >= $total[$key]) {
                                    $slotsCount[$key] = $total[$key];
                                }

                                if ($slotsCount[$key] <= 0) {

                                    if ($filterAMPM == "") {
                                        if ($slotsCount[$key] <= -50) {

                                            $slotsCount[$key] = "ERROR";
                                            $slotsType[$key] = "FULL";
                                        } else {

                                            $slotsCount[$key] = $slotsCount[$key];
                                            $slotsType[$key] = "FULL";
                                            // ;$slotsType[$key]="FULL";
                                        }
                                    } else {
                                        $slotsCount[$key] = null;
                                        $slotsType[$key] = null;
                                    }
                                }
                            }
                        }






                        //filter out only any time slots
                        if (isset($args['any'])) {
                            $slotsTypeTmp = array();
                            $slotsCountTmp = array();
                            foreach ($slotsType as $key => $value) {

                                if ($slotsType[$key] == 'ANY') {
                                    $slotsTypeTmp[$key] = "ANY";
                                    $slotsCountTmp[$key] = $slotsCount[$key];
                                }
                            }
                            $slotsType = $slotsTypeTmp;
                            $slotsCount = $slotsCountTmp;
                        }
//        echo"<pre>";
//        print_r($slotsCount);
//        echo"</pre>";
//         
                    }

                    //end
                }
            }

            //datatable
            //auto day set (today or tomorow if day not selected (17/12/2012 update)@andris
            if ($this->session->SPInfo['AutoSelectDay'] && !isset($args['setDate']) && !in_array($this->session->ActionType, array(2)) && !isset($args['timeSliders']))
                if ($this->session->ActionType == 1) {
                    switch ($this->session->SPInfo['AutoSelectDay']) {
                        case "Today":
                            if (isset($slotsType[date('dmY')])) {
                                $args['setDate'] = date('dmY') . $slotsType[date('dmY')];
                            }
                            break;
                        case "NextDay":
                            if (isset($slotsType[date('dmY', strtotime("+1 day"))])) {
                                $args['setDate'] = date('dmY', strtotime("+1 day")) . $slotsType[date('dmY', strtotime("+1 day"))];
                            }
                            break;
                    }
                }



            if (isset($args['setDate'])) {
                $date = substr($args['setDate'], 2, 2) . "/" . substr($args['setDate'], 0, 2) . "/" . substr($args['setDate'], 4, 4);
                $dateSQL = substr($args['setDate'], 4, 4) . "-" . substr($args['setDate'], 2, 2) . "-" . substr($args['setDate'], 0, 2);
                $slotTypeE = substr($args['setDate'], strlen($args['setDate']) - 2, 2);
                if ($slotTypeE == "NY") {
                    $slotTypeE = "ANY";
                }
                $this->session->DiarySelectedDate = $date;
                $this->session->DiarySelectedDateSlot = $slotTypeE;

                $this->session->SQLSelectedDate = $dateSQL;
            }
            if (isset($this->session->DiarySelectedDate)) {
                $this->smarty->assign('daySelected', $this->session->DiarySelectedDate);
            }
            if (!isset($this->session->SQLSelectedDate)) {
                $this->session->SQLSelectedDate = null;
            }



            $skillSetFilter = "0";
            if ((isset($skillID) && $skillID != "" && isset($postcode) && $postcode != "") || in_array($this->session->ActionType, array(5, 6))) {

                if (isset($args['tSkills'])) {
                    $skillSetFilter = $args['tSkills'];
                    $args['tSkills'] = explode(',', $args['tSkills']);
                }



                $args['tSkills'] = explode(',', $skillSetFilter);


                $engineersF = "0";

                if (isset($args['tEng'])) {
                    $engineers = $args['tEng'];
                    $engineers = explode(',', $args['tEng']);
                    foreach ($engineers as $k => $val) {
                        $engineersF.="," . $val;
                        $args['tEng'] = explode(',', $engineersF);
                    }
                }
                if (isset($this->session->idata['SLAppointmentID'])) {

                    $appID = $this->session->idata['SLAppointmentID'];
                    // $skillSetFilter=$this->session->ServiceProviderSkillsetID;
                } else {
                    $appID = false;
                }

                $slots = $diary->getFilledSlots($this->user->ServiceProviderID, $this->session->SQLSelectedDate, $skillSetFilter, $filterAMPM, $postcode, $appID, $engineersF); //1.spID,2.date,3.skillsetid,4.filtering slot type(am/pm/any)5.postcode, 6.IF need only 1 slot pas id              
                // echo"<pre>";
                //print_r($slots);
                //  echo"</pre>";
//s die(var_export($skillSetFilter,true));
                if (isset($this->session->ActionType) && $this->session->ActionType == 2 && isset($slots[0]['AppointmentID'])) {

                    $rowID = $slots[0]['AppointmentID'];
                    $this->smarty->assign('editRow', $rowID);
                    $this->session->editRow = $rowID;
                }

                $dubTMP = array();
                for ($i = 0; $i < sizeof($slots); $i++) {
                    if (isset($slots[$i]['PostalCode']) && $slots[$i]['PostalCode'] != "") {
                        $dubTMP[$i] = $slots[$i]['PostalCode'];
                    }
                    if (isset($slots[$i]['deliveryPostcode']) && $slots[$i]['deliveryPostcode'] != "") {
                        $dubTMP[$i] = $slots[$i]['deliveryPostcode'];
                    }
                }
                $pcCount = array_count_values($dubTMP);
            }
            $engineerList = $diary->getEngineersList($this->user->ServiceProviderID, $this->session->SQLSelectedDate,false,false,false);

            // $this->smarty->assign('TimeSlots',$TimeSlots);
//        if(isset($this->session->IDDetails)){
//            $this->smarty->assign('IDDetails',$this->session->IDDetails);
//        }
            if (isset($this->session->idata)) {
                $this->smarty->assign('iData', $this->session->idata);
            }

            if (isset($slots)) {
                $this->smarty->assign('slots', $slots);
            }


            if (isset($this->session->JobPostCode)) {
                $args['pc'] = $this->session->JobPostCode;
            }
            if (isset($this->session->ServiceProviderSkillsetID)) {

                $args['chSkill'] = $this->session->ServiceProviderSkillsetID;
            }
            if (!isset($this->session->idata)) {
                $this->smarty->assign("noJob", true);
            }
            if (isset($this->session->editRow)) {

                $this->smarty->assign("editRow", $this->session->editRow);
            }
            if (!isset($args['tSkills'])) {
                $args['tSkills'] = array();
            }
            if (!isset($args['tSkills'])) {
                $args['tSkills'] = array();
            }


            if (isset($this->session->idata['ProductType']) && isset($this->session->idata['ModelNumber'])) {

                $this->smarty->assign('modelInfo', $this->session->idata['ProductType'] . "-" . $this->session->idata['ModelNumber']);
            }
            if (isset($this->session->idata['CustTitle']) && isset($this->session->idata['CustSurname'])) {
                $this->smarty->assign('customerInfo', $this->session->idata['CustTitle'] . " " . $this->session->idata['CustSurname']);
            }
            if (!isset($pcCount)) {
                $pcCount = array();
            }
            if (!isset($recomendet)) {
                $recomendet = array();
            }
            if (isset($this->session->idata['WallMount']) && $this->session->idata['WallMount'] == '2') {
                $args['menReq'] = 2;
            }
            if ($this->session->ActionType == 2 && isset($this->session->ServiceProviderSkillsetID)) {
                $skillinfo = $diary->getSkillInfo($this->session->ServiceProviderSkillsetID);

                $repSkill = $skillinfo['RepairSkillID'];
                $appType = $skillinfo['AppointmentTypeID'];
            }
            ksort($appTypes);
            ksort($repskills);

            if (isset($this->session->chAppType) && !isset($args['chAppType'])) {
                $appType = $this->session->chAppType;
            }
            if (isset($this->session->chSkillType) && !isset($args['chSkillType'])) {
                $repSkill = $this->session->chSkillType;
            }
            if (!isset($repskills2man)) {
                $repskills2man = array();
            }
            if (!isset($appTypes2man)) {
                $appTypes2man = array();
            }
            if (!isset($tradeAccountsDays)) {
                $tradeAccountsDays = null;
            }

            $limitTimeInfo = $diary->getNextDayBookingOffTime($this->user->ServiceProviderID);
            $limitTime = $limitTimeInfo[0];
            if ($limitTime == "" || $limitTime == null) {
                $limitTime = "00:00:00";
            };

            if ($this->session->SPInfo['AutoDisplayTable']) {
                
            }
            //rebook mode app details 9/1/13
            if (isset($this->session->editRowData)) {
                $this->smarty->assign('editRowData', $this->session->editRowData);
            }



            //time sliders
            $unrApp = $diary->getUnreachable($this->user->ServiceProviderID, $this->session->SQLSelectedDate);
            if (isset($this->session->SQLSelectedDate) && isset($args['timeSliders'])) {

                $finalizedDays = $diary->getFinalizedDays($this->user->ServiceProviderID, '', true);
                $st_engs = $diary->getFullEngineersList($this->user->ServiceProviderID, $this->session->SQLSelectedDate);
                
                $st_engsList = $this->appendEngineersWithHolidayData($st_engs);  
                $st_apps = $diary->getAllEngineerAppointments($this->user->ServiceProviderID, $this->session->SQLSelectedDate);  
                $st_engApssS = array();

                for ($i = 0; $i < sizeof($st_apps); $i++) {
                    $st_engApssS[$st_apps[$i]['ServiceProviderEngineerID']][] = array
                        (
                        'ap_st' => $st_apps[$i]['apStart'],
                        'ap_en' => $st_apps[$i]['apEnd'],
                        'postcode' => $st_apps[$i]['postcode'],
                        'slotCol' => $st_apps[$i]['slotCol'],
                        'eId' => $st_apps[$i]['ServiceProviderEngineerID'],
                        'SkillsetName' => $st_apps[$i]['SkillsetName'],
                        'EngstartTimeSec' => $st_apps[$i]['EngstartTimeSec'],
                        'EngstartTime' => $st_apps[$i]['EngstartTime'],
                        'EngEndTimeSec' => $st_apps[$i]['EngEndTimeSec'],
                        'EngEndTime' => $st_apps[$i]['EngEndTime'],
                        'idle' => $st_apps[$i]['idle']
                    );
                }

                $this->smarty->assign('ts_engineers', $st_engsList);
                $this->smarty->assign('ts_apps', $st_engApssS);
                $this->smarty->assign('unrApp', $unrApp);
            }
            $this->session->slotsLeft = $slotsCount;
            $this->session->slotsType = $slotsType;

            //
            //
       //
     $errorapp = $diary->getErrorApp($this->user->ServiceProviderID, 0, $this->session->SQLSelectedDate);

            if (sizeof($unrApp) > 0 || $errorapp) {
                $this->smarty->assign('unrdayvar', 'yes');
            } else {
                $this->smarty->assign('unrdayvar', 'no');
            }
            $lockedBookingAfter = $limitTimeInfo[1];
            if (isset($this->session->idata['CustMobileNo'])) {
                if (isset($this->session->idata['CustMobileNo']) && $this->session->idata['CustMobileNo'] != "") {
                    $phone = $this->session->idata['CustMobileNo'];
                } elseif (isset($this->session->idata['CollContactNumber']) && $this->session->idata['CollContactNumber'] != "") {
                    $phone = $this->session->idata['CollContactNumber'];
                } elseif (isset($this->session->idata['CustHomeTelNo']) && $this->session->idata['CustHomeTelNo'] != "") {
                    $phone = $this->session->idata['CustHomeTelNo'];
                } else {
                    $phone = $this->session->idata['CustWorkTelNo'];
                }
            } else {
                $phone = null;

                $this->smarty->assign('phone', $phone);
            }

            //table main appointmnets column filtering
            if ($this->session->SPInfo['DiaryType'] == "FullViamente" || $this->session->SPInfo['DiaryType'] == "") {
                if ($this->session->ActionType == 1) {
                    $tfilter = array('false', 'false', 'true', 'true', 'true', 'false', 'false', 'false', 'false', 'true', 'true', 'true', 'true', 'false', 'false', 'true', 'true', 'true', 'true');
                } else {
                    $tfilter = array('false', 'false', 'true', 'true', 'true', 'true', 'true', 'true', 'true', 'true', 'false', 'true', 'true', 'true', 'true', 'true', 'true', 'true', 'true');
                }
            }

            if ($this->session->SPInfo['DiaryType'] == "NoViamente") {
                $tfilter = array('false', 'false', 'true', 'true', 'true', 'false', 'false', 'false', 'false', 'true', 'false', 'true', 'true', 'true', 'true', 'true', 'true', 'true', 'true');
            }

            $this->smarty->assign('t1Filter', $tfilter);

            ///
            $bankHolidays = $diary->getBankHolidays($this->user->ServiceProviderID);
            $bha = array();
            foreach ($bankHolidays as $a) {
                if ($a['Status'] != "Open") {
                    $bha[].=$a['HolidayDate'];
                }
            }
            if (isset($args['chSkillType'])) {
                $repairSkill = $args['chSkillType'];
            } else {
                if (isset($this->session->showFullDiary) && $this->session->showFullDiary == 1) {
                    $repairSkill = "$repSkill";
                } else {
                    $repairSkill = "";
                }
            }
            if (!isset($args['timeSliders'])) {
                $finalizedDays = $diary->getFinalizedDays($this->user->ServiceProviderID, $repairSkill);
            } else {
                if (!isset($finalizedDays)) {
                    $finalizedDays = array();
                }
            }
            $this->smarty->assign('bankHolidays', $bha);
            $this->smarty->assign('diaryType', $this->session->SPInfo['DiaryType']);
            if ($eng) {
                $this->smarty->assign('PartEngineerName', $this->session->engName);
                $this->smarty->assign('PartEngineerCode', $this->session->idata['PartsEngineer']);
            }
            $this->smarty->assign('adrGeotag', $this->session->addressGeoTag);
            $this->smarty->assign('allocationType', $this->session->SPInfo['DiaryAllocationType']);
            $this->smarty->assign('limitTime', $limitTime);
            $this->smarty->assign('fullPostcode', $this->session->fullPostcode);
            $this->smarty->assign('lockedBookingAfter', $lockedBookingAfter);
            $this->smarty->assign('AutoDisplayTable', $this->session->SPInfo['AutoDisplayTable']);
            $this->smarty->assign('spID', $this->user->ServiceProviderID);
            $this->smarty->assign('tradeAccountsDays', $tradeAccountsDays);
            $this->smarty->assign('vrunToday', $this->session->SPInfo['RunViamenteToday']);
            $this->smarty->assign('showFull', $this->session->showFullDiary);
            $this->smarty->assign('browser', $this->session->browser);
            $this->smarty->assign('repskill', $repSkill);
            $this->smarty->assign('ServiceProviderID', $this->user->ServiceProviderID);
            $this->session->repairSkillSelected = $repSkill;
            $this->smarty->assign('apptype', $appType);
            $this->smarty->assign('repskills', $repskills);
            $this->smarty->assign('appTypes', $appTypes);
            $this->smarty->assign('repskills2man', $repskills2man);
            $this->smarty->assign('appTypes2man', $appTypes2man);
            $this->smarty->assign('recomendet', $recomendet);
            $this->smarty->assign('actionType', $this->session->ActionType);
            $this->smarty->assign('finalizedDays', $finalizedDays);
            $this->smarty->assign('pcCount', $pcCount);
            $this->smarty->assign('refresh', 1);
            $this->smarty->assign('skills', $skills);
            $this->smarty->assign('skillsFull', $skillsFull);
            $this->smarty->assign('skills2man', $skills2man);
            $this->smarty->assign('engineerList', $engineerList);
            $this->smarty->assign('slotsType', $slotsType);
            $this->smarty->assign('args', $args);
            $this->smarty->assign('chSkill', $this->session->ServiceProviderSkillsetID);
            $this->smarty->assign('slotsCount', $slotsCount);
            $page = $this->messages->getPage('diaryMain', $this->lang);
            $this->smarty->assign('page', $page);
            $this->smarty->assign('viamenteServer', $this->config['Viamente']['ViamenteApiUrl']);
            $this->smarty->assign('timeSlidersStart', $this->session->SPInfo['EngineerDefaultStartTime']);
            $this->smarty->assign('timeSlidersStartSec', $this->session->SPInfo['startSec']);
            $this->smarty->assign('timeSlidersEnd', $this->session->SPInfo['EngineerDefaultEndTime']);
            $this->smarty->assign('timeSlidersEndSec', $this->session->SPInfo['endSec']);
            $this->smarty->assign('spinfo', $this->session->SPInfo);
            $this->smarty->display('diaryMain_sb.tpl');
        } else {
            $this->smarty->assign('lock', 'Yes'); //if sp id not set prevent any html generated and force select one (sa mode);
            $splist = $diary->getAllSPList();
            $this->smarty->assign('spList', $splist);
            $this->smarty->display('diary/diarySuperSelect.tpl');
        }

        // put this at the bottom of your page/script
    }
    
    /*
     * @author Dileep Bhimineni
     *    Function to sort Engineers list based on Absent 
     *  
     * @param
     *      $engineerList - array of Engineers List 
     * 
     * $return Sorted Array  with absent engineers at bottom
     */
    
    public function sortHolidayEngineers($engineerList) {
        
        if (empty($engineerList)) {
            
            return NULL;
        }
            
        $holidayEnginners   = array();
        $engineerListArray  = array();
        
        foreach ($engineerList as $value) {
            
            if ($value['hasHoliday'] == 'Holiday') {
                $holidayEnginners[] = $value;               
            } else {
                $engineerListArray[] = $value;
            }   
        }
        return array_merge($engineerListArray,$holidayEnginners);
        
    }

    
    
    public function showPopUpAction($args) {
        $this->checkSA($args);
        $diary = $this->loadModel('Diary');
        if (isset($_POST['update'])) {

            $data = $diary->EditAppointment($_POST['update']);
            if ($diary->getModifiedGeoTag($data[0]['postCode'], $this->user->ServiceProviderID)) {
                $this->smarty->assign('modiefiedGeoTag', 'yes');
            } else {
                $this->smarty->assign('modiefiedGeoTag', 'no');
            }
            if (!isset($data[0]['Latitude'])) {
                if (isset($this->session->addressGeoTagFullnames) && $this->session->addressGeoTagFullnames != "") {
                    $geotags = $this->session->addressGeoTagFullnames;
                } else {
                    $geotags = $diary->getGeoTag($data[0]['postCode'], $this->user->ServiceProviderID);
                }
            } else {
                $geotags = array('Latitude' => $data[0]['Latitude'], 'Longitude' => $data[0]['Longitude']);
                $this->smarty->assign('modiefiedGeoTag', 'yes');
            }
            $this->smarty->assign('Edit', $data);
            if ($diary->getModifiedGeoTag($data[0]['postCode'], $this->user->ServiceProviderID)) {
                $this->smarty->assign('modiefiedGeoTag', 'yes');
            } else {
                $this->smarty->assign('modiefiedGeoTag', 'no');
            }

            $this->smarty->assign('geotags', $geotags);
        }
        if (isset($this->session->addressGeoTagFullnames) && $this->session->addressGeoTagFullnames != "") {
            $geotags = $this->session->addressGeoTagFullnames;
            $this->smarty->assign('geotags', $geotags);
        }

        $fulladress = "error";
        if (isset($data[0]['ColAddTownCity']) && $data[0]['ColAddTownCity'] != "" && $data[0]['ColAddTownCity'] != "0") {

            $fulladress = $data[0]['ColAddStreet'] . " " . $data[0]['ColAddTownCity'];
        } elseif (isset($data[0]["CustomerAddress1"]) && $data[0]["CustomerAddress1"] != "") {

            $fulladress = $data[0]['CustomerAddress1'] . " " . $data[0]['CustomerAddress2'] . " " . $data[0]['CustomerAddress3'];
        } elseif (isset($data[0]['Street']) && $data[0]['Street'] != "") {
            $fulladress = $data[0]['Street'] . " " . $data[0]['LocalArea'] . $data[0]['TownCity'];
        }

        if (isset($data[0]['ServiceProviderSkillsetID'])) {
            $skillsetID = $data[0]['ServiceProviderSkillsetID'];
        } elseif (isset($this->session->ServiceProviderSkillsetID)) {
            $skillsetID = $this->session->ServiceProviderSkillsetID;
        }
        if(!isset($data[0])){
            $APPpostcode=$this->session->JobPostCode;
        }else{
            $APPpostcode=$data[0]['postCode'];
        }
        //$engineerList = $diary->getEngineersList($this->user->ServiceProviderID, $this->session->SQLSelectedDate,$skillsetID);
        $engList    = $this->appendEngineersWithHolidayData($diary->getEngineersList($this->user->ServiceProviderID, $this->session->SQLSelectedDate,$skillsetID,false,$APPpostcode,true));
        $endListAll = $this->appendEngineersWithHolidayData($diary->getEngineersList($this->user->ServiceProviderID, $this->session->SQLSelectedDate,false,false,false,true));
        $engineerList = $this->sortHolidayEngineers($engList);
        $engineerListALL = $this->sortHolidayEngineers($endListAll);
        if (isset($this->session->idata['SkillType'])) {
            $repSkillID = $this->session->idata['SkillType'];
        } elseif (isset($this->session->repairSkill)) {
            $repSkillID = $this->session->repairSkill;
        } else {
            $repSkillID = null;
        }
        if ((isset($this->session->idata['WallMount']) && $this->session->idata['WallMount'] == "2")) {
            $skillsetList = $diary->getSkillsets($this->user->ServiceProviderID, $repSkillID, 2);
        } else {
            $skillsetList = $diary->getSkillsets($this->user->ServiceProviderID, $repSkillID, 1);
        }
        $skillsetList1 = $diary->getSkillsets($this->user->ServiceProviderID, $repSkillID, 1);
        $skillsetList2 = $diary->getSkillsets($this->user->ServiceProviderID, $repSkillID, 2);
        $skillsetList = array_merge($skillsetList1, $skillsetList2);
        if (isset($data[0]['WallMount']) && $data[0]['WallMount'] == "2") {

            $skillsetList = $diary->getSkillsets($this->user->ServiceProviderID, $repSkillID, 2);
        }
        if (!isset($_POST['update'])) {
            $activeSkillDuration = $diary->getSkillInfo($this->session->ServiceProviderSkillsetID);
        } else {

            $activeSkillDuration = $diary->getSkillInfo($data[0]['ServiceProviderSkillsetID']);
        }
        $repairsList = $diary->getRepairsList($this->session->ServiceProviderSkillsetID);
        $this->smarty->assign('IDDetails', $this->session->IDDetails);

        if (isset($this->session->idata['CollAddress1']) && $this->session->idata['CollAddress1'] != "") {
            $fulladress = $this->session->idata['CollAddress1'] . " " . $this->session->idata['CollAddress2'] . " " . $this->session->idata['CollAddress3'] . " " . $this->session->idata['CollAddress4'];
        } elseif (isset($this->session->idata['CustAddress1']) && $this->session->idata['CustAddress1'] != "") {
            $fulladress = $this->session->idata['CustAddress1'] . " " . $this->session->idata['CustAddress2'] . " " . $this->session->idata['CustAddress3'] . " " . $this->session->idata['CustAddress4'];
        }

        if (!isset($this->session->DiaryDateSlotTimes)) {
            $this->session->DiaryDateSlotTimes = $diary->getSlotsTimes($this->user->ServiceProviderID);
        }
        $eod = $diary->checkFinalized($this->user->ServiceProviderID, $this->session->SQLSelectedDate, $skillsetID);
        if ($eod) {
            $this->smarty->assign('eod', 'yes');
        } else {
            $this->smarty->assign('eod', 'no');
        }
        //auto specify engineer by postcode allocation @andris 17/12/2012
//               if($this->session->SPInfo['AutoSpecifyEngineerByPostcode']=='Yes'){
//                   $specifyEngList=$diary->getSpecificPostcodeEngineers();
//               }
        if (!isset($data)) {
            $this->smarty->assign('modiefiedGeoTag', 'no');
        }
        if (isset($this->session->GeoTag) && $this->session->GeoTag != null) {

            $this->smarty->assign('appGeoTag', $this->session->GeoTag);
        }
        if ($this->session->SPInfo['DiaryAllocationType'] == "GridMapping") {
            $this->smarty->assign('appGeoTag', $this->session->addressGeoTag);
        }
        if ($this->session->engid) {
            $this->smarty->assign('forceEngPart', $this->session->engid);
            $this->smarty->assign('forceEngPartData', $diary->getEngineerData($this->session->engid));
        }

        $this->smarty->assign('ViamenteRunType', $this->session->SPInfo['ViamenteRunType']);

        if (isset($this->session->force_reason)) {
            $this->smarty->assign('force_reason', $this->session->force_reason);
        } else {
            $this->smarty->assign('force_reason', "");
        }
        $deferred = $diary->checkDeferredTimeSlot($this->user->ServiceProviderID, $this->session->SQLSelectedDate, $this->session->JobPostCode);
        $DiaryDateSlotTimes = $this->session->DiaryDateSlotTimes;
        if ($deferred) {
//                echo"<pre>";
//                print_r($deferred);
//                print_r($this->session->DiaryDateSlotTimes);
//                print_r($this->session->DiarySelectedDateSlotTimes);
//                
//                echo"</pre>";

            if ($deferred['DpType'] == "Working") {
                $tfrom = $deferred['EarliestWorkday'];
                $tTo = $deferred['LatestWorkday'];
            } else {
                $tfrom = $deferred['EarliestWeekend'];
                $tTo = $deferred['LatestWeekend'];
            }
            $DiaryDateSlotTimes['AM']['TimeFrom'] = $tfrom;
            $DiaryDateSlotTimes['PM']['TimeTo'] = $tTo;
            $DiaryDateSlotTimes['ANY']['TimeTo'] = $tTo;
            $DiaryDateSlotTimes['ANY']['TimeFrom'] = $tfrom;
            $this->smarty->assign('deferredTime', "yes");
        }
        $this->smarty->assign('fa', $fulladress);
        $this->smarty->assign('AutoSpecifyEngineerByPostcode', $this->session->SPInfo['AutoSpecifyEngineerByPostcode']);
        $this->smarty->assign('diaryType', $this->session->SPInfo['DiaryType']);
        $this->smarty->assign('repairSkills', $repairsList);
        $this->smarty->assign('repairSkills', $repairsList);
        $this->smarty->assign('actionType', $this->session->ActionType);
        $this->smarty->assign('date', $this->session->DiarySelectedDate);
        $this->smarty->assign('chSkill', $this->session->ServiceProviderSkillsetID);
        $this->smarty->assign('skillDuration', $activeSkillDuration['ServiceDuration']);
        $this->smarty->assign('iData', $this->session->idata);
        $this->smarty->assign('EngineersList', $engineerList);
        $this->smarty->assign('EngineersListALL', $engineerListALL);
        $this->smarty->assign('skillsetList', $skillsetList);
        $this->smarty->assign('DiaryDateSlotTimes', $DiaryDateSlotTimes);
        $this->smarty->assign('daySelectedTimes', $this->session->DiarySelectedDateSlotTimes);
        $this->smarty->assign('postcode', $this->session->JobPostCode);
        $this->smarty->assign('slotTypeE', $this->session->DiarySelectedDateSlot);
        $this->smarty->assign('fullAdress', "21+kettering+road+northampton");
        if ($this->session->browser == "Normal") {
            echo $this->smarty->fetch('diary/diaryAddAppointment.tpl');
        } else {
            echo $this->smarty->fetch('diary/diaryAddAppointment.tpl');
        }

        return;
    }

    /*
     * @author Dileep Bhimineni
     * 
     *      Function to append Engineers List with Absent Data
     * 
     * $parms 
     *      $engineer - Engineers Array
     * 
     * @return
     *      Array of Engineers with Holiday Data
     */
    
    public function appendEngineersWithHolidayData($engineer) {
        
        if (empty($engineer))
            return NULL;
        $diary = $this->loadModel('Diary');
        $st_engsList = array();
        foreach ($engineer as $key => $value) 
        {
            $st_engsList[$key]  = $value;
            //$st_engsList[$key]['holidayStart'] = 0;
            //$st_engsList[$key]['holidayEnd'] = 0;
            //$st_engsList[$key]['holidayStartTime'] = "00:00";
            //$st_engsList[$key]['holidayEndTime'] = "00:00";
            if (isset($value['hasHoliday'])){
                $getHolidayEntries = $diary->getEngineerHolidayEntries($value['ServiceProviderEngineerID'],$this->session->SQLSelectedDate);
                if (isset($getHolidayEntries)) 
                {
                    for($i=0;$i<count($getHolidayEntries);$i++)
                    {
                        $startHolidayTime = substr($getHolidayEntries[$i]['StartTime'],-8);
                        $endHolidayTime  = substr($getHolidayEntries[$i]['EndTime'],-8);
                        $startHolidaySecs   = $this->time2seconds($startHolidayTime);
                        $endHolidaySecs     = $this->time2seconds($endHolidayTime);
                        $st_engsList[$key][$value['ServiceProviderEngineerID']][$i]  = $value;
                        $st_engsList[$key][$value['ServiceProviderEngineerID']][$i]['holidayStart'] = $startHolidaySecs;
                        $st_engsList[$key][$value['ServiceProviderEngineerID']][$i]['holidayEnd'] = $endHolidaySecs;
                        $st_engsList[$key][$value['ServiceProviderEngineerID']][$i]['holidayStartTime'] = substr($startHolidayTime,0,-3);
                        $st_engsList[$key][$value['ServiceProviderEngineerID']][$i]['holidayEndTime'] = substr($endHolidayTime,0,-3);
                        if ($startHolidayTime == '00:00:00' && $endHolidayTime == '23:59:00')
                            $st_engsList[$key][$value['ServiceProviderEngineerID']][$i]['allDayHoliday'] = 'yes';
                        else
                            $st_engsList[$key][$value['ServiceProviderEngineerID']][$i]['allDayHoliday'] = 'no';
                    }
                }
            }
        }
        return $st_engsList;
    }

    /**
     * testOptimise
     *  
     * Test call of calling viamente optimise
     * 
     * @param $spId     The ID of the service provider
     *        $date     The date rqequired
     * 
     * @return associative array - 0 => Engineer ID
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     * ************************************************************************ */
    public function showGMapAction($args) {
        $this->checkSA($args);
        $appointment_model = $this->loadModel('Appointment');
        $pc_model = $this->loadModel('PostCodeLatLong');

        $apppoinments = $appointment_model->getAppointmentList($args[1], $args[0]);

        $plot_code = "";

        foreach ($apppoinments as $a) {
            $latlong = $pc_model->getLatLong($a['location']);
            if (!is_null($latlong)) {
                $plot_code .= "placeMarker({$latlong['latitude']}, {$latlong['longitude']}); ";
            }
        }

        $this->smarty->assign('plot_code', $plot_code);
        $this->smarty->display('diary/diaryGmap.tpl');
    }

    public function in_object($val, $obj) {

        if ($val == "") {
            trigger_error("in_object expects parameter 1 must not empty", E_USER_WARNING);
            return false;
        }
        if (!is_object($obj)) {
            $obj = (object) $obj;
        }

        foreach ($obj as $key => $value) {
            if (!is_object($value) && !is_array($value)) {
                if ($value == $val) {
                    return true;
                }
            } else {
                return $this->in_object($val, $value);
            }
        }
        return false;
    }

    public function insertSlotAction($args) {
        $diary = $this->loadModel('Diary');
        if (isset($_POST['slotInsert'])) {
            $diary->insertSlot($_POST['slotInsert']);
            $this->defaultAction($args);
        } else {
            $ret = $diary->getSlotsTimesInsert($this->user->ServiceProviderID, $this->session->SQLSelectedDate);
            $this->smarty->assign('sTypes', $ret);
            if ($this->session->browser == "Normal") {
                echo $this->smarty->fetch('diary/diaryInsertSlot.tpl');
            } else {
                echo $this->smarty->fetch('diary/diaryInsertSlot_IE7.tpl');
            }
        }
    }

    public function insertAppointmentAction($args, $ansd = false, $apID = null) {
        $this->checkSA($args);
        if (!$ansd) {
            $diary = $this->loadModel('Diary');
            if ($this->session->SPInfo['DiaryType'] == "FullViamente") {
                $apiV = $this->loadModel('APIViamente');
            }
//    echo"<pre>";
//    print_r($_POST);
//    echo"</pre>";
//    die();
            if (isset($_POST['ServiceProviderEngineerID'])) {
                $excludet = $diary->getEngineerData($_POST['ServiceProviderEngineerID']);
            }
            if (isset($excludet[0]) && $excludet[0]['ViamenteExclude'] == 'Yes') {
                $apID = $diary->insertAppointment($_POST, $this->user->ServiceProviderID, $this->session->SQLSelectedDate, false, false, true);
            } else {
                if ($this->session->SPInfo['AutoSpecifyEngineerByPostcode'] == 'Yes') {
                    $autoForceEng = true;
                } else {
                    $autoForceEng = false;
                }

                $apID = $diary->insertAppointment($_POST, $this->user->ServiceProviderID, $this->session->SQLSelectedDate, false, $autoForceEng);

//    echo"<pre>";
//    print_r();
//    echo"</pre>";
                if ($this->session->SPInfo['DiaryType'] == "FullViamente") {

                    if ((date("Y-m-d") != $this->session->SQLSelectedDate || $this->session->SPInfo['RunViamenteToday'] == 'Yes') && $diary->checkFinalized($this->user->ServiceProviderID, $this->session->SQLSelectedDate, $this->session->ServiceProviderSkillsetID) === false) {


                        $viaResponse = $this->viamenteUpdateAction($args, $_POST['ServiceProviderSkillsetID'], $apID);
                    }
                    if (!isset($viaResponse)) {

                        if ($diary->checkFinalized($this->user->ServiceProviderID, $this->session->SQLSelectedDate, $this->session->ServiceProviderSkillsetID) === false) {
                            $viaResponse = "YES";
                        } else {
                            $viaResponse = "Finalized";
                        }
                    }

                    if ($viaResponse == "YES") {
                        if (!isset($this->session->force) || $this->session->force != "yes") {
                            $capacity = $diary->checkEngineersCapacity($this->user->ServiceProviderID, $this->session->SQLSelectedDate, $this->session->ServiceProviderSkillsetID);

                            if ($this->session->SPInfo['BookingCapacityLimit'] < $capacity) {
                                $this->smarty->assign('del', $apID);
                                $appDet = $diary->getAppointmentDetails($apID);

                                $this->smarty->assign('appDet', $appDet);
                                $this->smarty->display("diary/diaryNotReached.tpl");
                                die();
                            }
                        } else {
//    $unreached=$diary->getUnreached($this->user->ServiceProviderID);
//              $this->sendEmailAction(1,$unreached);

                            $erroApp = $diary->getForceAppointmentDetails($apID);
                            if ($erroApp) {
                                $this->sendEmailAction(3, $erroApp);
                            }
                        }
                    }
                    $this->session->force = "no";
                    $this->session->force_reason = null;
                    if ($viaResponse == "Finalized") {
                        $this->smarty->assign('del', $apID);
                        $appDet = $diary->getAppointmentDetails($apID);



                        $this->smarty->assign('appDet', $appDet);
                        $this->smarty->display("diary/diaryFinalizedInsert.tpl");
                        die();
                    }
                }
            }
        }
        $this->smarty->assign('apptId',$apID);
        if ($this->session->ActionType != 6) {
            $apiA = $this->loadModel('APIAppointments');
            $response = $apiA->PutAppointmentDetails($apID);
            if ($this->session->SPInfo['DiaryType'] != "FullViamente") {
                $apiA->RoutedAppointments($this->session->SQLSelectedDate, $this->user->ServiceProviderID);
            }
            if ($this->debug)
                $this->log('SB response: ' . var_export($response, true));

            if (isset($response)) {
                if (isset($response['ResponseCode'])) {
                    if ($response['ResponseCode'] == "SC0001") {
                        $this->smarty->assign('type', 1);
                        $this->smarty->display("diary/diaryEndMessage.tpl");
                    } else {
                        $this->smarty->assign('type', 2);
                        $this->smarty->assign('response', $response);
                        $this->smarty->display("diary/diaryEndMessage.tpl");
                    }
                } else {
                    $this->smarty->assign('type', 7);
                    $this->smarty->assign('response', $response);
                    $this->smarty->display("diary/diaryEndMessage.tpl");
                }

                die();
            }
        }
        //$args['sbResponse']=$response;
        //$this->defaultAction($args);
        $jTyp = $diary->getJobType($apID);
        if($jTyp[0]['jobType'] == "skj")
        {
            $customerId = $diary->getJobCustomerId($jTyp[0]['JobID']);
            $diary->updateAppointmentCustomerEmail($customerId[0]['CustomerID'],$_POST['custEmail'],'skj');
            $this->session->CustEmail = $_POST['custEmail'];
        }
        else if($jTyp[0]['jobType'] == "nskj")
            $diary->updateAppointmentCustomerEmail($jTyp[0]['NonSkylineJobID'],$_POST['custEmail'],'nskj');
        $this->redirect('DiaryController', 'defaultAction');
        if (isset($response)) {

            $this->smarty->assign('type', 7);
            $this->smarty->assign('response', $response);
            $this->smarty->display("diary/diaryEndMessage.tpl");
        }
    }

    public function updateAppointmentAction($args) {
        $this->checkSA($args);
        $diary = $this->loadModel('Diary');
        $apiA = $this->loadModel('APIAppointments');
        $apiV = $this->loadModel('APIViamente');
        //die(print_r($_POST));
        $apID = $_POST['appID'];
        $jTyp = $diary->getJobType($apID);
        if($jTyp[0]['jobType'] == "skj")
        {
            $customerId = $diary->getJobCustomerId($jTyp[0]['JobID']);
            $diary->updateAppointmentCustomerEmail($customerId[0]['CustomerID'],$_POST['custEmail'],'skj');
        }
        else if($jTyp[0]['jobType'] == "nskj")
            $diary->updateAppointmentCustomerEmail($jTyp[0]['NonSkylineJobID'],$_POST['custEmail'],'nskj');



        if (isset($_POST['ServiceProviderEngineerID'])) {
            $excludet = $diary->getEngineerData($_POST['ServiceProviderEngineerID']);
        }
        if (isset($excludet[0]) && $excludet[0]['ViamenteExclude'] == 'Yes') {
            $diary->updateAppointment($_POST, $this->user->ServiceProviderID, true);
        } else {
            $diary->updateAppointment($_POST, $this->user->ServiceProviderID);

            if ((date("Y-m-d") != $this->session->SQLSelectedDate || $this->session->SPInfo['RunViamenteToday'] == 'Yes') && $diary->checkFinalized($this->user->ServiceProviderID, $this->session->SQLSelectedDate, $this->session->ServiceProviderSkillsetID) === false) {
                $this->viamenteUpdateAction($args, false, $apID);
            }
        }
        $this->smarty->assign('apptId',$apID);
        if ($this->session->ActionType != 6) {


            $response = $apiA->PutAppointmentDetails($apID);

            //  if ($this->debug) $this->log('SB response: '.var_export($response,true));
            if ($this->session->SPInfo['DiaryType'] != "FullViamente") {
                $apiA->RoutedAppointments($this->session->SQLSelectedDate, $this->user->ServiceProviderID);
            }
            if (isset($response)) {
                if (isset($response['ResponseCode'])) {
                    if ($response['ResponseCode'] == "SC0001") {
                        $this->smarty->assign('type', 4);
                        $this->smarty->display("diary/diaryEndMessage.tpl");
                    } else {
                        $this->smarty->assign('type', 2);
                        $this->smarty->assign('response', $response);
                        $this->smarty->display("diary/diaryEndMessage.tpl");
                    }
                } else {
                    $this->smarty->assign('type', 7);
                    $this->smarty->assign('response', $response);
                    $this->smarty->display("diary/diaryEndMessage.tpl");
                }
                if ($this->session->ActionType != 5) {
                    die();
                }
            }
        }


        $this->defaultAction($args);
    }

    public function deleteAppointmentAction($args, $delID = null) {
        $this->checkSA($args);
        $diary = $this->loadModel('Diary');
        $apiA = $this->loadModel('APIAppointments');
        $apiV = $this->loadModel('APIViamente');

        if (isset($_POST['delete'])) {
            if ($this->session->ActionType != 6) {
                $response = $apiA->DeleteAppointment($_POST['delete']);





                if ($this->debug)
                    $this->log('SB response: ' . var_export($response, true));
                //$apiV->OptimiseRoute($this->user->ServiceProviderID,$this->session->SQLSelectedDate);
                if (isset($response)) {
                    if (isset($this->session->SQLSelectedDate)) {
                        if ((date("Y-m-d") != $this->session->SQLSelectedDate || $this->session->SPInfo['RunViamenteToday'] == 'Yes') && $diary->checkFinalized($this->user->ServiceProviderID, $this->session->SQLSelectedDate, $this->session->ServiceProviderSkillsetID) === false) {
                            $this->viamenteUpdateAction($args, false, $_POST['delete']);
                        }
                    }
                    if (isset($response['ResponseCode'])) {
                        if ($response['ResponseCode'] == "SC0001") {
                            $diary->deleteAppointment($_POST['delete'], $this->user->ServiceProviderID);

                            $this->smarty->assign('type', 3);
                            $this->smarty->display("diary/diaryEndMessage.tpl");
                        } else {
                            $this->smarty->assign('type', 2);
                            $this->smarty->assign('response', $response);
                            $this->smarty->display("diary/diaryEndMessage.tpl");
                        }
                    } else {
                        $this->smarty->assign('type', 9);
                        $this->smarty->assign('response', $response);
                        $this->smarty->display("diary/diaryEndMessage.tpl");
                    }

                    if ($this->session->ActionType != 5) {
                        die();
                    }
                }
            } else {
                $diary->deleteAppointment($_POST['delete'], $this->user->ServiceProviderID);
                if (isset($this->session->SQLSelectedDate)) {
                    if ((date("Y-m-d") != $this->session->SQLSelectedDate || $this->session->SPInfo['RunViamenteToday'] == 'Yes') && $diary->checkFinalized($this->user->ServiceProviderID, $this->session->SQLSelectedDate, $this->session->ServiceProviderSkillsetID) === false) {
                        $this->viamenteUpdateAction($args, false, $_POST['delete']);
                    }
                }
            }
            $this->defaultAction($args);
        } else {

            if (isset($_POST['del'])) {
                $del = $_POST['del'];
            } elseif (isset($delID)) {
                $del = $delID;
            }
            $this->smarty->assign('del', $del);
            if (!isset($delID)) {

                if ($this->session->browser == "Normal") {

                    echo $this->smarty->fetch('diary/diaryDeleteAppointment.tpl');
                } else {
                    echo $this->smarty->fetch('diary/diaryDeleteAppointment_IE7.tpl');
                }
            } else {

                $this->smarty->assign('sb', true);
                if ($this->session->browser == "Normal") {
                    $this->smarty->display('diary/diaryDeleteAppointment.tpl');
                } else {
                    $this->smarty->display('diary/diaryDeleteAppointment_IE7.tpl');
                }
            }
        }
    }

    public function addDiarySlotAction($args) {
        $diary = $this->loadModel('Diary');
        if (isset($_POST['add'])) {

            $diary->addDiarySlot($_POST, $this->user->ServiceProviderID);
            $this->defaultAction($args);
        } else {

            $date = str_replace('CalendarDay', '', $_POST['day']);
            $times = $diary->getSlotsTimes($this->user->ServiceProviderID);

            $engineerList = $diary->getEngineersList($this->user->ServiceProviderID);
            $this->smarty->assign('engList', $engineerList);
            $this->smarty->assign('day', $date);
            $this->smarty->assign('times', $times);
            $this->smarty->assign('skillid', $this->session->ServiceProviderSkillsetID);

            if ($this->session->browser == "Normal") {
                echo $this->smarty->fetch('diary/diaryAddDiarySlot.tpl');
            } else {
                echo $this->smarty->fetch('diary/diaryAddDiarySlot_IE7.tpl');
            }
        }
    }

    /**
     * testOptimise
     *  
     * Test call of calling viamente optimise
     * 
     * @param $spId     The ID of the service provider
     *        $date     The date rqequired
     * 
     * @return associative array - 0 => Engineer ID
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     * ************************************************************************ */
    public function testOptimiseAction(/* $spId, $date */) {
        $api_viamente = $this->loadModel('APIViamente');

        $response = $api_viamente->OptimiseRoute(1, '2012-10-18', array(1, 2));

        if ($this->debug)
            $this->log("Output of Optimise route\n" . var_export($response, true));

        echo var_export($response, true);
    }

    /**
     * testProcessEngineerAction
     *  
     * Test call of calling viamente process engineer
     * 
     * @param $args associative array - 0 => Engineer ID
     *                                  1 => [ UPDATE | CREATE ]
     * 
     * @return 
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     * ************************************************************************ */
    public function testProcessEngineerAction($args) {
        $api_viamente = $this->loadModel('APIViamente');

        $response = $api_viamente->ProcessEngineer($args[0], $args[1]);

        if ($this->debug)
            $this->log("Output of Process Engineer\n" . var_export($response, true));

        echo var_export($response, true);
    }

    /**
     * testDeleteEngineerNameAction
     *  
     * Test call of calling viamente process engineer
     * 
     * @param array $args  0 => Engineer Name
     *                     1 => Key
     * 
     * @return 
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     * ************************************************************************ */
    public function testDeleteEngineerNameAction($args) {
        $api_viamente = $this->loadModel('APIViamente');

        $response = $api_viamente->DeleteEngineerByName($args[0], $args[1]);

        if ($this->debug)
            $this->log("Output of Process Engineer\n" . var_export($response, true));

        echo var_export($response, true);
    }

    public function testDeleteAllEngineersAction() {
        $api_viamente = $this->loadModel('APIViamente');
        $api_viamente->DeleteAllKeyEngineers();
    }

    public function confirmRebookAction($args) {
        $this->checkSA($args);
        $diary = $this->loadModel('Diary');
        $apiA = $this->loadModel('APIAppointments');
        $apiV = $this->loadModel('APIViamente');
        if ($this->session->DiarySelectedDateSlot == "LL") {

            $this->session->DiarySelectedDateSlot = "ANY";
        }
        if ($diary->checkFinalized($this->user->ServiceProviderID, $this->session->SQLSelectedDate, $this->session->ServiceProviderSkillsetID) === false) {


            $grid = false;
            if ($this->session->SPInfo['DiaryAllocationType'] == "GridMapping") {
                $grid = true;
            }
            $res = $diary->confirmRebook($this->session->SQLSelectedDate, $this->session->editRow, $this->session->DiarySelectedDateSlot, $this->session->ServiceProviderSkillsetID, $this->session->JobPostCode, $this->user->ServiceProviderID, $grid);
            if ($res) {
                if ($this->session->ActionType != 6) {
                    $response = $apiA->PutAppointmentDetails($this->session->editRow);
                    if ($this->session->SPInfo['DiaryType'] != "FullViamente") {
                        $apiA->RoutedAppointments($this->session->SQLSelectedDate, $this->user->ServiceProviderID);
                    }
                    if ($this->debug)
                        $this->log('SB response: ' . var_export($response, true));
                    //$apiV->OptimiseRoute($this->user->ServiceProviderID,$this->session->SQLSelectedDate);
                    if (isset($response)) {
                        if (isset($response['ResponseCode'])) {
                            if ($response['ResponseCode'] == "SC0001") {
                                $this->smarty->assign('type', 4);
                                $this->smarty->display("diary/diaryEndMessage.tpl");
                            } else {
                                $this->smarty->assign('type', 2);
                                $this->smarty->assign('response', $response);
                                $this->smarty->display("diary/diaryEndMessage.tpl");
                            }
                        } else {
                            
                        }

                        if ($this->session->ActionType != 5) {
                            die();
                        }
                    }
                }
            } else {
                $this->smarty->assign('type', 10);
                $this->smarty->display("diary/diaryEndMessage.tpl");
            }

            $this->defaultAction($args);
        } else {





            $this->smarty->display("diary/diaryFinalizedRebook.tpl");
            die();
        }
    }

    public function rebookFinalizedAction($args) {
        $this->checkSA($args);
        $diary = $this->loadModel('Diary');
        $apiA = $this->loadModel('APIAppointments');
        $apiV = $this->loadModel('APIViamente');
        $grid = false;
        if ($this->session->SPInfo['DiaryAllocationType'] == "GridMapping") {
            $grid = true;
        }
        $res = $diary->confirmRebook($this->session->SQLSelectedDate, $this->session->editRow, $this->session->DiarySelectedDateSlot, $this->session->ServiceProviderSkillsetID, $this->session->JobPostCode, $this->user->ServiceProviderID, $grid);
        if ($this->session->ActionType != 6) {
            $response = $apiA->PutAppointmentDetails($this->session->editRow);
            if ($this->session->SPInfo['DiaryType'] != "FullViamente") {
                $apiA->RoutedAppointments($this->session->SQLSelectedDate, $this->user->ServiceProviderID);
            }
            $erroApp = $diary->getForceAppointmentDetails($this->session->editRow);
            if ($erroApp) {
                $this->sendEmailAction(4, $erroApp);
            }
            if ($this->debug)
                $this->log('SB response: ' . var_export($response, true));
            //$apiV->OptimiseRoute($this->user->ServiceProviderID,$this->session->SQLSelectedDate);
            if (isset($response)) {
                if (isset($response['ResponseCode'])) {
                    if ($response['ResponseCode'] == "SC0001") {
                        $this->smarty->assign('type', 4);
                        $this->smarty->display("diary/diaryEndMessage.tpl");
                    } else {
                        $this->smarty->assign('type', 2);
                        $this->smarty->assign('response', $response);
                        $this->smarty->display("diary/diaryEndMessage.tpl");
                    }
                } else {
                    
                }

                if ($this->session->ActionType != 5) {
                    die();
                }
            }
        }
        $this->redirect('Diary/default');
    }

    public function setEditRowAction($args) {
        $diary = $this->loadModel('Diary');
        $this->session->editRow = $_POST['editRow'];
        $this->session->editRowData = $_POST['editRowData'];
        $appdet = $diary->getAppointmentDetails($_POST['editRow']);
        echo $appdet['BookedBy2'];
        // print_r($appdet);
    }

    public function cancelRebookingAction($args) {
        $this->session->editRow = null;
        $this->defaultAction($args);
    }

    public function samsungMoveReasonAction($args) {
        $this->smarty->display("diary/diarySamsungMoveReason.tpl");
    }

    /**
     * testDeleteEngineerAction
     *  
     * Test call of calling viamente process engineer
     * 
     * @param $args associative array - 0 => Engineer ID
     * 
     * @return 
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     * ************************************************************************ */
    public function testDeleteEngineerAction($args) {
        $api_viamente = $this->loadModel('APIViamente');

        $response = $api_viamente->DeleteEngineer($args[0]);

        if ($this->debug)
            $this->log("Output of Optimise route\n" . var_export($response, true));

        // echo var_export($response,true);
    }

    /**
     * testListEngineersAction
     *  
     * Test call of calling viamente process engineer
     * 
     * @param $none
     * 
     * @return 
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     * ************************************************************************ */
    public function testListEngineersAction($args) {
        $api_viamente = $this->loadModel('APIViamente');

        $response = $api_viamente->ListEngineers();

        if ($this->debug)
            $this->log("Output of List Engineers \n" . var_export($response, true));
        echo"<pre>";
        echo var_export($response, true);
        echo"</pre>";
    }

    /**
     * testPostCodeLatLongAction
     *  
     * Test call of converting post codes
     * 
     * @param $none
     * 
     * @return 
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     * ************************************************************************ */
    public function testPostCodeLatLongAction($args) {
        $postcode_lat_long_model = $this->loadModel('PostcodeLatLong');

        $postcodes = array("CV1 4BS", "CV1 9FN", "CV1 5RR", "CV1 5FB", "NN1 5EX", "G77 6UA", "BT1 5GS");

        for ($n = 0; $n < count($postcodes); $n++) {
            echo "{$postcodes[$n]} - ";
            $resp = $postcode_lat_long_model->getLatLong($postcodes[$n]);
            echo "Latitude = {$resp['latitude']}   Longitude = {$resp['latitude']}<br>";
        }
    }

    /**
     * testEndDayAction
     *  
     * Test call of converting post codes
     * 
     * @param $none
     * 
     * @return 
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     * ************************************************************************ */
    public function testEndDayAction($args) {
        $diary = $this->loadModel('Diary');
        $apiA = $this->loadModel('APIAppointments');

        $resp = $apiA->RoutedAppointments($args[0], $args[1]);
    }

    //this end day call
    public function viamenteUpdate2Action($args) {
        if ($this->session->SPInfo['DiaryType'] == "FullViamente") {
            $this->checkSA($args);
            // put this at the top of your page/script

            if (date("Y-m-d") != $this->session->SQLSelectedDate || $this->session->SPInfo['RunViamenteToday'] == 'Yes') {


                $api_viamente = $this->loadModel('APIViamente');
                $diary = $this->loadModel('Diary');


                $engineers = $diary->getAssignedEngineers($this->session->SQLSelectedDate, $this->session->JobPostCode, $this->user->ServiceProviderID);



//        for($i=0;$i<sizeof($engineers);$i++){
//            $engineers[$i];
//           $api_viamente->ProcessEngineer($engineers[$i], 'create');
//       }
                // $api_viamente->ProcessEngineer(3, 'create');
                $ret = $api_viamente->OptimiseRoute($this->user->ServiceProviderID, $this->session->SQLSelectedDate, $engineers);
                if ($this->debug)
                    $this->log($ret, 'vRespLog');
                if ($args['defaultRet'] == 2) {
                    if (!isset($ret->unreachedWaypointNames) || sizeof($ret->unreachedWaypointNumbers) == 0) {

                        if (!isset($ret->errorCode)) {
                            $diary->ViamenteFinalize($this->session->SQLSelectedDate, $this->user->ServiceProviderID);
                        }
                    }
                }
                if (isset($ret->reqID)) {
                    $diary->SaveMapUUID($ret->reqID, $this->user->ServiceProviderID, $this->session->SQLSelectedDate);
                }

                if (isset($ret->routes)) {


                    $diary->updateEngineerWorkload($ret->routes, $this->session->SQLSelectedDate, $this->user->ServiceProviderID);
                }

                if (isset($ret->routes)) {
                    $diary->removeBreaks($this->session->SQLSelectedDate, $this->user->ServiceProviderID, false);
                    foreach ($ret->routes as $r) {
                        foreach ($r->steps as $e)
                            if (isset($e->driveBreaks)) {
                                $apid = explode(' ', $e->waypointName);
                                $apid = $apid[sizeof($apid) - 1];
                                foreach ($e->driveBreaks as $w) {
                                    if ($apid * 1 == $apid) {
                                        $diary->addAppBreak($apid, $w->breakTimeSec, $w->durationSec);
                                    }
                                }
                                // 
                            }
                    }
                }
                if (isset($ret->unreachedWaypointNames) && sizeof($ret->unreachedWaypointNumbers) > 0) {

                    $diary->setUnreached($ret->unreachedWaypointNames);
                    $unreached = $diary->getUnreached($this->user->ServiceProviderID);
                    $this->sendEmailAction(1, $unreached);
                }

                if (isset($ret->errorCode) && $ret->errorCode == "-200") {
                    $this->smarty->assign('type', 11);
                    $this->smarty->display("diary/diaryEndMessage.tpl");
                    $this->sendEmailAction(6);
                } elseif (isset($ret->errorCode)) {
                    $this->smarty->assign('type', 12);
                    $this->smarty->assign('ret', $ret->errorDescription);
                    $this->smarty->display("diary/diaryEndMessage.tpl");
                } else {
                    if (isset($ret->unreachedWaypointNames) && sizeof($ret->unreachedWaypointNumbers) > 0 && $args['defaultRet'] == 2) {
                        if ($diary->getErrorApp($this->user->ServiceProviderID, 0, $this->session->SQLSelectedDate)) {
                            $this->smarty->assign('type', 14);
                            $this->smarty->display("diary/diaryEndMessage.tpl");
                        } else {
                            $this->smarty->assign('type', 13);
                            $this->smarty->display("diary/diaryEndMessage.tpl");
                        }
                    } else {
                        if ($this->session->ActionType != 6) {
                            $diary = $this->loadModel('Diary');
                            $apptIdsArrs = $diary->getDayAppointments($this->user->ServiceProviderID,$this->session->SQLSelectedDate);
                            foreach($apptIdsArrs as $apptIdsArr)
                            {
                                $apptIdsArray[] = $apptIdsArr['AppointmentID'];
                            }
                            $apptIdsStr = implode(", ",$apptIdsArray);
                            $this->smarty->assign('apptId', $apptIdsStr);
                            $response = $this->EndDayAction($args);
                            if (isset($response)) {
                                if (isset($response['ResponseCode'])) {
                                    if ($response['ResponseCode'] == "SC0001") {

                                        $this->smarty->assign('ret', $args['defaultRet']);
                                        $this->smarty->assign('type', 6);
                                        $this->smarty->display("diary/diaryEndMessage.tpl");
                                    } else {
                                        $this->smarty->assign('type', 2);
                                        $this->smarty->assign('response', $response);
                                        $this->smarty->display("diary/diaryEndMessage.tpl");
                                    }
                                } else {
                                    print_r($response);
                                }

                                if ($this->session->ActionType != 5) {
                                    
                                }
                            }
                        }
                    }
                }
                if (isset($args['defaultRet'])) {


                    $this->defaultAction($args);
                }
            } else {
                $this->defaultAction($args);
            }
        } else {
            $this->defaultAction($args);
        }
    }

    public function viamenteUpdateAction($args, $repType = null, $apID = null) {
        if ($this->session->SPInfo['DiaryType'] == "FullViamente") {
            if ($this->session->SQLSelectedDate <= date("Y-m-d", strtotime("+6 day"))) {


                $this->checkSA($args);
                $api_viamente = $this->loadModel('APIViamente');
                $diary = $this->loadModel('Diary');


                $engineers = $diary->getAssignedEngineers($this->session->SQLSelectedDate, $this->session->JobPostCode, $this->user->ServiceProviderID);
                


//        for($i=0;$i<sizeof($engineers);$i++){
//            $engineers[$i];
//           $api_viamente->ProcessEngineer($engineers[$i], 'create');
//       }
                // $api_viamente->ProcessEngineer(3, 'create');
                $engID = false;
                if ($this->session->SPInfo['ViamenteRunType'] == "AllEngineers") {
                    $ret = $api_viamente->OptimiseRoute($this->user->ServiceProviderID, $this->session->SQLSelectedDate, $engineers);
                    $workload = "all";
                } else {
                    $workload = "single";
                    if ($apID != null) {
                        $engID = $diary->getEngineerIDFromAPP($apID);
                    }
                    if ($engID && $engID * 1 == $engID && $engID != "") {
                        if ($engID != -1) {
                            $ret = $api_viamente->OptimiseRouteIndividualEngineers($this->user->ServiceProviderID, $this->session->SQLSelectedDate, array($engID));
                        }
                    } else {

                        $ret = $api_viamente->OptimiseRoute($this->user->ServiceProviderID, $this->session->SQLSelectedDate, $engineers);
                        $workload = "all";
                    }
                }
                $erroApp = $diary->getErrorApp($this->user->ServiceProviderID);
                //if($erroApp){
                //    $this->sendEmailAction(2,$erroApp);
                //   }
                if (isset($ret->routes)) {

                    $diary->updateEngineerWorkload($ret->routes, $this->session->SQLSelectedDate, $this->user->ServiceProviderID, $workload);
                }
                if (isset($ret->routes)) {
                    $diary->removeBreaks($this->session->SQLSelectedDate, $this->user->ServiceProviderID, $engID);
                    foreach ($ret->routes as $r) {
                        foreach ($r->steps as $e)
                            if (isset($e->driveBreaks)) {
                                $apid = explode(' ', $e->waypointName);
                                $apid = $apid[sizeof($apid) - 1];
                                foreach ($e->driveBreaks as $w) {
                                    if ($apid * 1 == $apid) {
                                        $diary->addAppBreak($apid, $w->breakTimeSec, $w->durationSec);
                                    }
                                }
                                // 
                            }
                    }
                }


                if (isset($ret->reqID)) {
                    $diary->SaveMapUUID($ret->reqID, $this->user->ServiceProviderID, $this->session->SQLSelectedDate);
                }


                if (isset($ret->unreachedWaypointNumbers) && sizeof($ret->unreachedWaypointNumbers) > 0) {

                    $responseUnreached = $diary->setUnreached($ret->unreachedWaypointNames, $apID);

                    if ($responseUnreached) {


                        return "YES";
                    } else {
                        return "NO";
                    }
                } else {
                    return "NO";
                }
            } else {
                return "NO";
            }
            if (isset($args['defaultRet'])) {
                $this->defaultAction($args);
            }
        } else {
            if (isset($args['defaultRet'])) {
                $this->defaultAction($args);
            }
        }
    }

    public function EndDayAction($args) {
        $this->checkSA($args);
        $diary = $this->loadModel('Diary');
        $apiA = $this->loadModel('APIAppointments');
        // echo $this->session->SQLSelectedDate."<br>";
        //  echo $this->user->ServiceProviderID."<br>";
        $resp = $apiA->RoutedAppointments($this->session->SQLSelectedDate, $this->user->ServiceProviderID);
        return $resp;
    }

    public function addresslookupAction($args) {
        $this->checkSA($args);
        $firstArg = isset($args[0]) ? $args[0] : '';

        $postcode = $this->loadModel('Postcode');

        //  $this->smarty->assign('page', $this->messages->getPage('AddressLookup'));

        $addresses = $postcode->getAddress($_POST['pk']);

        foreach ($addresses as $ak => $av) {

            $add_str = '';


            if ($addresses[$ak][4] || $addresses[$ak][6] || $addresses[$ak][7]) {
                $add_str = trim($addresses[$ak][4] . " " . $addresses[$ak][6] . " " . $addresses[$ak][7]);
            }


            if ($addresses[$ak][5] || $addresses[$ak][0]) {
                if ($add_str) {
                    $add_str .= ", ";
                }
                $add_str .= trim($addresses[$ak][5] . ' ' . $addresses[$ak][0]);
            }


            if ($addresses[$ak][2]) {
                if ($add_str) {
                    $add_str .= ", ";
                }
                $add_str .= $addresses[$ak][2];
            }

            if ($addresses[$ak][3]) {
                if ($add_str) {
                    $add_str .= ", ";
                }
                $add_str .= $addresses[$ak][3];
            }

            $addresses[$ak][11] = $add_str;
        }

        //if ($this->debug) $this->log(var_export($addresses,true));


        if (isset($addresses[0][0]) && $addresses[0][0] == "Wrong Post Code") {
            echo "1";
        } else {
            echo"1";
        }
    }

    public function ViamenteMapAction($args) {
        $this->checkSA($args);
        $diary = $this->loadModel('Diary');
        if (!isset($_POST['date']) || $_POST['date'] == "") {
            echo $uuid = $diary->getUUID($this->session->SQLSelectedDate, $this->user->ServiceProviderID);
        } else {
            $tmp = explode("/", $_POST['date']);
            $date = $tmp[2] . "-" . $tmp[1] . "-" . $tmp[0];
            echo $uuid = $diary->getUUID($date, $this->user->ServiceProviderID);
        }
        // echo $uuid="c22a5db4-b59b-4018-928c-c4813b07b188";
    }

    public function correctPostcode($postcode) {
        if (substr_count($postcode, ' ') == 0) {
            return $postcode;
        }
    }

    public function addAppointmentForceReasonSetAction($args) {
        $this->session->force_reason = $_POST['reason'];

        $this->showPopUpAction($args);
    }

    public function editAppointmentSamsungReasonSetAction($args) {
        $this->session->samsung_reason = $_POST['reason'];
    }

    public function menuInsertAppointmentAction($args) {
        $this->checkSA($args);
        $dayTMP = explode(' ', $_POST['day']);
        $day = $dayTMP[1];
        $slot = $dayTMP[2];
        // die($day."<-> ".$slot);
        $this->session->SQLSelectedDate = $day;
        $this->session->DiarySelectedDate = $day;

        if (!isset($_POST['slot'])) {
            $this->session->DiarySelectedDateSlot = $slot;
        } else {
            $this->session->DiarySelectedDateSlot = "ANY";
        }
        if (!isset($this->session->force_reason)) {
            $this->session->force_reason = null;
        }

        if ($this->session->force == "yes") {
            $this->smarty->display('diary/diaryForceAddReason.tpl');
            return;
        }
        /// echo $_POST['slot'];
        $this->showPopUpAction($args);
    }

    public function removeUnsyncAppAction($args) {
        $this->checkSA($args);
        $diary = $this->loadModel('Diary');
        if ($this->session->ActionType != 6) {
            $diary->removeUnsyncApp($this->user->ServiceProviderID);
        }
    }

    public function notReachedAction($args) {
        $this->checkSA($args);
        $diary = $this->loadModel('Diary');
        if ($_POST['ans'] == 1) {
            $this->insertAppointmentAction($args, true, $_POST['delete']);
        } else {
            $diary->deleteAppointment($_POST['delete'], $this->user->ServiceProviderID);
            $this->defaultAction($args);
        }
    }

    public function TestRoutedAppointmentsAction($args) {
        $date = $args[0];
        $eid = $args[1];

        $api_app = $this->loadModel('APIAppointments');

        $response = $api_app->TestRoutedAppointments($date, $eid, 8);

        return($response);
    }

    public function tableUpdateAction() {
        $diary = $this->loadModel('Diary');
        $ret = $diary->updateDuration($_POST);
        echo $_POST['value'];
    }

    //check superviser pass, for now hardocodet need table
    public function checkPassAction($args) {

        $this->checkSA($args);
        $diary = $this->loadModel('Diary');
        $pass = $diary->getDiarySupervisorPass($this->user->ServiceProviderID);
        //if ($this->debug) $this->log($pass." ".$_POST['pass'],'pass');
        if (trim($_POST['pass']) == trim($pass)) {
            // if ($this->debug) $this->log($pass."ok".$_POST['pass'],'pass');
            echo 12;
            $this->session->force = "yes"; //force ignore all warnings on appointment insert
        } else {
            echo "fail";
        }
    }

    public function updateJobDetailsAction($a) {
        $data = $a;
        $diary = $this->loadModel('Diary');
        $users = $this->loadModel('Users');
        if (isset($data['name']) && isset($data['password'])) {
            $spID = $diary->getSPID($data['name'], $users->encrypt($data['password']));

            $diary->updateJobDetails($data, $spID);
            if ($this->debug)
                $this->log($a, 'UpdateJobDetails_');
            return true;
        }
    }

    public function wallboardAction($args) {
        $this->checkSA($args);
        $diary = $this->loadModel('Diary');
        $data = $diary->getDiaryWallboardInfo($this->user->ServiceProviderID);
        $this->smarty->assign('data', $data);
        $this->smarty->display('WallBoard.tpl');
    }

    public function displaySummary($spID) {
        $diary = $this->loadModel('Diary');
        $summary = $diary->getSummary($spID, $this->session->SPInfo['DiaryType']);
        $this->smarty->assign('summary', $summary);
    }

    public function cleanDatabaseAction() {
        $diary = $this->loadModel('Diary');
        $diary->cleanDatabase();
    }

    public function showFullDiaryAction() {
        if ($this->session->showFullDiary == 0) {
            $this->session->showFullDiary = 1;
        } else {
            $this->session->showFullDiary = 0;
        }
    }

    public function gmapAction() {
        $diary = $this->loadModel('Diary');
        // $data=$diary->getDiaryWallboardInfo($this->user->ServiceProviderID);
        // $this->smarty->assign('data',$data);
        $this->smarty->display('diary/diaryGmap3.tpl');
    }

    public function showGoogleMAPAction() {
        $this->smarty->display('diary/diaryAddAppointmentMap.tpl');
    }

    public function editGeoTagAction() {
        $this->smarty->assign('pc', $_POST['pc']);
        $this->smarty->display('diary/diaryGmap3.tpl');
    }

    public function updateGeoTagAction($args) {
        $this->checkSA($args);
        $diary = $this->loadModel('Diary');

        $ret = $diary->updateGeoTag($_POST, $this->user->ServiceProviderID);
        $geotags = $diary->getGeoTag($_POST['postcode'], $this->user->ServiceProviderID, true);
        if ($ret == "deleted") {

            echo json_encode($geotags);
        }
        //$this->defaultAction($args);
    }

    public function printReport1Action($args) {
        $this->checkSA($args);
        $diary = $this->loadModel('Diary');
        $engs = explode(',', $args['tEng']);
        $args['tEng'] = trim($args['tEng'], ',');
        $date = $args[2] . "-" . $args[1] . "-" . $args[0];

        $data = $diary->printReport1($args['tEng'], $date, $this->user->ServiceProviderID, $this->session->SPInfo['DiaryType']);


        $this->smarty->assign('diaryType', $this->session->SPInfo['DiaryType']);
        $this->smarty->assign('data', $data);
        $html = $this->smarty->fetch('diary/diaryEngineerReport.tpl');

        include(APPLICATION_PATH . '/../libraries/MPDF56/mpdf.php');
        $mpdf = new mPDF();
        $mpdf->WriteHTML($html);
        $mpdf->Output();
        exit;
    }

    public function exportDiaryDataAction($args) {
        $this->checkSA($args);
        $diary = $this->loadModel('Diary');





        if (isset($_POST['type'])) {
            $type = $_POST['type'];
            $dateFrom = $_POST['dateFrom'];
            $dateTo = $_POST['dateTo'];
        }

        if (isset($args['type'])) {
            $type = $args['type'];
            $dateFrom = $args['dateFrom'];
            $dateTo = $args['dateTo'];
        }
        $engs = "";
        for ($i = 0; $i < sizeof($_POST['engIDExport']); $i++) {
            $engs.=$_POST['engIDExport'][$i] . ",";
        }
        $engs = trim($engs, ',');

        $df = str_replace('/', '-', $dateFrom);
        $dt = str_replace('/', '-', $dateTo);


        $qr = $diary->getExportData($type, $dateFrom, $dateTo, $this->user->ServiceProviderID, $engs);
        switch ($type) {
            case 1:$dbtable = "Engineer-Summary";
                break;
            case 2:$dbtable = "Appointments-Summary";
                break;
            default:$dbtable = "data-export";
                break;
        }

// Ok now we are going to send some headers so that this 
// thing that we are going make comes out of browser
// as an xls file.
// 
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");

//this line is important its makes the file name
        header("Content-Disposition: attachment;filename=skyline-diary-export-" . $dbtable . "_" . $df . "_$dt.csv ");

        header("Content-Transfer-Encoding: binary ");


// these will be used for keeping things in order.
        $col = 0;
        $row = 0;

// This tells us that we are on the first row
        $first = true;
//$separator="	";
        $separator = "	";

        $expa = "";
        while ($qrow = mysql_fetch_assoc($qr)) {

            // Ok we are on the first row
            // lets make some headers of sorts
            if ($first) {

                foreach ($qrow as $k => $v) {
                    $field = $k;
                    // take the key and make label
                    // make it uppper case and replace _ with ' '
                    if (preg_match('/\\r|\\n|,|"/', $k)) {
                        $field = '"' . str_replace('"', '""', $k) . '"';
                        $field = '"' . str_replace('\r', '', $k) . '"';
                        $field = '"' . str_replace('\n', '', $k) . '"';
                        $field = preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s', '', $field);
                    }
                    $expa.= $field . $separator;
                    if ($this->debug)
                        $this->log($field . $separator, 'export_CSV_log_');
                    $col++;
                }

                // prepare for the first real data row
                $col = 0;
                $row++;
                $first = false;
                $expa.= "\r\n";
            }
            // go through the data
            foreach ($qrow as $k => $v) {
                $field = $v;
                // take the key and make label
                // make it uppper case and replace _ with ' '
                if (preg_match('/\\r|\\n|,|"/', $v)) {
                    $field = '"' . str_replace('"', '""', $k) . '"';
                    $field = '"' . str_replace('\r', '', $k) . '"';
                    $field = '"' . str_replace('\n', '', $k) . '"';
                    $field = preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s', '', $field);
                }
                $expa.= $field . $separator;
            }
            $expa.= "\r\n";
        }
        if ($this->debug)
            $this->log($expa, 'export_CSV_log_');
        echo $expa;
//$this->xlsEOF();
        exit();
    }

//public function exportDiaryDataAction($args){
//    $this->checkSA($args);
//      $diary=$this->loadModel('Diary');
// 
//
//
//
//
//if(isset($_POST['type'])){
//    $type=$_POST['type'];
//    $dateFrom=$_POST['dateFrom'];
//    $dateTo=$_POST['dateTo'];
//}
// 
//if(isset($args['type'])){
//    $type=$args['type'];
//    $dateFrom=$args['dateFrom'];
//    $dateTo=$args['dateTo'];
// }
// $engs="";
// for($i=0;$i<sizeof($_POST['engIDExport']);$i++){
//     $engs.=$_POST['engIDExport'][$i].",";
// }
// $engs=trim($engs,',');
// 
//$df=  str_replace('/', '-', $dateFrom);
//$dt=  str_replace('/', '-', $dateTo);
//
//
//$report=$diary->getExportData($type,$dateFrom,$dateTo,$this->user->ServiceProviderID,$engs);
//switch ($type){
//    case 1:$dbtable="Engineer-Summary"; break;
//    case 2:$dbtable="Appointments-Summary"; break;
//    default:$dbtable="data-export"; break;
//}
//
//require_once("libraries/PHPExcel/PHPExcel.php");
//require_once("libraries/PHPExcel/PHPExcel/Writer/Excel2007.php");
//$helperModel = $this->loadModel("Helpers");
//$excel = new PHPExcel();
//	
//	$excel->setActiveSheetIndex(0);
//	
//	
//
//	$excel->getDefaultStyle()->getFont()->setSize(10); 
//	
//	
//	if(count($report) > 0) {
//	    
//	    $excel->getActiveSheet()->SetCellValue("A1", "No jobs found.");
//	    
//	} else {
//           $top = 10;
//	    $left = 1;
//	    
//	    $header = array_keys($report);
//
//	    $colCount = count($header) - 1;
//
//	    //header styling
//	    $headerStyle = [
//		'fill' => [
//		    'type' => PHPExcel_Style_Fill::FILL_SOLID,
//		    'color' => ['rgb'=>'0070C0']
//		],
//		'font' => [
//		    'bold' => true,
//		    'color' => ['rgb'=>'FFFFFF']
//		],
//		'borders' => [
//		    'outline' => [
//			'style' => PHPExcel_Style_Border::BORDER_THIN,
//			'color' => ['rgb' => 'D0D0D0']
//		    ]
//		],
//		"alignment" => [
//		    "horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
//		    "vertical" => PHPExcel_Style_Alignment::VERTICAL_CENTER
//		]
//	    ]; 
//            
//             //table headers
//	    $colNum = $left;
//	    foreach($header as $name) {
//		if($name == "TAT") {
//		    continue;
//		}
//		$colName = $helperModel->getExcelColName($colNum);
//		$excel->getActiveSheet()->getStyle($colName . ($top - 1))->applyFromArray($headerStyle);
//		$colNum++;
//	    }
//	    $excel->getActiveSheet()->SetCellValue("B9", "Repair Ref.");
//	    $excel->getActiveSheet()->SetCellValue("C9", "Customer Surname");
//	    $excel->getActiveSheet()->SetCellValue("D9", "Customer Title & First Name");
//	    $excel->getActiveSheet()->SetCellValue("E9", "Manufacturer");
//	    $excel->getActiveSheet()->SetCellValue("F9", "Product Type");
//	    $excel->getActiveSheet()->SetCellValue("G9", "Model No.");
//	    $excel->getActiveSheet()->SetCellValue("H9", "Serial No.");
//	    $excel->getActiveSheet()->SetCellValue("I9", "IMEI No.");
//	    $excel->getActiveSheet()->SetCellValue("J9", "Referral No.");
//	    $excel->getActiveSheet()->SetCellValue("K9", "Service Type");
//	    $excel->getActiveSheet()->SetCellValue("L9", "Date Booked/Created");
//	    $excel->getActiveSheet()->SetCellValue("M9", "Date Completed/Closed");
//	    $excel->getActiveSheet()->SetCellValue("N9", "TAT Days");
//	    $excel->getActiveSheet()->SetCellValue("O9", "Current Status");
//	    $excel->getActiveSheet()->SetCellValue("P9", "Service Request");
//	    $excel->getActiveSheet()->SetCellValue("Q9", "Service Report");
//	    $excel->getActiveSheet()->SetCellValue("S9", "Software Update");
//	    $excel->getActiveSheet()->SetCellValue("T9", "Education");
//	    $excel->getActiveSheet()->SetCellValue("U9", "Setup");
//	    $excel->getActiveSheet()->SetCellValue("V9", "Repair Required");
//	    $excel->getActiveSheet()->SetCellValue("W9", "Repair Center");
//	    $excel->getActiveSheet()->SetCellValue("X9", "Courier");
//	    $excel->getActiveSheet()->SetCellValue("Y9", "Consignment No.");
//	    $excel->getActiveSheet()->SetCellValue("Z9", "Labour Charge");
//	    $excel->getActiveSheet()->SetCellValue("AA9", "Parts Charge");
//	    $excel->getActiveSheet()->SetCellValue("AB9", "Delivery Charge");
//	    $excel->getActiveSheet()->SetCellValue("AC9", "VAT");
//	    $excel->getActiveSheet()->SetCellValue("AD9", "Total");
//            //grey row
//	    $greyRow = [
//		'fill' => [
//		    'type' => PHPExcel_Style_Fill::FILL_SOLID,
//		    'color' => ['rgb'=>'E8E8E8']
//		],
//		'borders' => [
//		    'outline' => [
//			'style' => PHPExcel_Style_Border::BORDER_THIN,
//			'color' => ['rgb' => 'D0D0D0']
//		    ]
//		]
//	    ];
//
//	    //standard row
//	    $whiteRow = [
//		'borders' => [
//		    'outline' => [
//			'style' => PHPExcel_Style_Border::BORDER_THIN,
//			'color' => ['rgb' => 'D0D0D0']
//		    ]
//		]
//	    ];
//	    
//	    //grey font
//	    $greyFont = [
//		'font' => [
//		    'color' => ['rgb'=>'737373']
//		]
//	    ];
//	    
//	    //blue cell
//	    $blueCell = [
//		'fill' => [
//		    'type' => PHPExcel_Style_Fill::FILL_SOLID,
//		    'color' => ['rgb'=>'0070C0']
//		]
//	    ];
//
//	    //alignment
//	    $excel->getActiveSheet()->getRowDimension("2")->setRowHeight(20);
//	    $excel->getActiveSheet()->getRowDimension("9")->setRowHeight(25);
//	    $excel->getActiveSheet()->getColumnDimension("A")->setWidth(3);
//	    $excel->getActiveSheet()->getStyle("R:V")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
//
//	    //title
//	    $titleStyle = [
//		"font" => [
//		    "bold" => true,
//		    "size" => 16
//		],
//		"alignment" => [
//		    "horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
//		    "vertical" => PHPExcel_Style_Alignment::VERTICAL_CENTER
//		]
//	    ];	  
//	    $excel->getActiveSheet()->mergeCells("B2:D2");	    
//	    $excel->getActiveSheet()->SetCellValue("B2", "Transaction Report");
//	    $excel->getActiveSheet()->getStyle("B2")->applyFromArray($titleStyle);
//            
//             //date from
//	    if($dateFrom) {
//		$excel->getActiveSheet()->SetCellValue("B6", "Date From:");
//		$excel->getActiveSheet()->SetCellValue("C6", date("Y-m-d",strtotime($dateFrom)));
//	    }
//	    
//	    //date to
//	    if($dateTo) {
//		$excel->getActiveSheet()->SetCellValue("B7", "Date To:");
//		$excel->getActiveSheet()->SetCellValue("C7", date("Y-m-d",strtotime($dateTo)));
//	    }
//	    
//	    //date created
//	    $excel->getActiveSheet()->SetCellValue("B8", "Date Created:");
//	    $excel->getActiveSheet()->SetCellValue("C8", date("Y-m-d"));
//	    
//	    //service action
//	    $excel->getActiveSheet()->mergeCells("S8:V8");	    
//	    $serviceStyle = [
//		'fill' => [
//		    'type' => PHPExcel_Style_Fill::FILL_SOLID,
//		    'color' => ['rgb'=>'E8E8E8']
//		],
//		'borders' => [
//		    'outline' => [
//			'style' => PHPExcel_Style_Border::BORDER_THIN,
//			'color' => ['rgb' => 'D0D0D0']
//		    ]
//		],
//		"font" => [
//		    "bold" => true,
//		    "size" => 11
//		],
//		"alignment" => [
//		    "horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
//		    "vertical" => PHPExcel_Style_Alignment::VERTICAL_CENTER
//		]
//	    ];
//	    $excel->getActiveSheet()->getStyle("S8:V8")->applyFromArray($serviceStyle);
//	    $excel->getActiveSheet()->SetCellValue("S8", "Service Action");
//	    
//	    $softwareTotal = 0;
//	    $educationTotal = 0;
//	    $setupTotal = 0;
//	    $repairTotal = 0;
//            
//        }
//        
//  
//        
//        
//	
//	
//        header("Pragma: public");
//    header("Expires: 0");
//    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
//    header("Content-Type: application/force-download");
//    header("Content-Type: application/octet-stream");
//    header("Content-Type: application/download");;
//    header("Content-Disposition: attachment;filename=test.xls");
//    header("Content-Transfer-Encoding: binary ");
//	$excelWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
//        $excelWriter->setOffice2003Compatibility(true);
//	$excelWriter->save('php://output'); 
//	exit();
//exit();
//}



    public function showDataExportAction($args) {
        $this->checkSA($args);
        if (!isset($this->session->SPInfo['DiaryType'])) {
            $aloc = "AllocationOnly";
        } else {
            $aloc = $this->session->SPInfo['DiaryType'];
        }
        $diary = $this->loadModel('Diary');
        $engineerList = $diary->getEngineersList($this->user->ServiceProviderID);
        $this->smarty->assign('diaryType', $aloc);
        $this->smarty->assign('engineerList', $engineerList);
        $this->smarty->display('diary/diaryDataExport.tpl');
    }

    public function getCalendarInfoTableAction($args) {
        $this->checkSA($args);
        $diary = $this->loadModel('Diary');
        if (isset($_POST['date'])) {
            $date = $_POST['date'];
            $data = $diary->getCalendarInfoTable($date, $this->user->ServiceProviderID);
            echo json_encode($data);
        }
    }

    public function specifyEngineerBulkAction($args) {
        $this->checkSA($args);
        $diary = $this->loadModel('Diary');
        $apiV = $this->loadModel('APIViamente');
        $apiA = $this->loadModel('APIAppointments');

        $_POST['apIds'] = trim($_POST['apIds'], ',');
        $apiids = explode(",", $_POST['apIds']);
        $diary->specifyEngineerBulk($_POST['apIds'], $_POST['engSel']);
        if ($diary->checkFinalized($this->user->ServiceProviderID, $this->session->SQLSelectedDate, $this->session->ServiceProviderSkillsetID) === false) {
            if ($this->session->SPInfo['ViamenteRunType'] == "AllEngineers") {
                $this->viamenteUpdateAction($args);
            } else {
                $this->viamenteUpdateAction($args, false, $apiids[0]);
            }
            //   $this->viamenteUpdateAction($args);
        }
        $response = $apiA->RoutedAppointments($this->session->SQLSelectedDate, $this->user->ServiceProviderID);
        if (isset($response)) {
            if (isset($response['ResponseCode'])) {
                $this->smarty->assign('mode', 1);
                if ($response['ResponseCode'] == "SC0001") {
                    $this->smarty->assign('type', 8);
                    $this->smarty->display("diary/diaryEndMessage.tpl");
                } else {
                    $this->smarty->assign('type', 2);
                    $this->smarty->assign('response', $response);
                    echo $this->smarty->fetch("diary/diaryEndMessage.tpl");
                }
            } else {
                $this->smarty->assign('type', 7);
                $this->smarty->assign('response', $response);
                echo $this->smarty->fetch("diary/diaryEndMessage.tpl");
            }
        }
    }

    public function unspecifyEngineerBulkAction($args) {
        $this->checkSA($args);
        $diary = $this->loadModel('Diary');
        $apiV = $this->loadModel('APIViamente');
        $apiA = $this->loadModel('APIAppointments');

        $_POST['apIds'] = trim($_POST['apIds'], ',');

        $diary->unspecifyEngineerBulk($_POST['apIds'], $_POST['engSel']);
        if ($diary->checkFinalized($this->user->ServiceProviderID, $this->session->SQLSelectedDate, $this->session->ServiceProviderSkillsetID) === false) {
            if ($this->session->SPInfo['ViamenteRunType'] == "AllEngineers") {
                $this->viamenteUpdateAction($args);
            }
        }
        $response = $apiA->RoutedAppointments($this->session->SQLSelectedDate, $this->user->ServiceProviderID);
        if (isset($response)) {
            if (isset($response['ResponseCode'])) {
                $this->smarty->assign('mode', 1);
                if ($response['ResponseCode'] == "SC0001") {
                    $this->smarty->assign('type', 8);
                    $this->smarty->display("diary/diaryEndMessage.tpl");
                } else {
                    $this->smarty->assign('type', 2);
                    $this->smarty->assign('response', $response);
                    echo $this->smarty->fetch("diary/diaryEndMessage.tpl");
                }
            } else {
                $this->smarty->assign('type', 7);
                $this->smarty->assign('response', $response);
                echo $this->smarty->fetch("diary/diaryEndMessage.tpl");
            }
        }
    }

    public function markAsComplitedAction($args) {
        $this->checkSA($args);
        $diary = $this->loadModel('Diary');
        $_POST['apIds'] = trim($_POST['apIds'], ',');
        $diary->markAsComplited($_POST['apIds'], $_POST['type']);
        $this->smarty->assign('mode', 1);
        $this->smarty->assign('type', 8);
        $this->smarty->display("diary/diaryEndMessage.tpl");
    }

    public function showPrintMenuAction($args) {
        $this->checkSA($args);
        $diary = $this->loadModel('Diary');
        $engineerList = $diary->getEngineersList($this->user->ServiceProviderID);

        $this->smarty->assign('engineerList', $engineerList);
        $this->smarty->assign('diaryType', $this->session->SPInfo['DiaryType']);
        $this->smarty->assign('date', $this->session->SQLSelectedDate);
        $this->smarty->assign('viamenteServer', $this->config['Viamente']['ViamenteApiUrl']);
        $this->smarty->display('diary/diaryDataPrint.tpl');
    }

    public function insertFinalizedAction($args) {
        $this->checkSA($args);
        $diary = $this->loadModel('Diary');
        $apiA = $this->loadModel('APIAppointments');
        $response = $apiA->PutAppointmentDetails($_POST['appID']);

        $erroApp = $diary->getForceAppointmentDetails($_POST['appID']);
        if ($erroApp) {
            $this->sendEmailAction(3, $erroApp);
        }
        if (isset($response)) {
            if (isset($response['ResponseCode'])) {
                if ($response['ResponseCode'] == "SC0001") {
                    $this->smarty->assign('type', 1);
                    $this->smarty->display("diary/diaryEndMessage.tpl");
                } else {
                    $this->smarty->assign('type', 2);
                    $this->smarty->assign('response', $response);
                    $this->smarty->display("diary/diaryEndMessage.tpl");
                }
            } else {
                $this->smarty->assign('type', 7);
                $this->smarty->assign('response', $response);
                $this->smarty->display("diary/diaryEndMessage.tpl");
            }

            die();
        }
    }

    public function sendEmailAction($type = null, $data = null, $mailto = null) {
        $diary = $this->loadModel('Diary');
        $email_model = $this->loadModel('Email');
        $MailFromName = "Skyline";
        $MailFromEmail = "diarysupport@skylinecms.com";

        if (!$mailto) {
            $mailto = $diary->getSuperviserEmail($this->user->ServiceProviderID);
        }
        $ReplyEmail = "diarysupport@skylinecms.com";
        $ReplyName = "Skyline Diary Support";
        if ($data) {
            $this->smarty->assign('data', $data);
        }
        if ($type) {
            $this->smarty->assign('type', $type);
        }

        if (isset($_POST['type'])) {
            $type = $_POST['type'];
        }
        if (isset($_POST['data'])) {
            $this->smarty->assign('data', $_POST['data']);
        }
        if (isset($_POST['name'])) {
            $mailto = $diary->getEngEmailFromName($_POST['name'], $this->user->ServiceProviderID);
        }
        if (isset($_POST['date'])) {
            $date = $_POST['date'];
        } else {
            $date = "";
        }
        $spAcr = $this->session->SPInfo['Acronym'];
        $this->smarty->assign('date', $date);
        switch ($type) {
            case 1:
                $this->smarty->assign('title', 'IMPORTANT NOTICE - UNREACHABLE APPOINTMENTS');
                $this->smarty->assign('errorText', 'The following appointments are unreachable and cannot be optimised');
                $MailSubject = "DIARY - UNREACHABLE APPOINTMENTS (" . $spAcr . ")";
                $MailBody = $this->smarty->fetch('diary/emails/unreachedEmail.tpl');
                break;
            case 2:
                $this->smarty->assign('title', 'IMPORTANT NOTICE - INVALID POSTCODE OR TIME WINDOW');
                $this->smarty->assign('errorText', 'The following appointments have invalid postcode or time window and cannot be optimised');
                $MailSubject = "DIARY - INVALID POSTCODE OR TIME WINDOW (" . $spAcr . ")";
                $MailBody = $this->smarty->fetch('diary/emails/unreachedEmail.tpl');
                break;
            case 3:
                $this->smarty->assign('title', 'FORCE ADD APPOINTMENT NOTIFICATION');
                $this->smarty->assign('errorText', 'The following appointment has been force added and may not be reachable:');
                $MailSubject = "DIARY - FORCE ADD APPOINTMENT (" . $spAcr . ")";
                $MailBody = $this->smarty->fetch('diary/emails/unreachedEmail.tpl');
                break;
            case 4:
                $this->smarty->assign('title', 'IMPORTANT NOTICE - RE-BOOKING AN APPOINTMENT AFTER DAY FINALIZED');
                $this->smarty->assign('errorText', 'The following appointment have been force re-booked and may not be reachable');
                $MailSubject = "DIARY - RE-BOOKING AN APPOINTMENT AFTER DAY FINALIZED (" . $spAcr . ")";
                $MailBody = $this->smarty->fetch('diary/emails/unreachedEmail.tpl');
                break;
            case 5:
                $this->smarty->assign('title', 'Your personal map on ' . $date);
                $MailSubject = "DIARY - Your personal map on $date";
                $MailBody = $this->smarty->fetch('diary/emails/viamenteMap.tpl');
                break;
            case 6:
                $this->smarty->assign('title', 'Viamente key error!');
                $MailSubject = "DIARY - Viamente key Provided is invalid or expired";
                $MailBody = $this->smarty->fetch('diary/emails/viamenteKey.tpl');
                break;
        }

        $PCCSadminEmail = $this->config['Diary']['PCCS_Diary_Admin_email'];

    $email_result = $email_model->Send(
                                       $MailFromName,
                                       $MailFromEmail,
                                       $mailto,
                                       $MailSubject,
                                       $MailBody,
				       $PCCSadminEmail,	    //cc email
                                       $ReplyEmail,
                                       $ReplyName,
                                       true
                                      );
                         if ($email_result)
                         {    
                           if ($this->debug) $this->log($mailto,'diary_emails_');
                            /* Mark the e-mail as sent */
                         } 
    }

    /**
     * sendDiaryEmailAction
     *  
     * Deal withs diary e-mails 
     * 
     * 
     * @param array $args   
     *                     
     * @return  nothing
     * 

     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     * 
     * ************************************************************************ */
    public function sendDiaryEmailAction($args) {

        $this->checkSA($args);

        //$this->smarty->assign( $job_model->current_record );

        $this->smarty->display('diaryEmail.tpl');
    }

    /**
     * getForceEngineerCounts
     *  
     * Deal withs getForceEngineerCount
     * 
     * 
     * @param array $args   
     *                     
     * @return  nothing
     * 

     * @author Andris 
     * 
     * ************************************************************************ */
    public function getForceEngineerCountAction($args) {
        $this->checkSA($args);
        $diary = $this->loadModel('Diary');
        if (!isset($_POST['slot'])) {
            $_POST['slot'] = "ANY";
        }
        if ($this->session->SPInfo['DiaryAllocationType'] != "GridMapping") {
            $geo = false;
        } else {
            $geo = $this->session->addressGeoTag;
        }
        $ret = $diary->getDiaryAllocationID($_POST['slot'], $this->session->ServiceProviderSkillsetID, $this->session->JobPostCode, $this->user->ServiceProviderID, false, false, true, $geo);

        echo json_encode($ret);
    }

    public function getEngWithAppAction($args) {
        $this->checkSA($args);
        $diary = $this->loadModel('Diary');

        $englist = $diary->getEngWithApp($this->user->ServiceProviderID, $_POST['date']);
        // for($i=0;$i<sizeof($englist);$)
        echo json_encode($englist);
    }

//emailEngAppList
    public function emailReport1Action($args) {
        print_r($args);
    }

    public function checkEmptyDayAction($args) {
        $this->checkSA($args);
        $diary = $this->loadModel('Diary');
        $numb = $diary->countAppDay($this->user->ServiceProviderID, $this->session->SQLSelectedDate);

        echo $numb;
    }

    public function checkCompletedAction() {
        $diary = $this->loadModel('Diary');
        if (isset($_POST['arr'])) {
            $numb = $diary->checkCompleted($_POST['arr']);
            if (sizeof($numb) > 0) {
                echo 1;
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }
    
    
    public function holidayDiaryAction($args) {
        
        $this->checkSA($args); 
        $diary = $this->loadModel('Diary');
        if (isset($args[0])) {
            
            $entrys = $diary->getHolidayEntrys($this->user->ServiceProviderID);
            $this->smarty->assign('getPastAbsentList', 'yes');
        } else {
            if(isset($this->session->SQLSelectedDate))
                $date = $this->session->SQLSelectedDate . " 00:00:00";
            else
                $date = date("Y-m-d") . " 00:00:00";
            $entrys = $diary->getHolidayEntrys($this->user->ServiceProviderID,$date);            
            $this->smarty->assign('getPastAbsentList', 'no');
        }
        $page = $this->messages->getPage('diaryHoliday', $this->lang);
        $this->smarty->assign('page', $page);
        $this->smarty->assign('entrys', $entrys);
        $this->smarty->display("diary/diaryHoliday.tpl");
        
    }
    

    public function showHolidayCardAction($args) {
        $this->checkSA($args);
        $diary = $this->loadModel('Diary');
        if (isset($_POST['update'])) {

            $data = $diary->getHolidayEdit($_POST['update']);
            $this->smarty->assign('data', $data);
        }
        $engineerList = $diary->getEngineersList($this->user->ServiceProviderID);
        $this->smarty->assign('engineerList', $engineerList);
        echo $this->smarty->fetch('diary/diaryHolidayCard.tpl');
    }
    
    /*
     * @author Dileep Bhimineni
     * 
     * Function to Validate a creating/updating Absent Lisr or Appointment
     * 
     * $params 
     *      NONE
     * 
     * Return 
     *      json encoded array of (Status and Message)
     * 
     * 
     */
    
    public function validateHolidayAppointmentAction() 
    {
        
        
        $startTime  = explode("/", $_POST['hStartDate']);
        $endTime    = explode("/", $_POST['hEndDate']);
        $startDate  = $startTime[2] . "-" . $startTime[1] . "-" . $startTime[0];
        $endDate    = $endTime[2] . "-" . $endTime[1] . "-" . $endTime[0];
        
        $startDateTime  = $startDate . " " . $_POST['hStartTime'] . ":00";
        $endDateTime    = $endDate . " " . $_POST['hEndTime'] . ":00";
        
        $id = $_POST['hEng'];
                
        $diary = $this->loadModel('Diary');
        $overlapAppointment = $diary->validateHolidayAppointment($id,$startDateTime,$endDateTime);
        $lockedEngineer     = $diary->validateEngineerLocked($id,$startDate,$endDateTime);        
        $count  = count($lockedEngineer);
        if($count == 0)
            $text = " no jobs";
        else if($count == 1)
            $text = $count." job";
        else if($count > 1)
            $text = $count." jobs";
        if ($overlapAppointment == "false") {
            echo json_encode(array(
                "status"    =>  "ERROR",
                "message"   =>  "This conflicts with a holiday that already exists in the absent diary, please review the previous entries for this engineer.\n\nPlease Note: If you type the engineers name in the search box you will see all of his entries"));
        } elseif ($count > 0) {
            echo json_encode(array(
                "status"    =>  "ERROR",
                "message"   =>  "This engineer has been manually specified to ". $text ." on " . $startTime[0]."/".$startTime[1]."/".$startTime[2]));        
        }else {
            echo json_encode(array(
                "status"    =>  "OK",
                "message"   =>  "Appointment Succeded"));
        }
        
    }

    public function showFaqCardAction($args) {
        $this->checkSA($args);
        $diary = $this->loadModel('Diary');
        if (isset($_POST['update'])) {

            $data = $diary->getFaqEdit($_POST['update']);
            $this->smarty->assign('data', $data);
        }
        $categoryList = $diary->getFaqCategoryList();
        $this->smarty->assign('categoryList', $categoryList);
        echo $this->smarty->fetch('diary/diaryFAQCard.tpl');
    }

    public function insertHolidayAction($args) {
        
        $this->checkSA($args);
        $diary = $this->loadModel('Diary');
        if (isset($_POST['id'])) {
            $diary->updateHoliday($_POST, $this->user->ServiceProviderID);
        } else {

            $diary->insertHoliday($_POST, $this->user->ServiceProviderID);
        }
        $api_viamente = $this->loadModel('APIViamente');

        $response = $api_viamente->ProcessEngineer($_POST['hEng'], 'update');

        $this->holidayDiaryAction($args);
    }

    public function insertFaqAction($args) {
        $this->checkSA($args);
        $diary = $this->loadModel('Diary');
        if (isset($_POST['id'])) {
            $diary->updateFaq($_POST);
        } else {

            $diary->insertFaq($_POST);
        }
        $this->faqAction($args);
    }

    public function addFaqCategoryAction($args) {
        $this->checkSA($args);
        $diary = $this->loadModel('Diary');
        $id = $diary->addFaqCategory($_POST['category']);
        echo $id;
    }

    public function deleteHolidayEntryAction($args) {
        $this->checkSA($args);
        $diary = $this->loadModel('Diary');
        $holidayEntryData = $diary->getHolidayEntry($args[0], $this->user->ServiceProviderID);
        if(!empty($holidayEntryData[0]['ServiceProviderEngineerID']))
        {
            $diary->deleteHolidayEntry($args[0], $this->user->ServiceProviderID);
            $api_viamente = $this->loadModel('APIViamente');
            $response = $api_viamente->ProcessEngineer($holidayEntryData[0]['ServiceProviderEngineerID'], 'update');
            $this->holidayDiaryAction($args);
        }
    }
    
    /*
     * @author Unknown
     * 
     * comments added by Dileep Bhimineni.
     * 
     * Updates Default Start time and End time based on slider selected time
     * 
     * @param $args - check for super user
     * 
     * @returns nothing
     */
    
    public function updateTimeSlidersAction($args) {
        
        
        $this->checkSA($args);
        $diary = $this->loadModel('Diary');
        $startDate  = $this->session->SQLSelectedDate . " " . gmdate("H:i:s", $_POST['s']);
        $endDate    = $this->session->SQLSelectedDate . " " . gmdate("H:i:s", $_POST['e']);
        $validateHolidayEntries = $diary->validateHolidayAppointment($_POST['id'],$startDate,$endDate);
        if ($validateHolidayEntries == 'true') {
            $diary->updateEngineerShift($_POST, $this->session->SQLSelectedDate);
            $apiV = $this->loadModel('APIViamente');
            $apiV->ProcessEngineer($_POST['id'], 'update');
        
        }        
    }

    public function reoptimizeAction($args) {
        $this->checkSA($args);
        if (isset($args['oneEng'])) {
            
        }
        $this->viamenteUpdateAction($args);
        $args['timeSliders'] = '1';
        $this->defaultAction($args);
    }

    public function multimapAction($args) {
        $this->checkSA($args);
        $diary = $this->loadModel('Diary');




        if (isset($this->session->idata)) {

            $jobTaged = null;
            if ($jobTaged) {
                $this->smarty->assign('appGeoTag', $jobTaged);
            }
            $d = $this->session->idata;
            if ($d['CollPostcode'] == "") {
                $this->smarty->assign('adress', $d['CustBuildingName'] . " " . $d['CustAddress1'] . " " . " " . $d['CustAddress2'] . " " . " " . $d['CustAddress3'] . " " . " " . $d['CustAddress4'] . " " . " " . $d['CustPostcode']);
            } else {
                $this->smarty->assign('adress', $d['CollBuildingName'] . " " . $d['CollAddress1'] . " " . " " . $d['CollAddress2'] . " " . " " . $d['CollAddress3'] . " " . " " . $d['CollAddress4'] . " " . " " . $d['CollPostcode']);
            }
        } else {
            $this->smarty->assign('adress', $this->session->fullPostcode);
        }

        $points = $diary->multiMapGetData($this->user->ServiceProviderID, $this->session->chSkillType);
        if ($this->session->idata != "") {
            $this->smarty->assign('insert', 1);
        } else {
            $this->smarty->assign('insert', 0);
        }




        if (!isset($this->session->SPInfo)) {
            $this->session->SPInfo = $diary->getAllServiceProviderInfo($this->user->ServiceProviderID);
        }
        $this->smarty->assign('mapcount', $this->session->SPInfo['Multimaps']);
        $this->smarty->assign('slots', $this->session->slotsLeft);
        $this->smarty->assign('slotsType', $this->session->slotsType);
        $this->smarty->assign('waypoints', $points);
        $this->smarty->display("diary/diaryMultiMap.tpl");
    }

    public function updateAppointmentGeoTagAction($args) {
        $this->checkSA($args);
        if (isset($_POST['appid'])) {
            $diary = $this->loadModel('Diary');
            $diary->updateAppointmentGeoTagAction($_POST, $this->user->ServiceProviderID);
        } else {
            $this->session->GeoTag = array('lat' => $_POST['lat'], 'lng' => $_POST['lng']);
        }
    }

    public function setSessionGeoTagAction($args) {
        $this->checkSA($args);

        $this->log($_POST, "geoLog");
        $this->session->GeoTag = array('lat' => $_POST['lat'], 'lng' => $_POST['lng']);
    }

    public function testviamenteUpdate3Action($args) {//this used only after import have been run on today
        // put this at the top of your page/script
        $api_viamente = $this->loadModel('APIViamente');
        $diary = $this->loadModel('Diary');





        $engineers = $diary->getAssignedEngineers($this->session->SQLSelectedDate, $this->session->JobPostCode, $this->user->ServiceProviderID);




        $ret = $api_viamente->OptimiseRoute($this->user->ServiceProviderID, $this->session->SQLSelectedDate, $engineers);
        if ($this->debug)
            $this->log($ret, 'vRespLog');

        if (isset($ret->reqID)) {
            $diary->SaveMapUUID($ret->reqID, $this->user->ServiceProviderID, $this->session->SQLSelectedDate);
        }

        if (isset($ret->routes)) {


            $diary->updateEngineerWorkload($ret->routes, $this->session->SQLSelectedDate, $this->user->ServiceProviderID);
        }
        if (isset($ret->unreachedWaypointNames) && sizeof($ret->unreachedWaypointNumbers) > 0) {

            $diary->setUnreached($ret->unreachedWaypointNames);
            $unreached = $diary->getUnreached($this->user->ServiceProviderID);
            $this->sendEmailAction(1, $unreached);
        }

        if (isset($ret->errorCode) && $ret->errorCode == "-200") {
            $this->smarty->assign('type', 11);
            $this->smarty->display("diary/diaryEndMessage.tpl");
        } elseif (isset($ret->errorCode)) {
            $this->smarty->assign('type', 12);
            $this->smarty->assign('ret', $ret->errorDescription);
            $this->smarty->display("diary/diaryEndMessage.tpl");
        } else {

            if ($this->session->ActionType != 6) {
                
            }
        }



        $this->defaultAction($args);
    }

    public function loadGridMapAction($args) {
        $this->checkSA($args);
        $diary = $this->loadModel('Diary');
        $sp = $args['sp'];
        $defaults = $diary->getGridDefaults($sp);
        $this->smarty->assign('def', $defaults);
        $this->smarty->assign('loadet', array());
        $this->smarty->assign('prew', 12);
        $this->smarty->assign('spid', $sp);
        if (!isset($args['prew'])) {
            $this->smarty->assign('prew', false);
            list($y, $m, $d) = explode('-', $args['s']);
            $activeDays = $diary->getActiveEngineerDays($args['e'], $args['s'], $limit = 7);
            for ($i = 0; $i < 7; $i++) {
                $dates[$i] = date('Y-m-d', mktime(0, 0, 0, $m, $d + $i, $y));
                if (!isset($activeDays[$i]['Status'])) {
                    $activeDays[$i]['Status'] = "In-Active";
                }
                $active[$i] = $activeDays[$i]['Status'];
            }
            $engName = $diary->checkEngineer($args['e']);

            $did = $diary->getDiaryAllocationIDForce($args['d'], $args['e']);
            if (!isset($did[0])) {
                $did[0]['DiaryAllocationID'] = $diary->createDiaryAllocation($this->user->ServiceProviderID, $args['d'], $args['e']);
            } else {
                if (sizeof($did) < 3 && sizeof($did) > 0) {

                    $diary->createDiaryAllocation($this->user->ServiceProviderID, $args['d'], $args['e'], true);
                    $did = $diary->getDiaryAllocationIDForce($args['d'], $args['e']);
                }
            }
            $loadet = $diary->loadGeoCell($did[0]['DiaryAllocationID']);

            $this->smarty->assign('spid', $this->user->ServiceProviderID);
            $this->smarty->assign('eid', $args['e']);

            $this->smarty->assign('active', $active);
            $this->smarty->assign('engName', $engName['EngineerFirstName'] . " " . $engName['EngineerLastName']);
            $this->smarty->assign('date', $args['d']);
            $this->smarty->assign('dates', $dates);
            $this->smarty->assign('loadet', $loadet);
        }
        $this->smarty->display("diary/defaultsMapGridMap.tpl");
    }

    public function testGetGeoCodeAction($args) {

        $lat = 51.44;
        $lng = -9.38;
        $this->getGeoCode(-10.6787, 55.4726, -5.4602, 51.4060, $lng, $lat, 10, 10);
    }

    //function getGeoCode is used to get GeoCode(home made postcode) for countries without postcodes
    //param1=top left corner latitude
    //param2=top left corner lng
    //param3=bottom right corner lat
    //param4=bottom right corner lng
    //param5=target lat
    //param6=target lng
    //param7=x lines
    //param8=y lines
    //return geocode in "xxx 111" format
    //
       public function getGeoCode($x1, $y1, $x2, $y2, $x3, $y3, $xnum, $ynum) {
        $alpha = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X ', 'Y', 'Z');
        if ($x3 >= $x1 && $x3 <= $x2 && $y3 <= $y1 && $y3 >= $y2) {

            $xsegSize = ($x1 - $x2) / $xnum; //x segment size
            if ($xsegSize < 0) {
                $xsegSize = $xsegSize * -1;
            }
            $ysegSize = ($y1 - $y2) / $ynum;   //y segment size
            if ($ysegSize < 0) {
                $ysegSize = $ysegSize * -1;
            }
            // echo"$x3-$x1/$xsegSize=";
            $xcurPosSegment = (integer) (($x3 - $x1) / $xsegSize);
            $xLetter1 = $alpha[$xcurPosSegment];
            //   echo"<br>";
            $ycurPosSegment = (integer) (($y3 - $y1) / $ysegSize);
            if ($ycurPosSegment < 0) {
                $ycurPosSegment = $ycurPosSegment * -1;
            }//geting size 1 segment values eg j9
            //size 2 segments start

            $xcurPosSegmentStart = $x1 + $xsegSize * $xcurPosSegment;
            $ycurPosSegmentStart = $y1 - $ysegSize * $ycurPosSegment;
            $xcurPosSegmentEnd = $xcurPosSegmentStart + $xcurPosSegment;
            $ycurPosSegmentEnd = $ycurPosSegmentStart - $ycurPosSegment;
            echo"x=$x1<br>y=$y1<br>";
            echo"xseg=$xsegSize<br>yseg=$ysegSize<br>";
            echo"xpos=$xcurPosSegment<br>ypos=$ycurPosSegment<br>";
            echo"start=$xcurPosSegmentStart<br>$ycurPosSegmentStart<br>";
            echo"end=$xcurPosSegmentEnd<br>$ycurPosSegmentEnd<br>";


            //size 2 segments end
            //echo"<br>";
            //echo $xsegSize;
            // echo"<br>";
            //echo $ysegSize;
            // echo "<br>in";
            echo"<br> Full Postcode=$xLetter1$ycurPosSegment";
        } else {
            echo "out";
        }
    }

    public function getJsonSkillsetListAction($args) {
        $this->checkSA($args);
        $diary = $this->loadModel('Diary');
        $skillsetList1 = $diary->getSkillsets($this->user->ServiceProviderID, null, 1);
        $skillsetList2 = $diary->getSkillsets($this->user->ServiceProviderID, null, 2);
        $skillsetList = array_merge($skillsetList1, $skillsetList2);
        foreach ($skillsetList as $a) {
            $ar[$a['SkillsetName'] . "<span style='display:none'>^-^" . $a['ServiceProviderSkillsetID'] . "</span>"] = $a['SkillsetName'];
        }

        echo json_encode($ar);
    }

    public function tableUpdateSkillsetAction() {
        $diary = $this->loadModel('Diary');
        $ret = $diary->updateSkillset($_POST);
        if ($this->session->SPInfo['DiaryType'] != "FullViamente") {
            $apiA = $this->loadModel('APIAppointments');
            $apiA->RoutedAppointments($this->session->SQLSelectedDate, $this->user->ServiceProviderID);
        }
        echo $_POST['value'];
    }

    public function saveSPGeoCellAction($args) {
        $this->checkSA($args);
        $diary = $this->loadModel('Diary');

        $diary->saveSPGeoCell($_POST, $this->user->ServiceProviderID);

        $this->redirect('AppointmentDiary/skillsAreaMap/spID=' . $this->user->ServiceProviderID . '/eID=' . $_POST['eid'] . '/d=' . $_POST['date']);
    }

    public function copyGeoMapToSessionAction($args) {
        $this->checkSA($args);
        $diary = $this->loadModel('Diary');

        $did = $diary->getDiaryAllocationIDForce($_POST['d'], $_POST['e']);

        if (!isset($did[0])) {
            echo "error";
        }
        $this->session->geoClipDiaryAllocationID = $did[0]['DiaryAllocationID'];
    }

    public function pasteGeoMapToDatabaseAction($args) {
        $this->checkSA($args);
        $diary = $this->loadModel('Diary');

        $this->session->geoClipDiaryAllocationID;
        $diary->pasteGeoMapToDatabase($this->session->geoClipDiaryAllocationID, $_POST['e'], $_POST['d'], $this->user->ServiceProviderID);
        echo "ALL I HAVE DONE is copied your map data like you asked";
    }

    public function phpInfoAction() {
        phpinfo();
    }

    public function showPointMapAction($args) {
        $this->checkSA($args);
        $id = $args[0];
        if (isset($args[1])) {
            $adr = str_replace(" ", "+", $args[1]);
            $this->smarty->assign('adr', $adr);
        } else {
            $this->smarty->assign('adr', "");
        }
        if (isset($args[2])) {

            $this->smarty->assign('main', 1);
        }
        $this->smarty->assign('id', $id);
        $adress="";
        if (isset($this->session->idata['CollAddress1'])&&$this->session->idata['CollAddress1'] != "") {


            $adress = $this->session->idata['CollAddress1'] . " " . $this->session->idata['CollAddress2'] . " " . $this->session->idata['CollAddress3'] . " " . $this->session->idata['CollAddress4'];
        } elseif(isset($this->session->idata['CustAddress1'])) {
            $adress = $this->session->idata['CustAddress1'] . " " . $this->session->idata['CustAddress2'] . " " . $this->session->idata['CustAddress3'] . " " . $this->session->idata['CustAddress4'];
        }
        
        $this->smarty->assign('iData', $this->session->idata);
        $this->smarty->assign('adress', $adress);
        if (isset($this->session->GeoTag)) {
            $this->smarty->assign('GeoTag', $this->session->GeoTag);
        }
        $this->smarty->display("diary/diaryPointmap.tpl");
    }

    public function saveGridDefaultsAction($args) {
        $this->checkSA($args);
        $diary = $this->loadModel('Diary');
        $diary->saveGridDefaults($_POST);
        echo"ok";
    }

    public function getEngineersToSkillsetAction($args) {
        $this->checkSA($args);
        $diary = $this->loadModel('Diary');
        $engs = $diary->getEngineersToSkillset($_POST['s'], $this->user->ServiceProviderID, $_POST['d'],$_POST['a']);
        echo json_encode($engs);
    }

    public function optimiseIndEngineersAction($args) {
        $this->checkSA($args);
        $api_viamente = $this->loadModel('APIViamente');
        $diary = $this->loadModel('Diary');

        $this->session->indEngineers = $_POST['v'];
        if ($this->debug)
            $this->log($this->session->indEngineers, "optEng_____");
        $ret = $api_viamente->OptimiseRouteIndividualEngineers($this->user->ServiceProviderID, $this->session->SQLSelectedDate, $_POST['v']);
        if (isset($ret->reqID)) {
            //save map
            $diary->SaveMapUUID($ret->reqID, $this->user->ServiceProviderID, $this->session->SQLSelectedDate);
        }

        if (isset($ret->routes)) {

            $diary->updateEngineerWorkload($ret->routes, $this->session->SQLSelectedDate, $this->user->ServiceProviderID, "ind");
        }

        if (isset($ret->unreachedWaypointNames)) {

            $diary->setUnreached($ret->unreachedWaypointNames);
            $unreached = $diary->getUnreached($this->user->ServiceProviderID);
            $this->sendEmailAction(1, $unreached);
        }



        $apiA = $this->loadModel('APIAppointments');
        $apiA->RoutedAppointments($this->session->SQLSelectedDate, $this->user->ServiceProviderID);
    }

    public function setSesGeoTagAction($args) {
        $this->checkSA($args);
        $gta = explode(',', $_POST['g']);
        $this->session->addressGeoTag = array('lat' => $gta[0], 'lng' => $gta[1]);
        $this->log($_POST, "geoLog");
        $this->session->GeoTag = array('lat' => $gta[0], 'lng' => $gta[1]);
        $this->session->addressGeoTagFullnames = array('Latitude' => $gta[0], 'Longitude' => $gta[1]);
        // print_r($this->session->addressGeoTag);
    }

    public function importCSVAction($args) {
        $u = 0;
        $diary = $this->loadModel('Diary');
        echo "Processing";
        $handle = fopen(APPLICATION_PATH . '/uploads/csv/test.csv', 'r');
        while (($row = fgetcsv($handle)) !== false) {
            foreach ($row as $field) {
                $diary->procesCSVIMport($row);
            }
            echo".";
            $u+=1;
            if ($u == 100) {
                $u = 0;
                echo"<br>";
            }
        }
        echo"file imported see logs for details!!!!!!";
        fclose($handle);
    }

    public function checkSA($args) {
        //sa setup begin

        if ($this->user->SuperAdmin == 1) {
            $this->smarty->assign('super', 'yes');
            if (isset($args['resetSp'])) {
                $this->session->spid = null;
                $this->user->ServiceProviderID = null;
            }
            if (isset($this->session->spid) && $this->session->spid != null) {
                $this->user->ServiceProviderID = $this->session->spid;
            }
            if (isset($args['setSp'])) {
                $this->user->ServiceProviderID = $args['setSp'];
                $this->session->spid = $args['setSp'];
            }
            if ($this->user->ServiceProviderID == "") {
                $this->user->ServiceProviderID = 0;
            }
        }

        //sa setup end
    }

    public function checkDayErrorsAction($args) {
        $this->checkSA($args);
        $diary = $this->loadModel('Diary');
        $ret = $diary->checkDayErrors($this->user->ServiceProviderID, $this->session->SQLSelectedDate);
        if ($ret) {
            echo 2;
        } else {
            echo 0;
        }
    }

    public function emailViamnteMapAction($args, $eng = false, $date = false, $spID = false) {
        $diary = $this->loadModel('Diary');
        if (isset($_POST['mode']) && $_POST['mode'] == 2) {
            $englist = $diary->getEngDetailsToEmail($this->session->indEngineers);
        }


        if (!$date) {
            $date = $this->session->SQLSelectedDate;
        }
        if (!$spID) {
            $spID = $this->user->ServiceProviderID;
        }
        if (!$eng && !isset($englist)) {
            $englist = $diary->getEngWithApp($spID, $date);
        }
        $uuid = $diary->getUUID($date, $spID);
        $weekday = date('l', strtotime($date));
        foreach ($englist as $e) {

            $data = $this->config['Viamente']['ViamenteApiUrl'] . "/export/directions/" . $uuid . "/" . $e['nn'] . " " . $e['ss'] . "_" . $weekday;
            $this->sendEmailAction(5, $data, $e['EmailAddress']);
        }
    }

    public function tableUpdateSortOrderAction($args) {

        $diary = $this->loadModel('Diary');
        $ret = $diary->tableUpdateSortOrder($_POST);
        if ($this->session->SPInfo['DiaryType'] != "FullViamente" || $this->session->GlobalViamenteStatus) {
            $apiA = $this->loadModel('APIAppointments');
            $apiA->RoutedAppointments($this->session->SQLSelectedDate, $this->user->ServiceProviderID);
        }
        echo $_POST['value'];
    }

//no viamente diary map of all appointments in main diary page
    public function displayAppointmentMapAction($args) {
        $this->checkSA($args);
        $diary = $this->loadModel('Diary');
        if (isset($args['date'])) {
            if (isset($this->session->idata)) {

                $jobTaged = null;
                if ($jobTaged) {
                    $this->smarty->assign('appGeoTag', $jobTaged);
                }
                $d = $this->session->idata;
                if ($d['CollPostcode'] == "") {
                    $this->smarty->assign('adress', $d['CustBuildingName'] . " " . $d['CustAddress1'] . " " . " " . $d['CustAddress2'] . " " . " " . $d['CustAddress3'] . " " . " " . $d['CustAddress4'] . " " . " " . $d['CustPostcode']);
                } else {
                    $this->smarty->assign('adress', $d['CollBuildingName'] . " " . $d['CollAddress1'] . " " . " " . $d['CollAddress2'] . " " . " " . $d['CollAddress3'] . " " . " " . $d['CollAddress4'] . " " . " " . $d['CollPostcode']);
                }
            } else {
                $this->smarty->assign('adress', $this->session->fullPostcode);
            }

            $points = $diary->multiMapGetData($this->user->ServiceProviderID, $this->session->chSkillType, $args['date']);
            if ($this->session->idata != "") {
                $this->smarty->assign('insert', 1);
            } else {
                $this->smarty->assign('insert', 0);
            }


            $this->smarty->assign('waypoints', $points);

            if (!isset($this->session->SPInfo)) {
                $this->session->SPInfo = $diary->getAllServiceProviderInfo($this->user->ServiceProviderID);
            }
            $this->smarty->assign('mapcount', $this->session->SPInfo['Multimaps']);
            $this->smarty->assign('slots', $this->session->slotsLeft);
            $this->smarty->assign('slotsType', $this->session->slotsType);
        } else {

            $data = $diary->multiMapGetData($this->user->ServiceProviderID, 0, $this->session->SQLSelectedDate);
            $this->smarty->assign('waypoints', $data);
        }
        $this->smarty->display("diary/diaryAppointmentMap.tpl");
    }

//show allcoation only table of appointments
    public function allocationOnlyTableAction($args) {
        $this->checkSA($args);
        $diary = $this->loadModel('Diary');
        $data = $diary->getallocationOnlyTableData($this->user->ServiceProviderID);
        $this->smarty->assign('data', $data);
        $this->smarty->display("diary/diaryAllocationOnlyTable.tpl");
    }

//this used for allocation only diary setup to mark appointments as confirmed
    public function markAsConfirmedAction($args) {
        $this->checkSA($args);
        $diary = $this->loadModel('Diary');
        $_POST['arr'] = trim($_POST['arr'], ',');
        $diary->markAsConfirmed($_POST['arr']);
    }

//this function is called by remote engineer to change appointmnet status
    public function setAppointmentStatusAction($get) {
        //print_r($_GET);
        if (sizeof($_GET) > 0) {
            $data = $_GET;
        } elseif (sizeof($get)) {
            $data = $get;
        }
        $this->log($data, "AppointmentStatusUpdate________");
        $diary = $this->loadModel('Diary');
        $users = $this->loadModel('Users');

//     echo"<pre>";
//      print_r($data);
//      echo"</pre>";

        if (isset($data['username']) && isset($data['password']) && 1 == 0) {
            $spID = $diary->getSPID($data['username'], $users->encrypt($data['password']));
            $eid = $diary->getEngineerIDFromAPPOnly($data['slid']);
            $this->log("ServiceProvider=" . $spID, "AppointmentStatusUpdate________");
            $this->log("Engineer=" . $eid, "AppointmentStatusUpdate________");
            if ($spID && $eid) {
                $this->user->ServiceProviderID = $spID;
                $apiV = $this->loadModel('APIViamente');

                if (isset($data['closed']) && $data['closed'] == 'Y') {
                    $type = "3";
                }
                if (isset($data['outcard']) && $data['outcard'] == 'Y') {
                    $type = "4";
                }
                $diary->markAsComplited($data['slid'], $type, $spID);
                $ret = $apiV->ProcessEngineer($eid, "update", $today = true);
                $dateToday = date('Y-m-d', strtotime("today"));

//       echo"<pre>";
//      print_r($ret);
//      echo"</pre>";
                $ret = $apiV->OptimiseRouteIndividualEngineers($spID, $dateToday, array($eid));
                if (isset($ret->routes2)) {
                    $diary->removeBreaks($dateToday, $spID, false);
                    foreach ($ret->routes as $r) {
                        foreach ($r->steps as $e)
                            if (isset($e->driveBreaks)) {
                                $apid = explode(' ', $e->waypointName);
                                $apid = $apid[sizeof($apid) - 1];
                                foreach ($e->driveBreaks as $w) {
                                    if ($apid * 1 == $apid) {
                                        $diary->addAppBreak($apid, $w->breakTimeSec, $w->durationSec);
                                    }
                                }
                                // 
                            }
                    }
                }
                if (isset($ret->unreachedWaypointNames) && sizeof($ret->unreachedWaypointNumbers) > 0) {

                    $diary->setUnreached($ret->unreachedWaypointNames);
                    $unreached = $diary->getUnreached($this->user->ServiceProviderID);
                    $this->sendEmailAction(1, $unreached);
                }
                $apiA = $this->loadModel('APIAppointments');
                // echo $this->session->SQLSelectedDate."<br>";
                //  echo $this->user->ServiceProviderID."<br>";
                $resp = $apiA->RoutedAppointments($dateToday, $this->user->ServiceProviderID);

                return true;
            } else {
                return false;
            }
        }
    }

//its called by remote engineer app to set engineer curent location viamente proces 
//engineer will be called to to update viamente start location
    public function setEngineerLocationAction($get) {
        if (sizeof($_GET) > 0) {
            $data = $_GET;
        } elseif (sizeof($get)) {
            $data = $get;
        }
        //  print_r($_GET);
        $this->log($data, "EngineerLocationUpdate________");
        $location = $data['location'];
        $diary = $this->loadModel('Diary');
        $users = $this->loadModel('Users');
        if (isset($data['username']) && isset($data['password']) && 1 == 0) {
            $spID = $diary->getSPID($data['username'], $users->encrypt($data['password']));
            $engid = $diary->getEngIdFromSBCode($data['engCode'], $spID);
            if ($spID && $engid) {
                $this->log("ServiceProvider=" . $spID, "EngineerLocationUpdate________");
                $this->log("Engineer=" . $engid, "EngineerLocationUpdate________");
                $this->user->ServiceProviderID = $spID;

                $diary->setEngineerLocation($engid, $location);
            }
        }
    }

    public function setUserModifiedGeoTagAction($args) {
        $data = $_POST;
        $diary = $this->loadModel('Diary');
        $diary->setUserModifiedGeoTag($data['appid'], $data['mode']);
    }

    public function viamenteUpdate2VisualFXAction($args) {
        $notUnlock = false;
        if (!isset($args['defaultRet'])) {
            $args['defaultRet'] = 1;
        }
        if ($this->session->SPInfo['DiaryType'] == "FullViamente") {
            $this->checkSA($args);
            // put this at the top of your page/script

            if (date("Y-m-d") != $this->session->SQLSelectedDate || $this->session->SPInfo['RunViamenteToday'] == 'Yes') {


                $api_viamente = $this->loadModel('APIViamente');
                $diary = $this->loadModel('Diary');

                //print_r($args);
                if ($args['mode'] == 'Brown') {
                    if ($diary->checkFinalized($this->user->ServiceProviderID, $this->session->SQLSelectedDate, "Brown")) {
                        $notUnlock = true;
                    }


                    $engineers = $diary->getAssignedEngineers($this->session->SQLSelectedDate, $this->session->JobPostCode, $this->user->ServiceProviderID, "Brown");
                }
                if ($args['mode'] == 'White') {
                    if ($diary->checkFinalized($this->user->ServiceProviderID, $this->session->SQLSelectedDate, "White")) {
                        $notUnlock = true;
                    }

                    $engineers = $diary->getAssignedEngineers($this->session->SQLSelectedDate, $this->session->JobPostCode, $this->user->ServiceProviderID, "White");
                }
                if ($args['mode'] == 'Both') {

                    for ($y = 0; $y < 2; $y++) {
                        if ($y == 0) {
                            $mode = "Brown";
                            if ($diary->checkFinalized($this->user->ServiceProviderID, $this->session->SQLSelectedDate, "Brown")) {
                                $notUnlock = true;
                            }
                            $engineers = $diary->getAssignedEngineers($this->session->SQLSelectedDate, $this->session->JobPostCode, $this->user->ServiceProviderID, "Brown");
                        }
                        if ($y == 1) {
                            $mode = "White";
                            if ($diary->checkFinalized($this->user->ServiceProviderID, $this->session->SQLSelectedDate, "White")) {
                                $notUnlock = true;
                            }
                            $engineers = $diary->getAssignedEngineers($this->session->SQLSelectedDate, $this->session->JobPostCode, $this->user->ServiceProviderID, "White");
                        }
                        if ($engineers) {
                            $ret = $api_viamente->OptimiseRoute($this->user->ServiceProviderID, $this->session->SQLSelectedDate, $engineers, $mode, $notUnlock);
                        }
                        if ($args['defaultRet'] == 2) {
                            if (!isset($ret->unreachedWaypointNames) || sizeof($ret->unreachedWaypointNumbers) == 0) {

                                if (!isset($ret->errorCode)) {
                                    if ($y == 0) {
                                        $diary->ViamenteFinalize($this->session->SQLSelectedDate, $this->user->ServiceProviderID, $args['mode']);
                                    }
                                }
                            }
                        }
                        if (isset($ret->reqID)) {
                            $diary->SaveMapUUID($ret->reqID, $this->user->ServiceProviderID, $this->session->SQLSelectedDate);
                        }

                        if (isset($ret->routes)) {


                            $diary->updateEngineerWorkload($ret->routes, $this->session->SQLSelectedDate, $this->user->ServiceProviderID);
                        }

                        if (isset($ret->routes)) {
                            $diary->removeBreaks($this->session->SQLSelectedDate, $this->user->ServiceProviderID, false);
                            foreach ($ret->routes as $r) {
                                foreach ($r->steps as $e)
                                    if (isset($e->driveBreaks)) {
                                        $apid = explode(' ', $e->waypointName);
                                        $apid = $apid[sizeof($apid) - 1];
                                        foreach ($e->driveBreaks as $w) {
                                            if ($apid * 1 == $apid) {
                                                $diary->addAppBreak($apid, $w->breakTimeSec, $w->durationSec);
                                            }
                                        }
                                        // 
                                    }
                            }
                        }
                        if (isset($ret->unreachedWaypointNames) && sizeof($ret->unreachedWaypointNumbers) > 0) {

                            $diary->setUnreached($ret->unreachedWaypointNames);
                            $unreached = $diary->getUnreached($this->user->ServiceProviderID);
                            $this->sendEmailAction(1, $unreached);
                        }
                    }
                } else {
                    if ($engineers) {
                        $ret = $api_viamente->OptimiseRoute($this->user->ServiceProviderID, $this->session->SQLSelectedDate, $engineers, $args['mode'], $notUnlock);
                    }

                    if ($args['defaultRet'] == 2) {
                        if (!isset($ret->unreachedWaypointNames) || sizeof($ret->unreachedWaypointNumbers) == 0) {

                            if (!isset($ret->errorCode)) {
                                $diary->ViamenteFinalize($this->session->SQLSelectedDate, $this->user->ServiceProviderID, $args['mode']);
                            }
                        }
                    }

                    if ($this->debug)
                        $this->log($ret, 'vRespLog');

                    if (isset($ret->reqID)) {
                        $diary->SaveMapUUID($ret->reqID, $this->user->ServiceProviderID, $this->session->SQLSelectedDate);
                    }

                    if (isset($ret->routes)) {


                        $diary->updateEngineerWorkload($ret->routes, $this->session->SQLSelectedDate, $this->user->ServiceProviderID);
                    }

                    if (isset($ret->routes)) {
                        $diary->removeBreaks($this->session->SQLSelectedDate, $this->user->ServiceProviderID, false);
                        foreach ($ret->routes as $r) {
                            foreach ($r->steps as $e)
                                if (isset($e->driveBreaks)) {
                                    $apid = explode(' ', $e->waypointName);
                                    $apid = $apid[sizeof($apid) - 1];
                                    foreach ($e->driveBreaks as $w) {
                                        if ($apid * 1 == $apid) {
                                            $diary->addAppBreak($apid, $w->breakTimeSec, $w->durationSec);
                                        }
                                    }
                                    // 
                                }
                        }
                    }
                    if (isset($ret->unreachedWaypointNames) && sizeof($ret->unreachedWaypointNumbers) > 0) {

                        $diary->setUnreached($ret->unreachedWaypointNames);
                        $unreached = $diary->getUnreached($this->user->ServiceProviderID);
                        $this->sendEmailAction(1, $unreached);
                    }
                }
                if (isset($ret->errorCode) && $ret->errorCode == "-200") {
                    $this->smarty->assign('type', 11);
                    $this->smarty->display("diary/diaryEndMessage.tpl");
                    $this->sendEmailAction(6);
                } elseif (isset($ret->errorCode)) {
                    $this->smarty->assign('type', 12);
                    $this->smarty->assign('ret', $ret->errorDescription);
                    $this->smarty->display("diary/diaryEndMessage.tpl");
                } else {
                    if (isset($ret->unreachedWaypointNames) && sizeof($ret->unreachedWaypointNumbers) > 0 && $args['defaultRet'] == 2) {
                        if ($diary->getErrorApp($this->user->ServiceProviderID, 0, $this->session->SQLSelectedDate)) {
                            $this->smarty->assign('type', 14);
                            $this->smarty->display("diary/diaryEndMessage.tpl");
                        } else {
                            $this->smarty->assign('type', 13);
                            $this->smarty->display("diary/diaryEndMessage.tpl");
                        }
                    } else {
                        if ($this->session->ActionType != 6) {
                            $response = $this->EndDayAction($args);
                            if (isset($response)) {
                                if (isset($response['ResponseCode'])) {
                                    if ($response['ResponseCode'] == "SC0001") {

                                        $this->smarty->assign('ret', $args['defaultRet']);
                                        $this->smarty->assign('type', 6);
                                        $this->smarty->display("diary/diaryEndMessage.tpl");
                                    } else {
                                        $this->smarty->assign('type', 2);
                                        $this->smarty->assign('response', $response);
                                        $this->smarty->display("diary/diaryEndMessage.tpl");
                                    }
                                } else {
                                    print_r($response);
                                }

                                if ($this->session->ActionType != 5) {
                                    
                                }
                            }
                        }
                    }
                }
                if (isset($args['defaultRet'])) {


                    $this->defaultAction($args);
                }
            } else {
                $this->defaultAction($args);
            }
        } else {
            $this->defaultAction($args);
        }
    }

    public function setBankHolidayStatusAction($args) {
        $d = $_POST;
        $diary = $this->loadModel('Diary');
        $diary->setBankHolidayStatus($d['spid'], $d['bid'], $d['status']);
    }

    public function testgetOneTouchForcedEngineersAction($args) {
        $diary = $this->loadModel('Diary');
        $TimeSlot = "AM";
        $skillset = 9;
        $Postcode = "nn1 4ph";
        $SpID = 8;
        $date = "2013-02-20";
        $r = $diary->getOneTouchForcedEngineers($TimeSlot, $skillset, $Postcode, $SpID, $date);
        echo "$r";
    }

    public function faqAction($args) {
        $diary = $this->loadModel('Diary');
        if (isset($args['cat'])) {
            $cat = $args['cat'];
        } else {
            $cat = false;
        }
        $data = $diary->getAllFaq($cat);
        $this->smarty->assign('data', $data);
        $categoryList = $diary->getFaqCategoryList();
        $this->smarty->assign('categoryList', $categoryList);
        $this->smarty->assign('cat', $cat);
        if ($this->user->SuperAdmin == 1) {
            $this->smarty->assign('super', $this->user->SuperAdmin);
        } else {
            $this->smarty->assign('super', false);
        }
        $this->smarty->display("diary/diaryFAQ.tpl");
    }

    public function deleteFaqEntryAction($args) {
        $this->checkSA($args);
        $diary = $this->loadModel('Diary');
        $diary->deleteFaqEntry($args[0]);
        $this->faqAction($args);
    }

    public function confirmAppointmentEmailAction($args) {
        $diary = $this->loadModel('Diary');
        //print_r($args);
        //print_r($_GET);
        if (isset($args['confirmApp']) || isset($_GET['confirmApp'])) {
            if (isset($args['confirmApp'])) {
                $ap = $args['confirmApp'];
            };
            if (isset($_GET['confirmApp'])) {
                $ap = $_GET['confirmApp'];
            };
            $en = $this->decrypt($ap);
            //$en=$args['confirmApp'];
            $st = $diary->checkConfirmed($en);
            if ($st == "updated") {
                $this->smarty->assign('mode', 'u');
                $this->smarty->display("diary/msg/appUpdated.tpl");
            } else {
                if ($st == "notupdated") {
                    $this->smarty->assign('mode', 'n');
                    $this->smarty->display("diary/msg/appUpdated.tpl");
                } else {
                    $this->smarty->assign('mode', 'ne');
                    $this->smarty->display("diary/msg/appUpdated.tpl");
                }
            }
        }
    }

    public function encrypt($text) {

        $crypttext = base64_encode($text);
        return $crypttext;
    }

    public function decrypt($text) {

        $crypttext = $crypttext = base64_decode($text);
        return $crypttext;
    }

    public function LockAppWindowAction($args) {

        $this->checkSA($args);
        $diary = $this->loadModel('Diary');
        $diary->LockAppWindow($this->user->ServiceProviderID, $this->session->SQLSelectedDate);
    }

    public function testAction($args) {
        $this->checkSA($args);
        $diary = $this->loadModel('Diary');
        echo "dd";
        echo $diary->getEngIdFromSBCode('RYVES', 64);
    }

    public function engcapTestAction($args) {
        $diary = $this->loadModel('Diary');
        $ret = $diary->checkEngineersCapacity(8, '2013-05-10', 1);
    }
    
    /*
     * @author Dileep Bhimineni
     * 
     *      Function to change time to seconds
     * $params 
     *      $time in (hours:minutes:seconds) format
     * 
     * $return
     *      $time in seconds .
     * 
     */
    
    public function time2seconds($time='00:00:00')
    {
        list($hours, $mins, $secs) = explode(':', $time);
        return ($hours * 3600 ) + ($mins * 60 ) + $secs;
    }
    
    public function sendAppointmentReminderAction($args) {
        if(isset($args) && count($args) > 0)
            $apptIdsArr = explode(", ",$args);
        else
            $apptIdsArr = explode(", ",$_POST['apptId']);
        foreach($apptIdsArr as $apptId)
        {
            $email = $this->loadModel('Email');
            $email->sendAppointmentDetailsEmail($apptId, "reminder", false, false, false);
        }
    }
    
    public function getDayAppointmentsAction($args) {
        $diary = $this->loadModel('Diary');
        $apptIdsArrs = $diary->getDayAppointments($this->user->ServiceProviderID,$_POST['curDate']);
        foreach($apptIdsArrs as $apptIdsArr)
        {
            $apptIdsArray[] = $apptIdsArr['AppointmentID'];
        }
        $apptIdsStr = implode(", ",$apptIdsArray);
        $this->sendAppointmentReminderAction($apptIdsStr);
    }

}

?>
