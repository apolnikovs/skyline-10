<?php

/**
 * JobsAPIContoller.class.php
 * 
 * Implementation of Jobs API for SkyLine
 * API Spec Section 8.1.4.7
 * API Spec Section 8.1.4.9
 * API Spec Section 8.1.4.10
 * API Spec Section 8.1.4.13
 * API Spec Section 8.1.4.14
 * API Spec Section 8.1.4.15 
 *
 * @author     Andrew Williams <a.williams@pccsuk.com>
 * @copyright  2012 - 2013 PC Control Systems
 * @link       
 * @version    1.92
 * 
 * Changes
 * Date        Version Author                Reason
 * 25/06/2012  1.00    Andrew J. Williams    Initial Version
 * 16/07/2012  1.01    Andrew J. Williams    Issue 40 - Consent field moved to customer table.
 * 14/08/2012  1.02    Andrew J. Williams    Issue 53 - Changes to the way UnitType field is handled plus handling to call Sasmsung API for creation
 * 24/08/2012  1.03    Andrew J. Williams    TrackerBase VMS Log 29 - Alterations to Skyline API to work with ServiceBase
 * 17/09/2012  1.04    Andrew J. Williams    Issue 72 - Add NetworkRefNo to Skyline.PutJobDetails
 * 18/09/2012  1.05    Andrew J. Williams    Issue 77 - Some dates being imported from RMA Tracker as 01/01/1970 & Issue 79 - PutJob failing when CustomerCompany set
 * 24/09/2012  1.06    Andrew J. Williams    Issue 84 - Status being imported bu not StatusID
 * 25/09/2012  1.07    Andrew J. Williams    BRS 114 - Changes to PutJob Details due to appointments table changes for Appoinment Diary funtionality.
 * 02/10/2012  1.08    Brian Etherington     Switch off debug logging
 * 11/10/2012  1.09    Andrew J. Williams    Issue 96 - Servicebase is sending all paramters in upper case, so PutJobDetails fails
 * 15/10/2012  1.10    Andrew J. Williams    Issue 96 - Servicebase is sending all paramters in upper case, so PutJobDetails fails - further changes
 * 24/10/2012  1.11    Andrew J. Williams    Issue 105 - Issues on with Import of Status history, Contcat History and RMA Tracker call on PutJob Details update
 * 24/10/2012  1.12    Andrew J. Williams    Issue 102 - ContactHistory import failing and problem with Model Number importing
 * 25/10/2012  1.13    Andrew J. Williams    Issue 107 - PutJobDetails not recording Service Base Licence No
 * 26/10/2012  1.14    Andrew J. Williams    Issue 110 - PutJob Details fails on parts being added
 * 26/10/2012  1.15    Andrew J. Williams    Issue 112 - PutJobDetails Unable to remove existing ModelID when record changed to unknown model (in ServiceBaseModel)
 * 29/10/2012  1.16    Andrew J. Williams    Issue 113 - Duplicate child files in Appointment and Status History
 * 29/10/2012  1.17    Andrew J. Williams    Add capability to receive Collection Address fields in PutJobDetails
 * 31/10/2012  1.18    Andrew J. Williams    PutJobDetails should not import appointments when ServiceCentre uses OnlineDiary
 * 01/11/2012  1.19    Andrew J. Williams    Issue 112 - Reissue follwoing DB Structure Change - PutJobDetails Unable to remove existing ModelID when record changed to unknown model (in ServiceBaseModel)
 * 01/11/2012  1.20    Andrew J. Williams    Deal with ModelID being set to 0 by Jobs API argument validate routine when null is passed.
 * 05/11/2012  1.21    Andrew J. Williams    Issue 118 - Error in Complete job model import (Model ID)
 * 07/11/2012  1.22    Andrew J. Williams    Issue 120 - Errors in for Samsung comms
 * 13/11/2012  1.23    Andrew J. Williams    Issue 128 - PutJobDetails - Changes for FixZone Jobs
 * 15/11/2012  1.24    Andrew J. Williams    Issue 134 - ModelID not clearing
 * 16/11/2012  1.25    Andrew J. Williams    Issue 135 - Validated Job Details blnaks for some jobs in PutJobDetails
 * 22/11/2012  1.26    Andrew J. Williams    Issue 137 - Unknown column 'ColAddCounty' in 'field list'
 * 23/11/2012  1.27    Andrew J. Williams    Issue 138 - Customer ID not updating
 * 26/11/2012  1.28    Andrew J. Williams    Issue 138 - CustomerID further fixes to robustness of updating
 * 28/11/2012  1.29    Andrew J. Williams    Issue 143 - Samsung Skyline job upload issues
 * 29/11/2012  1.30    Andrew J. Williams    Issue 144 - Error in JobsAPI, ContactPhoneExt
 * 29/11/2012  1.31    Andrew J. Williams    Issue 145 - Error when Creating Branch from API - Undefined property: JobsAPI::$statuses
 * 03/12/2012  1.32    Andrew J. Williams    Touched file to force merge
 * 10/12/2012  1.33    Andrew J. Williams    Log parameters passed to API for testing
 * 12/12/2012  1.34    Andrew J. Williams    Issue 154 - New fields needed in upload to Skyline
 * 12/12/2012  1.35    Andrew J. Williams    Issue 156 - Upload to Service Centre when parameter SendToASC = Y is passed
 * 14/12/2021  1.36    Andrew J. Williams    Issue 157 - BranchID set on putJobDetails new jobs
 * 19/12/2012  1.37    Andrew J. Williams    Issue 163 - Duplicate Part Entry Created
 * 07/01/2012  1.38    Andrew J. Williams    Issue 170 - Vestel field - Manufacture not Manufacturer
 * 07/01/2013  1.39    Andrew J. Williams    Trackerbase VMS Log 97 - Add extra fields for Samsung
 * 08/01/2013  1.40    Andrew J. Williams    Trackerbase VMS Log 107 - Add new Originator field to Skyline.putJobDetails
 * 08/01/2013  1.41    Andrew J. Williams    Trackerbase VMS Log 108 - putJobDetails changes for when SendToSC flag is set
 * 15/01/2013  1.42    Andrew J. Williams    Trackerbase VMS Log 128 - Don't write unknown to the Unit Types on job import 
 * 17/01/2013  1.43    Andrew J. Williams    Trackerbase VMS Log 136 - Populate JobSourceID on job from Originator field 
 * 18/01/2013  1.44    Andrew J. Williams    Trackerbase VMS Log 140 - Unit type issues on importing models 
 * 23/01/2013  1.45    Andrew J. Williams    Issue 181 - Blank strings being written into the database as false
 * 21/01/2013  1.46    Andrew J. Williams    Issue 183 - No Collection telephone number and e-mail delivery into ServiceBase
 * 31/01/2013  1.47    Andrew J. Williams    Issue 194 - JobSourceID not being correctly set in PutJobDetails
 * 31/01/2013  1.48    Andrew J. Williams    Trackerbase VMS Log 157 - Match new job with non skyline jobs on Network ID and Network Ref No
 * 06/02/2013  1.49    Brian etherington     Add $args to PutNewJobsResponse call in put method 
 * 08/02/2013  1.50    Andrew J. Williams    Issue 208 - putJobDetails : API Error: Undefined index: NetworkID
 * 14/02/2013  1.51    Andrew J. Williams    Trackerbase VMS Log 174 - Move Non Skyline Appointments over on job import 
 * 25/02/2013  1.52    Brian Etherington     GetJobDetails - corrected SQL in model method and removed check for null JobID in retutrned data.                          Change put reponse code checks to SC000? instead of SB00?
 * 01/03/2013  1.53    Andrew J. Williams    Issue 241 - Duplicate Contact History Records
 * 11/03/2013  1.54    Andrew J. Williams    Trackerbase VMS Log 168 - Allow Samsung Ancrum hjobs to download to two places
 * 11/03/2013  1.55    Andrew J. Williams    Trackerbase VMS Log 230 - getNewjobs API changes for Linhill - Error Codes
 * 12/03/2013  1.56    Andrew J. Williams    PutJobDetails move DateBooked checker to create only
 * 14/03/2013  1.57    Andrew J. Williams    Trackerbase VMS Log 232 - Fix for Ancrum issue 
 * 15/03/2013  1.58    Andrew J. Williams    Issue 262 - PutJobDetails - Some customers not being created on update / create
 * 21/03/2013  1.59    Andrew J. Williams    Issue 271 - One Touch for Ancrum/South Esk
 * 22/03/2013  1.60    Andrew J. Williams    Ancrum - Send Appoint to Servicebase for Secondary Service Provider
 * 22/03/2013  1.61    Andrew J. Williams    Added ServiceProviderDespatchDate to PutJobDeatils
 * 26/03/2013  1.62    Andrew J. Williams    Issue 283 - PutJobDetails - Branch Model Create Requires ServiceAppraisalRequired
 * 08/04/2013  1.63    Andrew J. Williams    Trackerbase VMS Log 244 - Add Network and Client to Notes field on booking
 * 08/04/2013  1.64    Andrew J. Williams    Trackerbase VMS Log 248 - Change Branch Account No to Branch Number 
 * 08/04/2013  1.65    Andrew J. Williams    Issue 295 - Only set Closed Date from ServiceProviderDespatchDate When Not Branch
 * 11/04/2013  1.66    Andrew J. Williams    Issue 300 - PHP Fatal error - Get Network Name
 * 19/04/2013  1.67    Andrew J. Williams    Data Integrity Check - Refresh Process
 * 24/04/2013  1.68    Andrew J. Williams    Unknown Cointact History Items will now be created (Request from Neil W / Chris B)
 * 25/04/2013  1.69    Andrew J. Williams    Private Contact History notes and Usercode field lengthened for Chris Berry
 * 25/04/2013  1.70    Andrew J. Williams    Issue 319 - PutJobDetails - Undefined index: Notes
 * 29/04/2013  1.71    Andrew J. Williams    Issue 330 - Some Contact History Items not being Updated
 * 29/04/2013  1.72    Andrew J. Williams    Issue 331 - API Error: Undefined index: BranchID
 * 30/04/2013  1.73    Andrew J. Williams    Issue 333 - When Parts or Call History not Passed Refresh Existing Records not Removed
 * 30/04/2013  1.74    Andrew J. Williams    Issue 337 - UserCode in Status History No Longer Restricted to 3 Characters
 * 30/04/2013  1.75    Andrew J. Williams    Issue 338 - PutJobDetails - Servicebase Sending Additional Supplier Field
 * 01/05/2013  1.76    Andrew J. Williams    Issue 343 - Ignore Client Number Check in PutJobDetails for ServiceBase Refresh
 * 01/05/2013  1.77    Andrew J. Williams    Issue 344 - For a Servicebase Refresh call to PutJobDetails always send to RMA Tracker
 * 03/05/2013  1.78    Andrew J. Williams    Issue 349 - Urgent changes for Skyline to RMA
 * 09/05/2013  1.79    Andrew J. Williams    Added check for NoSendOnRefresh flag befor sending refresh job to RMA Tracker (See Neil W for details)
 * 16/05/2013  1.80    Andrew J. Williams    Issue 363 - Errors in log file for API
 * 16/05/2013  1.81    Andrew J. Williams    Issue 365 - Jobs not being correctly closed.
 * 16/05/2013  1.82    Andrew J. Williams    Added Remote Engineer ImageURL and ThumbnailURL
 * 03/06/2013  1.83    Brian Etherington     validate api_jobs fields is getting called twice - 2nd time Model ID is not bveing checked for zero
 *                                           add code to re-check this and just before creating job re-check again!!!!
 *                                           Failure to do this is causing an integrity constraint failure
 * 06/06/2013  1.84    Brian Etherington     Ignore requests to update Customer title, county and country when invalid name values are supplied.
 * 17/06/2013  1.85    Brian Etherington     Remove call to urldecode in validate_args because received parameters via GET,POST,PUT & DELETE
 *                                           are already URL decoded!!!
 * 17/06/2013  1.86    Brian Etherington     Log # 275 - Set source field for Remote Engineer Actions
 * 19/06/2013  1.87    Brian Etherington     Log # 275 - Add ImageURL and ThumbnailURL columns to $api_call_history_fields
 * 09/07/2013  1.88    Brian Etherington     Log # 497 - Add Warranty Fields to putJobDetails
 * 16/07/2013  1.89    Brian Etherington     Log # 485 - Add call to ServiceBase PutJobDetails in Skyline PutJobdetails
 * 16/07/2013  1.90    Brian Etherington     Defend against invalid data being sent by RMA.  Ignore contact history with
 *                                           blank date/time.
 * 17/07/2013  1.91    Brian Etherington     Move call to serviceBase PutJobDetails to after job related tables are updated
 * 17/07/2013  1.92    Brian Etherington     Move call to serviceBase PutJobDetails so that it is conditional upon the update action.
 *                                           if update call ServiceBase PutJobDetails otherwise call ServiceBase PutNewJob
 ********************************************************************************/

require_once('CustomRESTController.class.php');
include_once(APPLICATION_PATH.'/controllers/API/SkylineAPI.class.php');
include_once(APPLICATION_PATH.'/controllers/SamsungClientController.class.php');    /* Required for interfacing with Samsung */

class JobsAPI extends SkylineAPI {
  
    public $debug = false;
    
    public $config;
    public $session;
    public $skyline;
    public $messages;
    public $lang = 'en';
    public $page;

    private $searchKey = array(
                              'Postcode' => 'cu.PostalCode',
                              'Surname' => 'cu.ContactLastName',
                              'SLNumber' => 'j.JobID',
                              'PhoneNumber' => 'sp.ContactPhone',
                              'ClientRef' => 'j.AgentRefNo',
                              'SCJobNo' => 'j.SCJobNo'
                              /* TODO: address */
                              );

    /*
     * API fields mapped to database fields
     */
    
    private $api_job_fields = array(
                                    'RMANumber' => array ('field' => 'RMANumber', 'type' => 'Integer'),
                                    'SCJobNo' => array ('field' => 'ServiceCentreJobNo', 'type' => 'String'),
                                    'ReferralNumber' => array ('field' => 'AgentRefNo', 'type' => 'String', 'length' => 20),
                                    'NetworkRefNo' => array ('field' => 'NetworkRefNo', 'type' => 'String', 'length' => 20),
                                    'SLNumber' => array ('field' => 'JobID', 'type' => 'Integer'),
                                    'DOP' => array ('field' => 'DateOfPurchase', 'type' => 'Date'),
                                    'ProductID' => array ('field' => 'ProductID', 'type' => 'Integer'),
                                    'ReportedFault' => array ('field' => 'ReportedFault', 'type' => 'String'),
                                    'ConditionCode' => array ('field' => 'ConditionCode', 'type' => 'String', 'length' => 4),
                                    'SymptomCode' => array ('field' => 'SymptomCode', 'type' => 'String', 'length' => 4),
                                    'GuaranteeCode' => array ('field' => 'GuaranteeCode', 'type' => 'String', 'length' => 4),
                                    'DateFaultOccurred' => array ('field' => 'FaultOccurredDate', 'type' => 'Date'),
                                    'ReferralNumber' => array ('field' => 'AgentRefNo', 'type' => 'String'),
                                    'ServiceTypeID' => array ('field' => 'ServiceTypeID', 'type' => 'Integer'),
                                    'JobTypeID' => array ('field' => 'JobTypeID', 'type' => 'Integer'),
                                    'CustomerID' => array ('field' => 'CustomerID', 'type' => 'Integer'),
                                    'OriginalRetailer' => array ('field' => 'OriginalRetailer', 'type' => 'String', 'length' => 30),
                                    'RetailerLocation' => array ('field' => 'RetailerLocation', 'type' => 'String', 'length' => 100),
                                    'ModelNo' => array ('field' => 'ServiceBaseModel', 'type' => 'String', 'length' => 50),
                                    'ModelID' => array ('field' => 'ModelID', 'type' => 'Integer'),
                                    'SerialNo' => array ('field' => 'SerialNo', 'type' => 'String', 'length' => 50),
                                    'Manufacturer' => array ('field' => 'ServiceBaseManufacturer', 'type' => 'String', 'length' => 50),
                                    'ManufacturerID' => array ('field' => 'ManufacturerID', 'type' => 'Integer'),
                                    'UnitType' => array ('field' => 'ServiceBaseUnitType', 'type' => 'String', 'length' => 50),
                                    'PolicyNo' => array ('field' => 'PolicyNo', 'type' => 'String', 'length' => 20),
                                    'Insurer' => array ('field' => 'Insurer', 'type' => 'String', 'length' => 30),
                                    'DefectType' => array ('field' => 'DefectType', 'type' => 'String', 'length' => 4),
                                    'DateRepairComplete' => array ('field' => 'RepairCompleteDate', 'type' => 'Date'),
                                    'RepairDescription' => array ('field' => 'RepairDescription', 'type' => 'String'),
                                    'DateUnitReceived' => array ('field' => 'DateUnitReceived', 'type' => 'Date'),
                                    'ClosedDate' => array ('field' => 'ServiceProviderDespatchDate', 'type' => 'Date'),
                                    'ETDDate' => array ('field' => 'ETDDate', 'type' => 'Date'),
                                    'BranchID' => array ('field' => 'BranchID', 'type' => 'Integer'),                
                                    'ServiceProviderID' => array ('field' => 'ServiceProviderID', 'type' => 'Integer'),
                                    'NetworkID' => array ('field' => 'NetworkID', 'type' => 'Integer'),
                                    'ClientID' => array ('field' => 'ClientID', 'type' => 'Integer'),      
                                    'Status' => array ('field' => 'Status', 'type' => 'String', 'length' => 30),
                                    'StatusID' => array ('field' => 'StatusID', 'type' => 'Integer'),
                                    'AgentStatus' => array ('field' => 'AgentStatus', 'type' => 'String', 'length' => 30),
                                    'CompletionStatus' => array ('field' => 'CompletionStatus', 'type' => 'String', 'length' => 30),
                                    'Accessories' => array ('field' => 'Accessories', 'type' => 'String', 'length' => 200),
                                    'UnitCondition' => array ('field' => 'UnitCondition', 'type' => 'String', 'length' => 50),
                                    'JobSite' => array ('field' => 'JobSite', 'type' => 'enum', 'enums' => array ('F'=>"field call",'W'=>"workshop")),
                                    'EngineerCode' => array ('field' => 'EngineerCode', 'type' => 'String', 'length' => 20),
                                    'EngineerName' => array ('field' => 'EngineerName', 'type' => 'String', 'length' => 50),
                                    'ClaimNumber' => array ('field' => 'ClaimNumber', 'type' => 'Integer'),
                                    'AuthorisationNumber' => array ('field' => 'AuthorisationNo', 'type' => 'String', 'length' => 20),
                                    'WarrantyNotes' => array ('field' => 'WarrantyNotes', 'type' => 'String', 'length' => 100),
                                    'DateBooked' => array ('field' => 'DateBooked', 'type' => 'Date'),
                                    'TimeBooked' => array ('field' => 'TimeBooked', 'type' => 'Time'),
                                    'ClaimTransmitStatus' => array ('field' => 'ClaimTransmitStatus', 'type' => 'Integer'),
                                    'eInvoiceStatus' => array ('field' => 'eInvoiceStatus', 'type' => 'Integer'),
                                    'ChargeableLabourCost' => array ('field' => 'ChargeableLabourCost', 'type' => 'Decimal'),
                                    'ChargeableLabourVATCost' => array ('field' => 'ChargeableLabourVATCost', 'type' => 'Decimal'),
                                    'ChargeablePartsCost' => array ('field' => 'ChargeablePartsCost', 'type' => 'Decimal'),
                                    'ChargeableDeliveryCost' => array ('field' => 'ChargeableDeliveryCost', 'type' => 'Decimal'),
                                    'ChargeableDeliveryVATCost' => array ('field' => 'ChargeableDeliveryVATCost', 'type' => 'Decimal'),
                                    'ChargeableVATCost' => array ('field' => 'ChargeableVATCost', 'type' => 'Decimal'),
                                    'ChargeableSubTotal' => array ('field' => 'ChargeableSubTotal', 'type' => 'Decimal'),
                                    'ChargeableTotalCost' => array ('field' => 'ChargeableTotalCost', 'type' => 'Decimal'),
                                    'AgentChargeableInvoiceNo' => array ('field' => 'AgentChargeableInvoiceNo', 'type' => 'Integer'),
                                    'AgentChargeableInvoiceDate' => array ('field' => 'AgentChargeableInvoiceDate', 'type' => 'Date'),
				    'Notes' => array ('field' => 'Notes', 'type' => 'String'),
                                    'RepairType' => array ('field' => 'RepairType', 'type' => 'String'),
                                    'ServiceBaseUnitType' => array ('field' => 'ServiceBaseUnitType', 'type' => 'String', 'length' => 50),
                                    'DownloadedToSC' => array ('field' => 'DownloadedToSC', 'type' => 'Integer'),
                                    'BookedBy' => array ('field' => 'BookedBy', 'type' => 'Integer'),
                                    'ModifiedUserID' => array ('field' => 'ModifiedUserID', 'type' => 'Integer'),
                                    'ModifiedDate' => array ('field' => 'ModifiedDate', 'type' => 'DateTime'),       
                                    'ColAddCompanyName' => array('field' => 'ColAddCompanyName', 'type' => 'String', 'length' => 100),
                                    'ColAddBuildingName' => array('field' => 'ColAddBuildingNameNumber', 'type' => 'String', 'length' => 40),
                                    'ColAddStreet' => array('field' => 'ColAddStreet', 'type' => 'String', 'length' => 40),
                                    'ColAddArea' => array('field' => 'ColAddLocalArea', 'type' => 'String', 'length' => 40),
                                    'ColAddTownCity' => array('field' => 'ColAddTownCity', 'type' => 'String', 'length' => 60),
                                    'ColAddPostcode' => array('field' => 'ColAddPostcode', 'type' => 'String', 'length' => 10),
                                    'ColAddEmail' => array('field' => 'ColAddEmail', 'type' => 'String', 'length' => 40),
                                    'ColAddPhoneNo' => array('field' => 'ColAddPhone', 'type' => 'String', 'length' => 50),
                                    'VestelModelCode' => array('field' => 'VestelModelCode', 'type' => 'String', 'length' => 30),
                                    'VestelManufactureDate' => array('field' => 'VestelManufactureDate', 'type' => 'String', 'length' => 4),
                                    'NewFirmwareVersion' => array('field' => 'NewFirmwareVersion', 'type' => 'String', 'length' => 20),
                                    'ExtendedWarrantyNo' => array('field' => 'ExtendedWarrantyNo', 'type' => 'String', 'length' => 20),
                                    'SamsungSubServiceType' => array('field' => 'SamsungSubServiceType', 'type' => 'String', 'length' => 2),
                                    'SamsungRefNo' => array('field' => 'SamsungRefNo', 'type' => 'String', 'length' => 15),
                                    'ConsumerType' => array('field' => 'ConsumerType', 'type' => 'String', 'length' => 30),
                                    'JobSourceID' => array('field' => 'JobSourceID', 'type' => 'Integer'),
                                    'RefreshDate' => array('field' => 'RefreshDate', 'type' => 'DateTime'),
                                    'ImageURL' => array('field' => 'ImageURL', 'type' => 'string', 'length' => 50),
                                    'ThumbnailURL' => array('field' => 'ThumbnailURL', 'type' => 'string', 'length' => 50),
                                    'ClaimBillNumber' => array ('field' => 'ClaimBillNumber', 'type' => 'string', 'length' => 15),
                                    'WarrantyLabourCost' => array ('field' => 'WarrantyLabourCost', 'type' => 'Decimal'),
                                    'WarrantyDeliveryCost' => array ('field' => 'WarrantyDeliveryCost', 'type' => 'Decimal')
                                   );

    
    private $api_customer_fields = array(
                                         'CustomerForename' => array('field' => 'ContactFirstName', 'type' => 'String'),
                                         'CustomerSurname' => array('field' => 'ContactLastName', 'type' => 'String'),
                                         'CustomerCompany' => array('field' => 'CompanyName', 'type' => 'String', 'length' => 100),
                                         'CustomerBuildingName' => array('field' => 'BuildingNameNumber', 'type' => 'String'),
                                         'CustomerStreet' => array('field' => 'Street', 'type' => 'String'),
                                         'CustomerAddressArea' => array('field' => 'LocalArea', 'type' => 'String'),
                                         'CustomerAddressTown' => array('field' => 'TownCity', 'type' => 'String'),
                                         'CustomerPostcode' => array('field' => 'PostalCode', 'type' => 'String'),
                                         'CustomerHomeTelNo' => array('field' => 'ContactHomePhone', 'type' => 'String'),
                                         'CustomerWorkTelNo' => array('field' => 'ContactWorkPhone', 'type' => 'String'),
                                         'CustomerMobileNo' => array('field' => 'ContactMobile', 'type' => 'String'),
                                         'CustomerEmailAddress' => array('field' => 'ContactEmail', 'type' => 'String'),
                                         'DataConsent' => array('field' => 'DataProtection', 'type' => 'enum', 'enums' => array ('Y'=>"Yes",'N'=>"No",'U'=>null))
                                        );
    
    private $api_parts_fields = array (
                                       'SBPartID' => array ('field' => 'SBPartID', 'type' => 'Integer'),
                                       'PartNo' => array ('field' => 'PartNo', 'type' => 'String', 'length' => 25),
                                       'Description' => array ('field' => 'PartDescription', 'type' => 'String', 'length' => 30),
                                       'Quantity' => array ('field' => 'Quantity', 'type' => 'Integer'),
                                       'PartStatus' => array ('field' => 'PartStatus', 'type' => 'String', 'length' => 30),
                                       'SectionCode' => array ('field' => 'SectionCode', 'type' => 'String', 'length' => 4),
                                       'DefectCode' => array ('field' => 'DefectCode', 'type' => 'String', 'length' => 4),
                                       'RepairCode' => array ('field' => 'RepairCode', 'type' => 'String', 'length' => 4),
                                       'CircuitRef' => array ('field' => 'CircuitReference', 'type' => 'String', 'length' => 10),
                                       'PartSerialNo' => array ('field' => 'PartSerialNo', 'type' => 'String', 'length' => 20),
                                       'OldPartSerialNo' => array ('field' => 'OldPartSerialNo', 'type' => 'String', 'length' => 20),
                                       'OrderDate' => array ('field' => 'OrderDate', 'type' => 'Date'),
                                       'OrderReceivedDate' => array ('field' => 'ReceivedDate', 'type' => 'Date'),
                                       'OrderNo' => array ('field' => 'OrderNo', 'type' => 'Integer'),
                                       'SupplierOrderNo' => array ('field' => 'SupplierOrderNo', 'type' => 'Integer'),
                                       'OrderInvoiceNo' => array ('field' => 'InvoiceNo', 'type' => 'String', 'length' => 30),
                                       'UnitCost' => array ('field' => 'UnitCost', 'type' => 'Decimal'), 
                                       'SaleCost' => array ('field' => 'SaleCost', 'type' => 'Decimal'),
                                       'VATRate' => array ('field' => 'VATRate', 'type' => 'Decimal'),
                                       'IsOtherCost' => array ('field' => 'IsOtherCost', 'type' => 'String', 'length' => 1),
                                       'IsAdjustment' => array ('field' => 'IsAdjustment', 'type' => 'String', 'length' => 1),
                                       'Supplier' => array ('field' => 'Supplier', 'type' => 'String', 'length' => 100)        
                                      );
    
    private $api_appointment_fields = array (
                                              'SBAppointID' => array ('field' => 'SBAppointID', 'type' => 'Integer'),
                                              'AppointmentDate' => array ('field' => 'AppointmentDate', 'type' => 'Date'),
                                              'AppointmentTime' => array ('field' => 'AppointmentTime', 'type' => 'String', 'length' => 10),
                                              'AppointmentType' => array ('field' => 'AppointmentType', 'type' => 'String', 'length' => 30),
                                              'OutCardLeft' => array ('field' => 'OutCardLeft', 'type' => 'enum', 'enums' => array ('Y'=>"Y",'N'=>"N")),
                                              'EngineerCode' => array ('field' => 'EngineerCode', 'type' => 'String', 'length' => 3)
                                             );
    
    private $api_call_history_fields = array (
                                               'EntryDate' => array ('field' => 'ContactDate', 'type' => 'Date'),
                                               'EntryTime' => array ('field' => 'ContactTime', 'type' => 'Time'),
                                               'Notes' => array ('field' => 'Note', 'type' => 'String', 'length' => 254),
                                               'UserCode' => array ('field' => 'UserCode', 'type' => 'String', 'length' => 20),
                                               'ImageURL' => array ('field' => 'ImageURL', 'type' => 'String', 'length' => 50),
                                               'ThumbnailURL' => array ('field' => 'ThumbnailURL', 'type' => 'String', 'length' => 50),
                                               'Private' => array ('field' => 'Private', 'type' => 'enum', 'enums' => array ('Y'=>"Y",'N'=>"N"))
                                             );
    
    private $api_model_fields = array (
                                       'ManufacturerID' => array ('field' => 'ManufacturerID', 'type' => 'Integer'),
                                       'UnitTypeID' => array ('field' => 'UnitTypeID', 'type' => 'Integer'),
                                       'ModelNo' => array ('field' => 'ModelNumber', 'type' => 'String'),
                                      );
    
    private $api_branch_fields = array (                      
                                        'BranchName' => array ('field' => 'BranchName', 'type' => 'String', 'length' => 40),
                                        'BranchBuildingName' => array ('field' => 'BuildingNameNumber', 'type' => 'String', 'length' => 40),
                                        'BranchStreet' => array ('field' => 'Street', 'type' => 'String', 'length' => 40),
                                        'BranchLocalArea' => array ('field' => 'LocalArea', 'type' => 'String', 'length' => 40),
                                        'BranchTown' => array ('field' => 'TownCity', 'type' => 'String', 'length' => 60),
                                        'BranchPostalCode' => array ('field' => 'PostalCode', 'type' => 'String', 'length' => 10),
                                        'BranchContactPhone' => array ('field' => 'ContactPhone', 'type' => 'String', 'length' => 40),
                                        'BranchContactFax' => array ('field' => 'ContactFax', 'type' => 'String', 'length' => 40),
                                        'BranchContactEmail' => array ('field' => 'ContactEmail', 'type' => 'String', 'length' => 40),
                                        'BranchAccountNo' => array ('field' => 'AccountNo', 'type' => 'String', 'length' => 40),
                                        'BranchNumber' => array ('field' => 'BranchNumber', 'type' => 'String', 'length' => 40)
                                       );
    
        private $api_claim_response_fields = array (                      
                                        'ErrorCode' => array ('field' => 'ErrorCode', 'type' => 'String', 'length' => 18),
                                        'ErrorDescription' => array ('field' => 'ErrorDescription', 'type' => 'String', 'length' => 50)
                                       );


    /*
     * Arrays for skyline->getFieldFromTable for matching strings supplied to API
     * from third party to Skyline IDs 
     */
    
    private $get_branch_user_id = array(
                                        'want' => 'BranchID',                   /* For branch users get the branch ID */
                                        'got' => 'UserID',                      /* Will return NULL for other user types */
                                        'from' => 'user'                
                                       );
    
    
    private $get_client_network = array(
                                        'want' => 'NetworkID',
                                        'got' => 'ClientID',
                                        'from' => 'network_client'                
                                       );
    
    private $get_client_id_from_client_account_no = array(
                                                          'want' => 'ClientID',
                                                          'got' => 'AccountNumber',
                                                          'from' => 'client'                
                                                         );
     
    private $get_country_id = array(
                                    'want' => 'CountryID',
                                    'got' => 'Name',
                                    'from' => 'country'                
                                   );
    
    private $get_county_id = array(
                                   'want' => 'CountyID',
                                   'got' => 'Name',
                                   'from' => 'county'                
                                  );
    
    private $get_customer_id_from_job = array(
                                              'want' => 'CustomerID',
                                              'got' => 'JobID',
                                              'from' => 'job'                
                                             );
    
    private $get_customer_title_id = array(
                                           'want' => 'CustomerTitleID',
                                           'got' => 'Title',
                                           'from' => 'customer_title'                
                                          );
    
    private $get_contact_history_action_id = array(
                                                   'want' => 'ContactHistoryActionID',
                                                   'got' => 'Action',
                                                   'from' => 'contact_history_action'                
                                                   );
    
    private $get_job_type_id_from_service_type_id = array(
                                                          'want' => 'JobTypeID',
                                                          'got' => 'ServiceTypeID',
                                                          'from' => 'service_type'                
                                                         );
    
    private $get_job_network = array(
                                     'want' => 'NetworkID',
                                     'got' => 'JobID',
                                     'from' => 'job'                
                                    );
    
    private $get_manufacturer_id = array(
                                         'want' => 'ManufacturerID',
                                         'got' => 'ManufacturerName',
                                         'from' => 'manufacturer'                
                                        );
    
    private $get_model_id = array(
                                  'want' => 'ModelID',
                                  'got' => 'ModelNumber',
                                  'from' => 'model'                
                                 );

    private $get_repair_skill_id_from_unit_type = array(
                                                        'want' => 'RepairSkillID',
                                                        'got' => 'UnitTypeID',
                                                        'from' => 'unit_type'                
                                                       );
    
    private $get_service_centre_user_id = array(                                /* For service centre users get the service centre ID */
                                                'want' => 'ServiceProviderID',  /* Will return NULL for other user types */
                                                'got' => 'UserID',
                                                'from' => 'user'                
                                               );
    
    private $get_service_centre_job_id = array(                                 /* For a  job get the service centre ID */
                                               'want' => 'ServiceProviderID',
                                               'got' => 'JobID',
                                               'from' => 'job'                
                                              );
    
    private $get_service_type_id = array(
                                         'want' => 'ServiceTypeID',
                                         'got' => 'Code',
                                         'from' => 'service_type'                
                                        );
    
    private $get_status_id = array(
                                   'want' => 'StatusID',
                                   'got' => 'StatusName',
                                   'from' => 'status'                
                                  );
    
    private $get_unit_type_id = array(
                                      'want' => 'UnitTypeID',
                                      'got' => 'UnitTypeName',
                                      'from' => 'unit_type'                
                                     );


    /**
     * Description
     * 
     * Deal with get  
     * 
     * @param array $args   Associative array of arguments passed                    
     * 
     * @return 
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/

    public function get( $args ) {
        
        if($this->debug) $this->log("AppointmentsAPIController::post  ".  var_export($args,true),'job_api_');
        
        switch (count($args)) {
            case 0:
                /* No arguments passed - SkyLine.GetNewJobs */
                $response = $this->getNewJobs();
                break;

            case 1:
                if (isset($args['SLNumber'])) {    
                    /* Check if SLNumber is passed */
                    $response = $this->getJobDetails(intval($args['SLNumber']));    /* Yes - so call Skyline.GetJobDetails */
                } else {
                    $response = array('Message' => "One paramter passed for Jobs API GET implies GetJobDetails, but required paramter SLNumber not passed");
                    $this->sendResponse(406,$response);
                    exit();
                }
                break;

            case 2:
                if ( isset($args['SearchString']) AND isset($args['SearchType']) ) {    /* If search string and seach type then call Skyline.ClientJobSearch */
                    $where = $this->buildSearch( urldecode($args['SearchString']), $args['SearchType'] );   /* Build where string */ 
                    if ($where == false) {
                        $response = array('Message' => "SearchType not valid");
                        $this->sendResponse(406,$response);
                        exit();
                    }
                    $response = $this->clientJobSearch($where);
                } elseif (
                    ( isset($args['SLNumber']) AND isset($args['PhoneNumber']) )    /* If any two of SLNumber, Phone number */
                    OR ( isset($args['SLNumber']) AND isset($args['Postcode']) )    /* and Postcode set then call */
                        OR ( isset($args['PhoneNumber']) AND isset($args['Postcode']) ) /* Skyline.ConsumerJobSearch */
                ) {
                    /* Build where */
                    //$where = $this->buildSearchArray($args);
                    $response = $this->consumerJobSearch($args);
                } else {
                    $response = array('Message' => "Two paramters passed for Jobs API GET implies ClientJobSearch or ConsumerJobSearch, but required paramters don't match either call");
                    $this->sendResponse(406, $response);
                    exit();
                }

                break;

            default:
                $response = array('Message' => "Number of partameteres passed for Jobs API GET does not match any known call");
                $this->sendResponse(501, $response);
                exit;
        }


        $this->sendResponse(200, $response);
        /*print_r($response);*/
    }

    
    /**
     * Description
     * 
     * Deal with put  
     * 
     * @param array $args   Associative array of arguments passed                    
     * 
     * @return 
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/

    public function put( $args ) {
        
        //$this->log(__METHOD__);
        //$this->debug = true;
                      
        switch (count($args)) {
            case 2:
                /* Two arguments - check what they are */
                if (isset($args['SLNumber']) AND isset($args['SCJobNo'])) {
                    /* PutNewJobs response */
                    $response = $this->PutNewJobsResponse($args);
                    
                    switch ($response['ResponseCode']) {
                       case "SC0001":                                           /* Success */
                            $httpcode = "202";                                   /* Accepted */
                            break;

                       case "SC0003":                                           /* Job not found*/
                            $httpcode = "401";                                   /* Unauthorised */
                            break;

                        default:
                            $httpcode = "422";                                   /* Unprocessable entity  */
                            break;
                    }
                    
                } else {
                    $response = array('Message' => "Two paramters passed for Jobs API PUT/POST implies PutNewJobsResponse, but required paramters SLNumber and SCJobNo not found");
                    $this->sendResponse(406, $response);
                    exit();
                }
                break;

            default:
                /* Other number of paramters */
               if ( isset($args['SCJobNo']) OR isset($args['SCJOBNO']) ) {
                    /* SkyLine Put Job Details */
                    $response = $this->putJobDetails($args);
                    
                    switch ($response['Code']) {
                       case "SC0001":                                           /* Job update successful */
                            if ($response['Response'] == "Created") {
                                $httpcode = "201";                               /* Created */
                            } else {
                                $httpcode = "202";                               /* Accepted */
                            }
                            break;

                       case "SC0006":                                           /* Username or password error */
                            $httpcode = "401";                                   /* Unauthorised */
                            break;

                        default:
                            $httpcode = "422";                                   /* Unprocessable entity  */
                            break;
                    }
                } else {
                    $response = array('Message' => "Number of paramters passed for Jobs API PUT/POST implies PutJobDeatils, but required paramter SCJobNo not found");
                    $this->sendResponse(406, $response);
                    exit();
                }
        }
        $this->sendResponse($httpcode, $response);
    }

    
    /**
     * Validate parameters passed to API
     * 
     * @param array $params     Associative array of fields to be validated 
     *        array $table      Associative array of paramters to db mapping
     * 
     * @return validated_args   Fields taken from prameter matched to table fields
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    
    public function validate_args($params, $table) {
        $validated = array();                                                   /* Validated fields */

        foreach($table as $tf => $tv) {
            $table[strtoupper($tf)] = $tv;
        }

        foreach($params as $pf => $pv) { 
            //if($this->debug) $this->log("JobsAPIController::validate_args   pf = {var_export($pf,true}  pv = pf = {var_export($pf,true}");
            if ( isset($table[$pf]) ) {
                switch (strtolower($table[$pf]['type'])) {
                    case 'string':
                        if (! empty($pv) ) {
                            $pv = str_replace("CRLF", "\r\n", $pv);
                            if ( isset($table[$pf]['length']) ) {
                                //$validated[$table[$pf]['field']] = substr((string) $pv,0,  urldecode($table[$pf]['length']));
                                $validated[$table[$pf]['field']] = substr((string) $pv,0,  $table[$pf]['length']);
                            } else {
                                //$validated[$table[$pf]['field']] = (string) urldecode($pv);
                                $validated[$table[$pf]['field']] = (string) $pv;
                            }
                        } /* fi (if ! empty */
                        break;

                    case 'date':
                        if ( ! is_null($pv) ) {
                            if ( $pv != 0 ) {
                                $validated[$table[$pf]['field']] = date( 'Y-m-d', strtotime (str_replace('/','.',$pv)) );
                            } /* fi $pv != 0 */
                        } /* fi ! is_null ($pv) */
                        break;

                    case 'time':
                        $validated[$table[$pf]['field']] = date( 'H:i:s', strtotime($pv) );
                        break;

                    case 'decimal':
                        $validated[$table[$pf]['field']] = (float) $pv;
                        break;

                    case 'integer':
                        $validated[$table[$pf]['field']] = intval($pv);
                        break;

                    case 'enum':
                        foreach ($table[$pf]['enums'] as $i => $e) {
                            if ( strtolower($i) == strtolower($pv) ) {
                                $validated[$table[$pf]['field']] = $e;
                            }
                        }
                        break;

                    default:
                        $validated[$table[$pf]['field']] = $pv;
                }
            }
        }
        return($validated);
    }

    /**
     * Description
     * 
     * Skyline API  Skyline.PutJobDetails
     * API Spec Section 8.1.4.7
     *  
     * Get new jobs which have not been marked as downloaded.
     * 
     * Outline:
     * 1)  Check user is a service centre or branch
     * 2)  Update service base license version details if neccessary
     * 3)  Check validity of fields
     * 4)  Decide if update or create
     * 5)  Check validity / get of SCAccount No
     * 6)  Create/Update Jobs
     * 7)  If neccessary create / update customers
     * 8)  If neccessary create / update Parts
     * 9)  If neccessary create / update Appointments
     * 10) If neccessary create / update Status Updates
     * 11) If neccessary create / update Call History
     * 
     * @param array $fields    Associative array of fields
     *              
     * 
     * @return putJobDetails  Associative array containing respose
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    
    private function putJobDetails($fields) {  
        $api_appointments_model = $this->loadModel('APIAppointments');
        $api_user_model = $this->loadModel('APIUsers');
        $api_jobs_model = $this->loadModel('APIJobs');
        $allocation_reassignmnet = $this->loadModel('AllocationReassignment');
        $appointment_model = $this->loadModel('Appointment');
        $branches_model = $this->loadModel('Branches');
        $client_model = $this->loadModel('Clients');
        $contact_history_actions_model = $this->loadModel('ContactHistoryActions');
        $contact_history_model = $this->loadModel('ContactHistory');
        $county_model = $this->loadModel('County');
        $customer_model = $this->loadModel('Customer');
        $job_model = $this->loadModel('Job');
        $job_source_model = $this->loadModel('JobSource');
        $manufacturers_model = $this->loadModel('Manufacturers');
        $models_model = $this->loadModel('Models');
        $non_skyline_job_model = $this->loadModel('NonSkylineJob');
        $part_model = $this->loadModel('Part');
        $product_model = $this->loadModel('Product');
        $service_networks_model = $this->loadModel('ServiceNetworks');
        $service_providers_model = $this->loadModel('ServiceProviders');
        $service_types_model = $this->loadModel('ServiceTypes');
        $skyline_model = $this->loadModel('Skyline');
        $status_history_model = $this->loadModel('StatusHistory');
        $claim_response_model = $this->loadModel('ClaimResponse');
        $sb_model = $this->loadModel('ServiceBaseBusinessModel');
                      
        if ( isset($fields['DOWNLOADEDTOSC']) ) $fields['DownloadedToSC'] = $fields['DOWNLOADEDTOSC'];
        if ( isset($fields['DATEBOOKED']) ) $fields['DateBooked'] = $fields['DATEBOOKED'];
        if ( isset($fields['TIMEBOOKED']) ) $fields['TimeBooked'] = $fields['TIMEBOOKED'];
        if ( isset($fields['MANUFACTURER']) ) $fields['Manufacturer'] = $fields['MANUFACTURER'];
        if ( isset($fields['MANUFACTURERID']) ) $fields['ManufacturerID'] = $fields['MANUFACTURERID'];
        if ( isset($fields['CLOSEDDATE']) ) $fields['ClosedDate'] = $fields['CLOSEDDATE'];
        if ( isset($fields['CUSTOMERFORENAME']) ) $fields['CustomerForename'] = $fields['CUSTOMERFORENAME'];
        if ( isset($fields['CUSTOMERSURNAME']) ) $fields['CustomerSurname'] = $fields['CUSTOMERSURNAME'];
        if ( isset($fields['CUSTOMERBUILDINGNAME']) ) $fields['CustomerBuildingName'] = $fields['CUSTOMERBUILDINGNAME'];
        if ( isset($fields['CUSTOMERSTREET']) ) $fields['CustomerStreet'] = $fields['CUSTOMERSTREET'];
        if ( isset($fields['CUSTOMERADDRESSAREA']) ) $fields['CustomerAddressArea'] = $fields['CUSTOMERADDRESSAREA'];
        if ( isset($fields['CUSTOMERADDRESSTOWN']) ) $fields['CustomerAddressTown'] = $fields['CUSTOMERADDRESSTOWN'];
        if ( isset($fields['CUSTOMERCOUNTY']) ) $fields['CustomerCounty'] = $fields['CUSTOMERCOUNTY'];
        if ( isset($fields['CUSTOMERCOUNTRY']) ) $fields['CustomerCountry'] = $fields['CUSTOMERCOUNTRY'];
        if ( isset($fields['CUSTOMERPOSTCODE']) ) $fields['CustomerPostcode'] = $fields['CUSTOMERPOSTCODE'];
        if ( isset($fields['CUSTOMERHOMETELNO']) ) $fields['CustomerHomeTelNo'] = $fields['CUSTOMERHOMETELNO'];
        if ( isset($fields['CUSTOMERWORKTELNO']) ) $fields['CustomerWorkTelNo'] = $fields['CUSTOMERWORKTELNO'];
        if ( isset($fields['CUSTOMERMOBILENO']) ) $fields['CustomerMobileNo'] = $fields['CUSTOMERMOBILENO'];
        if ( isset($fields['CUSTOMEREMAILADDRESS']) ) $fields['CustomerEmailAddress'] = $fields['CUSTOMEREMAILADDRESS'];
        if ( isset($fields['SERVICETYPE']) ) $fields['ServiceType'] = $fields['SERVICETYPE'];
        if ( isset($fields['JOBTYPE']) ) $fields['JobType'] = $fields['JOBTYPE'];
        if ( isset($fields['UNITTYPE']) ) $fields['UnitType'] = $fields['UNITTYPE'];
        if ( isset($fields['MODELNO']) ) $fields['ModelNo'] = $fields['MODELNO'];
        if ( isset($fields['PRODUCTNUMBER']) ) $fields['ProductNumber'] = $fields['PRODUCTNUMBER'];
        if ( isset($fields['STATUSUPDATE']) ) $fields['StatusUpdate'] = $fields['STATUSUPDATE'];
        if ( isset($fields['STATUS']) ) $fields['Status'] = $fields['STATUS'];
        if ( isset($fields['SLNUMBER']) ) $fields['SLNumber'] = $fields['SLNUMBER'];
        if ( isset($fields['RMANUMBER']) ) $fields['RMANumber'] = $fields['RMANUMBER'];
        if ( isset($fields['SCJOBNO']) ) $fields['SCJobNo'] = $fields['SCJOBNO'];
        if ( isset($fields['SBLICENCENO']) ) $fields['SBLicenceNo'] = $fields['SBLICENCENO'];
        if ( isset($fields['SBVERSION']) ) $fields['SBVersion'] = $fields['SBVERSION'];
        if ( isset($fields['PARTS']) ) $fields['Parts'] = $fields['PARTS'];
        if ( isset($fields['APPOINTMENTS']) ) $fields['Appointments'] = $fields['APPOINTMENTS'];
        if ( isset($fields['STATUSUPDATE']) ) $fields['StatusUpdate'] = $fields['STATUSUPDATE'];
        if ( isset($fields['CALLHIST']) ) $fields['CallHist'] = $fields['CALLHIST'];
        if ( isset($fields['StatusUpdates']) ) $fields['StatusUpdate'] = $fields['StatusUpdates'];
        if ( isset($fields['ContactHistory']) ) $fields['CallHist'] = $fields['ContactHistory'];
                
        $samsungApi = new SamsungClientController();

        if($this->debug) $this->log('JobsAPI->putJobDetails  START','JobsAPI_');

        $job_fields = array ();                                                 /* Array to contain avclidated jobs fields */

        /* Check user is either a service centre or a branch */
        if ( ($this->getUserType() != "ServiceProvider" ) AND ($this->getUserType() != "Branch") ) {
            $response = array (
                'Code' => "SC0006",
                'Response' => "Incorrect user type. Must be Service Centre or Branch.",
                'ResponseCode' => "SC0006",
                'ResponseDetails' => "Incorrect user type. Must be Service Centre or Branch.",
                'User' => $this->user->UserID,
                'UserType' => $this->getUserType()
            );
            return($response);
        }

        if ( isset($this->user->ServiceProviderID) ) {                          /* Of the user has a service provider ID set */
            $fields['ServiceProviderID'] = intval($this->user->ServiceProviderID);  /* use it */
        }

        if($this->debug) $this->log('fields = '.var_export($fields,true),'JobsAPI_');

        /****************************************
         * Perfom initial validation and checks *
         ***************************************/

        $fields['DownloadedToSC'] = true;                                       /* If we are uploading fom SC then we don't need to download data ! */
        
        if ( isset($fields['RefreshAll']) && $fields['RefreshAll'] == 'Y' ) {   /* Check if he refresh process is being called */
            $refresh = true;
            $fields['RefreshDate'] = date( 'Y-m-d H:i:s' );
        } else {
            $refresh = false;
        }

        /* Issue 114 - deal with closed date for field calls, see https://bitbucket.org/pccs/skyline/issue/114/closeddate-auto-fill-for-field-calls 
         * Neil Wrighting -  In Skyline.putJobDetails if the RepairCompleteDate is filled in and the ClosedDate is blank, check if the job site is 'field call'
         * and if it is fill in the ClosedDate with the same date as the RepairCompelteDate (this will then need to trigger the code to do update the model number
         * etc) */
        if (isset($fields['DateRepairComplete'])) {
            if ( isset($fields['JobSite']) && ($fields['JobSite'] == 'F') ) {
                $fields['ClosedDate'] = $fields['DateRepairComplete'];
            } /* fi */
        } /* fi isset $fields['DateRepairComplete'] */

        /*
         * Manufacturer
         */
        if (isset($fields['Manufacturer'])) {                                   /* Deal with manufacturer field if set */  
            $fields['ManufacturerID'] = $skyline_model->getFieldFromTable($this->get_manufacturer_id, $fields['Manufacturer']); /* Get the ID of the manufacturer */
            if (is_null($fields['ManufacturerID'])) {                           /* Manufacturer not found */
                if (isset($fields['ClosedDate'])) {                             /* But Closed date has been set */
                    $fields['ManufacturerID'] = $skyline_model->getFieldFromTable($this->get_manufacturer_id, 'UNKNOWN');   /* Set Manufacturer to Unknown */
                } else {
                    unset ($fields['ManufacturerID']);                          /* No Match and not closed - Don't want to set ID field in database as it is a foreign key */
                    //$fields['ManufacturerID'] = "NULL";
                } /* fi isset $args['ClosedDate'] */
            } else {
                $fields['Manufacturer'] = null;                                 /* When the ManufacturerID can be found we want to set ServiceBase Manufacturer to anything */
            } /* is_null $fields['ManufacturerID'] */
        }

        /*
         * Customer
         */
        if ( isset($fields['CustomerTitle'])                                    /* Check if any customer fields are set */
                OR isset($fields['CustomerForename'])
                OR isset($fields['CustomerSurname'])
                OR isset($fields['CustomerBuildingName'])
                OR isset($fields['CustomerStreet'])
                OR isset($fields['CustomerAddressArea'])
                OR isset($fields['CustomerAddressTown'])
                OR isset($fields['CustomerCounty'])
                OR isset($fields['CustomerCountry'])
                OR isset($fields['CustomerPostcode'])
                OR isset($fields['CustomerHomeTelNo'])
                OR isset($fields['CustomerWorkTelNo'])
                OR isset($fields['CustomerMobileNo'])
                OR isset($fields['CustomerEmailAddress'])
        ) {
            $customer_fields = $this->validate_args($fields, $this->api_customer_fields);

            if (isset($fields['CustomerTitle'])) {                              /* Check if we have Customer Title */
                $titleID = $skyline_model->getFieldFromTable($this->get_customer_title_id, addslashes($fields['CustomerTitle']));
                if ($titleID !== null) {
                    $customer_fields['CustomerTitleID'] = $titleID;
                }
            }

            if (isset($fields['CustomerCounty'])) {                             /* Check if we have Country */
                $countyID = $skyline_model->getFieldFromTable($this->get_county_id, addslashes($fields['CustomerCounty']));
                if ($countyID !== null) {
                    $customer_fields['CountyID'] = $countyID; 
                }                          
            }

            if (isset($fields['CustomerCountry'])) {                        /* Check if we have Country */
                $countryID = $skyline_model->getFieldFromTable($this->get_country_id, addslashes($fields['CustomerCountry']));
                if ($countryID !== null) {
                    $customer_fields['CountryID'] = $countryID;
                }
            }
            if($this->debug) $this->log('customer_fields = '.var_export($customer_fields,true),'JobsAPI_');
        }

        /*
         * Service Type
         */
        if (isset($fields['ServiceType'])) {                                                           /* Deal with service type field if set */
            if (isset($fields['JobType'])) {
                $service_job_id = $service_types_model->getServiceTypeIdJobTypeIdFromBoth($fields['ServiceType'], $fields['JobType']);
                $fields['ServiceTypeID'] = $service_job_id['ServiceTypeID'];
                $fields['JobTypeID'] = $service_job_id['JobTypeID'];
            } else {
                $service_job_id = $service_types_model->getServiceTypeIdJobTyeId($fields['ServiceType']);  /* Get the JobTypeID and ServiceTypeID */
                $fields['ServiceTypeID'] = $service_job_id['ServiceTypeID'];
                $fields['JobTypeID'] = $service_job_id['JobTypeID'];
            }

            if ( is_null($service_job_id['ServiceTypeID']) ) {                                                    /* Set error code and exit for nor found */
                $response = array (
                    'Code' => "SC0007",
                    'Response' => "{$fields['SCJobNo']}: no such service type {$fields['ServiceType']}"
                );
                return($response);
            }

            $fields['ServiceTypeID'] = $service_job_id['ServiceTypeID'];
            $fields['JobTypeID'] = $service_job_id['JobTypeID'];
        } elseif (isset($fields['JobType'])) {
            $service_job_id = $service_types_model->getServiceTypeIdJobTyeId($fields['JobType']);  /* Get the JobTypeID and ServiceTypeID */

            if ( is_null($service_job_id['ServiceTypeID']) ) {                                                    /* Set error code and exit for nor found */
                $response = array (
                    'Code' => "SC0007",
                    'ResponseCode' => "SC0007",
                    'Response' => "{$fields['SCJobNo']}: no such service type {$fields['JobType']}",
                    'ResponseDetails' => "{$fields['SCJobNo']}: no such service type {$fields['JobType']}"
                );
                return($response);
            }

            $fields['ServiceTypeID'] = $service_job_id['ServiceTypeID'];
            $fields['JobTypeID'] = $service_job_id['JobTypeID'];
        }

        /*
         * Unit Type
         */
        if (isset($fields['UnitType'])) {                                                                            /* Deal with unit type field if set */  
            $fields['UnitTypeID'] = $skyline_model->getFieldFromTable($this->get_unit_type_id, $fields['UnitType']); /* Get the ID of the unit type */
            if (is_null($fields['UnitTypeID'])) {                                                                    /* Unit type ID not found so don't write */
                unset ($fields['UnitTypeID']);                                  /* No Match and not closed - Don't want to set ID field in database as it is a foreign key */
            } else {
                unset ($fields['UnitType']);                                    /* Have UnitTypeID so don't need ServiceBaseUnitType */
            }
        }

        /*
         * Model
         */

        /* If we have a model number and a unit type ID then get Model ID (If we have no UnitTypeID we don't want a model ID as we need both, See VMS Trackerbase Log 140 */
        if ( isset($fields['ModelNo']) && isset($fields['UnitTypeID']) ) {
            $fields['ModelID'] = $skyline_model->getFieldFromTable($this->get_model_id, $fields['ModelNo']);        /* Get the ID of the model */
            if (is_null($fields['ModelID'])) {                                                                      /* Model ID not found */
                if (isset($fields['ClosedDate'])
                        AND isset($fields['UnitTypeID'])
                        AND isset($fields['ManufacturerID'])
                   ) {                               /* But Closed date has been set */
                    $model_fields = $this->validate_args($fields, $this->api_model_fields);                         /* Get & validate fields for model */
                    $model_result = $models_model->create($model_fields);                                           /* Create model */
                    if ( $model_result['status'] == 'FAIL') {
                        $response = array (
                            'Code' => "SC0004",
                            'Response' => "Can not create model {$fields['ModelNo']}"
                        );
                        return($response);
                    }
                    $fields['ModelID'] = $model_result['modelId'];
                    $fields['ModelNo'] = null;
                } else {
                    $fields['ModelID'] = 0;                                     /* No Match and not closed - Set ID field to 0 - this will later be turned into NULL in database as it is a foreign key */
                    //unset($fields['ModelID']);
                } /* fi isset $args['ClosedDate'] */
            } else {
                $fields['ModelNo'] = '';                                        /* We have a match for UnitTypeModel so don't need ServiceCentreModel */
                $fields['UnitType'] = '';                                       /* Therefore we must also have a unit type as well so don't need ServiceCentreUnitType */
            } /* fi is_null $fields['ModelID'] */
        }

        /*
         *  Product
         */
        if ( isset($fields['ProductNumber']) ) {                                /* Deal with Product ID if set */
            $fields['ProductID'] = $product_model->getIdFromProductNo($fields['ProductNumber']);
            if (is_null($fields['ProductID'])) {                                /* check if we go a match */
                unset($fields['ProductID']);                                    /* No match - so don't want to set Product ID */
            }
        }

        /*
         * Status Update
         */
        if (isset($fields['StatusUpdate'])) {                                                           /* If status update (status history) set */
            $status_updates_fields_params = json_decode($fields['StatusUpdate'],true);
            if($this->debug) $this->log('$status_updates_fields_params = '.var_export($status_updates_fields_params,true),'JobsAPI_');

            $status_update = array();

            for ( $n = 0; $n < count($status_updates_fields_params); $n++ ) {
                if ( $status_updates_fields_params[$n]['NewStatus'] != "")  {                        /* Some service centres send blank status - ignore if this is the case */
                    $status_update[$n]['UserID'] = intval($this->user->UserID);
                    if ( isset($status_updates_fields_params[$n]['UserCode']) ) {
                        $status_update[$n]['UserCode'] = $status_updates_fields_params[$n]['UserCode'];
                    } else {
                        $status_update[$n]['UserCode'] = '';
                    }

                    $status_update[$n]['Date'] = date(
                            'Y-m-d H:i:s'
                                                    ,
                                                    strtotime(
                                                                str_replace(                        /* strreplace will change dd/mm/yyyy to dd.mm.yyyy to prevent PHP */
                                                                            '/',                    /* from converting to American mdy format (default when / is used ) */
                                                                            '.',
                                                                            $status_updates_fields_params[$n]['DateChanged']." ".$status_updates_fields_params[$n]['TimeChanged']
                                    )
                            )
                    );
                    $status_update[$n]['StatusID'] = intval($skyline_model->getFieldFromTable($this->get_status_id, $status_updates_fields_params[$n]['NewStatus']));

                    if(is_null($status_update[$n]['StatusID'])) {                                              /* Invalid status - no match */
                        $response = array (
                            'Code' => "SC0007",
                                        'Response' => $fields['SCJobNo'].": no such status ".$status_updates_fields_params[$n]['NewStatus']
                        );
                        return($response);
                    }
                }
            }
            if($this->debug) $this->log('$status_update = '.var_export($status_update,true),'JobsAPI_');
        }

        if (isset($fields['Status'])) {
            $fields['StatusID'] = $skyline_model->getFieldFromTable($this->get_status_id, $fields['Status']);
        }

        $fields['ModifiedUserID'] = $this->user->UserID;
        $fields['ModifiedDate'] = date('Y-m-d H:i:s');

        /************************************************************************
         *                                                                       *
         * Check Skyline and RMA number to deicde if we are creating or updating *
         *                                                                       *
         ************************************************************************/

        if ( (! isset($fields['SLNumber'])) && (isset($fields['RMANumber'])) ) {/* If No JobID but a RMS number sent */
            $jId = $job_model->getIdFromRmaNo($fields['RMANumber']);            /* Search for the JobID against RMA numbre */

            if (! is_null($jId) ) {                                              /* If return value is not null */
                $fields['SLNumber'] = $jId;                                     /* Then we have matched a JobID so set it */
            }
        }

        if (isset($fields['SLNumber'])) {
            /*******************************
             *  Job ID exists so is update *
             ******************************/
            $action = "update";
            if($this->debug) $this->log('putJobDetails: Update '.$fields['SLNumber'],'JobsAPI_');

            if ( ! $skyline_model->getJobExistsUser($fields['SLNumber'], $this->user->UserID) ) {
                $response = array (                                             /* Job number not found under current user */
                    'Code' => "SC0006",
                    'ResponseCode' => "SC0006",
                                   'Response' => "Job {$fields['SLNumber']} not found for user ".$this->user->Username,
                                   'ResponseDetails' => "Job {$fields['SLNumber']} not found for user ".$this->user->Username
                );
                return($response);
            }
            
            /* save a ocpy of job prior to updating any fields */

            $sb_model->FetchSkylineJob($fields['SLNumber']);

            /*
             * VMS Trackerbase  Log 168
             * When receiving an update to a job through putJobDetails (job already exists), if the postcode area and secondary service provider 
             * match in the incoming job, do not import the details (just reply to say successfully imported).
             */
            
            if ( isset($fields['CustomerPostcode']) && isset($fields['ServiceProviderID'] ) ) {                     /* Need a customer post code to get the area for */          
                $area = trim(substr($fields['CustomerPostcode'], 0, -3));   /* Get UK postode area */

                $secondarySpId = $allocation_reassignmnet->getSecondaryServiceProvider($fields['ServiceProviderID'], $area);
                
                /* VMS Trackerbase Log 232 - Only check allocation reassignment if not the assigned Service Provider */
                if ($job_model->getServiceCentre($fields['SLNumber']) != $fields['ServiceProviderID']) {
                /* END VMS Trackerbase Log 232 */
                    if ( $allocation_reassignmnet->isSecondaryServiceProvider($fields['ServiceProviderID'], $area)) {                   

                        /* Is secondary so just ignore and report OK  */
                         $response = array (                 
                                 'Code' => "SC0001",
                                 'ResponseCode' => "SC0001",
                                 'Response' =>  "Updated",
                                 'ResponseDetails' => "Updated",
                                 'SLNumber' => $fields['SLNumber'],
                                 'Note' => 'Secondary Provider'
                                );

                         return($response);
                    } /* fi isSecondaryServiceProvider $fields['ServiceProviderID'], $area */
                } /* fi Job ServiceProviderId != This users service ProviderID -- Trackerbase VMS Log 232 */
            } /* fi isset $fields['CustomerPostcode'] && isset$fields['ServiceProviderID'] */

            /* END VMS Trackerbase Log 168  */
            
            /* BEGIN Issue 343 - ajw - 01/05/2013 */
            /*
             * Ignore Client Number Check in PutJobDetails for ServiceBase Refresh
             * https://bitbucket.org/pccs/skyline/issue/343/ignore-client-number-check-in
             * 
             * WARNING This is a temporary patch (as it has security implications) until such time as the Trade Accounts Issue is solved
             */
            if ( $refresh ) {                                                   /* If we are running servicbase refresh */
                unset($fields['ClientAccountNo']);                              /* Then we are not interested in Client Account Number */
            }
            /* END Issue 343 */

            if ( isset($fields['ClientAccountNo']) ) {
                if ( ! $api_user_model->checkUserValidClientAccountNo($fields['ClientAccountNo'], $this->user->UserID, $this->getUserType()) ) {    /* No match on client id */
                    $network_service_provider_array = $skyline_model->getNetworkServiceProviderAccountNo($fields['ClientAccountNo']);                               /* So check network */
                    if (! $skyline_model->getFieldFromTable($this->get_job_network,$fields['SLNumber']) == $network_service_provider_array['NetworkID']) {
                        $response = array (                                             /* User does not match client account number or nsp account number */
                            'Code' => "SC0006",
                            'ResponseCode' => "SC0006",
                            'Response' => "Logged in user ".$this->user->Username." does not match ClientAccountNo ".$fields['ClientAccountNo'],
                            'ResponseDetails' => "Logged in user ".$this->user->Username." does not match ClientAccountNo ".$fields['ClientAccountNo']
                        );
                        return($response);
                    }
                }
            }

            $job_fields = $this->validate_args($fields, $this->api_job_fields); /* Get fields that have been passed, extract the ones fo the job table, and validate type */
            $product_location = $job_model->getProductLocation($fields['SLNumber']);    /* Get the product loction */
            if ( is_null($product_location) || ($product_location != 'Branch') ) {
                $job_fields['ClosedDate'] = isset($fields['ServiceProviderDespatchDate']) ? $fields['ServiceProviderDespatchDate'] : null;
            }

            if ( isset($fields['ModelID']) && ($fields['ModelID'] == 0) ) {
                $job_fields['ModelID'] = null;
            }

            if (isset($fields['ColAddCounty'])) {
                $job_fields['ColAddCountyID'] = $skyline_model->getFieldFromTable($this->get_county_id, addslashes($fields['ColAddCounty']));
            }

            if (isset($fields['ColAddCountry'])) {
                $job_fields['ColAddCountryID'] = $skyline_model->getFieldFromTable($this->get_county_id, addslashes($fields['ColAddCountry']));
            }

            if (isset($fields['CustomerID'])) $job_fields['CustomerID'] = $fields['CustomerID'];

            if ( isset($customer_fields) ) {                                    /* We have customer fields to deal with */
                $customerId = $skyline_model->getFieldFromTable($this->get_customer_id_from_job, $fields['SLNumber']);      /* Get the cutsomer ID */
                if($this->debug) $this->log("Get Customer ID from Job ID = $customerId",'JobsAPI_');

                if (is_null($customerId)) {                                      /* No valid customer ID */

                    if ( isset($fields['CustomerForename']) || isset($fields['CustomerSurname']) || isset($fields['CustomerPostcode']) ) {
                        if (!isset($fields['CustomerForename'])) $fields['CustomerForename'] = "";  /* If Name/Postcode not sent set to blank */
                        if (!isset($fields['CustomerSurname'])) $fields['CustomerSurname'] = "";
                        if (!isset($fields['CustomerPostcode'])) $fields['CustomerPostcode'] = "";

                        $customerId = $skyline_model->getCustomerIdByNamePostcode(addslashes($fields['CustomerForename']), addslashes($fields['CustomerSurname']), $fields['CustomerPostcode']);
                        if($this->debug) $this->log("Get Customer ID from Name and Post Code = $customerId",'JobsAPI_');
                        if ( is_null($customerId) ) {
                            /*$response = array (
                              'Code' => "SC0003",
                              'ResponseCode' => "SC0003",
                              'Response' => "Cannot find customer for {$fields['SLNumber']}",
                              'ResponseDetails' => "Cannot find customer for {$fields['SLNumber']}"
                              );
                            return($response);*/
                            if($this->debug) $this->log("Use customer model to create record",'JobsAPI');
                            
                            $customer_create_output = $customer_model->create($customer_fields);    /* Create Customer */
                            $customerId = $customer_create_output['id'];
                            
                            if($this->debug) $this->log('Customer model (create) output ='.var_export($customer_create_output, true),'JobsAPI_');
                        }

                    }

                }

                $customer_model->updateByID($customer_fields, $customerId);
            }

            if (isset($customerId)) $job_fields['CustomerID'] = $customerId;

            if($this->debug) $this->log('$job_fields = '.var_export($job_fields,true),'JobsAPI_');

            $job_response = $job_model->update($job_fields);                    /* Pass these fields to the jobs model and update */
            
            if ($job_response['status'] == 'FAIL') {                            /* Check the return value */
                $response = array (
                    'Code' => "SC0005",
                    'ResponseCode' => "SC0005",
                    'Response' => "Record insert error. PDO Error : {$job_response['message']}",
                    'ResponseDetails' => "Record insert error. PDO Error : {$job_response['message']}"
                );
                return($response);
            }
            
            /* send updates to service base only if originator field is not ServiceBase */
            //if ($action == "update" && (empty($fields['Originator']) || strtoupper($fields['Originator']) != 'SERVICEBASE')) {
            //    $sb_model->PutJobDetails($fields['SLNumber']);

            //}
            
        } else {
            /*********************************
            * No job ID so we need to create *
            *********************************/
            $action = "create";
            if($this->debug) $this->log('putJobDetails: Create','JobsAPI');

            $fields['BookedBy']  = $this->user->UserID;
            
            /*
             * Booked Date and time
             */

            if(!isset($fields['DateBooked'])) {                                 /* Has the user passed a DateBooked field to the API? */
                $fields['DateBooked'] = date('Y-m-d');                          /* If not set to today */
            }

            if(!isset($fields['TimeBooked'])) {                                 /* Has the user passed a TimeBooked field to the API? */
                $fields['TimeBooked'] = date('H:i:s');                          /* If not set to now */
            }

            if ( ! isset($fields['ClientAccountNo']) ) {
                $response = array (                                             /* No Client Account number sent - must be sent for create */
                    'Code' => "SC0004",
                    'Response' => "Must pass Client Account Number when creating job"
                );
                return($response);
            }
            if ( ! isset($fields['ServiceTypeID']) ) {
                $response = array (                                             /* No Service Type - must be sent for create */
                    'Code' => "SC0004",
                    'Response' => "Must pass Service Type"
                );
                return($response);
            }

            /* ClientID is ClientID in jobs table */
            $client_array = $client_model->getClientAccNoId($fields['ClientAccountNo']);
            $fields['ClientID'] = $client_array['ClientID'];
            if (is_null($fields['ClientID'])) {
                $network_service_provider_array = $skyline_model->getNetworkServiceProviderAccountNo($fields['ClientAccountNo']);

                if( ! is_null($network_service_provider_array) ) {              /* Check Account Number in network_service_provider */
                    $fields['ServiceProviderID'] = $network_service_provider_array['ServiceProviderID'];
                    $fields['NetworkID'] = $network_service_provider_array['NetworkID'];
                    $fields['ClientID'] = $skyline_model->getNetworkDefaultClient($fields['NetworkID']);
                    if (is_null($fields['ClientID'])){
                        $response = array(
                            'Code' => "SC0006",
                            'Response' => "Cannot find client for ClientAccountNo {$fields['ClientAccountNo']} from Network {$fields['NetworkID']}"
                        );
                        return($response);
                    }
                    $fields['BranchID'] = $skyline_model->getClientDefaultBranch($fields['ClientID']);
                    if (is_null($fields['BranchID'])){
                        $response = array(
                            'Code' => "SC0006",
                            'Response' => "No default branch set for Client {$fields['ClientID']}"
                        );
                        return($response);
                    }
                } else {                                                        /* No valid client ID */
                    $response = array (
                        'Code' => "SC0006",
                        'Response' => "Cannot find client for ClientAccountNo ".$fields['ClientAccountNo']
                    );
                    return($response);
                }
            } else {
                if ( !is_null($client_array['ServiceProviderID']) ) $fields['ServiceProviderID'] = $client_array['ServiceProviderID'];
                $fields['NetworkID'] = $skyline_model->getFieldFromTable($this->get_client_network, $fields['ClientID']);
            }

            /*
             * Check Branch details
             * See enhancement from Neil Wrighting in issue #128 - https://bitbucket.org/pccs/skyline/issue/128/putjobdetails-changes-for-fixzone-jobs
             */

            if(isset($fields['BranchNumber']) && ($fields['BranchNumber'] != '')) {
                $fields['BranchID'] = $branches_model->getBranchIdByBranchNoClient($fields['BranchNumber'], $fields['ClientID']);

                if (is_null($fields['BranchID'])) {                             /* Branch not found so we need to create */
                    $branch_fields = $this->validate_args($fields, $this->api_branch_fields );
                    $branch_fields['ClientID'] = $fields['ClientID'];
                    $branch_fields['NetworkID'] = $fields['NetworkID'];
                    $branch_fields['Status'] = 'Active';
                    if ( ! isset($branch_fields['Name']) ) $branch_fields['Name'] = "";     /* Deal with fields not passed */
                    if ( ! isset($branch_fields['BuildingNameNumber']) ) $branch_fields['BuildingNameNumber'] = "";
                    if ( ! isset($branch_fields['Street']) ) $branch_fields['Street'] = "";
                    if ( ! isset($branch_fields['LocalArea']) ) $branch_fields['LocalArea'] = "";
                    if ( ! isset($branch_fields['TownCity']) ) $branch_fields['TownCity'] = "";
                    if ( ! isset($branch_fields['PostalCode']) ) $branch_fields['PostalCode'] = "";
                    if ( ! isset($branch_fields['ContactPhone']) ) $branch_fields['ContactPhone'] = "";
                    if ( ! isset($branch_fields['ContactPhoneExt']) ) $branch_fields['ContactPhoneExt'] = "";
                    if ( ! isset($branch_fields['ContactFax']) ) $branch_fields['ContactFax'] = "";
                    if ( ! isset($branch_fields['ContactEmail']) ) $branch_fields['ContactEmail'] = "";
                    if ( ! isset($branch_fields['BrachType']) ) $branch_fields['BranchType'] = "";
                    if ( ! isset($branch_fields['BranchNumber']) ) $branch_fields['BranchNumber'] = "";
                    $branch_fields['CountyID'] = "";
                    $branch_fields['CountryID'] = "";
                    $branch_fields['ServiceAppraisalRequired'] = "No";
                    $branch_fields['BrandID'] = $this->user->DefaultBrandID;

                    $brresp = $branches_model->create($branch_fields);

                    if (is_null($brresp['BranchID'])) {
                        $response = array(
                            'Code' => "SC0004",
                            'Response' => "Can't create new branch for {$fields['BranchAccountNo']}"
                        );
                        return($response);
                    } else {
                        $fields['BranchID'] = $brresp['BranchID'];
                        if ($this->debug) $this->log("Created Branch {$fields['BranchID']} for AccountNo {$fields['BranchAccountNo']}");
                    }
                } /*fi is_null BranchID */
            } /* fi isset BranchAccountNo & BranchAccountNo ! '' */

            $default_branch = $skyline_model->getClientDefaultBranch($fields['ClientID']);
            if (! isset($fields['BranchID']) && (! is_null($default_branch)) ) {
                $fields['BranchID'] = $default_branch;                          /* If no branch set use default brannch if availiable */
            }

            /* Unit Type */
            if ( isset($fields['UnitTypeID']) ) {                                                                                               /* If we have unit type */
                $fields['RepairSkillID'] = $skyline_model->getFieldFromTable($this->get_repair_skill_id_from_unit_type, $fields['UnitTypeID']); /* Get Reapir Skill */
            }

            if ( isset($fields['ServiceTypeID']) ) {                                                                                               /* If we have ServiceType ID */
                $fields['JobTypeID'] = $skyline_model->getFieldFromTable($this->get_job_type_id_from_service_type_id, $fields['ServiceTypeID']);/* We can get JobTypeID */
            }

            if ( $this->getUserType() == "ServiceProvider" ) {                  /* User is a service centre */
                $fields['ServiceProviderID'] = $skyline_model->getFieldFromTable($this->get_service_centre_user_id, $this->user->UserID);
                if($this->debug) $this->log('User is service Provider, SP ID = '.$fields['ServiceProviderID']);
            } else {                                                            /* User must be a branch (as other types are not allowed to use this API */
                if (! isset($fields['BranchID']) ) $fields['BranchID'] = $skyline_model->getFieldFromTable($this->get_branch_user_id, $this->user->UserID);

                $fscArgs = array();                                             /* Arguments for skyline_model->findServiceCentre() */
                if ( isset($fields['NetworkID']) ) $fscArgs['NetworkID'] = $fields['NetworkID'];
                $fscArgs['Status'] = 'Active';                                  /* If new job being put is must be active ! */
                if ( isset($fields['CountryID']) ) $fscArgs['CountryID'] = $fields['CountryID'];                        
                if ( isset($fields['RepairSkillID']) ) $fscArgs['RepairSkillID'] = $fields['RepairSkillID'];
                if ( isset($fields['ClientID']) ) $fscArgs['ClientID'] = $fields['ClientID'];
                if ( isset($fields['ManufacturerID']) ) $fscArgs['ManufacturerID'] = $fields['ManufacturerID'];
                if ( isset($fields['JobTypeID']) ) $fscArgs['JobTypeID'] = $fields['JobTypeID'];
                $fscArgs['ServiceTypeID'] = $fields['ServiceTypeID'];
                if ( isset($fields['UnitTypeID']) ) $fscArgs['UnitTypeID'] = $fields['UnitTypeID'];
                if ( isset($fields['CountyID']) ) $fscArgs['CountyID'] = $fields['CountyID'];
                if ( isset($fields['CustomerAddressTown']) ) $fscArgs['CustomerAddressTown'] = $fields['CustomerAddressTown'];

                $fields['ServiceProviderID'] = $skyline_model->findServiceCentre($fscArgs);
                if($this->debug) $this->log('User is service Branch, BranchID = '.$fields['BranchID'].', SP ID = '.$fields['ServiceProviderID']);
            }

            if ( $fields['ServiceProviderID'] == 0) {                           /* If after attempting to allocate we still don't have a ServiceProvideer */
                if($this->debug) $this->log('No service Provider Id set');
                unset($fields['ServiceProviderID']);                            /* then we don't want to record service provider */
            }
            
            /* Originator */
            
            if ( ! isset($fields['Originator']) ) $fields['Originator'] = '';
            $fields['JobSourceID'] = $job_source_model->getIdFromName($fields['Originator']);   /* Get the job source id from the originator field */
            if (is_null($fields['JobSourceID'])) {                                              /* Check if Job Source ID not found */
                $fields['JobSourceID'] = $job_source_model->getIdFromName('Unknown');           /* If so set to Unknown */
            }

            $job_fields = $this->validate_args($fields, $this->api_job_fields); /* Get fields that have been passed, extract the ones fo the job table, and validate type */
            if (isset($fields['ProductLocation']) && $fields['ProductLocation'] == 'branch') {
                $job_fields['ClosedDate'] = $fields['ServiceProviderDespatchDate'];
            }
            
            if ( isset($fields['ModelID']) && ($fields['ModelID'] == 0) ) {
                $job_fields['ModelID'] = null;
            }

            /* ServiceBase send the customer as string parameters and we have no idea if it is and existing or new customers. So we search on name and postcode to find
             * out if the customer is in the database. If they are we update details, otherwise we create */
            if ( isset($fields['CustomerForename']) || isset($fields['CustomerSurname']) || isset($fields['CustomerPostcode']) ) {
                if (!isset($fields['CustomerForename'])) $fields['CustomerForename'] = "";  /* If Name/Postcode not sent set to blank */
                if (!isset($fields['CustomerSurname'])) $fields['CustomerSurname'] = "";
                if (!isset($fields['CustomerPostcode'])) $fields['CustomerPostcode'] = "";

                $fields['CustomerID'] = $skyline_model->getCustomerIdByNamePostcode(addslashes($fields['CustomerForename']), addslashes($fields['CustomerSurname']), $fields['CustomerPostcode']);
                if($this->debug) $this->log("Get Customer ID from Name and Post Code = ".var_export($fields['CustomerID'],true),'JobsAPI_');
                if ( is_null($fields['CustomerID']) ) {
                    $customer_create_output = $customer_model->create($customer_fields);    /* Create Customer */
                    $fields['CustomerID'] = $customer_create_output['id'];
                    if($this->debug) $this->log('Customer model (create) output ='.var_export($customer_create_output, true),'JobsAPI_');
                } else {
                    $customer_model->updateByID($customer_fields, $fields['CustomerID']);   /* Update customer */
                }

                if (isset($fields['CustomerID'])) $job_fields['CustomerID'] = $fields['CustomerID'];

                if (isset($fields['ColAddCounty'])) {
                    $job_fields['ColAddCountyID'] = $skyline_model->getFieldFromTable($this->get_county_id, addslashes($fields['ColAddCounty']));
                }

                if (isset($fields['ColAddCountry'])) {
                    $job_fields['ColAddCountryID'] = $skyline_model->getFieldFromTable($this->get_county_id, addslashes($fields['ColAddCountry']));
                }
            }

            if(isset($fields['CustomerID'])) $job_fields['CustomerID'] = $fields['CustomerID'];
            
            /*
             * Trackerbase VMS Log 244
             * Add Network and Client to Notes field on boooking
             */
            
            $userType = $this->getUserType();
            if ($userType == "Branch" || $userType == "Client") {  /* If the user type is a branch or client */
                $netline = "";
                
                if ( isset($fields['NetworkID'] )) {                            /* We need to get the network name */
                    $network = $service_networks_model->fetchRow(array(         /* Call model fetch row to get record for network */
                                                                    'NetworkID' => $fields['NetworkID']
                                                                  ));
                    if ( count($network > 0) ) {                                /* If matching record */
                        $netline .= 'Network: '.strtoupper($network['CompanyName']);     /* Write network line */
                    } /*fi count $network > 0 */
                    
                    if (isset($fields['ClientID'])) {
                        $params = array('ClientID' => $fields['ClientID']);
                        $client_array = $client_model->Select('select ClientName from client where ClientId=:ClientID', $params);
                        if (count($client_array) > 0) {
                            $netline .= ' Client: '.strtoupper($client_array[0]['ClientName']);
                        }
                    }
                    
                    //if ( isset($fields['BranchID']) ) {                         /* If the Branch ID is set */
                    //    $branch_name = $branches_model->name($fields['BranchID']);  /* Get the branch name */

                    //    if (
                    //        ($branch_name != '')                                /* If we have a branch name */
                    //        && ( isset($network['CompanyName']) )               /* and it is not the same as the network name */
                    //        && ( strtoupper($branch_name) != strtoupper($network['CompanyName']) )
                    //       ) {
                    //        $netline .= 'Branch: '.strtoupper($branch_name);    /* Write network line */
                    //    } /* fi $branch_name != $network['CompanyName'] */
                    //} /* fi isset $fields['BranchID'] */ 
                }
                                   
                if ( isset($job_fields['Notes']) ) {                            /* If we have a notes field */
                    //$job_fields['Notes'] = "{$netline}\n{$job_fields['Notes']}";    /* Add network / branch to notes field */
                    $job_fields['Notes'] = "{$job_fields['Notes']}\n{$netline}";    /* Append network / client to notes field */
                } else {
                    $job_fields['Notes'] = $netline;                            /* Otherwise notes field becomes just network and branch line */
                } /* fi isset $job_fields['Notes'] */
            } /* fi UserType != "Branch" */
            
            /* End Trackerbase VMS Log 244 */
            
            // make absoultely certain ModelID is not zero !!!!
           if ( isset($job_fields['ModelID']) && ($job_fields['ModelID'] == 0) ) {
                $job_fields['ModelID'] = null;
            }

            if($this->debug) $this->log('fields = '.var_export($fields,true),'JobsAPI_');
            if($this->debug) $this->log('$job_fields = '.var_export($job_fields,true),'JobsAPI_');

            $job_response = $job_model->create($job_fields);                    /* Pass these fields to the jobs model and create */

            if ($job_response['jobId'] == 0) {                                  /* Check the JobID for the created job */
                $response = array (
                    'Code' => "SC0004",
                    'ResponseCode' => "SC0004",
                    'Response' => "Record insert error. PDO Error : {$job_response['message']}",
                    'ResponseDetails' => "Record insert error. PDO Error : {$job_response['message']}"
                );
                return($response);
            } /* fi $job_response['jobId'] == 0 */

            $fields['SLNumber'] = $job_response['jobId'];                       /* Set the JobID (we put it in SLNumber to match where it is passed for update. */
            
            if ( isset($fields['CustomerPostcode']) && isset($fields['ServiceProviderID'] ) ) {                     /* Need a customer post code to get the area for */          
                $area = trim(substr($fields['CustomerPostcode'], 0, -3));   /* Get UK postode area */

                $secondarySpId = $allocation_reassignmnet->getSecondaryServiceProvider(($fields['ServiceProviderID']), $area);
            } else {
                $secondarySpId = null;
            }
        } /* fi (isset($fields['SLNumber'])) */

        /******************************************************
        * Job written to table - write additional information *
        ******************************************************/
        
        /*
         * Check if job is in Nonskyline jobs and if so update appointments with JobId, delete NonSkylineJobID and remove from non_skyline_jobs
         * Only if no seondary service provider id (Issue 271 - https://bitbucket.org/pccs/skyline/issue/271/one-touch-for-ancrum-south-esk )
         */
        
        if ( ! isset($secondarySpId) || is_null($secondarySpId)) { 
            if($this->debug) $this->log('No Secondary Service Provider check if repacing Non Skyline Job','JobsAPI_');
            if ( isset($fields['NetworkRefNo']) && isset($fields['NetworkID']) ) {              /* On NetworkRefNo and NetworkID */
                $non_skyline_job_model->JobIsNowSkylineNet($fields['SLNumber'], $fields['NetworkID'], $fields['NetworkRefNo']);
            } elseif ( isset($fields['SCJobNo']) && isset($fields['ServiceProviderID']) ) {     /* On Service Provider Job No and ID */
                $non_skyline_job_model->JobIsNowSkylineSp($fields['SLNumber'], $fields['ServiceProviderID'], $fields['SCJobNo']);
            }
        } else {
            if($this->debug) $this->log("Secondary Service Provider $secondarySpId",'JobsAPI_');
            if ( isset($fields['NetworkRefNo']) && isset($fields['NetworkID']) ) {
                if($this->debug) $this->log("Network Ref Set {$fields['NetworkRefNo']} Network ID set {$fields['NetworkID']}",'JobsAPI_');
                $nsjId = $non_skyline_job_model->getIdFromNetworkRef($fields['NetworkID'], $fields['NetworkRefNo']);       /* Get the Non Skyline JobID */
                if($this->debug) $this->log('Look for non skyline job','JobsAPI_');
                if (! is_null($nsjId)) {
                    if($this->debug) $this->log("Found! NonSkylineJobID = $nsjId",'JobsAPI_');
                    $app = $appointment_model->getNextAppointmentNonSkyline($nsjId);
                    if($this->debug) $this->log("Next appointment details = ".var_export($app),'JobsAPI_');
                    if ( ! isnull($app) ) {
                        $api_appointments_model->PutAppointmentDetails($app['AppointmentID']);
                    } /* fi ! isnull $app */
                } /* fi ! isnull $nsjId */
            } /* fi isset NetworkRefNo && NetworkID*/
        } /* fi is_null $secondarySpId */

        //PutAppointmentDetails();
        
        
        /*
         * Service Centre Details 
         */
        if ( isset($fields['SBLicenceNo']) or isset($fields['SBVersion']) ) {   /* Service Base softeare details passed */
            if ( isset($fields['ServiceProviderID']) ) {         
                $sp_args = array(                                               /* We know service provider (from create) */
                    'ServiceProviderID' => $fields['ServiceProviderID']
                );
            } else {
                $sp_args = array(                                               /* If we don't know then get from JobNo */
                                 'ServiceProviderID' => $skyline_model->getFieldFromTable($this->get_service_centre_job_id,$fields['SLNumber'])
                );

                $fields['ServiceProviderID'] = $sp_args['ServiceProviderID'];

                if ( isset($fields['SBLicenceNo']) ) {
                    $sp_args['ServiceBaseLicence'] = $fields['SBLicenceNo'];    /* Need to update Service Base license no */
                } /* fi isset($fields['SBLicenseNo']) */

                if ( isset($fields['SBVersion']) ) {
                    $sp_args['ServiceBaseVersion'] = $fields['SBVersion'];      /* Need to update Service Base version*/
                } /* fi isset($fields['SBVersion'] */

                if($this->debug) $this->log('$sp_args = '.var_export($sp_args,true),'JobsAPI_');
                $service_providers_model->updateByID($sp_args);
            } /* fi isset($fields['ServiceProviderID']) */
        }

        /*
         * Parts fields 
         */
                    
        if ( $refresh ) {                                                       /* Data Integrity Check - Delete all existing parts if refresh is set (as they will all be reloded */
            $part_model->deleteByJobId($fields['SLNumber']);
        }

        if (isset($fields['Parts'])) {                                          /* If parts fields included */
            $parts_fields_params = json_decode($fields['Parts'],true); 
            if($this->debug) {
                $this->log('$fields[Parts] = '.var_export($fields['Parts'],true),'JobsAPI_');
                $this->log('$parts_field_params = '.var_export($parts_fields_params,true),'JobsAPI_');
            }
            
            if (is_null($parts_fields_params)) {
                $this->log('json_last_error() ='.json_last_error(),'JobsAPI_');
            } else {
                foreach ($parts_fields_params as $part_param) {
                    $part = $this->validate_args($part_param, $this->api_parts_fields);

                    $part['JobID'] = $fields['SLNumber'];
                    if ( isset($part_param['Deleted']) && strtoupper($part_param['Deleted'])  == 'Y') {              /* check if we are deleteing or adding (SB handles modification by deleting and adding) */
                        $part_model->delete( array (                                /* Delete part */
                            'SBPartID' => $part['SBPartID']
                                                    ) );
                    } else {
                        $pid = $part_model->getPartIdFromSbId($part['SBPartID'], $part['JobID']);
                        if (is_null($pid)) {                                     /* Check if part exists */
                            $part_model->create($part);                                 /* Pass array of parts details to create */
                        } else {
                            $part['PartID'] =  $pid;
                            $part_model->update($part);                         /* Part exists - so update */
                        } /* fi getPartIdFromSbId */
                    } /* fi ($part['Deleted']) */
                    if($this->debug) $this->log('$part = '.var_export($part,true),'JobsAPI_');
                } /* next part */
            } /* if is_null */
        } /* fi (isset($fields['Parts'])) */

        /*
         * Appointments fields 
         */

        if (isset($fields['ServiceProviderID'])) {
            if($this->debug) $this->log('Check online diary status for Service Provider '.$fields['ServiceProviderID'].' is '.$service_providers_model->getServiceCentreOnlineDiary($fields['ServiceProviderID']),'JobsAPI_');
            if ( $service_providers_model->getServiceCentreOnlineDiary($fields['ServiceProviderID']) == 'In-Active' ) {
                if (isset($fields['Appointments'])) {                                   /* If appointments fields included */
                    $appointments_fields_params = json_decode($fields['Appointments'],true);
                    if($this->debug) $this->log('$appointment_fields_parms = '.var_export($appointments_fields_params,true),'JobsAPI_');
                    
                    /* Data Integrity Check - Delete all existing parts if refresh is set (as they will all be reloded */
                    if ( $refresh ) {
                        $appointment_model->deleteByJobId($fields['SLNumber']);
                    }

                    foreach ($appointments_fields_params as $appointment_param) {
                        if($this->debug) $this->log('appointment_param = '.var_export($appointment_param,true),'JobsAPI_');
                        $appointment = $this->validate_args($appointment_param, $this->api_appointment_fields);
                        if($this->debug) $this->log('appointment = '.var_export($appointment,true),'JobsAPI_');

                        $appointment['JobID'] = $fields['SLNumber'];
                        $appointment['UserID'] = $this->user->UserID;
                        if ( isset($appointment['OutCardLeft']) ) {                     /* If we have to deal with OutCardLeft */
                            if (strtoupper($appointment['OutCardLeft']) == 'Y') {       /* Convert supplied y/n to boolean */
                                $appointment['OutCardLeft'] = true;
                            } else {
                                $appointment['OutCardLeft'] = false;
                            }
                        }

                        if ( isset($fields['ServiceProviderID']) ) $appointment['ServiceProviderID'] = $fields['ServiceProviderID'];

                        if ( ! isset ($appointment_param['Deleted']) ) $appointment_param['Deleted'] = 'N';
                        if ( strtoupper($appointment_param['Deleted']) == 'Y' ) {       /* check if we are deleteing or adding (SB handles modify by delete then add ) */
                            $appointment_model->delete( array (                         /* Delete Appointment */
                                'SBAppointID' => $appointment['SBAppointID']
                                                            ) );
                        } else {
                            if ($appointment_model->doesSBAppointExist($appointment_param['SBAppointID'])) {
                                $appointment['AppointmentID'] = $appointment_model->getIdFromSBAppoint($appointment_param['SBAppointID']);
                                $appointment_model->update( $appointment );             /* Update */
                            } else {
                                $appointment_model->create( $appointment );             /* Add */
                            }
                        } /* fi ($appointment['Deleted']) */
                        if($this->debug) $this->log('$appointment = '.var_export($appointment,true));
                    } /* next $appointment */
                } /* fi isset $fields['Appointments'] */
            } /* fi $service_providers_model->getServiceCentreOnlineDiary($fields['ServiceProviderID'] */
        } else {
             if($this->debug) $this->log('No fields[ServiceProviderID]','JobsAPI_');
        } /*fi  fi isset fields['ServiveProviderID] */

        /*
         * Status fields 
         */
                    
        if ( $refresh ) {                                                       /* Data Integrity Check - Delete all existing parts if refresh is set (as they will all be reloded */
            $status_history_model->deleteByJobId($fields['SLNumber']);
        }
        
        if (isset($fields['StatusUpdate'])) {                                   /* If status update fields included */
                
            for ($n = 0; $n < count($status_update); $n++) {
                if($this->debug) $this->log('$status_update['.$n.'] = '.var_export($status_update[$n],true),'JobsAPI_');
                    
                if ( isset($status_update[$n]['StatusID']) ) {
                    $status_update[$n]['JobID'] = intval($fields['SLNumber']);
                    //$status_update[$n]['Date'] = $status_update[$n]['Date'];
                    if (! $status_history_model->doesStatusUpdatetExist($status_update[$n]['JobID'], $status_update[$n]['Date'], $status_update[$n]['StatusID'])) {
                        if($this->debug) $this->log('Status History update params = '.var_export($status_update[$n],true),'JobsAPI_');
                        $sthu = $status_history_model->create($status_update[$n]);
                        if($this->debug) $this->log($sthu,'JobsAPI_');
                    }
                }
            }
        }

        /*
         *   History fields
         */
        if (isset($fields['CallHist'])) { /* If call history (contact history) set */
            if ($this->debug) $this->log('$fields[CallHist] = ' . var_export($fields['CallHist'], true),'JobsAPI_');
            $fields['CallHist'] = str_replace("\r", "", $fields['CallHist']);
            $fields['CallHist'] = str_replace("\n", "\\n", $fields['CallHist']);
            $contact_history_fields_params = json_decode($fields['CallHist'], true);
            if ($this->debug) $this->log('$contact_history_field_params = ' . var_export($contact_history_fields_params, true),'JobsAPI_');
            
            if ( is_array($contact_history_fields_params) ) {
                /* Data Integrity Check - Delete all existing parts if refresh is set (as they will all be reloded) */
                if ( $refresh ) {
                    $contact_history_model->deleteByJobId($fields['SLNumber']);
                }

                foreach ($contact_history_fields_params as $contact_history_fields_param) {
                    $contact_history = $this->validate_args($contact_history_fields_param, $this->api_call_history_fields);
                    
                    // ignore record if contact date/time not set
                    // defend against invalid data being sent from RMA
                    if (!isset($contact_history['ContactDate']) || !isset($contact_history['ContactTime'])) {
                        continue;
                    }

                    $contact_history['JobID'] = $fields['SLNumber'];
                    $contact_history['IsDownloadedToSC'] = true;                /* If we are uploading fom SC then we don't need to download data ! */

                    $contact_history['ContactHistoryActionID'] = $skyline_model->getFieldFromTable($this->get_contact_history_action_id, $contact_history_fields_param['Action']);

                    if (is_null($contact_history['ContactHistoryActionID'])) {  /* If no ID found then we need to create */
                        
                        $newch = array (
                            'Action' => $contact_history_fields_param['Action'],
                            'Type' => 'User Defined',
                            'Status' => 'Active',
                            'BrandID' => 1000
                        );
                        
                        // Log # 275 - Set source field for Remote Engineer Actions
                        if (!empty($contact_history_fields_param['Action']) 
                            && substr($contact_history_fields_param['Action'], 0, 12) == 'REMOTE ENG: ') {
                            $newch['Source'] = 'Remote Engineer';
                        } 

                        $chret = $contact_history_actions_model->create( $newch );

                        if (isset($chret['ContactHistoryActionID'])) {
                            $contact_history['ContactHistoryActionID'] = $chret['ContactHistoryActionID'];
                        } else {
                            if ($this->debug) $this->log($fields['SLNumber'].': Can not create CallHist action '.$contact_history_fields_param['Action'],'JobsAPI_');
                            $response = array (
                                'Code' => "SC0007",
                                'ResponseCode' => "SC0007",
                                            'Response' => $fields['SLNumber'].": Can not create  CallHist action ".$contact_history_fields_param['Action'],
                                            'ResponseDetails' => $fields['SLNumber'].": Can not create such CallHist action ".$contact_history_fields_param['Action']
                            );
                            return($response);
                        }
                    }

                    if ( strtoupper($contact_history_fields_param['Deleted']) ==  'Y' ) {        /* check if we are deleteing or adding */
                        $contact_history_model->delete(                         /* Delete */
                                                       $contact_history['ContactDate'],
                                                       $contact_history['ContactTime'],
                                                       $contact_history['JobID']
                        );
                    } else {
                        if (! isset($contact_history['Note']) ) $contact_history['Note'] = '';   /* If no contact history note then set to blank */
                        if ($contact_history_model->CheckExists(                /* If contact history records does not exist */
                                                                $contact_history['ContactDate'], 
                                                                $contact_history['ContactTime'], 
                                                                $contact_history['JobID'],
                                                                $contact_history['Note'] 
                                                               )
                                                               == 0) {    
                            $contact_history_model->create($contact_history);   /* Add */
                        } /* fi $contact_history_model->CheckExists */
                    } /* fi (contact_history['Deleted']) */
                    if($this->debug) $this->log('$contact_history_field_param = '.var_export($contact_history_fields_param,true),'JobsAPI_');
                } /* next $contact_history_field+param */
            } /* fi is_array */
        }
        
        /*
         * Claim Response Errors
         */
        if (isset($fields['ClaimResponseErrors'])) {                                          /* If ClaimResponseErrors fields included */
            $claim_response_errors = json_decode($fields['ClaimResponseErrors'],true); 
            foreach ($claim_response_errors as $claim_response_error) {
                $claim_response_fields = $this->validate_args($claim_response_error, $this->api_claim_response_fields);
                $claim_response_fields['JobID'] = $fields['SLNumber'];
                $claim_response_model->create($claim_response_fields);
            }
        }
        
        $response = array (                                                     /* All done ! */ 
                           'Code' => "SC0001",
                           'ResponseCode' => "SC0001",
                           'Response' => $action == "update" ? "Updated" : "Created",
                           'ResponseDetails' => $action == "update" ? "Updated" : "Created",
                           'SLNumber' => $fields['SLNumber'],
                           'SCJobNo' => $fields['SCJobNo']
                          );

        if (isset($fields['RMANumber'])) $response['RMANumber'] = $fields['RMANumber'];

        if ($this->getUserType() == "Branch") {                                 /* Find out if user is branch. */
            if ( !isset($fields['ServiceProviderID']) ) {                       /* Yes - do we have service provider? (We will if create but not if update) */
                $fields['ServiceProviderID'] = $job_model->getServiceCentre($fields['SLNumber']);       /* No - so get it */
            }
            if ( !is_null($fields['ServiceProviderID']) ) {                     /* Yes - Get service provider details (if set) */
                $sc = $service_providers_model->fetchRowById( $fields['ServiceProviderID'] );       

                $response['Service Centre Name'] = $sc['CompanyName'];
                $response['Building Name'] = $sc['BuildingNameNumber'];
                $response['Street'] = $sc['Street'];
                $response['Area'] = $sc['LocalArea'];
                $response['Town'] = $sc['TownCity'];
                $scCountyId = $sc['CountyID'];
                $scCounty = $county_model->fetchRow( array('CountyID' => $scCountyId ) );
                $response['County'] = $scCounty['Name'];
            } else {
                $response['Service Centre Name'] = "Unallocated";               /* No ServiceProviderID so is unallocated */
            }
        }

        /******************************
        * Call other APIs as required *
        ******************************/
        
        /* send updates to service base only if originator field is not ServiceBase */
        //if ($action == "update" && isset($fields['Originator']) && strtoupper($fields['Originator']) != 'SERVICEBASE') {
        //    $sb_model->PutJobDetails($fields['SLNumber']);
        //}

        /*
         * Samsung API Calls
         */

        if($this->debug) $this->log('JobsAPIController::PutJobDetails Check if Samsung','JobsAPI_');

        if ((isset($fields['ServiceProviderID'])) && (isset($fields['ManufacturerID']))) {          /* Check if  Service Proder and Manufacturer ID set */
            if($this->debug) $this->log("ServiceProviderID:{$fields['ServiceProviderID']} ServiceProviderID:{$fields['ManufacturerID']}",'JobsAPI_');
            if (                                                                                        /* Yes, so ... */
                ($fields['ManufacturerID'] == $manufacturers_model->getManufacturerId('SAMSUNG'))           /* Find out if Manufacturer is Samsung */
                && ($service_providers_model->getServiceCentreSamsungUpload($fields['ServiceProviderID']) ==  1)  /* and service provider can upload to Samsung */
                && (strtolower($fields['ServiceType']) == 'w' )                                             /* the Service Type of the job is Warranty */
            ) {
                $netRefNo = $job_model->getNetworkRefNo($fields['SLNumber']);
                if(                                                                                         /* Is Samsung Warranty item so */
                    ( is_null($netRefNo) )                                                                  /* Is the NetworkRefNo field in the job is empty */
                    && ( $status_history_model->getLastStatus($fields['SLNumber']) != '29 JOB CANCELLED' )  /* and the status of the job is not â€™29 JOB CANCELLEDâ€™ */
                ) {
                    $samsungApi->putServiceRequestAction(array($fields['SLNumber']));                       /* Yes so call Samsung API putServiceRequest */
                } elseif (
                          ! is_null($job_model->getNetworkRefNo($fields['SLNumber']))                       /* But if the WarrantyRefNo isn't filled */
                          && ( $netRefNo != 0 )                                                             /* And it isn't 0 */
                ) {
                    $samsungApi->putRepairStatusAction(array($fields['SLNumber']));                         /* Yes so call Samsung API putRepairStatus */
                } /* fi isNull NetworkRefNo and Status isn't Canclled */
            } /* fi Manufacturer is Samsing AND Service Centre Can upload to Samsung */
        } /* fi isset ManufacturerID and ServiceProviserID*/

        /*
         * The Warranty Claims upload will also need to run in Skyline.putJobDetails after the tracking upload has completed.  
         * Again it will only do it if the manufacturer is the one specified in the Samsung Defaults but also only if 
         * ClaimTransmitStatus uploaded is 1 or 2 (not 0).
         */

        /*
         * If service centre is set deal with sending to RMA / Service base
         */

        if($this->debug) $this->log('Check if service centre set','JobsAPI_');
        if ( isset($fields['ServiceProviderID']) ) {
            $sc = $service_providers_model->fetchRowById( $fields['ServiceProviderID'] );
            if($this->debug) $this->log('Service Centre details = '.var_export($sc, true),'JobsAPI_'); 

            /*
             * If platform is not RMA tracker send to rma tracker
             */

            if($this->debug)$this->log('Check if need to call RMA','JobsAPI_');

            if ( 
                 ($refresh) && ($this->config['RMATracker']['NoSendOnRefresh'] == 0)
               || ($sc['Platform'] != 'RMA Tracker' /* If platform is not RMA Tracker */
                    && (
                        (isset($fields['Originator']) && $fields['Originator'] != 'RMA Tracker') /* and originator is set an not RMA Tracker */
                    ||
                        ! isset($fields['Originator'])                                             /* or originator is not set */
                    )
                    && ( ! $refresh )                                           /* And not refresh (if vaid for refresh will be true for first clasuse */
                  )
                ) {
                if($this->debug) $this->log('Calling rmaPutJobDetails','JobsAPI_'); 
                $api_jobs_model->rmaPutJobDetails($fields['SLNumber']);         /*  send to RMA Tracker */
            }
                     
            /*
             * If the fields SendToASC is set to Y then send details to Service centre, dependant on platform
             */

            if (isset($fields['SendToASC']) && strtoupper($fields['SendToASC']) == 'Y') {
                if ($this->debug)
                    $this->log('JobsAPIController::PutJobDetails SendToASC ==  Y - Checking platform','JobsAPI_');
                
                /* VMS Trackerbase Log 168 
                 *
                 * When jobs are uploaded to Skyline through putJobDetails and the job does not exist already in Skyline and the SendToASC flag is on, 
                 * after saving the job, check if the postcode area and Main service provider (linked to the service provider sending the job) match 
                 * the job details, if they do, send the job to the Secondary service provider first and then send it to the Main one (one that is 
                 * linked on the job), currently this sends the job to main service provider on the job only. 
                 */
                
                if ( isset($fields['CustomerPostcode']) ) {                     /* Need a customer post code to get the area for */          
                    $area = trim(substr($fields['CustomerPostcode'], 0, -3));   /* Get UK postode area */
                    
                    $secondarySpId = $allocation_reassignmnet->getSecondaryServiceProvider(($fields['ServiceProviderID']), $area);
                    
                    if (! is_null($secondarySpId)) {                            /* Check if we have a secondary service provider */
                        /* Deal with secondary service proder */
                        $api_jobs_model->PutNewJob($fields['SLNumber'],$secondarySpId); /* Call PutNewJob with secondary service provider id */
                    }
                }
                
                /* END VMS Trackerbase Log 168  */
                
                switch ($sc['Platform']) {
                    case 'ServiceBase':
                        if ($action == 'update') {
                            $sb_model->PutJobDetails($fields['SLNumber']);
                        } else {
                            $api_jobs_model->PutNewJob($fields['SLNumber']);
                        }
                        break;
                    
                    case  'API':
                        $job_model->update(array("DownloadedToSC" => 0, "JobID" => $fields['SLNumber']));
                        break;
                    
                    case 'Skyline':
                    case 'RMA Tracker':
                        $job_model->update(array("DownloadedToSC" => 1, "JobID" => $fields['SLNumber']));
                        break;
                } /* switch */
            } /* fi $fields['SendToASC'] == 'Y' */
        } /* fi isset $fields['ServiceProviderID'] */

        if ($this->debug) {
            $this->log('$response = ' . var_export($response, true),'JobsAPI_');
            $this->log('JobsAPI->putJobDetails  FINISH','JobsAPI_');
        }

        return($response);
        
    }

    /**
     * Description
     * 
     * Skiline API  Skyline.GetNewJobs
     * API Spec Section 8.1.4.9
     *  
     * Get new jobs which have not been marked as downloaded.
     * 
     * @param none                  
     * 
     * @return getNewJobs   Associative array containing respose
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
      *************************************************************************/
    
    private function getNewJobs() {
        $api_jobs_model = $this->loadModel('APIJobs');
        
        $rec_jobs = $api_jobs_model->getNewJobs($this->user->UserID);
        
        $response = array(
            'Jobs' => $this->recordSetSerialise($rec_jobs, 'Job')
        );
        
        /* Contact History for Job */
        for ($n=0;$n<count($response['Jobs']);$n++) {
           // if( isset($response['Jobs'][$n]['Job']['SLNumber']) ) {                     /* Make sure it is not null */
                $rec_contact_history = $api_jobs_model->getContactHistory($response['Jobs'][$n]['Job']['SLNumber']);

                $response['Jobs'][$n]['Job']['ContactHistory'] = $this->recordSetSerialise($rec_contact_history, 'Contact');

                $rec_warranty_response = $api_jobs_model->getWarrantyClaimResponse($response['Jobs'][$n]['Job']['SLNumber']);

                $response['Jobs'][$n]['Job']['WarrantyClaim'] = $rec_warranty_response;

                $rec_errors = $api_jobs_model->getWarrantyClaimResponseErrors($response['Jobs'][$n]['Job']['SLNumber']);

                $response['Jobs'][$n]['Job']['Errors'] = $this->recordSetSerialise($rec_errors, 'Error');

                $rec_part = $api_jobs_model->getWarrantyClaimResponseParts($response['Jobs'][$n]['Job']['SLNumber']);

                $response['Jobs'][$n]['Job']['Parts'] = $this->recordSetSerialise($rec_part, 'Part');
            //} else {
                // delete jobs with null primary key - this is correcting a problem to do with the sql selection joins
            //    unset($response['Jobs'][$n]);
            //}
        }
                  
        return($response);
    }

    /**
     * Description
     * 
     * Skiline API  Skyline.PutNewJobsResponse
     * API Spec Section 8.1.4.10
     *  
     * Once the new jobs have downloaded, the service centre will need to upload 
     * a response to let Skyline know the job has been successfully downloaded.
     * Skyline should continue to send the jobs in the GetNewJobs call until the
     * response is sent.
     * 
     * 
     * @param array $fields    Associative array of fields
     *              
     * 
     * @return putNewJobsResponse  Associative array containing respose
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    
    private function putNewJobsResponse($fields) {
        $skyline_model = $this->loadModel('Skyline');
        $api_jobs_model = $this->loadModel('APIJobs');

        if ( ! $skyline_model->getJobExistsUser($fields['SLNumber'], $this->user->UserID) ) {
            $response = array (                                                 /* Job number not found under current user */
                'ResponseCode' => "SC0003",
                               'ResponseDescription' => "Job ".$fields['SLNumber']." not found for user ".$this->user->Username
            );
            return($response);
        }

        $dbResult = $api_jobs_model->putNewJobsResponse($fields['SLNumber'],$fields['SCJobNo']);

        if ( ! $dbResult['code'] == 'OK' ) {
            $response = array (                                                 /* Error with the query */
                'ResponseCode' => "SC0005",
                               'ResponseDescription' => "Query Error\n".
                                                        "SQLSTATE error code: ".$dbResult['code']."\n".
                                                        "MySQL error code: ".$dbResult['drivercode']."\n".
                                                        "Message: ".$dbResult['message']."\n"
            );
            return($response);
        } else {
            $response = array (
                'ResponseCode' => "SC0001",
                'ResponseDescription' => 'Success'
            );
            return($response);
        }
    }

    
    /**
     * Description
     * 
     * Skiline API  Skyline.clientJobSearch
     * API Spec Section 8.1.4.13
     *  
     * Get new jobs which have not been marked as downloaded.
     * 
     * @param none                  
     * 
     * @return clientJobSearcth   Associative array containing respose
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
      *************************************************************************/
    
    private function clientJobSearch($where) {

        $api_jobs_model = $this->loadModel('APIJobs');

        $rec_jobs = $api_jobs_model->getClientJobSearch($this->user->UserID, $where);

        $response = array('Jobs' => $this->recordSetSerialise($rec_jobs,'Job'));

        if ( count($response['Jobs']) == 0 ) {
            $response['Code'] = 'SC0003';                                       // No retults found
            $response['Response'] = 'No Results Found';
        } else {
            $response['Code'] = 'SC0001';                                       // OK
            $response['Response'] = 'Successful Validation';
        }

        return($response);

    }

    
    /**
     * Description
     * 
     * Skiline API  Skyline.GetJobDetails
     * API Spec Section 8.1.4.14
     *  
     * Return single job details
     * 
     * @param none                  
     * 
     * @return getNewJobs   Associative array containing respose
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
      *************************************************************************/
    
    private function getJobDetails($SLNumber) {
        $api_jobs_model = $this->loadModel('APIJobs');

        $rec_jobs = $api_jobs_model->getJobDetails($SLNumber, $this->user->UserID);

        $response = array(
            'Jobs' => $this->recordSetSerialise($rec_jobs, 'Job')
        );

        /* Parts for Job */
        for ($n=0;$n<count($response['Jobs']);$n++) {
            $rec_parts = $api_jobs_model->getJobParts($response['Jobs'][$n]['Job']['SLNumber']);

            $response['Jobs'][$n]['Job']['Parts'] = $this->recordSetSerialise($rec_parts, 'Part');
        }

        /* Appointments for Job */
        for ($n=0;$n<count($response['Jobs']);$n++) {
            $rec_appointments = $api_jobs_model->getJobAppointments($response['Jobs'][$n]['Job']['SLNumber']);

            $response['Jobs'][$n]['Job']['Appointments'] = $this->recordSetSerialise($rec_appointments, 'Appointment');
        }

        /* Status for Job */
        for ($n=0;$n<count($response['Jobs']);$n++) {
            $rec_status = $api_jobs_model->getJobAppointments($response['Jobs'][$n]['Job']['SLNumber']);

            $response['Jobs'][$n]['Job']['StatusUpdates'] = $this->recordSetSerialise($rec_status, 'StatusUpdate');
        }

        /* Contact (Call) History for Job */
        for ($n=0;$n<count($response['Jobs']);$n++) {
            $rec_contact_history = $api_jobs_model->getContactHistory($response['Jobs'][$n]['Job']['SLNumber']);

            $response['Jobs'][$n]['Job']['CallHistory'] = $this->recordSetSerialise($rec_contact_history, 'Call');
        }

        if ( count($response['Jobs']) == 0 ) {
            $response['Code'] = 'SC0003';                                       // No retults found
            $response['Response'] = 'Job not found';
        } else {
            $response['Code'] = 'SC0001';                                       // OK
            $response['Response'] = 'Successful Validation';
        }

        return($response);
    }

    
    /**
     * Description
     * 
     * Skiline API  Skyline.ConsumerJobSearch
     * API Spec Section 8.1.4.15
     *  
     * Return single job details
     * 
     * @param none                  
     * 
     * @return getNewJobs   Associative array containing respose
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    
    public function consumerJobSearch($args) {
        $api_jobs_model = $this->loadModel('APIJobs');

        $where = "";

        if ( isset($args['PhoneNumber']) ) {
            $where = "$where AND (cu.`ContactHomePhone` = '{$args['PhoneNumber']}'
                                  OR cu.`ContactWorkPhone` = '{$args['PhoneNumber']}'
                                  OR cu.`ContactMobile` = '{$args['PhoneNumber']}')";
        }

        if ( isset($args['SLNumber']) ) {
            $where = "$where AND j.JobID = {$args['SLNumber']}";
        }

        if ( isset($args['Postcode']) ) {
            $where = "$where AND sp.PostalCode = {$args['Postcode']}";
        }

        $rec_jobs = $api_jobs_model->consumerJobSearch($this->user->UserID, $where);

        $response = array(
            'Jobs' => $this->recordSetSerialise($rec_jobs, 'Job')
        );

        return($response);
    }


    /**
     * Description
     * 
     * Build where cluase for search from arguments
     * 
     * @param string $searchString      The string to be searched for
     *        string $item              The type of item to be searched for            
     * 
     * @return buildSearch      String with WHERE clause for query or false if  incorrect parameters
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    
    private function buildSearch( $searchString, $item) { 
        if (isset($this->searchKey[$item])) {
            $response = $this->searchKey[$item]."  = '$searchString'";
        } else {
            /* Item not supported */
            $response = false;
        }

        return($response);
    }

    
    /**
     * Description
     * 
     * Build where cluase for search from arguments
     * 
     * @param string $searchArray       The array to be used to build where query          
     * 
     * @return buildSearchArray         String with WHERE clause for query or false if  incorrect parameters
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/

    private function buildSearchArray( $searchArray) {
        $response="";
        
        foreach ($searchArray as $field=>$data) {
            $response = $response.' AND '.$this->buildSearch($data, $field);
        }

        return($response);
    }

}

?>
